# Procedure of installation of hampath
#
# 1. create a lib and a lib directory
# 2. compile the code and put o files into lib directory
# 3. create a bin directory
# 3. create a bin file with hampath_home variable set into bin directory
#

# HamPath home directory
hampath_home=`pwd`
hampath_name='hampath'
lib_dir=${hampath_home}/lib
bin_dir=${hampath_home}/bin
compilo='gfortran'
wrapperpython='f2py'

# ----------------------------------------------------------------------
# 1. create a lib directory
# ----------------------------------------------------------------------
mkdir -p ${lib_dir}

# ----------------------------------------------------------------------
# 2. compile the code and put o files into lib directory
# ----------------------------------------------------------------------

make -f ${hampath_home}/install/makefile_install --silent   hampath_home=${hampath_home}    \
                                                            lib_dir=${lib_dir}/             \
                                                            compilo=${compilo}              \
                                                            installCMD

# ----------------------------------------------------------------------
# 3. create a bin directory
# ----------------------------------------------------------------------
mkdir -p ${bin_dir}

# ----------------------------------------------------------------------
# 3. create a bin file with hampath_home variable set into bin directory
# ----------------------------------------------------------------------

binfile=${bin_dir}/hampath
echo "#!/bin/sh"                            >   ${binfile}
echo "hampath_home="${hampath_home}         >>  ${binfile}
echo "lib_dir="${lib_dir}                   >>  ${binfile}
echo "compilo="${compilo}                   >>  ${binfile}
echo "wrapperpython="${wrapperpython}       >>  ${binfile}
cat ${hampath_home}/install/hampath.sh      >>  ${binfile}
chmod 755 ${binfile}
