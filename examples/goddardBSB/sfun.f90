!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief Shooting function.
!!  \param[in] ny       Shooting variable dimension
!!  \param[in] y        Shooting variable
!!  \param[in] npar     Number of optional parameters
!!  \param[in] par      Optional parameters
!!  \param[out] s       Shooting value, same dimension as y
!!  \attention          The vector par can be used for constant values
!!                      or homotopic parameters.
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine sfun(ny,y,npar,par,s)
    use mod_exphv4sfun
    implicit none
    integer, intent(in)                             :: ny
    integer, intent(in)                             :: npar
    double precision,  dimension(ny),   intent(in)  :: y
    double precision,  dimension(npar), intent(in)  :: par
    double precision,  dimension(ny),   intent(out) :: s

    external printandstop

    !!  The user can call hfun and exphv subroutines inside sfun
    !!
    !!  hfun: code the true Hamiltonian
    !!
    !!      syntax: call hfun(t,n,z,iarc,npar,par,h)
    !!
    !!      see hfun.f90 for details.
    !!
    !!  exphv: computes the chronological exponential of the
    !!          Hamiltonian vector field hv defined by h.
    !!
    !!      syntax: call exphv(tspan,n,z0,iarc,npar,par,zf)
    !!
    !!       integer         , intent(in)    :: n, iarc, npar
    !!       double precision, intent(in)    :: tspan(:) = [t0 ... tf]
    !!       double precision, intent(in)    :: z0(2*n)
    !!       double precision, intent(in)    :: par(npar)
    !!       double precision, intent(out)   :: zf(2*n) = z(tf,t0,z0,par)
    !!

    !local variables
    double precision :: t0,t1,t2,tf,t0norm,t1norm,t2norm,tfnorm,tspan(2)
    double precision :: q0(3),p0(3),z0(6),z1(6),z2(6)
    double precision :: expz0(6),expz1(6),expz2(6)
    double precision :: H1,H01,mf
    integer          :: iarc,nparmin,n

    nparmin = 11

    IF (ny.NE.17) THEN
            call printandstop('Error in sfun: wrong shooting variable dimension.')
    END IF
    IF (npar.LE.nparmin) THEN
            call printandstop('Error in sfun: wrong par dimension.')
    END IF

    n  = 3
    p0 = y(1:3)  ; t1      = y(4)  ; z1 = y(5:10)  ; t2 = y(11)  ; z2 = y(12:17)
    tf = par(1)  ; t0      = par(7); q0 = par(8:10); mf = par(11)
    z0(1:3) = q0 ; z0(4:6) = p0
    t0norm  = 0d0; t1norm  = (t1-t0)/(tf-t0);
                    t2norm  = (t2-t0)/(tf-t0); tfnorm = 1d0

    !Integration on the first arc
    iarc    = 1; tspan = (/t0norm, t1norm/)
    call exphv(tspan,n,z0,iarc,npar,par,expz0)
    call geth1(t1norm,n,z1,iarc,npar,par,H1)
    call geth01(t1norm,n,z1,iarc,npar,par,H01)

    !Integration on the second arc
    iarc    = 2; tspan = (/t1norm, t2norm/)
    call exphv(tspan,n,z1,iarc,npar,par,expz1)

    !Integration on the third arc
    iarc    = 3; tspan = (/t2norm, tfnorm/)
    call exphv(tspan,n,z2,iarc,npar,par,expz2)

    s(1:6)  = z1         - expz0 ! Matching condition
    s(7:12) = z2         - expz1 ! Matching condition
    s(13)   = H1                 ! Contact with the switching surface
    s(14)   = H01                ! Contact of order 2
    s(15)   = expz2(n+1) - 1d0   ! Transervsality condition on ph
    s(16)   = expz2(n+2)         ! Transervsality condition on pv
    s(17)   = expz2(n)   - mf    ! Final condition on m(tf)

end subroutine sfun
