program main
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !Problem definition
    !max h(tf)
    !
    !dot h = v
    !dot v = 1/m (u*c -D) - g
    !dot m = -u
    !
    !h(0) = 0, v(0) = 0, m(0) = m0
    !m(1) = mf
    !
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !1) Module used in the main program
    !
    !use mod_expdhvfun
    use mod_exphvfun
    !use mod_hamfun
    !use mod_hampath
    !use mod_hvfun
    !use mod_hybrd
    use mod_options
    !use mod_sfun
    !use mod_sjac
    use mod_ssolve

    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !2) variables declaration
    !
    implicit none
    type(TOptions)   :: options
    !ssolve variables
    double precision :: yGuess(17), y0(17), ti(4), tspan(3), z0(6), par(14), ssol(17)
    double precision :: u, t0, t1, t1norm, t2, t2norm, tf
    integer          :: nfev,njev

    !exphvfun variables
    double precision, allocatable, dimension(:)    :: tout
    double precision, allocatable, dimension(:,:)  :: z

    !Common variables
    integer                                        :: flag

    !Print options
    !call getOption or setOption to deal with hampath options
    write(*,*) "----------------------------------------------------------------------------"
    call printOptions(options)

    !First shooting: lambda = 0
    write(*,*) "----------------------------------------------------------------------------"
    write(*,*) "Shooting..."

    t0           = 0d0
    t1           = 4.1211968208713d0
    t2           = 45.9573259680671d0
    tf           = 206.661d0
    t1norm       = (t1-t0)/(tf-t0)
    t2norm       = (t2-t0)/(tf-t0)
    ti           = (/  0d0,  t1norm, t2norm, 1d0 /)
    z0           = (/ 0d0, 0d0, 214.839d0, 9.45632204560262d0, 205.487457284517d0, 1758.67755658218d0 /)
    ! par(1:nparmin) =  tf, alpha, beta, g0, umax, c, t0, h0, v0, m0, mf
    par          = (/ tf, 0.01227d0, 0.000145d0, 9.81d0, 9.52551d0, 2060d0, ti(1), z0(1), z0(2), z0(3), 67.9833d0,1d0,2d0,0d0 /)
    call exphvfun(ti, z0, ti, options, par, tout, z, flag, nfev) ! Get value at t1

    ! Initial guess
    ! yGuess = [p0 t1 z1 t2 z2]
    yGuess(1:3)  = z(4:6,1)
    yGuess(4)    = ti(2)*par(1)
    yGuess(5:10) = z(:,2)
    yGuess(11)   = ti(3)*par(1)
    yGuess(12:17)= z(:,3)
    call ssolve(yGuess, options, par, y0, ssol, nfev, njev, flag)

    deallocate (tout)
    deallocate (z)

end program main
