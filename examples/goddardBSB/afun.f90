!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief Control.
!!  \param[in] t    Time
!!  \param[in] n    State dimension
!!  \param[in] z    State and adjoint state at t
!!  \param[in] iarc Index of the current arc
!!  \param[in] npar Number of optional parameters
!!  \param[in] par  Optional parameters
!!
!!  \param[out] u   Optimal control
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine control(t,n,z,iarc,npar,par,u)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t
    double precision, dimension(2*n),  intent(in)   :: z
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: u

    external printandstop

    !local variables
    double precision :: tf,D,Dh,Dv,Dvv,Dvh,alpha,beta,g,g0,c
    double precision :: ht,v,m,ph,pv,pm,H0,H1,umax
    integer          :: nparmin,labelArc

    nparmin = 11

    IF (n.NE.3) THEN
        call printandstop('Error in control: wrong state dimension.')
    END IF
    IF (npar.LE.nparmin) THEN
        call printandstop('Error in control: wrong par dimension.')
    END IF

    ht = z(  1); v  = z(  2); m  = z(  3)
    ph = z(n+1); pv = z(n+2); pm = z(n+3)
    tf = par(1); alpha = par(2); beta = par(3);
    g0 = par(4); umax  = par(5); c    = par(6)

    D   = (alpha*v**2)      *exp(-beta*ht)  ! Drag function
    Dh  = (-beta*alpha*v**2)*exp(-beta*ht)  ! d/dh (drag function)
    Dv  = (2*alpha*v)       *exp(-beta*ht)  ! d/dv (drag function)
    Dvv = (2*alpha)         *exp(-beta*ht)  ! d2/dv2 (drag function)
    Dvh = (-beta*2*alpha*v) *exp(-beta*ht)  ! d2/dhdv (drag function)

    labelArc = nint(par(nparmin+iarc))
    select case(labelArc)
        case (0)        ! The third Bang arc is u = 0
            u = 0d0
        case (1)        ! The first Bang arc is u = umax
            u = umax
        case (2)        ! The second Bang arc is u = us
            u = D/c+m*((c-v)*Dh+g0*Dv+c*g0*Dvv-c*v*Dvh)/(D+2*c*Dv+Dvv*c**2)
        case default
            call printandstop('Error control: invalid value in par for the control structure!');
    end select

end subroutine control

!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief H0.
!!  \param[in] t    Time
!!  \param[in] n    State dimension
!!  \param[in] z    State and adjoint state at t
!!  \param[in] iarc Index of the current arc
!!  \param[in] npar Number of optional parameters
!!  \param[in] par  Optional parameters
!!
!!  \param[out] H0  H0
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine geth0(t,n,z,iarc,npar,par,H0)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t
    double precision, dimension(2*n),  intent(in)   :: z
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: H0

    external printandstop

    !local variables
    double precision :: ht,v,m,ph,pv,pm,tf,alpha,beta,g0,umax,c,D,t0
    integer          :: nparmin

    nparmin = 11

    IF (n.NE.3) THEN
        print*,n
        call printandstop('Error in geth0: wrong state dimension.')
    END IF
    IF (npar.LE.nparmin) THEN
        call printandstop('Error in geth0: wrong par dimension.')
    END IF

    ht = z(1); v = z(2); m = z(3)
    ph = z(1+n); pv = z(2+n); pm = z(3+n)

    tf = par(1); alpha = par(2); beta = par(3)
    g0 = par(4); umax = par(5); c = par(6); t0 = par(7)

    D  = (alpha*v**2)*exp(-beta*ht) !Drag function

    H0 = ph*v-pv*(D/m+g0)

end subroutine geth0

!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief H1.
!!  \param[in] t    Time
!!  \param[in] n    State dimension
!!  \param[in] z    State and adjoint state at t
!!  \param[in] iarc Index of the current arc
!!  \param[in] npar Number of optional parameters
!!  \param[in] par  Optional parameters
!!
!!  \param[out] H1  H1
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine geth1(t,n,z,iarc,npar,par,H1)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t
    double precision, dimension(2*n),  intent(in)   :: z
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: H1

    external printandstop

    !local variables
    double precision :: m,pv,pm,c
    integer          :: nparmin

    nparmin = 11

    IF (n.NE.3) THEN
        call printandstop('Error in geth1: wrong state dimension.')
    END IF
    IF (npar.LE.nparmin) THEN
        call printandstop('Error in geth1: wrong par dimension.')
    END IF

    m  = z(3); pv = z(2+n); pm = z(3+n)
    c  = par(6)
    H1 = pv*c/m-pm

end subroutine geth1

!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief H01.
!!  \param[in] t    Time
!!  \param[in] n    State dimension
!!  \param[in] z    State and adjoint state at t
!!  \param[in] iarc Index of the current arc
!!  \param[in] npar Number of optional parameters
!!  \param[in] par  Optional parameters
!!
!!  \param[out] H01 H01 value
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine geth01(t,n,z,iarc,npar,par,H01)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t
    double precision, dimension(2*n),  intent(in)   :: z
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: H01

    external printandstop

    !local variables
    double precision :: tf,D,Dh,Dv,Dvv,Dvh,alpha,beta,g,g0,umax,u,c
    double precision :: ht,v,m,ph,pv,pm
    integer          :: nparmin

    nparmin = 11

    IF (n.NE.3) THEN
        call printandstop('Error in geth01: wrong state dimension.')
    END IF
    IF (npar.LE.nparmin) THEN
        call printandstop('Error in geth01: wrong par dimension.')
    END IF

    ht  = z(  1); v  = z(  2); m  = z(  3)
    ph  = z(n+1); pv = z(n+2); pm = z(n+3)
    tf  = par(1); alpha = par(2); beta = par(3)
    g0  = par(4); umax  = par(5); c    = par(6)
    D   = (alpha*v**2)*exp(-beta*ht)
    Dv  = (2*alpha*v) *exp(-beta*ht)
    H01 = (1/m**2)*(pv*(D+c*Dv)-ph*c*m)

end subroutine geth01
