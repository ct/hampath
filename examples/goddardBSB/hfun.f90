!!********************************************************************
!>  \ingroup problemDefinition
!!  \brief Maximized Hamiltonian.
!!  \param[in] t      Time
!!  \param[in] n      State dimension
!!  \param[in] z      State and adjoint state at t
!!  \param[in] iarc   Index of the current arc
!!  \param[in] npar   Number of optional parameters
!!  \param[in] par    Optional parameters
!!
!!  \param[out] H   True Hamiltonian
!!  \attention      The vector par can be used for
!!                    constant values or homotopic parameters.
!!
!!  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine hfun(t,n,z,iarc,npar,par,H)
      implicit none
      integer, intent(in)                             :: n,npar,iarc
      double precision, intent(in)                    :: t
      double precision, dimension(2*n),  intent(in)   :: z
      double precision, dimension(npar), intent(in)   :: par
      double precision, intent(out)                   :: H

      external printandstop

      !local variables
      double precision :: t0,tf,H0,H1,u
      integer          :: nparmin

      nparmin = 11

      IF (n.NE.3) THEN
          print*,n
          call printandstop('Error in hfun: wrong state dimension.')
      END IF
      IF (npar.LE.nparmin) THEN
          call printandstop('Error in hfun: wrong par dimension.')
      END IF

      t0 = par(7); tf = par(1)

      call   geth0(t,n,z,iarc,npar,par,H0)
      call   geth1(t,n,z,iarc,npar,par,H1)
      call control(t,n,z,iarc,npar,par,u)

      H  = (tf-t0)*(H0+u*H1)

end subroutine hfun

