% Main file for the Bang-Sinular-Bang case for Goddard problem
%
% Problem definition
%
%   max h(tf)
%   dot(h) = v
%   dox(v) = (u-D)/m - 1/r**2
%   dot(m) = -u
%   h(0) = h_0, v(0) = v_0, m(0) = m_0, m(1) = m_f
%
%  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
%  \date   2016
%
clear;
close all;
format long;

set(0,  'defaultaxesfontsize'   ,  14     , ...
'DefaultTextVerticalAlignment'  , 'bottom', ...
'DefaultTextHorizontalAlignment', 'left'  , ...
'DefaultTextFontSize'           ,  14);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 1: parameters initialization\n')

% Initial guess
n       = 3;                                % Dimension state
t0      = 0.0;                              % Initial time
t0norm  = 0.0;                              % Normalized initial time
tf      = 206.661;                          % Final time
tfnorm  = 1.0;                              % Normalized final time
t1      = 4.1211968208713;                  % First guessed switching time
t1norm  = (t1-t0)/(tf-t0);                  % Normalized first guessed switching time
t2      = 45.9573259680671 ;                % Second guessed switching time
t2norm  = (t2-t0)/(tf-t0);                  % Normalized second guessed switching time
q0      = [0.0 0.0 214.839];                % Initial state h_0 v_0 m_0
p0      = [0.945632204560262E+01 0.205487457284517E+03 0.175867755658218E+04];
par     = [tf 0.01227 0.000145 9.81 9.52551 2060 t0 q0(1) q0(2) q0(3) 67.9833 1 2 0]'; % tf alpha beta g0 umax c t0 q0 mf
options = hampathset                        % Hampath options

% Initial guess
[ tout, z, flag] = exphvfun([t0norm t1norm t2norm], [q0 p0]', [t0norm t1norm t2norm tfnorm], options, par);
z1      = z(:,2);                           % z1 = integration of z0 between t0 and t1
z2      = z(:,3);                           % z2 = integration of z0 between t1 and t2
yGuess  = [p0 t1 z1' t2 z2'];               % yGuess = [p0, t1, z1, t2, z2]

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 2: first shooting\n')

[y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 3: plotting the solution\n')

% Solution
z0          = zeros(2*n,1);
z0(1:n)     = q0;
z0(n+1:end) = y0(1:3);
t1          = y0(4);
t2          = y0(11);
t1norm      = (t1-t0)/(tf-t0);
t2norm      = (t2-t0)/(tf-t0);
ti          = [t0norm t1norm t2norm tfnorm];
[ tout, z, flag ] = exphvfun([t0 tfnorm], z0, ti, options, par);
subplot(2,5,1);plot(tout,z(1,:),'b');xlabel('t');ylabel('h');   drawnow; title('State solution');
subplot(2,5,2);plot(tout,z(2,:),'b');xlabel('t');ylabel('v');   drawnow;
subplot(2,5,3);plot(tout,z(3,:),'b');xlabel('t');ylabel('m');   drawnow;
subplot(2,5,6);plot(tout,z(4,:),'b');xlabel('t');ylabel('p_h'); drawnow; title('Co-state solution');
subplot(2,5,7);plot(tout,z(5,:),'b');xlabel('t');ylabel('p_v'); drawnow;
subplot(2,5,8);plot(tout,z(6,:),'b');xlabel('t');ylabel('p_m'); drawnow;

% Control
u   = control(tout,z,ti,par);
subplot(2,5,4);plot(tout,u,'r');xlabel('t');ylabel('u');        drawnow; title('Control');

% H1
H1  = geth1(tout,z,ti,par);
subplot(2,5,9);plot(tout,H1,'r');xlabel('t');ylabel('H_1');     drawnow; title('H_1');

% H01
H01 = geth01(tout,z,ti,par);
subplot(2,5,10);plot(tout,H01,'r');xlabel('t');ylabel('H_0_1'); drawnow; title('H_0_1');
