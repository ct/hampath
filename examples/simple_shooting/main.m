% Main file for the simple shooting case
%
% Problem definition
%
%   min int_0^1 u^2
%   dot(x) = v
%   dox(v) = -lambda v^2 + u
%   x(0) = x_0, x(1) = x_f, v(0) = v_0, v(1) = v_f
%
%  \author Olivier Cots (INP-ENSEEIHT-IRIT)
%  \date   2016
%
clear;
close all;
format long;

set(0,  'defaultaxesfontsize'   ,  14     , ...
'DefaultTextVerticalAlignment'  , 'bottom', ...
'DefaultTextHorizontalAlignment', 'left'  , ...
'DefaultTextFontSize'           ,  14);

%
addpath('libhampath/');

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 1: parameters initialization\n');

    t0      = 0.0;                                      % Initial time
    tf      = 1.0;                                      % Final time
    q0      = [-1.0 0.0]';                              % Initial state
    yGuess  = [0.1 0.1]';                               % Initial guess for the shooting metdhod
    par     = [t0 tf q0(1) q0(2) 0.0 0.0 0.0]';         % t0, tf, x_0, v_0, x_f, v_f, lambda_0
    par0    = par;                                      % par_0
    parf    = par; ilambda = 7; parf(ilambda) = 1.0;    % par_f with lambda_f = 1.0 : for homotopy on lambda
    options = hampathset                                % Hampath options
    n       = 2;                                        % State dimension

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 2: first shooting\n');

    [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par0);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 3: continuation on lambda from %g to %g\n',par0(ilambda),parf(ilambda));

    parspan = [par0 parf];
    [parout,yout,~,~,~,~,~,flag] = hampath(parspan,y0,options);

    lout    = parout(ilambda,:);
    p0f     = yout(:,end);
    parf    = parout(:,end);

    % Figures
    % Paths
    figure;
    subplot(2,5,1);plot(yout(1,:),lout,'k');xlabel('p_x(0)');ylabel('\lambda'); drawnow; title('Paths');
    subplot(2,5,6);plot(yout(2,:),lout,'k');xlabel('p_v(0)');ylabel('\lambda'); drawnow;

    % Solution
    [tout,z,flag] = exphvfun([t0 tf],[q0;p0f],options,parf);

    subplot(2,5,2);plot(tout,z(1,:),'b');xlabel('t');ylabel('x(t)');   drawnow; xlim([t0 tf]); title('State solution');
    subplot(2,5,3);plot(tout,z(3,:),'b');xlabel('t');ylabel('p_x(t)'); drawnow; xlim([t0 tf]); title('Co-state solution');
    subplot(2,5,7);plot(tout,z(2,:),'b');xlabel('t');ylabel('v(t)');   drawnow; xlim([t0 tf]);
    subplot(2,5,8);plot(tout,z(4,:),'b');xlabel('t');ylabel('p_v(t)'); drawnow; xlim([t0 tf]);

    % Control
    u       = control(tout,z,parf);
    subplot(2,5,[4 5]);plot(tout,u,'r');xlabel('t');ylabel('u(t)');    drawnow; xlim([t0 tf]); title('Control');

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 4: optimality checked on final solution\n');

    % Compute Jacobi fields
    k = n; % regular case, k = n : fixed tf
    z0  = [q0;p0f]; dz0 = [zeros(n);eye(n)];
    [tout,z,dz,flag] = expdhvfun([t0 tf],z0,dz0,options,parf);

    sv=[]; de=[];
    for j = 1:length(tout)
        dq    = dz(1:n,1+(j-1)*k:j*k);
        sv(j) = min(svd(dq));           % Get smallest singular value
        de(j) = det(dq);                % Get determinant
    end;

    subplot(2,5,[9 10]); plot(tout,sv,'m'); hold on; plot(tout,max(sv)*sign(de),'k--'); hold on; xlabel('t');
    leg = legend('$\sigma_{min}$','$sign(\det(\delta q))$','location','West');
    set(leg,'Interpreter','latex');
    drawnow; title('Conjugate points');
