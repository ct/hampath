program main
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !Problem definition
    !max h(tf)
    !
    !dot h = v
    !dot v = 1/m (u*c -D) - g
    !dot m = -u
    !
    !h(0) = 0, v(0) = 0, m(0) = m0
    !m(1) = mf
    !
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !1) Module used in the main program
    !
    !use mod_expdhvfun
    use mod_exphvfun
    !use mod_hamfun
    !use mod_hampath
    !use mod_hvfun
    !use mod_hybrd
    use mod_options
    !use mod_sfun
    !use mod_sjac
    use mod_ssolve

    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !2) variables declaration
    !
    implicit none
    type(TOptions)   :: options
    !ssolve variables
    double precision :: yGuess(10), y0(10), ti(3), z0(6), par(13), ssol(10), u
    integer          :: nfev,njev

    !exphvfun variables
    double precision, allocatable, dimension(:)    :: tout
    double precision, allocatable, dimension(:,:)  :: z

    !Common variables
    integer                                        :: flag

    !Print options
    !call getOption or setOption to deal with hampath options
    write(*,*) "----------------------------------------------------------------------------"
    call printOptions(options)

    !First shooting: lambda = 0
    write(*,*) "----------------------------------------------------------------------------"
    write(*,*) "Shooting..."

    ti  = (/  0d0,  0.75d0, 1d0 /)
    z0  = (/ 0d0, 0d0, 214.839d0, 0.1d0, 0.1d0, 0.1d0 /)
    par = (/ 20d0, 0.01227d0, 0.000145d0, 9.81d0, 9.52551d0, 2060d0, ti(1), z0(1), z0(2), z0(3), 67.9833d0,1d0,0d0 /) ! tf,alpha,beta,g0,umax,c,t0,h0,v0,m0,mf
    call exphvfun(ti, z0, ti, options, par, tout, z, flag, nfev) !Get value at t1

    !Unknown of the shooting function:
    !yGuess = [p0 t1 z1]
    yGuess(1:3)  = z(4:6,1)
    yGuess(4)    = ti(2)*par(1)
    yGuess(5:10) = z(:,2)
    call ssolve(yGuess, options, par, y0, ssol, nfev, njev, flag)

    deallocate (tout)
    deallocate (z)

end program main
