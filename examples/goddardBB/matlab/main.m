% Main file for the Bang-Bang case for Goddard problem
%
% Problem definition
%
%   max h(tf)
%   dot(h) = v
%   dox(v) = (u-D)/m - 1/r**2
%   dot(m) = -u/c
%   h(0) = h_0, v(0) = v_0, v(1) = v_f, m(0) = m_0, m(1) = m_f
%
%  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
%  \date   2016
%
clear;
close all;
format long;

set(0,  'defaultaxesfontsize'   ,  14     , ...
'DefaultTextVerticalAlignment'  , 'bottom', ...
'DefaultTextHorizontalAlignment', 'left'  , ...
'DefaultTextFontSize'           ,  14);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 1: parameters initialization\n');

% Initial guess
n       = 3                                 % State dimension
t0      = 0.0;                              % Initial time
t1norm  = 0.75;                             % Guessed normalized switching time
tf      = 20.0;                             % Final time
t0norm  = 0.0;
tfnorm  = 1.0;                              % Normalized final time
q0      = [0.0 0.0 214.839];                % Initial state h_0 v_0 m_0
p0      = [0.1 0.1 0.1];
par     = [tf 0.01227 0.000145 9.81 9.52551 2060 t0 q0(1) q0(2) q0(3) 67.983310 1 0]';  % tf alpha beta g0 umax c t0 q0 mf
options = hampathset                        % Hampath options

% Initial guess
[ tout, z, flag] = exphvfun([t0norm t1norm], [q0 p0]', [t0 t1norm tfnorm], options, par);
z1      = z(:,end);                         % z1 = integration of z0 between t0 and t1
yGuess  = [p0 t1norm*tf z1'];                  % yGuess = [p0, t1, z1]

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 2: first shooting\n')

[y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 3: plotting the solution\n')

% Solution
z0          = zeros(2*n,1);
z0(  1:  n) = q0;
z0(n+1:2*n) = y0(1:3);
t1          = y0(4);
t1norm      = (t1-t0)/(tf-t0);
ti  = [t0norm t1norm tfnorm];
[tout,z,flag] = exphvfun([t0norm tfnorm],z0,ti,options,par);
subplot(2,4,1); plot(tout,z(1,:),'b'); xlabel('t'); ylabel('h(t)');   drawnow; title('State solution');
subplot(2,4,2); plot(tout,z(2,:),'b'); xlabel('t'); ylabel('v(t)');   drawnow;
subplot(2,4,3); plot(tout,z(3,:),'b'); xlabel('t'); ylabel('m(t)');   drawnow;
subplot(2,4,5); plot(tout,z(4,:),'b'); xlabel('t'); ylabel('p_h(t)'); drawnow; title('Co-state solution');
subplot(2,4,6); plot(tout,z(5,:),'b'); xlabel('t'); ylabel('p_v(t)'); drawnow;
subplot(2,4,7); plot(tout,z(6,:),'b'); xlabel('t'); ylabel('p_m(t)'); drawnow;

% Control
u   = control(tout,z,ti,par);
subplot(2,4,4); plot(tout,u,'r') ; xlabel('t'); ylabel('u') ; drawnow; title('Control');

% H1
H1  = geth1(tout,z,ti,par);
subplot(2,4,8); plot(tout,H1,'r'); xlabel('t'); ylabel('H1'); drawnow; title('H_1');
