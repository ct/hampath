#Problem definition
#min tf
#
#dot x = v
#dot v = -lambda v**2 + u
#
#x(0) = -1, x(1) = 0
#v(0) = -1, v(1) = 0

import numpy as np
import matplotlib.pyplot as plt

# It is assumed that HamPath lib files are placed in LIBHAMPATH
from LIBHAMPATH.hampathcode import *   # Change directory name if needed
from LIBHAMPATH.control_p import *     # Change directory name if needed

#-------------------------------------------------------------------------------------------------------------%
print('\nStep 1: parameters initialization\n')

#Initial guess
t1      = 2.0
tf      = 4.0
p0      = np.array([0.1, 0.1])
yGuess  = np.array([t1, tf, p0[0], p0[1]])              # Initial guess for the shooting metdhod

#Parameters
t0      = 0.0                                           # Initial time
x0      = np.array([-1.0, 0.0])                         # Initial state
par     = np.array([t0, x0[0], x0[1], 0.0, 0.0, 0.0])   # t0, x_0, v_0, x_f, v_f, lambda_0
npar    = len(par)
par0    = np.array(par)                                 # par_0
parf    = np.array(par); parf[-1] = 1                   # par_f with lambda_f = 1.0: for homotopy on lambda
n       = len(x0)                                       # State dimension

#Options
options = HampathOptions()                              # Hampath options
print(options)

#Initial guess
z0          = np.zeros((2*n,))
z0[0:n]     = x0
z0[n:2*n]   = p0
ti          = np.array([t0, t1, tf])                    # ti is required since there are two arcs !
[tout, z, flag] = exphvfun(np.array([t0, t1]), z0, ti, options, par0)
z1          = z[:,-1]
yGuess      = np.append(yGuess,z1)

#-------------------------------------------------------------------------------------------------------------
print('\nStep 2: first shooting\n')

[y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par0)

#-------------------------------------------------------------------------------------------------------------%
print('\nStep 3: continuation on lambda from ', par0[-1], ' to ', parf[-1], '\n')

parspan = np.zeros((npar,2))
parspan[:,0] = par0
parspan[:,1] = parf

[ parout, xout, sout, viout, dets, normS, ps, flag ] = hampath(parspan,y0,options)

lout = parout[-1,:]
t1f  = xout[0,-1]
tff  = xout[1,-1]
p0f  = xout[2:4,-1]
parf = parout[:,-1]

# Figures
fig, axarr = plt.subplots(nrows=2, ncols=4)
#fig.tight_layout() # Or equivalently,  "plt.tight_layout()"

left    = 0.125 # the left side of the subplots of the figure
right   = 0.9   # the right side of the subplots of the figure
bottom  = 0.1   # the bottom of the subplots of the figure
top     = 0.9   # the top of the subplots of the figure
wspace  = 0.3   # the amount of width reserved for blank space between subplots
hspace  = 0.3   # the amount of height reserved for white space between subplots

fig.subplots_adjust(left=left, bottom=bottom, right=right, top=top, wspace=wspace, hspace=hspace)

# Paths
lig,col = 0,0
axarr[lig,col].plot(xout[2,:],lout,'k')
axarr[lig,col].set_xlabel('$p_x(0)$')
axarr[lig,col].set_ylabel('$\lambda$')
axarr[lig,col].set_title('Paths')

lig,col = 1,0
axarr[lig,col].plot(xout[3,:],lout,'k')
axarr[lig,col].set_xlabel('$p_v(0)$')
axarr[lig,col].set_ylabel('$\lambda$')
axarr[lig,col].set_xlim(1-0.1,1+0.1)

# Solution
z0          = np.zeros((2*n,))
z0[0:n]     = x0
z0[n:2*n]   = p0f
ti          = np.array([t0, t1f, tff])
[ tout, z, flag ] = exphvfun(np.array([t0, tff]), z0, ti, options, parf)

lig,col = 0,1
axarr[lig,col].plot(tout,z[0,:],'b')
axarr[lig,col].set_xlabel('t')
axarr[lig,col].set_ylabel('$x(t)$')
axarr[lig,col].set_title('State solution')

lig,col = 0,2
axarr[lig,col].plot(tout,z[2,:],'b')
axarr[lig,col].set_xlabel('t')
axarr[lig,col].set_ylabel('$p_x(t)$')
axarr[lig,col].set_title('Co-state solution')

lig,col = 1,1
axarr[lig,col].plot(tout,z[1,:],'b')
axarr[lig,col].set_xlabel('t')
axarr[lig,col].set_ylabel('$v(t)$')

lig,col = 1,2
axarr[lig,col].plot(tout,z[3,:],'b')
axarr[lig,col].set_xlabel('t')
axarr[lig,col].set_ylabel('$p_v(t)$')

# Control
u   = control(tout,z,np.array([t0, t1f, tff]),parf)
lig,col = 0,3
axarr[lig,col].plot(tout,u[0,:],'r')
axarr[lig,col].set_xlabel('t')
axarr[lig,col].set_ylabel('$u(t)$')
axarr[lig,col].set_title('Control')

#-------------------------------------------------------------------------------------------------------------%
axarr[-1, -1].axis('off')
plt.show()
