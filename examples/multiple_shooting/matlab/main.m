% Main file for the multiple shooting case
%
% Problem definition
%
%   min tf
%   dot(x) = v
%   dox(v) = -lambda v^2 + u
%   x(0) = x_0, x(1) = x_f, v(0) = v_0, v(1) = v_f
%
%  \author Olivier Cots (INP-ENSEEIHT-IRIT)
%  \date   2016
%
clear;
close all;
format long;

set(0,  'defaultaxesfontsize'   ,  14     , ...
'DefaultTextVerticalAlignment'  , 'bottom', ...
'DefaultTextHorizontalAlignment', 'left'  , ...
'DefaultTextFontSize'           ,  14);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 1: parameters initialization\n');

    %Parameters
    t0      = 0.0                                   % Initial time
    q0      = [-1.0  0.0]'                          % Initial state
    par     = [t0 q0(1) q0(2) 0.0 0.0 0.0]';        % t0, x_0, v_0, x_f, v_f, lambda_0
    par0    = par                                   % par_0
    parf    = par; ilambda = 6; parf(ilambda) = 1;  % par_f with lambda_f = 1.0: for homotopy on lambda

    %Options
    options = hampathset                            % Hampath options

    %Initial guess
    t1      = 2.0;
    tf      = 4.0;
    p0      = [0.1 0.1]';
    yGuess  = [t1 tf p0(1) p0(2)]'
    ti      = [t0 t1 tf];                                       % ti is required since there are two arcs!
    [tout,z,flag] = exphvfun([t0 t1],[q0;p0],ti,options,par0);  % See help exphvfun
    z1      = z(:,end);
    yGuess  = [yGuess ; z1];

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 2: first shooting\n');

    [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par0);

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 3: continuation on lambda from %g to %g\n',par0(ilambda),parf(ilambda));

    parspan = [par0 parf];
    [parout,yout,~,~,~,~,~,flag] = hampath(parspan,y0,options);

    lout = parout(ilambda,:);
    parf = parout(:,end);
    t1   = yout(1,end);
    tf   = yout(2,end);
    p0f  = yout(3:4,end);
    ti   = [t0 t1 tf];  % ti is required since there are two arcs!

    % Figures
    % Paths
    figure;
    subplot(2,5,1);plot(yout(3,:),lout,'k');xlabel('p_x(0)');ylabel('\lambda'); drawnow; title('Paths');
    subplot(2,5,6);plot(yout(4,:),lout,'k');xlabel('p_v(0)');ylabel('\lambda'); drawnow; xlim([1-0.1 1+0.1]);

    % Solution
    [tout,z,flag] = exphvfun([t0 tf],[q0;p0f],ti,options,parf);

    subplot(2,5,2);plot(tout,z(1,:),'b');xlabel('t');ylabel('x(t)');   drawnow; xlim([t0 tf]); title('State solution');
    subplot(2,5,3);plot(tout,z(3,:),'b');xlabel('t');ylabel('p_x(t)'); drawnow; xlim([t0 tf]); title('Co-state solution');
    subplot(2,5,7);plot(tout,z(2,:),'b');xlabel('t');ylabel('v(t)');   drawnow; xlim([t0 tf]);
    subplot(2,5,8);plot(tout,z(4,:),'b');xlabel('t');ylabel('p_v(t)'); drawnow; xlim([t0 tf]);

    % Control
    u    = control(tout,z,ti,parf);
    subplot(2,5,[4 5]);plot(tout,u,'r');xlabel('t');ylabel('u(t)'); drawnow; title('Control'); xlim([t0 tf]);
