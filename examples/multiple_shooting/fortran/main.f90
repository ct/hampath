program main
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !Steps of the main program:
    !1) include modules used
    !2) declare all variables
    !3) initialize options
    !4) Make a first shoot
    !5) Make a continuation on lambda parameter from 0 to 1.0
    !
    !Problem definition
    !min tf
    !
    !dot x = v
    !dot v = -lambda v**2 + u
    !
    !x(0) = -1, x(1) = 0
    !v(0) =  0, v(1) = 0
    !
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !1) Module used in the main program
    !
    !use mod_expdhvfun
    use mod_exphvfun
    !use mod_hamfun
    use mod_hampath
    !use mod_hvfun
    !use mod_hybrd
    use mod_options
    !use mod_sfun
    !use mod_sjac
    use mod_ssolve

    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !2) variables declaration
    !
    implicit none
    type(TOptions)   :: options
    !ssolve variables
    double precision :: yGuess(8), y0(8), ti(3), z0(4), par(6), ssol(8), u
    integer          :: nfev, njev

    !hampath variables
    double precision,  dimension(6,2)              :: parspan
    double precision,  dimension(:,:), allocatable :: parout
    double precision,  dimension(:,:), allocatable :: yout
    double precision,  dimension(:),   allocatable :: sout
    double precision,  dimension(:,:), allocatable :: viout
    double precision,  dimension(:),   allocatable :: dets
    double precision,  dimension(:),   allocatable :: normS
    double precision,  dimension(:),   allocatable :: ps

    !exphvfun variables
    double precision, allocatable, dimension(:)     :: tout
    double precision, allocatable, dimension(:,:)   :: z

    !Common variables
    integer          :: flag

    !Print options
    !call getOption or setOption to deal with hampath options
    write(*,*) "----------------------------------------------------------------------------"
    call printOptions(options)

    !First shooting: lambda = 0
    write(*,*) "----------------------------------------------------------------------------"
    write(*,*) "Shooting..."

    ti      = (/  0d0,  2d0, 4d0 /)
    z0      = (/ -1d0,  0d0, 0.1d0, 0.1d0 /)
    par     = (/ ti(1), z0(1), z0(2),  0d0, 0d0, 0d0  /)   ! t0, x_0, v_0, x_f, v_f, lambda_0
    call exphvfun(ti, z0, ti, options, par, tout, z, flag, nfev) !Get value at t1

    !Unknown of the shooting function:
    !   yGuess = [t1 tf px(0) pv(0) z(t1)]
    !
    yGuess(1)   = ti(2)
    yGuess(2)   = ti(3)
    yGuess(3)   = z0(3)
    yGuess(4)   = z0(4)
    yGuess(5:8) = z(:,2)
    call ssolve(yGuess, options, par, y0, ssol, nfev, njev, flag)

    !Homotopy on lambda, from 0 to 1
    write(*,*) "----------------------------------------------------------------------------"
    write(*,*) "Homotopy..."   
    parspan(:,1) = par
    parspan(:,2) = par
    parspan(6,2) = 1d0 !for homotopy
    call hampath(parspan,y0,options,parout,yout,sout,viout,dets,normS,ps,flag)

    deallocate(parout,yout,sout,viout,dets,normS,ps,tout,z)

end program main
