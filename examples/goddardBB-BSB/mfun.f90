!!************************************************************************
!>  \ingroup problemDefinition
!!  \brief Monitoring during continuation. This function is called
!!          at each accepted step during hampath call (during homotopy).
!!
!!  \param[in]       Niter    Number of iterations
!!  \param[in]       ny       Shooting variable dimension
!!  \param[in]       npar     Number of optional parameters
!!  \param[in,out]   flag     Flag: homotopy stops when flag < 0
!!                            Values from -1 to -10 are already assigned
!!                            Please choose values less than -11
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!
Subroutine mfun(Niter,ny,npar,flag)
    use mod_exphv4sfun
    implicit none
    integer, intent(in)     :: Niter, ny, npar
    integer, intent(inout)  :: flag

    !! ------------------------------------------------------------------------------
    !!
    !!  The user can call getPointsFromPath and exphv subroutines inside mfun
    !!
    !! ------------------------------------------------------------------------------
    !!
    !! getPointsFromPath: get the N last points from the path of zeros.
    !!
    !!      syntax: call getPointsFromPath(ny,npar,N,arclengths,ys,pars)
    !!
    !!       integer         , intent(in)    :: ny
    !!       integer         , intent(in)    :: npar
    !!       integer         , intent(in)    :: N
    !!       double precision, intent(out)   :: arclengths(N)
    !!       double precision, intent(out)   :: ys(ny,N)
    !!       double precision, intent(out)   :: pars(npar,N)
    !!
    !! ------------------------------------------------------------------------------
    !!
    !!  exphv: computes the chronological exponential of the
    !!          Hamiltonian vector field hv defined by h.
    !!
    !!      syntax: call exphv(tspan,n,z0,iarc,npar,par,z)
    !!
    !!       integer         , intent(in)    :: n
    !!       integer         , intent(in)    :: iarc
    !!       integer         , intent(in)    :: npar
    !!       double precision, intent(in)    :: tspan(:) = [t0, t1, ... tf]
    !!       double precision, intent(in)    :: z0(2*n)
    !!       double precision, intent(in)    :: par(npar)
    !!
    !!  Either
    !!
    !!       double precision, intent(out)   :: z(2*n) = z(tf)
    !!
    !!  Or
    !!
    !!       double precision, intent(out)   :: z(2*n,nt) = [z(t0), z(t1), ... z(tf)]
    !!
    !!  Remark: in each case, the integration is made on only one single arc.
    !!
    !! ------------------------------------------------------------------------------

    !local variables
    integer             :: np
    double precision    :: arclengths(1), ys(ny,1), pars(npar,1)
    double precision    :: t1, t2, tf

    np = 1 ! Get the last point from the path of zeros, ie y and par
    call getPointsFromPath(ny,npar,np,arclengths,ys,pars)

    t1 = ys(4,1) ! The first switching time

    !Bang-Singular-Bang case
    if(ny.eq.17) then 

        t2 = ys(11,1) ! The second switching times

        !if t1>t2, it means the structure has changed from BSB to BB
        if(t1-t2.ge.0d0)then
            flag = -11 ! hampath will exit with flag = -11
        end if

    !Bang-Bang case
    else if(ny.eq.10) then

        tf = pars(1,1) ! The final time

        !if t1>tf, it means the structure has changed from BB to B
        if(t1-tf.ge.0d0)then
            flag = -12 ! hampath will exit with flag = -12
        end if

    end if

End Subroutine mfun
