# Main file for the Bang-Bang and Bang-Singular-Bang case for Goddard problem
#
# Problem definition
#
#   max h(tf)
#   dot(h) = v
#   dox(v) = (u-D)/m - 1/r**2
#   dot(m) = -u/c
#   h(0) = h_0, v(0) = v_0, v(1) = v_f, m(0) = m_0, m(1) = m_f
#
#  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
#  \date   2016
#
import numpy as np
import matplotlib.pyplot as plt
# It is assumed that HamPath lib files are placed in LIBHAMPATH
from LIBHAMPATH.hampathcode import *
from LIBHAMPATH.control_p import *
from LIBHAMPATH.geth1_p import *
from LIBHAMPATH.geth01_p import *

plt.close('all')

#-------------------------------------------------------------------------------------------------------------%
print('\nStep 1: parameters initialization\n')

# Initial guess
t0      = 0.0                               # Initial time
t0norm  = 0.0                               # Normalized initial time
tf      = 70.0                              # Final time
tfnorm  = 1.0                               # Normalized final time
t1      = 5.0                               # First guessed switching time
t1norm  = (t1-t0)/(tf-t0)                   # Normalized first guessed switching time
t2      = 25.0                              # Second guessed switching time
t2norm  = (t2-t0)/(tf-t0)                   # Normalized second guessed switching time
q0      = np.array([0.0,0.0,214.839])       # Initial state h_0 v_0 m_0
n       = len(q0)                           # State dimension
p0      = np.array([5.0, 100.0, 500.0])
par     = np.array([tf, 0.01227, 0.000145, 9.81, 9.52551, 2060, t0, q0[0], q0[1], q0[2], 67.9833,1,2,0])  # tf, alpha, beta, g0, umax, c, t0, q0, mf
par0    = np.array(par)
parf    = np.array(par); indtf = 0; parf[indtf] = 0.0 # We do a homotopy from tf = 70 to tf = 0
nparmin = 11
options = HampathOptions()
print(options)

# Initial guess
z0          = np.zeros((2*n,))
z0[0:n]     = q0
z0[n:2*n]   = p0
[ tout, exphv, flag] = exphvfun(np.array([t0norm,t1norm,t2norm]), z0, np.array([t0norm, t1norm, t2norm, tfnorm]), options, par)
z1          = exphv[:,1]                        # z2 = z(t1, z0)
z2          = exphv[:,2]                        # z2 = z(t2, z(t1, z0))
yGuess      = np.concatenate((p0, [t1], z1, [t2], z2))    # yGuess = [p0, t1, z1, t2, z2]

#-------------------------------------------------------------------------------------------------------------%
print('\nStep 2: shootings and homotopies\n')

# First shooting in the Bang-Singular-Bang case
[y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par)

# First homotopy with Bang-Singular-Bang structure
parspan      = np.zeros((nparmin+3,2))
parspan[:,0] = par0
parspan[:,1] = parf
[ parout, yout, sout, viout, dets, normS, ps, flag ] = hampath(parspan,y0,options)

if flag==-11: # Change in the structure detected: from BSB to BB

    # Shooting to get the exact value of tf at the change of structure
    # The limit structure is here BB with H1(tf) = H01(tf) = 0
    par         = np.concatenate((parout[0:nparmin,-1], [1], [0]))
    yGuess      = np.zeros((11,))
    yGuess[0:10]= yout[0:10,-1]
    yGuess[10]  = par[indtf]
    [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par)
    t1BSB = y0[3]
    tfBSB = y0[10]

    # Second homotopy with Bang-Bang structure
    par[indtf]   = tfBSB # Value of tf when the structure changes
    par0         = np.array(par)
    parf         = np.array(par); parf[indtf] = 0.0
    parspan      = np.zeros((nparmin+2,2))
    parspan[:,0] = par0
    parspan[:,1] = parf
    [ parout, yout, sout, viout, dets, normS, ps, flag ] = hampath(parspan,y0[0:10],options)

    if flag==-12: # Change in the structure detected: from BB to B

        # Shooting to get the exact value of tf at the change of structure
        # The limit structure is Bang with H1(tf) = 0
        par         = np.concatenate((parout[0:nparmin,-1], [1]))
        yGuess      = np.zeros((4,))
        yGuess[0:3] = yout[0:3,-1]
        yGuess[3]   = par[indtf]
        [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par)
        tfBB = y0[3]

        print("\nResults:\n")
        print("The structure is BSB from tf = " + str(tf)    + " to tf = " + str(tfBSB))
        print("The structure is BB  from tf = " + str(tfBSB) + " to tf = " + str(tfBB) )
