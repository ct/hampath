function main()
% Main file for the Bang-Bang and Bang-Singular-Bang case for Goddard problem
%
% Problem definition
%
%   max h(tf)
%   dot(h) = v
%   dox(v) = (u-D)/m - 1/r**2
%   dot(m) = -u/c
%   h(0) = h_0, v(0) = v_0, v(1) = v_f, m(0) = m_0, m(1) = m_f
%
%  \author Olivier Cots & Jean-Matthieu Khoury (INP-ENSEEIHT-IRIT)
%  \date   2016
%
    close all;
    format long;

    set(0,  'defaultaxesfontsize'   ,  14     , ...
    'DefaultTextVerticalAlignment'  , 'bottom', ...
    'DefaultTextHorizontalAlignment', 'left'  , ...
    'DefaultTextFontSize'           ,  14);
    fontSize = 20;
    lineWidth = 1.5;

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 1: parameters initialization\n')

    % Initial guess
    n       = 3;                    % Dimension state
    t0      = 0.0;                  % Initial time
    t0norm  = 0.0;                  % Normalized initial time
    tf      = 70;                   % Final time
    tfnorm  = 1.0;                  % Normalized final time
    t1      = 5.0;                  % First guessed switching time
    t1norm  = (t1-t0)/(tf-t0);      % Normalized first guessed switching time
    t2      = 25.0 ;                % Second guessed switching time
    t2norm  = (t2-t0)/(tf-t0);      % Normalized second guessed switching time
    q0      = [0.0 0.0 214.839];    % Initial state h_0 v_0 m_0
    p0      = [5.0 100.0 500.0];
    % par = [ tf alpha beta g0 umax c t0 q0 mf 1 2 0]
    par     = [tf 0.01227 0.000145 9.81 9.52551 2060 t0 q0(1) q0(2) q0(3) 67.9833 1 2 0]';
    options = hampathset            % Hampath options
    par0=par; parf=par; indtf=1; parf(indtf)=0.0; % Homotopy from tf = 70 to 0
    nparmin = 11;

    % Initial guess
    [ tout, z, flag] = exphvfun([t0norm t1norm t2norm], [q0 p0]', [t0norm t1norm t2norm tfnorm], options, par);
    z1      = z(:,2);               % z1 = z(t1, z0)
    z2      = z(:,3);               % z2 = z(t2, z(t1, z0))
    yGuess  = [p0 t1 z1' t2 z2'];   % yGuess = [p0, t1, z1, t2, z2]

%-------------------------------------------------------------------------------------------------------------%
fprintf('\nStep 2: shootings and homotopies\n')

    % First shooting in the Bang-Singular-Bang case
    [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par);

    % First homotopy with Bang-Singular-Bang structure
    parspan = [par0 parf];
    [parout,yout,sout,~,~,~,~,flag] = hampath(parspan,y0,options);

    if(flag==-11) % Change in the structure detected: from BSB to BB

        % Shooting to get the exact value of tf at the change of structure
        % The limit structure is Bang-Bang with H1(tf) = H01(tf) = 0
        par         = [parout(1:nparmin,end); 1; 0];
        yGuess      = yout(1:10,end); yGuess(11)  = par(indtf);
        [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par);
        t1BSB = y0(4); tfBSB = y0(11);

        %Plotting
        hFig     = figure; hold on;
        nt       = length(sout);
        t0out    = zeros(1,nt); t1out = [yout(4,1:nt-1) t1BSB]; t2out = [yout(11,1:nt-1) t1BSB]; tfout = [parout(indtf,1:nt-1) tfBSB];
        tfmax    = tfout(1);
        plot(tfout, t0out, 'LineWidth',lineWidth,'Color','b'); text(tfout(1)+2,t0out(1)-2,'t_0','FontSize',fontSize,'Color','k');
        plot(tfout, t1out, 'LineWidth',lineWidth,'Color','b'); text(tfout(1)+2,t1out(1)-2,'t_1','FontSize',fontSize,'Color','k');
        plot(tfout, t2out, 'LineWidth',lineWidth,'Color','b'); text(tfout(1)+2,t2out(1)-2,'t_2','FontSize',fontSize,'Color','k');
        plot(tfout, tfout, 'LineWidth',lineWidth,'Color','b'); text(40,40+2,'t_f','FontSize',fontSize,'Color','k');
        text(40,2,'u_m_a_x' ,'FontSize',fontSize,'Color','b');
        text(40,12,'u_s'    ,'FontSize',fontSize,'Color','b');
        text(40,25,'0'      ,'FontSize',fontSize,'Color','b');
        xlabel('t_f');
        axis([0 tfmax -5 50]);
        daxes(tfmax,-10,'b--');

        % Second homotopy with Bang-Bang structure
        par(indtf)  = tfBSB % Value of tf when the structure changes
        par0 = par; parf = par0; parf(indtf) = 0.0; parspan = [par0 parf];
        [parout,yout,sout,~,~,~,~,flag] = hampath(parspan,y0(1:10),options);

        if(flag==-12) % Change in the structure detected: from BB to B

            % Shooting to get the exact value of tf at the change of structure
            % The limit structure is Bang with H1(tf) = 0
            par         = [parout(1:nparmin,end); 1];
            yGuess      = yout(1:3,end); yGuess(4)   = par(indtf);
            [y0,ssol,nfev,njev,flag] = ssolve(yGuess,options,par); tfBB=y0(4);

            % Plotting
            nt       = length(sout);
            t0out    = zeros(1,nt); t1out = [yout(4,1:nt-1) tfBB]; tfout = [parout(indtf,1:nt-1) tfBB];
            figure(hFig); hold on;
            plot(tfout, t0out, 'LineWidth', lineWidth,'Color','r');
            plot(tfout, t1out, 'LineWidth', lineWidth,'Color','r');
            plot(tfout, tfout, 'LineWidth', lineWidth,'Color','r');
            text(16.5,2,'u_m_a_x','FontSize',fontSize,'Color','r');
            text(20.0,16,'0'     ,'FontSize',fontSize,'Color','r');
            set(gca,'XTick',[0 tfBB tfBSB tfmax]);
            set(gca,'XTickLabel',{'0','15.42','24.98','70'});
            daxes(tfBSB,-10,'r--');
            daxes(tfBB,-10,'k--');

        end;

    end;

return

function h = daxes(x, y, c)
% daxes -- Draw axes
%
%  Usage
%    daxes(x, y[, c])
%
%  Inputs
%    x, y   origin coordinates
%    c      color
%
%  Outputs
%    h       integer, current figure handle
%
%  Description
%    Draw axes with (x,y) origin.
%
%  See also
%    axis
%

    if (nargin == 0)
    x = 0;
    y = 0;
    c = 'b';
    elseif (nargin == 2)
    c = 'b';
    end;

    ax = axis;
    washold = ishold;
    if ~washold hold on; end;
    plot([ax(1) ax(2)], [y y], c, 'LineWidth',1.0);
    plot([x x], [ax(3) ax(4)], c, 'LineWidth',1.0);
    if ~washold hold off; end;

    h = gcf;

return
