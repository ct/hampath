program main
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !Problem definition
    !max h(tf)
    !
    !dot h = v
    !dot v = 1/m (u*c -D) - g
    !dot m = -u
    !
    !h(0) = 0, v(0) = 0, m(0) = m0
    !m(1) = mf
    !
    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !1) Module used in the main program
    !
    !use mod_expdhvfun
    use mod_exphvfun
    !use mod_hamfun
    use mod_hampath
    !use mod_hvfun
    !use mod_hybrd
    use mod_options
    !use mod_sfun
    !use mod_sjac
    use mod_ssolve

    !----------------------------------------------------------------------------------------------------!
    !----------------------------------------------------------------------------------------------------!
    !2) variables declaration
    !
    implicit none
    type(TOptions)   :: options
    !ssolve variables
    double precision :: yGuess(17), y0(17), ti(4), tspan(3), z0(6), par(14), ssol(17)
    double precision :: u, t0, t0norm, t1, t1norm, t2, t2norm, tf, tfnorm, t1BSB, tfBSB, tfBB
    integer          :: nfev, njev, nparmin, nt, indtf

    !hampath variables
    double precision                               :: parspan(14,2)
    double precision,  dimension(:,:), allocatable :: parout
    double precision,  dimension(:,:), allocatable :: yout
    double precision,  dimension(:),   allocatable :: sout
    double precision,  dimension(:,:), allocatable :: viout
    double precision,  dimension(:),   allocatable :: dets
    double precision,  dimension(:),   allocatable :: normS
    double precision,  dimension(:),   allocatable :: ps

    !exphvfun variables
    double precision, allocatable, dimension(:)    :: tout
    double precision, allocatable, dimension(:,:)  :: z

    !Common variables
    integer                                        :: flag

    !Print options
    !call getOption or setOption to deal with hampath options
    write(*,*) "----------------------------------------------------------------------------"
    call printOptions(options)

    !First shooting: lambda = 0
    write(*,*) "----------------------------------------------------------------------------"
    write(*,*) "Shooting..."

    nparmin      = 11
    t0           = 0d0
    t1           = 5d0
    t2           = 25d0
    tf           = 70d0
    t0norm       = 0d0
    tfnorm       = 1d0
    t1norm       = (t1-t0)/(tf-t0)
    t2norm       = (t2-t0)/(tf-t0)
    ti           = (/  t0norm,  t1norm, t2norm, tfnorm /)
    z0           = (/ 0d0, 0d0, 214.839d0, 5d0, 100d0, 500d0 /)
    ! par(1:nparmin) =  tf, alpha, beta, g0, umax, c, t0, h0, v0, m0, mf
    par(1:nparmin+3) = (/ tf, 0.01227d0, 0.000145d0, 9.81d0, 9.52551d0, 2060d0, t0, &
                          z0(1), z0(2), z0(3), 67.9833d0, 1d0, 2d0, 0d0 /)
    call exphvfun(ti, z0, ti, options, par, tout, z, flag, nfev) !Get value at t1

    !Initial guess
    !yGuess = [p0 t1 z1 t2 z2]
    yGuess(1:17) = (/ z(4:6,1), t1, z(:,2), t2, z(:,3) /)
    deallocate (tout,z)

    !First shooting in the Bang-Singular-Bang case
    call ssolve(yGuess(1:17), options, par, y0(1:17), ssol(1:17), nfev, njev, flag)

    !First homotopy with Bang-Singular-Bang structure
    indtf                  = 1
    parspan(1:nparmin+3,1) = par
    parspan(1:nparmin+3,2) = par
    parspan(indtf,2)       = 0d0
    call hampath(parspan,y0(1:17),options,parout,yout,sout,viout,dets,normS,ps,flag)

    if(flag.eq.-11)then

        ! Shooting to get the exact value of tf at the change of structure
        ! The limit structure is here BB with one contact of order 2 with the switching manifold
        nt               = size(sout)
        par(1:nparmin+2) = (/ parout(1:nparmin,nt), 1d0, 0d0 /)
        yGuess(1:10)     = yout(1:10,nt)
        yGuess(11)       = par(indtf)
        call ssolve(yGuess(1:11), options, par(1:nparmin+2), y0(1:11), ssol(1:11), nfev, njev, flag)
        t1BSB = y0(4)
        tfBSB = y0(11)

        ! Second homotopy with Bang-Bang structure
        par(indtf)              = tfBSB ! Value of tf when the structure changes
        parspan(1:nparmin+2,1)  = par(1:nparmin+2);
        parspan(1:nparmin+2,2)  = par(1:nparmin+2);
        parspan(indtf,2)        = 0d0
        call hampath(parspan(1:nparmin+2,:),y0(1:10),options,parout,yout,sout,viout,dets,normS,ps,flag)

        if(flag.eq.-12)then
            ! Shooting to get the exact value of tf at the change of structure
            ! The limit structure is B with one contact with the switching manifold at tf
            nt               = size(sout)
            par(1:nparmin+1) = (/ parout(1:nparmin,nt), 1d0/);
            yGuess(1:3)      = yout(1:3,nt)
            yGuess(4)        = par(indtf)
            call ssolve(yGuess(1:4), options, par(1:nparmin+1), y0(1:4), ssol(1:4), nfev, njev, flag)
            tfBB = y0(4);

            print*,"Results:"
            print*,"The structure is BSB from tf = ", tf, " to tf = ", tfBSB
            print*,"The structure is BB  from tf = ", tfBSB," to tf = ", tfBB
            print*,""

        end if

    end if

    deallocate (parout,yout,sout,viout,dets,normS,ps)

end program main
