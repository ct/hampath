compilo='gfortran'
wrapperpython='f2py'

hampath_home=${PREFIX}
lib_dir=${PREFIX}/hampath_lib
bin_dir=${PREFIX}/bin

make -f ${SRC_DIR}/install/makefile_install --silent   hampath_home=${SRC_DIR} \
                                                            lib_dir=${lib_dir}/     \
                                                            compilo=${compilo}      \
                                                            installCMD

mkdir -p ${bin_dir}

binfile=${bin_dir}/hampath
echo "#!/bin/sh"                            >   ${binfile}
echo "hampath_home="${hampath_home}         >>  ${binfile}
echo "lib_dir="${lib_dir}                   >>  ${binfile}
echo "compilo="${compilo}                   >>  ${binfile}
echo "wrapperpython="${wrapperpython}       >>  ${binfile}
# To improve :
cp -r ${SRC_DIR}/src ${hampath_home}
cp -r ${SRC_DIR}/install ${hampath_home}
cat ${SRC_DIR}/install/hampath.sh           >>  ${binfile}
chmod 755 ${binfile}
