# hampath command name
hampath_cmd_name=`basename $0`

# choice of the interface
if [ $# -eq 0 ]; then
    interface=python
fi

for i in $* ; do
    case "$i" in
    -octave)  interface=octave;;
    -python)  interface=python;;
    *)  echo "${hampath_cmd_name}: Invalid command!" 
        exit;;
    esac
done

# directory from where hampath is called
current_dir=`pwd`

# files for hampath compilation
hfun=./hfun.f90
sfun=./sfun.f90
pfun=./pfun.f90
mfun=./mfun.f90
afun=./afun.f90
hvfun=./hvfun.f90
h1fun=./h1fun.f90
h0fun=./h0fun.f90

# print hampath execution
tirets="------------------------------------------------------------------------------"
echo ${tirets}
echo " ${hampath_cmd_name} execution..."
echo ${tirets}

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
# detection of the problem definition
echo " Detection of the problem definition:"
testcase=0
testparf=true
testmfun=true
testafun=true
testh0h1fun=true
if [ ! -f $hfun ]; then
    if [ ! -f $sfun ]; then
        if [ ! -f $hvfun ]; then
            echo "    Error: hfun.f90 and sfun.f90 files not found. ${hampath_cmd_name} will exit!"
            exit
        fi
    fi
    if [ ! -f $sfun ]; then
        testcase=9010 #9 + hfun 0, hvfun 1, sfun 0
    else
        if [ ! -f $hvfun ]; then
            testcase=9001 #9 + hfun 0, hvfun 0, sfun 1
        else
            testcase=9011 #9 + hfun 0, hvfun 1, sfun 1
        fi
    fi
    echo "    Remark: hfun.f90 file not found. ${hampath_cmd_name} will take it into account."
else
    if [ ! -f $hvfun ]; then
        if [ ! -f $sfun ]; then
            testcase=9100 #9 + hfun 1, hvfun 0, sfun 0
            echo "    Remark: sfun.f90 file not found. ${hampath_cmd_name} will take it into account."
        else
            testcase=9101 # CAS CLASSIQUE # 9 + hfun 1, hvfun 0, sfun 1
        fi
    else
        echo "It is not allowed to have hfun.f90 and hvfun.f90 files in the current directory!"
    fi
fi
if [ ! -f $pfun ]; then
    testparf=false     #On copiera le pfun.f90 par defaut
    echo "    Remark: pfun.f90 file not found. ${hampath_cmd_name} will use default pfun."
fi
if [ ! -f $mfun ]; then
    testmfun=false     #On copiera le mfun.f90 par defaut
    echo "    Remark: optional mfun.f90 file not found."
fi
if [ ! -f $afun ]; then
    testafun=false     #On copiera le afun.f90 par defaut
    echo "    Remark: optional afun.f90 file not found."
fi

if [ -f $h0fun ]; then
    if [ ! -f $h1fun ]; then
        testafun=false     #On copiera le afun.f90 par defaut
        echo "    If you have h0fun.f90 then you need to write h1fun.f90."
        exit
    fi
fi
if [ -f $h1fun ]; then
    if [ ! -f $h0fun ]; then
        testafun=false     #On copiera le afun.f90 par defaut
        echo "    If you have h1fun.f90 then you need to write h0fun.f90."
        exit
    fi
fi
if [ ! -f $h1fun ]; then
    testh0h1fun=false     #On copiera les h0fun.f90 et h1fun.f90 par defaut
    echo "    Remark: optional h0fun.f90 and h1fun.90 file not found."
fi

echo ""
echo ${tirets}
echo ""

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
# copy of the needed files into a temporary directory

# tmp directory
tmp=`mktemp -d tmphampathXXXX` || exit 1

echo " Temporary directory "$tmp" created for main processing..."
echo " Copy of all needed files in temporary directory"

# copy of the lib and interfaces files
hampath_src=${hampath_home}/src/hampath
#
cp ${lib_dir}/*                        $tmp    &> /dev/null

if [ ${interface} = "octave" ]; then
    cp ${hampath_src}/interfaces/matlab/*           $tmp    &> /dev/null
    cp ${hampath_src}/interfaces/commonMatlab/*     $tmp    &> /dev/null
    cp ${hampath_src}/interfaces/mod_redirect.f90   $tmp    &> /dev/null
else
    if [ ${interface} = "python" ]; then
        cp    ${hampath_src}/interfaces/python/*.*          $tmp    &> /dev/null
        cp -r ${hampath_src}/interfaces/python/resources    $tmp    &> /dev/null
    else
        echo "error of the code"
    fi
fi

# copy of pb definition files
if [ $testcase = "9101" ]; then
    cp $hfun $tmp/hfun.f90
    cp ${hampath_src}/core/hvfun/hvfun.f90 $tmp/hvfun.f90
    cp $sfun $tmp/sfun.f90
fi
if [ $testcase = "9100" ]; then
    cp $hfun $tmp/hfun.f90
    cp ${hampath_src}/core/hvfun/hvfun.f90 $tmp/hvfun.f90
    cp ${hampath_src}/utils/problemDef/sfun.f90 $tmp/sfun.f90
fi
if [ $testcase = "9001" ]; then
    cp ${hampath_src}/utils/problemDef/hfun.f90 $tmp/hfun.f90
    cp ${hampath_src}/core/hvfun/hvfun.f90 $tmp/hvfun.f90
    cp $sfun $tmp/sfun.f90
fi
if [ $testcase = "9011" ]; then
    cp ${hampath_src}/utils/problemDef/hfun.f90 $tmp/hfun.f90
    cp $hvfun $tmp/hvfun.f90
    cp $sfun $tmp/sfun.f90
fi
if [ $testcase = "9010" ]; then
    cp ${hampath_src}/utils/problemDef/hfun.f90 $tmp/hfun.f90
    cp $hvfun $tmp/hvfun.f90
    cp ${hampath_src}/utils/problemDef/sfun.f90 $tmp/sfun.f90
fi
if [ $testparf = "true" ]; then
    cp $pfun $tmp/pfun.f90
else
    cp ${hampath_src}/utils/problemDef/pfun.f90 $tmp/pfun.f90
fi
if [ $testmfun = "true" ]; then
    cp $mfun $tmp/mfun.f90
else
    cp ${hampath_src}/utils/problemDef/mfun.f90 $tmp/mfun.f90
fi
if [ $testafun = "true" ]; then
    cp $afun $tmp/afun.f90
else
    cp ${hampath_src}/utils/problemDef/afun.f90 $tmp/afun.f90
fi
if [ $testh0h1fun = "true" ]; then
    cp $h0fun $tmp/h0fun.f90
    cp $h1fun $tmp/h1fun.f90
else
    cp ${hampath_src}/utils/problemDef/h0fun.f90 $tmp/h0fun.f90
    cp ${hampath_src}/utils/problemDef/h1fun.f90 $tmp/h1fun.f90
fi
cp ${hampath_src}/core/singularcontrol/h01fun.f90 $tmp/h01fun.f90
cp ${hampath_src}/core/singularcontrol/phi1fun.f90 $tmp/phi1fun.f90
cp ${hampath_src}/core/singularcontrol/singularcontrol.f90 $tmp/singularcontrol.f90

if [ ${interface} = "octave" ]; then
    # liste des fichier fortran supplementaire a compiler
    nomlistfunfile=listfun.txt
    listfunObjet=mod_redirect.o
    nommainProg=mainProg
    echo $listfunObjet > $tmp/$nomlistfunfile
fi

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------
# main process
echo " Execution of the main process:"
echo ""

# creation of the libhampath directory containing compiled files
libdirrel=lib${hampath_cmd_name}
libdir=${current_dir}/${libdirrel}
if [ ! -d ${libdir} ]; then
    mkdir ${libdir}
fi

# we enter into the tmp directory to call tapenade and compile
cd $tmp

if [ ${interface} = "octave" ]; then
    # adding automatically matlab function with same signature as hfun
    sh dofindroutine.sh ${testafun} ${libdir}
    echo ""
else
    if [ ${interface} = "python" ]; then
        #On cree le fichier hampathcode.py
        sh doHampathCode.sh ${testcase}
    else
        echo "error of the code"
    fi
fi

# automatic differentiation
if make -f $hampath_home/install/makefile_diffauto --silent hampath_home=${hampath_home}                \
                                                            testcase=$testcase                          \
                                                            diffAutoCmd
then
    if [ ${interface} = "octave" ]; then
        # compilation of problem files
        if make -f $hampath_home/install/makefile_pb   --silent hampath_home=${hampath_home}                \
                                                                nomlistfunfile=$nomlistfunfile              \
                                                                nommainProg=$nommainProg                    \
                                                                compilo=${compilo}                          \
                                                                problemCMD
        then
            cp ${nommainProg} ${libdir}
            cp  *.m ${libdir}
            echo ""
            echo " All is complete!"
            echo ""
            echo " IMPORTANT:"
            echo ""
            echo "  Add this line to your script: addpath('${libdirmatrel}/');"
            echo ""
        else
            echo ""
            echo " Error during compilation!"
            echo ""
        fi
    else
        if [ ${interface} = "python" ]; then
            if make -f $hampath_home/install/makefile_pb_python --silent hampath_home=${hampath_home}       \
                                                                         compilo=${compilo}                 \
                                                                         testcase=${testcase}               \
                                                                         wrapperpython=${wrapperpython}     \
                                                                         problemCMD
            then
                sh dofindroutine.sh ${testafun} ${libdir} ${hampath_home} ${wrapperpython} ${compilo} ${libdirrel}
                cp *.py         ${libdir}
                cp *.so         ${libdir}
                cp -fr *.so.*   ${libdir}
                touch ${libdir}/__init__.py
                echo ""
                echo " All is complete!"
                echo ""
                echo " IMPORTANT:"
                echo ""
                echo " Write "
                echo ""
                echo "   >>> from ${libdirrel}.hampathcode import *"
                echo " or "
                echo "   >>> import ${libdirrel}.hampathcode as hampath"
                echo ""
                echo " to use HamPath code!"
                echo ""
            else
                echo ""
                echo " Error during compilation!"
                echo ""
            fi
        else
            echo "error of the code"
        fi
    fi
else
    echo ""
    echo " Error during Automatic differentiation!"
    echo ""
fi
echo ${tirets}
echo ""

# go back to current dir
cd ${current_dir}

# remove tmp dir
rm -rf $tmp

#
echo " Temporary directory "$tmp" removed."
echo ""
echo ${tirets}
echo ""


