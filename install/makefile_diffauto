# Automatic differentiation

# need hampath_home, testcase

# export_tapenade_home
tapenade_home=${hampath_home}/src/tapenade3.8

# TAP
TAP=${tapenade_home}/bin/tapenade

# funcnameopt
funcnameopt=tgtfuncname

# usehead
usehead=true

# OPTT
OPTT  = -tangent -fixinterface -inputlanguage fortran90 -outputlanguage fortran90 -nooptim activity #do not optimize detecting derivatives that are always zero

# export_tapenade_home
ifeq (${tapenade_home}, )
export_tapenade_home =
else
export_tapenade_home = TAPENADE_HOME=${tapenade_home}
endif

# diff auto cmd
ifdef testcase
ifeq (${testcase}, 9101) # CAS CLASSIQUE # 9 + hfun 1, hvfun 0, sfun 1
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d hvfun_d_no_echo hvfun_d_d_no_echo sfun_d pfun_d_no_echo
endif
ifeq (${testcase}, 9100) #9 + hfun 1, hvfun 0, sfun 0
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d hvfun_d_no_echo hvfun_d_d_no_echo sfun_d_no_echo pfun_d_no_echo
endif
ifeq (${testcase}, 9001) #9 + hfun 0, hvfun 0, sfun 1
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d_no_echo hvfun_d_no_echo hvfun_d_d_no_echo sfun_d pfun_d_no_echo
endif
ifeq (${testcase}, 9011) #9 + hfun 0, hvfun 1, sfun 1
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d_no_echo hvfun_d hvfun_d_d_no_echo sfun_d pfun_d_no_echo
endif
ifeq (${testcase}, 9010) #9 + hfun 0, hvfun 1, sfun 0
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d_no_echo hvfun_d hvfun_d_d_no_echo sfun_d_no_echo pfun_d_no_echo
endif
else
diffAutoDef  =  h0fun_d_no_echo h1fun_d_no_echo h01fun_d_no_echo phi1fun_d_no_echo hfun_d hvfun_d hvfun_d_d sfun_d pfun_d_no_echo
endif
diffAutoCmd: ${diffAutoDef}

#--------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------------------#
#
# ATTENTION : bien que je demande par rapport a quelle variables deriver, tapenade derive /r a toutes les variables
# a cause de l'option -nooptim activity
#
# Le passage de paramètre dépend de la version de tapenade
# usehead=true pour les versions après 3.8, version 3.8 incluse.
ifeq (${usehead}, true)
headcmd=-head "$(1)($(2))>($(3))"
else
headcmd=-root $(1) -vars "$(2)" -outvars "$(3)"
endif

h0fun_d_head=$(call headcmd,h0fun,t x p,h0)
h1fun_d_head=$(call headcmd,h1fun,t x p,h1)
h01fun_d_head=$(call headcmd,h01fun,t x p,h01)
phi1fun_d_head=$(call headcmd,phi1fun,t x p,phi1)

hfun_d_head=$(call headcmd,hfun,t z par,h)
hvfun_d_head=$(call headcmd,hvfun,t z par,hv)
hvfun_d_d_head=$(call headcmd,HVFUN_DH,t z par,hvd)

pfun_d_head=$(call headcmd,pfun,lambda,par)

sfun_d_head=$(call headcmd,sfun,y par,s)

#hfunHEAD=-head "hfun(t z par)>(H)"

nettoyageSFUN=| sed "/EXTERNAL PRINTANDSTOP_DS/d" | sed "s/PRINTANDSTOP_DS/PRINTANDSTOP/g" | sed "/MOD_EXPHV4SFUN/d" | sed "/MOD_EXPHV4SFUN_D/d" | sed "/MOD_EXPHV4SFUN_DS/d" | sed "s/IMPLICIT NONE/ use mod_exphv4sfun; IMPLICIT NONE;/g" | sed "/EXTERNAL EXPHV_DS/d" | sed "/EXTERNAL EXPHV/d" | sed "/EXTERNAL EXPDHV_DS/d" | sed "/EXTERNAL EXPDHV/d" | sed "/EXTERNAL DET_DS/d" | sed "/EXTERNAL DET/d"
nettoyageHFUN=| sed "/EXTERNAL PRINTANDSTOP_DH/d" | sed "s/PRINTANDSTOP_DH/PRINTANDSTOP/g" | sed "/EXTERNAL PRINTANDSTOP_DH0/d" | sed "s/PRINTANDSTOP_DH0/PRINTANDSTOP/g"
nettoyagePFUN=| sed "/EXTERNAL PRINTANDSTOP_DP/d" | sed "s/PRINTANDSTOP_DP/PRINTANDSTOP/g"

hfun_d:
	@echo "          Automatic differentiation on hfun"
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hfun_d_head} -o hfun hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hfun_dh.f90 ${nettoyageHFUN}  > hfun_d_new.f90
	mv hfun_d_new.f90 hfun_dh.f90
	cat hfun_dh.msg | sed "/printandstop/d" | sed "/Equality test on reals is not reliable/d"

hvfun_d:
	@echo "          Automatic differentiation on hvfun"
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hvfun_d_head} -o hvfun hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hvfun_dh.f90 ${nettoyageHFUN} > hvfun_d_new.f90
	mv hvfun_d_new.f90 hvfun_dh.f90
	cat hvfun_dh.msg | sed "/printandstop/d" | sed "/Equality test on reals is not reliable/d"

hvfun_d_d:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hvfun_d_d_head} -o hvfun_dh hvfun_dh.f90 hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hvfun_dh_dh.f90 ${nettoyageHFUN}  > hvfun_d_d_new.f90
	mv hvfun_d_d_new.f90 hvfun_dh_dh.f90
	cat hvfun_dh_dh.msg | sed "/printandstop/d" | sed "/Equality test on reals is not reliable/d"

sfun_d:
	@echo "          Automatic differentiation on sfun"
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _ds ${sfun_d_head} -o sfun sfun.f90 hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat sfun_ds.f90 ${nettoyageSFUN}  > sfun_d_new.f90
	mv sfun_d_new.f90 sfun_ds.f90
	cat sfun_ds.msg | sed "/printandstop/d" | sed "/mod_exphv4sfun/d" | sed "/exphv/d" | sed "/expdhv/d" | sed "/det/d" | sed "/Equality test on reals is not reliable/d"

pfun_d:
	@echo "          Automatic differentiation on pfun"
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dp ${pfun_d_head} -o pfun pfun.f90 afun.f90`
	cat pfun_dp.f90 ${nettoyagePFUN}  > pfun_d_new.f90
	mv pfun_d_new.f90 pfun_dp.f90

#  -------
#  No echo
#  -------

phi1fun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dphi ${phi1fun_d_head} -o phi1fun phi1fun.f90 h1fun_dc1.f90 h1fun.f90 afun.f90`
	cat phi1fun_dphi.f90 ${nettoyageHFUN}  > phi1fun_d_new.f90
	mv phi1fun_d_new.f90 phi1fun_dphi.f90

h01fun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _d01 ${h01fun_d_head} -o h01fun h01fun.f90 h1fun_dc1.f90 h0fun_dc0.f90 h1fun.f90 h0fun.f90 afun.f90`
	cat h01fun_d01.f90 ${nettoyageHFUN}  > h01fun_d_new.f90
	mv h01fun_d_new.f90 h01fun_d01.f90

h0fun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dc0 ${h0fun_d_head} -o h0fun h0fun.f90 afun.f90`
	cat h0fun_dc0.f90 ${nettoyageHFUN}  > h0fun_d_new.f90
	mv h0fun_d_new.f90 h0fun_dc0.f90

h1fun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dc1 ${h1fun_d_head} -o h1fun h1fun.f90 afun.f90`
	cat h1fun_dc1.f90 ${nettoyageHFUN}  > h1fun_d_new.f90
	mv h1fun_d_new.f90 h1fun_dc1.f90

#
hfun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hfun_d_head} -o hfun hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hfun_dh.f90 ${nettoyageHFUN}  > hfun_d_new.f90
	mv hfun_d_new.f90 hfun_dh.f90

hvfun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hvfun_d_head} -o hvfun hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hvfun_dh.f90 ${nettoyageHFUN}  > hvfun_d_new.f90
	mv hvfun_d_new.f90 hvfun_dh.f90

hvfun_d_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dh ${hvfun_d_d_head} -o hvfun_dh hvfun_dh.f90 hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat hvfun_dh_dh.f90 ${nettoyageHFUN}  > hvfun_d_d_new.f90
	mv hvfun_d_d_new.f90 hvfun_dh_dh.f90

sfun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _ds ${sfun_d_head} -o sfun sfun.f90 hvfun.f90 hfun_dh.f90 hfun.f90 afun.f90 singularcontrol.f90 phi1fun_dphi.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 h1fun.f90 h0fun.f90`
	cat sfun_ds.f90 ${nettoyageSFUN}  > sfun_d_new.f90
	mv sfun_d_new.f90 sfun_ds.f90

pfun_d_no_echo:
	dummyvar=`${export_tapenade_home} $(TAP) $(OPTT) -${funcnameopt} _dp ${pfun_d_head} -o pfun pfun.f90 afun.f90`
	cat pfun_dp.f90 ${nettoyagePFUN}  > pfun_d_new.f90
	mv pfun_d_new.f90 pfun_dp.f90

#--------------------------------------------------------------------------------------------------------------#
#--------------------------------------------------------------------------------------------------------------#
# Written on 2021
# by Oivier Cots - ENSEEIHT-IRIT

