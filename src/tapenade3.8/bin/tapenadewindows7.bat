@echo off
title Tapenade

IF NOT DEFINED TAPENADE_HOME set TAPENADE_HOME=..
IF NOT DEFINED JAVA_HOME set JAVA_HOME="C:\Program Files (x86)\Java\jre6"
IF NOT DEFINED JAVA_BIN set JAVA_BIN="C:\Program Files (x86)\Java\jre6\bin\java.exe"

set HEAP_SIZE=-mx256m
set CLASSPATH="..\jars\tapenade.jar"
set BROWSER="C:\Program Files\Internet Explorer\iexplore.exe"

%JAVA_BIN% %HEAP_SIZE% -classpath %CLASSPATH% -Dali.java_home=%JAVA_HOME% -Dali.tapenade_home=%TAPENADE_HOME% -Dali.browser=%BROWSER% topLevel.Tapenade %*
