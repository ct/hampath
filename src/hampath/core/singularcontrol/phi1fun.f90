Subroutine phi1fun(t,n,x,p,npar,par,phi1)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision,                  intent(out)  :: phi1

    double precision                                :: td, xd(n), pd(n), h1, h1d, pard(npar)

    ! phi1 = dH1/dt

    td   = 1d0
    xd   = 0d0
    pd   = 0d0
    pard = 0d0
    call H1FUN_DC1(t, td, n, x, xd, p, pd, npar, par, pard, h1, h1d)
    phi1 = h1d

end Subroutine phi1fun
