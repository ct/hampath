!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module utils
    use defs
    implicit none
    contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Kronecker product C = A x B.
!!       \param[in]  A           dimension(ma,na)
!!       \param[in]  B           dimension(mb,nb)
!!       \param[out] C           dimension(ma*mb,na*nb)
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
subroutine kronecker(ma,na,A,mb,nb,B,C)
    implicit none
    integer, intent(in)             :: ma, na, mb, nb
    double precision, intent(in)    :: A(ma,na), B(mb,nb)
    double precision, intent(out)   :: C(ma*mb,na*nb)

    !local variables
    integer :: i, j

    do j=1,na
        do i=1,ma

            C((i-1)*mb+1:(i-1)*mb+mb,(j-1)*nb+1:(j-1)*nb+nb) = A(i,j) * B

        end do
    end do


end subroutine kronecker

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Print a matrix to the terminal.
!!       \param[in] nom         name of the matrix to print
!!       \param[in] A           matrix to print
!!       \param[in] m           number of rows of A
!!       \param[in] n           number of columns of A
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine printMatrice(nom,A,m,n)
        implicit none
        CHARACTER(len=*),   intent(in)  :: nom
        integer,            intent(in)  :: m, n
        double precision,   intent(in)  :: A(m,n)

        integer :: i, j
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') trim(nom)    ; call myprint(LINE,.false.)
        WRITE (LINE,'(a)') ' = ['       ; call myprint(LINE,.false.)
        DO i = 1, m

            DO j=1,n
                if(j.eq.1)then
                    if(i.eq.1)then
                        WRITE (LINE,'(e23.15)') A(i,j)
                    else
                        WRITE (LINE,'("          ",e23.15)') A(i,j)
                    end if
                    call myprint(LINE,.false.)
                elseif(j.eq.n)then
                    if(i.eq.m)then
                        WRITE (LINE,'(e23.15)') A(i,j)
                        call myprint(LINE,.false.)
                    else
                        WRITE (LINE,'(e23.15)') A(i,j)
                        call myprint(LINE,.true.)
                    end if
                else
                    WRITE (LINE,'(e23.15)') A(i,j)
                    call myprint(LINE,.false.)
                end if
            END DO

        END DO
        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)

    end subroutine printMatrice

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Print a vector to the terminal.
!!       \param[in] nom         name of the vector to print
!!       \param[in] v           vector to print
!!       \param[in] n           number of elements of v
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine printVector(nom,v,n)
        implicit none
        CHARACTER(len=*),   intent(in)  :: nom
        integer,            intent(in)  :: n
        double precision,   intent(in)  :: v(n)

        integer :: i
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') trim(nom)    ; call myprint(LINE,.false.)
        WRITE (LINE,'(a)') ' = ['       ; call myprint(LINE,.false.)

        DO i=1,n
            if(i.eq.1)then
                WRITE (LINE,'(e23.15)') v(i)
            else
                WRITE (LINE,'("          ",e23.15)') v(i)
            end if
            call myprint(LINE,.false.)
        END DO

        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)

    end subroutine printVector

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write an integer in a file, well formatted for matlab.
!!       \param[in] inte          data to print
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to inte
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeIntegerMatlab(inte,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: inte,unitfileout
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,*) trim(namevec),' = '
        WRITE (unit=unitfileout,fmt='(a)'   ,advance='no') trim(LINE)
        WRITE (unit=unitfileout,fmt='(i0.1)',advance='no') inte
        WRITE (unit=unitfileout,fmt='(a)'   ,advance='yes') ';'

    end subroutine writeIntegerMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a row vector in a file, well formatted for matlab.
!!       \param[in] vec           datas to print
!!       \param[in] n             number of elements of vec
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to vec
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeRowVectorDbleMatlab(vec,n,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: n,unitfileout
        double precision, dimension(n), intent(in) :: vec
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i

        WRITE (LINE,*) trim(namevec),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,n
            WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeRowVectorDbleMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a column vector in a file, well formatted for matlab.
!!       \param[in] vec           datas to print
!!       \param[in] n             number of elements of vec
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to vec
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeColVectorDbleMatlab(vec,n,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: n,unitfileout
        double precision, dimension(n), intent(in) :: vec
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i

        WRITE (LINE,*) trim(namevec),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,n
            if(i.eq.n)THEN
                WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
            ELSE
                WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
                WRITE (unit=unitfileout,fmt='(a)',advance='yes') ';'
            end if
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeColVectorDbleMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a matrix in a file, well formatted for matlab.
!!       \param[in] mat           datas to print
!!       \param[in] m             number of rows of mat
!!       \param[in] n             number of column of mat
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namemat       the name of the variable corresponding to mat
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeMatrixDbleMatlab(mat,m,n,namemat,unitfileout)
        implicit none
        integer         , intent(in) :: m,n,unitfileout
        double precision, dimension(m,n), intent(in) :: mat
        character(len=*), intent(in) :: namemat

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i,j

        WRITE (LINE,*) trim(namemat),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,m
            do j=1,n
                if(j.eq.n)THEN
                    if(i.eq.m)THEN
                        WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                    ELSE
                        WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                        WRITE (unit=unitfileout,fmt='(a)',advance='yes') ';'
                    end IF
                ELSE
                    WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                end if
            end do
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeMatrixDbleMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief the index of current arc
!!     \param[in] t      time
!!     \param[in] nbarc  Number of arcs
!!     \param[in] ti     ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!
!!     \param[out] arc   index of arc corresponding to time t
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine getArc(t,nbarc,ti,arc)
        implicit none
        integer, intent(in)                                 :: nbarc
        double precision, intent(in)                        :: t
        double precision, intent(in),  dimension(nbarc+1)   :: ti
        integer, intent(out)                                :: arc

        !local variables
        logical :: fini

        fini = .false.
        arc  = 1
        do while(.not.fini .and. arc.lt.nbarc+1)
            if((ti(arc+1)-t)*(t-ti(arc)).ge.0d0)then
                !if(t.eq.ti(arc+1).and.arc.lt.nbarc)then
                !    arc = arc + 1
                !end if
                fini = .true.
            else
                arc = arc + 1
            end if
        end do

    end subroutine getArc

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief Computes deb, fin and inc for exphvfun and expdhvfun
!!
!!     \param[in] t0     Initial time
!!     \param[in] tf     Final time
!!     \param[in] nbarc  Number of arcs
!!     \param[in] ti     ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!
!!     \param[out] deb    first arc
!!     \param[out] fin    final arc
!!     \param[out] inc    incremental direction
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine getArea(t0,tf,nbarc,ti,deb,fin,inc)
        implicit none
        double precision,   intent(in)                      :: t0,tf
        integer,            intent(in)                      :: nbarc
        double precision,   intent(in), dimension(nbarc+1)  :: ti
        integer,            intent(out)                     :: deb,fin,inc

        !local variables
        integer :: j
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        call getArc(t0,nbarc,ti,deb) !get the index of first arc
        call getArc(tf,nbarc,ti,fin) !get the index of last arc

        if(tf-t0.ge.0d0)then !if tf is greater than t0 then inc is positive
            inc = 1
            !we check if t0 is equal to ti(deb+1)
            !if it is the case, then the initial arc is one after
            if(deb.le.nbarc)then
                if(abs(t0-ti(deb+1)) .le. eps)then
                    deb = deb + 1
                    if(abs(tf-t0) .le. eps)then
                        fin = fin + 1
                    end if
                end if
            end if
        else                !else inc is negative
            inc = -1
            j = fin
            !we check if tf is equal to ti(fin)
            !if it is the case, then the final arc is one before
            if(fin.le.nbarc .and. nbarc.gt.1)then   ! Dans le cas particulier où l'on a un arc, on ne fait rien
                                                    ! car le vecteur ti n'est pas dans l'ordre croissant
                                                    ! alors que sinon il est croissant
                if(abs(tf-ti(fin+1)) .le. eps)then
                    fin = fin + 1
                end if
            end if
        end if

    end subroutine getArea

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief the tspan for exphvfun and expdhvfun
!!     \param[in] t0     Initial time
!!     \param[in] tf     Final time
!!     \param[in] nbarc      Number of arcs
!!     \param[in] ti     ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!     \param[in] deb    first arc
!!     \param[in] fin    final arc
!!     \param[in] inc    incrementation direction
!!
!!     \param[out] tspan the integration grid which contains only values of ti
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine getTspan(t0,tf,nbarc,ti,deb,fin,inc,tspan)
        implicit none
        double precision, intent(in)                                :: t0,tf
        integer,          intent(in)                                :: deb,fin,inc,nbarc
        double precision, intent(in) , dimension(nbarc+1)           :: ti
        double precision, intent(out), dimension(inc*(fin-deb)+2)   :: tspan

        !local variables
        integer :: i,j

        !we add to tspan the values of ti contained in [t0,tf]
        tspan(1) = t0
        j = 2
        do i=deb+(inc+1)/2,fin+(-inc+1)/2,inc
            tspan(j)   = ti(i)
            j = j + 1
        end do
        tspan(inc*(fin-deb)+2) = tf

    end subroutine getTspan

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief make parameters vector wrt lambda
!!       \param[in] lpar   Parameters vector dimension
!!       \param[in] lambda Homotopic parameter
!!
!!       \param[out] par   Optional parameters at lambda
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine getPar(lambda,lpar,par)
      implicit none
      integer,          intent(in)                    :: lpar
      double precision, intent(in)                    :: lambda
      double precision, intent(out), dimension(lpar)  :: par

      if(nparhomG.eq.1)then !the homotopic parameter is equals to lambda if there is only one
          par = par0G
          par(iparhomG(1)) = lambda
      else !else it is a relation between par0G and parfG computed by pfun
          call pfun(lambda,lpar,par0G,parfG,par)
      end if

    end subroutine getPar

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief make the vector c given to hampath
!!       \param[in] ny     Shooting variable dimension
!!       \param[in] y      Shooting variable
!!       \param[in] lambda Current homotopic parameter
!!
!!       \param[out] c    Vector of zeros path for hampath: c = (y, lambda)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    subroutine doc(ny,y,lambda,c)
      implicit none
      integer,          intent(in)                   :: ny
      double precision, intent(in),  dimension(ny)   :: y
      double precision, intent(in)                   :: lambda
      double precision, intent(out), dimension(ny+1) :: c

      c(1:ny) = y
      c(ny+1) = lambda

    end subroutine doc

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief unmake the unknown c given to hampath
!!       \param[in]  ny     Shooting variable dimension
!!       \param[out] y      Shooting variable
!!       \param[out] lambda Current homotopic parameter
!!       \param[in]  c    Vector of zeros path for hampath: c = (y, lambda)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    subroutine undoc(ny,y,lambda,c)
      implicit none
      integer,          intent(in)                   :: ny
      double precision, intent(out), dimension(ny)   :: y
      double precision, intent(out)                  :: lambda
      double precision, intent(in),  dimension(ny+1) :: c

      y      = c(1:ny)
      lambda = c(ny+1)

    end subroutine undoc

end module utils
