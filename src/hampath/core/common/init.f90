!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
   module init
        use defs
        use utils
        implicit none
        contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize all the parameters
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
    subroutine initOptions(ndw,niw,nsw,dw,iw,sw,lsw,idFcn)
        implicit none
        integer,            intent(in) :: ndw, niw, nsw, idFcn
        double precision,   intent(in) :: dw(ndw)
        integer,            intent(in) :: iw(niw), lsw(nsw)
        character(32*nsw),  intent(in) :: sw

        !local variables
        integer            :: dispIter
        integer            :: MaxStepsOde
        integer            :: MaxStepsOdeHam
        integer            :: StopAtTurningPoint
        integer            :: MaxFEval
        integer            :: MaxIterCorrection

        double precision   :: MaxSf
        double precision   :: MaxSfunNorm
        double precision   :: MaxStepSizeOde
        double precision   :: MaxStepSizeOdeHam
        double precision   :: MaxStepSizeHomPar
        double precision   :: TolOdeAbs
        double precision   :: TolOdeRel
        double precision   :: TolOdeHamAbs
        double precision   :: TolOdeHamRel
        double precision   :: TolDenseHamEnd
        double precision   :: TolDenseHamX
        double precision   :: TolX

        integer            :: dimDerivative
        integer            :: dimDisplay
        integer            :: dimDoSavePath
        integer            :: dimODE
        integer            :: dimODEHam
        integer            :: dimIrkInit
        integer            :: dimIrkSolver
        integer            :: dimSolverMethod

        character(32)      :: Derivative
        character(32)      :: Display
        character(32)      :: DoSavePath
        character(32)      :: ODE
        character(32)      :: ODEHam
        character(32)      :: IrkInit
        character(32)      :: IrkSolver
        character(32)      :: SolverMethod

        integer :: i

        !Integer
        i = 1
        dispIter               = iw(i); i = i + 1;
        MaxFEval               = iw(i); i = i + 1;
        MaxIterCorrection      = iw(i); i = i + 1;
        MaxStepsOde            = iw(i); i = i + 1;
        MaxStepsOdeHam         = iw(i); i = i + 1;
        StopAtTurningPoint     = iw(i); i = i + 1;

        !Float
        i = 1
        MaxSf                  = dw(i); i = i + 1;
        MaxSfunNorm            = dw(i); i = i + 1;
        MaxStepSizeOde         = dw(i); i = i + 1;
        MaxStepSizeOdeHam      = dw(i); i = i + 1;
        MaxStepSizeHomPar      = dw(i); i = i + 1;
        TolOdeAbs              = dw(i); i = i + 1;
        TolOdeRel              = dw(i); i = i + 1;
        TolOdeHamAbs           = dw(i); i = i + 1;
        TolOdeHamRel           = dw(i); i = i + 1;
        TolDenseHamEnd         = dw(i); i = i + 1;
        TolDenseHamX           = dw(i); i = i + 1;
        TolX                   = dw(i); i = i + 1;

        !String
        i = 1
        dimDerivative          = lsw(i); i = i + 1;
        dimDisplay             = lsw(i); i = i + 1;
        dimDoSavePath          = lsw(i); i = i + 1;
        dimIrkInit             = lsw(i); i = i + 1;
        dimIrkSolver           = lsw(i); i = i + 1;
        dimODE                 = lsw(i); i = i + 1;
        dimODEHam              = lsw(i); i = i + 1;
        dimSolverMethod        = lsw(i); i = i + 1;

        i = 1
        Derivative  = ''
        Display     = ''
        DoSavePath  = ''
        IrkInit     = ''
        IrkSolver   = ''
        ODE         = ''
        ODEHam      = ''
        SolverMethod= ''

        Derivative(1:dimDerivative)     = sw(i:i+dimDerivative  -1); i = i + dimDerivative;
        Display(1:dimDisplay)           = sw(i:i+dimDisplay     -1); i = i + dimDisplay;
        DoSavePath(1:dimDoSavePath)     = sw(i:i+dimDoSavePath  -1); i = i + dimDoSavePath;
        IrkInit(1:dimIrkInit)           = sw(i:i+dimIrkInit     -1); i = i + dimIrkInit;
        IrkSolver(1:dimIrkSolver)       = sw(i:i+dimIrkSolver   -1); i = i + dimIrkSolver;
        ODE(1:dimODE)                   = sw(i:i+dimODE         -1); i = i + dimODE;
        ODEHam(1:dimODEHam)             = sw(i:i+dimODEHam      -1); i = i + dimODEHam;
        SolverMethod(1:dimSolverMethod) = sw(i:i+dimSolverMethod-1); i = i + dimSolverMethod;

        isFirstDisplayG = .true.

        !Ce qui est commun à toutes les routines
        call initMaxStepInfos(MaxStepsOde,MaxStepSizeOde)
        call initTol(TolOdeAbs,TolOdeRel)
        call initIRK(IrkInit,IrkSolver)

        !Initialization
        select case (idFcn)

            case (idHamg)

                call initDisplay(dispIter,Display)
                call initMaxStepsHam(MaxStepsOdeHam)
                call initStopAtTurningPt(StopAtTurningPoint)
                call initMaxArcLength(MaxSf)
                call initNormeSfunMax(MaxSfunNorm)
                call initMaxStepSizeHam(MaxStepSizeOdeHam)
                call initMaxStepSizeHomPar(MaxStepSizeHomPar)
                call initTolCont(TolOdeHamAbs,TolOdeHamRel,TolDenseHamEnd,TolDenseHamX)
                call initDerivative(Derivative)
                call initSavePath(DoSavePath)
                call initIntegEqvar(ODE)
                call initIntegHam(ODEHam)
                call initMaxIterCorr(MaxIterCorrection)

                ! ceci est utile pour mfun.f90
                call initIntegExphvfun(ODE)

            case (idExpdg)

                call initIntegExpdhvfun(ODE)
                call initDisplayExpdhv(Display)
                call initDerivative(Derivative)

            case (idExpg)

                call initIntegExphvfun(ODE)
                call initDisplayExphv(Display)

            case (idSFung)

                call initIntegSfun(ODE)
                call initDerivative(Derivative) ! pour expdhv dans l'appel de sfun

            case (idSJacg)

                call initIntegEqvar(ODE)
                call initDerivative(Derivative)

            case (idSsog)

                call initTolSolve(TolX)
                call initIntegEqvar(ODE)
                call initDerivative(Derivative)
                call initDisplaySolve(Display)
                call initSolverMethod(SolverMethod)
                call initMaxFEval(MaxFEval)

            case default

                CALL printandstop('  ||| ERROR: wrong idFcn!')

        end select

    end subroutine initOptions

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::maxitercorrg parameter.
!!     \param[in]    mic      defs::maxitercorrg value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
   subroutine initMaxIterCorr(mic)
      implicit none
      integer, intent(in) :: mic
      maxitercorrg = mic
   end subroutine initMaxIterCorr

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::maxfevg parameter.
!!     \param[in]    mfe      defs::maxfevg value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
   subroutine initMaxFEval(mfe)
      implicit none
      integer, intent(in) :: mfe
      maxfevg = mfe
   end subroutine initMaxFEval

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::rootmethodg parameter.
!!     \param[in]  irkiG    defs::rootmethodg value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
    subroutine initSolverMethod(sm)
      implicit none
      character(32), intent(in) :: sm

      rootmethodg = trim(sm)

   end subroutine initSolverMethod


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::msfG parameter.
!!     \param[in]    msf      defs::msfG value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
   subroutine initMaxArcLength(msf)
      implicit none
      double precision, intent(in) :: msf
      msfG = msf
   end subroutine initMaxArcLength

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::irkiG and defs::irksG parameters.
!!     \param[in]  irkiG    defs::irkiG value
!!     \param[in]  irksG    defs::irksG value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
    subroutine initIRK(irki,irks)
      implicit none
      character(32), intent(in) :: irki,irks

      irkiG = trim(irki)
      irksG = trim(irks)

   end subroutine initIRK

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::displayg and defs::dispiterg parameters.
!!     \param[in]  displ    defs::displayg value
!!     \param[in]  disp     defs::dispiterg = 1 if disp = 'on' and 0 if 'off'
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine initDisplay(displ,disp)
      implicit none
      integer, intent(in)       :: displ
      character(32), intent(in) :: disp

      select case (trim(disp))
        case ('on')
            displayg = 1
        case ('off')
            displayg = 0
        case default
            CALL printandstop('invalid options.Display: on or off ? ')
      end select
      IF (displ.LT.0) THEN
        CALL printandstop('options.DispIter must be positive.')
      ENDIF
      dispiterg = displ

   end subroutine initDisplay

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::stopatturningpointg parameter.
!!     \param[in]  satp    defs::stopatturningpointg = true if sapa = 'on' and false if 'off'
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine initStopAtTurningPt(satp)
      implicit none
      integer, intent(in) :: satp

      select case (satp)
        case (1)
            stopAtTurningPtg = .true.
        case (0)
            stopAtTurningPtg = .false.
        case default
            CALL printandstop('invalid options.StopAtTurningPoint: 0 or 1 ?')
      end select

   end subroutine initStopAtTurningPt


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::dosavepathinfileg arameter.
!!     \param[in]  sapa    defs::dosavepathinFileg = 1 if sapa = 'on' and 0 if 'off'
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine initSavePath(sapa)
      implicit none
      character(32), intent(in) :: sapa

      select case (trim(sapa))
        case ('on')
            doSavePathInFileg = 1
        case ('off')
            doSavePathInFileg = 0
        case default
            CALL printandstop('invalid options.DoSavePath = ' // trim(sapa) // ': on or off ?')
      end select

   end subroutine initSavePath

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::displayg parameter.
!!        \param[in]    disp    defs::displayg = 1 if disp = 'on' and 0 if 'off'
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initDisplaySolve(disp)
      implicit none
     character(32), intent(in) :: disp

     select case (trim(disp))
       case ('on')
            displayg = 1
       case ('off')
            displayg = 0
       case default
            CALL printandstop('invalid options.Display = ' // trim(disp) // ': on or off ?')
     end select

   end subroutine initDisplaySolve

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::displayg parameter.
!!     \param[in]    disp    defs::displayg = 1 if disp = 'on' and 0 if 'off'
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
   subroutine initDisplayExphv(disp)
      implicit none
     character(32), intent(in) :: disp

     select case (trim(disp))
       case ('on')
            displayg = 1
       case ('off')
            displayg = 0
       case default
            CALL printandstop('invalid options.Display = ' // trim(disp) // ': on or off ?')
     end select

   end subroutine initDisplayExphv

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::displayg parameter.
!!     \param[in]    disp    defs::displayg = 1 if disp = 'on' and 0 if 'off'
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
   subroutine initDisplayExpdhv(disp)
      implicit none
     character(32), intent(in) :: disp

     select case (trim(disp))
       case ('on')
            displayg = 1
       case ('off')
            displayg = 0
       case default
            CALL printandstop('invalid options.Display = ' // trim(disp) // ': on or off ?')
     end select

   end subroutine initDisplayExpdhv

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::grilleg parameter.
!!
!!     Detailed description :
!!     We check first if ti is increasing only if there is more than one arc,
!!     then we check if tspan is contained in ti and sorted.
!!     Finbally we add each instant from tspan in the list grilleg for the
!!     dense output during the call to exphvfun() or expdhvfun().
!!
!!     \param[in] n       dimension of the grid
!!     \param[in] tspan   the grille to transform into a list
!!     \param[in] nbarc   Number of arcs
!!     \param[in] ti      ti = [t0 t1 .. t_nbarc-1 tf], where t0 is the initial time and tf the final time.
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initTspanTi(n,tspan,nbarc,ti)
      implicit none
      integer, intent(in)                      :: n
      integer, intent(in)                      :: nbarc
      double precision, intent(in), dimension(n)   :: tspan
      double precision, intent(in), dimension(nbarc+1) :: ti

      !local variables
      integer                    :: i
      double precision, dimension(1) :: val

      !ti must be increasing if there is more than one arc
      if(nbarc.gt.1)then
          i=2
          do while(i.le.nbarc+1)
            if((ti(i)-ti(i-1)).lt.0d0)then
               CALL printandstop('ti must be increasing')
            end if
            i = i + 1
          end do
      end if

      !tspan must be contained in ti and tspan must be sorted
      i=2
      if((ti(nbarc+1)-tspan(1))*(tspan(1)-ti(1)).lt.0d0)then
        CALL printandstop('tspan must be contained in ti - a')
      end if
      do while(i.lt.n)
        if((tspan(i)-tspan(i-1))*(tspan(i+1)-tspan(i)).lt.0d0)then
           CALL printandstop('tspan must be sorted')
        end if
        if((ti(nbarc+1)-tspan(i))*(tspan(i)-ti(1)).lt.0d0)then
           CALL printandstop('tspan must be contained in ti - b')
        end if
        i = i + 1
      end do
      if((ti(nbarc+1)-tspan(n))*(tspan(n)-ti(1)).lt.0d0)then
        CALL printandstop('tspan must be contained in ti - c')
      end if

      !we add each instant from tspan in the list grilleg
      val(1)=0
      call CREER_STRUCTURE(grilleg,n-3)
      do i=1,n
        call INSERER(tspan(n-i+1),1,val,grilleg)
      end do

   end subroutine initTspanTi

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::par0g and defs::parfg parameters.
!!
!!     Detailed description :
!!     <ul>
!!     <li> We allocate the global parameters which contain
!!          the initial and final homotopic values.</li>
!!     <li> We count how many homotopic parameters we have.
!!          The number corresponds to the total of distinct
!!          initial and final parameters.</li>
!!     <li> We save the indexes of homotopic parameters.
!!          We use two "for" because we need to allocate
!!          the vector containing the indexes with the number
!!          of homotopic parameters.</li>
!!     <li> Of course we do not free the memory allocated here.</li>
!!     </ul>
!!
!!     \param[in]    lpar    dimension of par1 and par2
!!     \param[in]    par1    initial parameters
!!     \param[in]    par2    final parameters
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initPar(lpar,par1,par2)
      implicit none
      integer     , intent(in)                  :: lpar
      double precision, intent(in), dimension(lpar) :: par1,par2

      integer :: i,k
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

      !we allocate the global parameters which contain
       !the initial and final homotopic values
      allocate(par0g(lpar),parfg(lpar))

      lparg = lpar
      par0g = par1
      parfg = par2

      !we count how many homotopic parameters we have
       !the number corresponds to the total of distinct
       !initial and final parameters
      k = 0
      do i=1,lpar
         if(abs(par0g(i)-parfg(i)).gt.eps)then
            k = k+1
         end if
      end do

      !we save the indexes of homotopic parameters
       !we use two "for" because we need to allocate
       !the vector containing the indexes with the number
       !got before
      nparhomg = k
      allocate(iparhomg(nparhomg))
      k = 0
      do i=1,lpar
         if(abs(par0g(i)-parfg(i)).gt.eps)then
            k = k+1
            iparhomg(k) = i
         end if
      end do
   end subroutine initPar

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Free the memory allocated in initPar().
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine closePar()
      deallocate(par0g,parfg,iparhomg)
   end subroutine closePar

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::derivativeg parameter.
!!     \param[in]    der     defs::derivativeg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
   subroutine initDerivative(der)
      implicit none
      character(32), intent(in) :: der
      derivativeg = trim(der)
   end subroutine initDerivative


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::integeqvarg parameter.
!!        \param[in]    integ    defs::integeqvarg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initIntegSfun(integ)
      implicit none
      character(32), intent(in) :: integ
      integsfung = trim(integ)
   end subroutine initIntegSfun

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::integeqvarg parameter.
!!        \param[in]    integ    defs::integeqvarg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initIntegEqvar(integ)
      implicit none
      character(32), intent(in) :: integ
      integeqvarg = trim(integ)
      integsfung = trim(integ)
   end subroutine initIntegEqvar

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::integexpdhvfung parameter.
!!        \param[in]    integ     defs::integexpdhvfung value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initIntegExpdhvfun(integ)
      implicit none
      character(32), intent(in) :: integ
      integexpdhvfung = trim(integ)
   end subroutine initIntegExpdhvfun

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::integexphvfung parameter.
!!        \param[in]    integ    defs::integexphvfung value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initIntegExphvfun(integ)
      implicit none
      character(32), intent(in) :: integ
      integexphvfung = trim(integ)
   end subroutine initIntegExphvfun

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::integhamg parameter.
!!        \param[in]    integ    defs::integhamg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initIntegHam(integ)
      implicit none
      character(32), intent(in) :: integ
       select case (trim(integ))
       case ('dopri5','dop853','radau5','radau9','radau13','radau')
                integhamg = trim(integ)
            case default
                CALL printandstop('ERROR: Integrate >>> only variable step-size schemes are available for homotopy')
                !CALL printandstop('ERROR: Integrate >>> can not use something else than dopri5 or dop853 for homotopy')
       end select
   end subroutine initIntegHam

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::maxstepsg and defs::hmaxg parameters.
!!     \param[in]    ms      defs::maxstepsg value
!!     \param[in]    mss      defs::hmaxg value
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
   subroutine initMaxStepInfos(ms,mss)
        implicit none
        integer, intent(in) :: ms
        double precision, intent(in) :: mss

        if(ms.lt.0 .or. mss.lt.0d0) then
            CALL printandstop('ERROR: MaxStepsOde and MaxStepSizeOde options must be positive')
        end if

        maxstepsg = ms
        hmaxg     = mss

   end subroutine initMaxStepInfos

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::maxstepscontg parameter.
!!        \param[in]    msh      defs::maxstepscontg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initMaxStepsHam(msh)
      implicit none
      integer, intent(in) :: msh
      maxstepscontg = msh
   end subroutine initMaxStepsHam

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::hmaxcontg parameter.
!!     \param[in]    mssh      defs::hmaxcontg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initMaxStepSizeHam(mssh)
      implicit none
      double precision, intent(in) :: mssh
      hmaxcontg = mssh
   end subroutine initMaxStepSizeHam


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::msshpG parameter.
!!     \param[in]    msshp      defs::msshpG value
!!
!!  \author Olivier Cots
!!  \date   2017
!!  \copyright LGPL
!!
   subroutine initMaxStepSizeHomPar(msshp)
      implicit none
      double precision, intent(in) :: msshp
      msshpG = msshp
   end subroutine initMaxStepSizeHomPar

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::atolg and defs::rtolg parameters.
!!        \param[in]    atole    defs::atolg value
!!        \param[in]    rtole    defs::rtolg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initTol(atole,rtole)
      implicit none
       double precision, intent(in) :: atole
      double precision, intent(in) :: rtole
       atolg = atole
      rtolg = rtole
   end subroutine initTol

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::atolcontg, defs::rtolcontg, defs::densetolg and defs::densetolrelg parameters.
!!        \param[in]    atolecont   atolcontg value
!!        \param[in]    rtolecont   rtolcontg value
!!        \param[in]    densetole   densetolg value
!!        \param[in]    dtr         densetolrelg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initTolCont(atolecont,rtolecont,densetole,dtr)
        implicit none
        double precision, intent(in) :: atolecont
        double precision, intent(in) :: rtolecont
        double precision, intent(in) :: densetole
        double precision, intent(in) :: dtr
        densetolrelg = dtr
        atolcontg = atolecont
        rtolcontg = rtolecont
        densetolg = densetole
   end subroutine initTolCont

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::xtolg parameter.
!!       \param[in]    xtole   defs::xtolg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initTolSolve(xtole)
      implicit none
       double precision, intent(in) :: xtole
       xtolg = xtole
   end subroutine initTolSolve

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonInitialization
!!     \brief   Initialize defs::normesfunmaxg parameter.
!!       \param[in]    nsm   normesfunmaxg value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
   subroutine initNormeSfunMax(nsm)
      implicit none
       double precision, intent(in) :: nsm
       NormeSfunMaxg = nsm
   end subroutine initNormeSfunMax

end module init
