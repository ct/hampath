!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module derivatives
    implicit none
    contains

!! -----------------------------------------------------------------------------
!!
!>  \ingroup commonDerivatives
!!  \brief Partial derivative of parfun with respect to lambda
!!
!!  \param[in]  lambda   Current homotopic parameter
!!  \param[in]  npar dimension of value
!!
!!  \param[out] dpdl = dpar/dl
!!
!!  \author Olivier Cots
!!  \date   2009-2016
!!  \copyright LGPL
    Subroutine dpardl(lambda,npar,dpdl)
        use defs
        implicit none
        integer,          intent(in)                   :: npar
        double precision, intent(in)                   :: lambda
        double precision, intent(out), dimension(npar) :: dpdl

        !local variables
        double precision                  :: lambdad
        double precision, dimension(npar) :: par, par0d, parfd

        if(nparhomG.eq.1)then
            dpdl = 0d0
            dpdl(iparhomG(1)) = 1d0 !if there is one homotopic parameter, the derivative is always 1
        else
            lambdad  = 1d0
            par0d    = 0d0
            parfd    = 0d0
            call PFUN_DP(lambda, lambdad, npar, par0G, par0d, parfG, parfd, par, dpdl)
        end if

    end subroutine dpardl

end module derivatives
