!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module adds
        use ModGestListe
        use utils
        use defs
        implicit none
!! -----------------------------------------------------------------------------
!!     Module adds
!!
!!         This module contains all global lists
!!
!>  \ingroup  globalVariables
!!  \brief Homotopic path
    type(TLISTE), save :: pathG
!>  \ingroup  globalVariables
!!  \brief States-costates from t0 to tf
    type(TLISTE), save :: etatsG
!>  \ingroup  globalVariables
!!  \brief Jacobian fields from t0 to tf
    type(TLISTE), save :: etatsdhvG

    contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonLists
!!     \brief initialize the right lists depending on the input 
!!            parameter
!!
!!     if n = 1 then hampath   initialization
!!     if n = 2 then exphvfun  initialization
!!     if n = 3 then expdhvfun initialization
!!
!!     \param[in] n between 1 and 3
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine initListe(n)
        implicit none
        integer, intent(in) :: n

        select case (n)
        case(idHamg)
            call CREER_STRUCTURE(pathG)
        case(idExpg)
            call CREER_STRUCTURE(etatsG)
        case(idExpdg)
            call CREER_STRUCTURE(etatsdhvG)
        case default
            CALL printandstop('Error: initListe')
        end select

    end subroutine initListe

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonLists
!!     \brief   adds to the list pathG
!!
!!     Detailed description :
!!     The input variables are concatenated and added to the list pathG. This
!!     list contains information about the path of zeros computed by hampath()
!!
!!        \param[in]    s      arc length where c, det, norms and pds are computed
!!        \param[in]    n1     size of c
!!        \param[in]    c      vector containing x, p and lambda at s
!!        \param[in]    toc    tangent vector at c
!!        \param[in]    det    det(S'(c(s)),c'(s))
!!        \param[in]    norms  |S(c(s))|
!!        \param[in]    pds    <c'(s_old)|c'(s_new)> (scalar product)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    Subroutine addpath(s,n1,c,toc,det,norms,pds)
        implicit none

        integer, intent(in)                          :: n1
        double precision, intent(in)                 :: s
        double precision, intent(in)                 :: det
        double precision, intent(in)                 :: norms
        double precision, intent(in)                 :: pds
        double precision, intent(in), dimension(n1)  :: c
        double precision, intent(in), dimension(n1)  :: toc

        !local variables
        double precision, dimension(3+2*n1) :: aux

        aux(1:n1) = c
        aux(n1+1:2*n1) = toc
        aux(2*n1+1) = det
        aux(2*n1+2) = norms
        aux(2*n1+3) = pds

       call INSERER(s,3+2*n1,aux,pathG)

    end subroutine addpath

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonLists
!!     \brief   redirect to addstateshv or addstatesdhv
!!
!!          @param[in]  nz          size of z
!!          @param[in]  time        time where z is computed
!!          @param[in]  z           vector containing x and p (flow at time)
!!          @param[in]  told        Previous time
!!          @param[in]  dpar        Double precision parameters
!!          @param[in]  ldpar       Dimension of dpar
!!          @param[in]  ipar        Integer parameters
!!          @param[in]  lipar       Dimension of ipar
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014-2015
!!  \copyright LGPL
!!
    Subroutine addstatesgeneral(addstates,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)
        implicit none
        integer,            intent(in)                      :: neq
        double precision,   intent(in)                      :: time
        double precision,   intent(in), dimension(neq)      :: y
        double precision,   intent(in)                      :: told
        integer,            intent(in)                      :: ldpar
        double precision,   intent(in), dimension(ldpar)    :: dpar
        integer,            intent(in)                      :: lipar
        integer,            intent(in), dimension(lipar)    :: ipar
        character(32),      intent(in)                      :: nameinte

        interface
        Subroutine addstates(nz,time,z)
            implicit none
            integer, intent(in)                             :: nz
            double precision, intent(in)                    :: time
            double precision, intent(in), dimension(nz)     :: z
        end Subroutine addstates
        end interface

        !local variables
        integer                          :: i
        integer                          :: fini
        double precision, dimension(1)   :: val
        double precision, dimension(neq) :: yg
        double precision                 :: t,hstep
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        select case (nameinte)

        case ('dopri5','radau5old','dop853','radau5','radau9','radau13','radau','radause')

                if(GET_PARAM(grilleG).EQ.-1)then !if tspan given by the user is [t0 tf]
                    call addstates(neq,time,y)
                else                            !if tspan given by the user is [t0 t1 .. tf]
                !we have to find all ti from tspan (ie grilleG) contained in [told time]
                !and use dense output to get the value
                t     = GET_TIME(grilleG)
                fini  = 0
                hstep = time - told
                    do while(((told-t)*(time-t).LE.0).AND.(fini.EQ.0))
                        call EXTRAIRE(t,1,val,grilleG)
                        if(abs(time-t).gt.eps)then
                            do i=1,neq
                                call denseOutput(nameinte,i,t,yg(i),told,time,hstep,lipar,ipar,ldpar,dpar)
                            end do
                        else
                            yg=y
                        end if
                        call addstates(neq,t,yg)
                        if(LONGUEUR(grilleG).EQ.0)then
                            fini=1
                        else
                            t = GET_TIME(grilleG)
                        end if
                    end do
                end if

            case default

                call addstates(neq,time,y)

        end select

    end subroutine addstatesgeneral

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonLists
!!     \brief   adds to the list etatsG
!!
!!     Detailed description :
!!     This method adds z to the list etatsG which contains the output 
!!     for exphvfun() 
!!
!!        \param[in]    nz     size of z
!!        \param[in]    time   time where z is computed
!!        \param[in]    z      vector containing x and p (flow at time)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    Subroutine addstateshv(nz,time,z)
        implicit none

        integer, intent(in)                             :: nz
        double precision, intent(in)                    :: time
        double precision, intent(in), dimension(nz)     :: z

        !local variables
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        if(LONGUEUR(etatsG).GE.1)then
            if(abs(time-GET_TIME(etatsG)).gt.eps)then !we can not have two elements with the same time
                call INSERER(time,nz,z,etatsG)
            end if
        else
            call INSERER(time,nz,z,etatsG)
        end if

    end subroutine addstateshv


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonLists
!!     \brief   adds to the list etatsdhvG
!!
!!     Detailed description :
!!     This method adds dz to the list etatsG which contains the output 
!!     for expdhvfun() 
!!
!!        \param[in]    ndz   size of dz
!!        \param[in]    time  time where dz is computed
!!        \param[in]    dz    vector containing the flow the jacobi fields
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    Subroutine addstatesdhv(ndz,time,dz)
        implicit none

        integer, intent(in)                             :: ndz
        double precision, intent(in)                    :: time
        double precision, intent(in), dimension(ndz)    :: dz

        !local variables
        double precision :: t
        double precision, dimension(ndz) :: val
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        if(LONGUEUR(etatsdhvG).GE.1)then
            if(abs(time-GET_TIME(etatsdhvG)).gt.eps)then !we can not have two elements with the same time
                call INSERER(time,ndz,dz,etatsdhvG)
            !else
            !    call EXTRAIRE(t,ndz,val,etatsdhvG)
            !    call INSERER(time,ndz,dz,etatsdhvG)
            end if
        else
            call INSERER(time,ndz,dz,etatsdhvG)
        end if

    end subroutine addstatesdhv

end module adds
