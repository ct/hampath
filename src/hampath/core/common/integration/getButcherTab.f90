!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------

!! -----------------------------------------------------------------------------
!!
!>     @ingroup commonIntegration
!!     @brief   Construct the Butcher Table for integration
!!     @param[in]    inte       integrator name
!!     @param[out]   BTable     Butcher table
!!     @param[out]   infos      explicit or implicit
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014-2016
!!  \copyright LGPL
!!
subroutine getButcherTab(inte,BTable,infos)
    use String_Utility
    use ModGestButcher
    implicit none
    character(32),                      intent(in)  :: inte
    character(8),                       intent(out) :: infos
    type(TButcherTable),                intent(out) :: BTable

    !local variables
    double precision, dimension(10,10)  :: A
    double precision, dimension(10)     :: b
    double precision, dimension(10)     :: c
    integer                             :: s, ordre

    !gauss6, 8 et raudas
    double precision :: r536, s15, s1524, s1536, s1530, s1515
    double precision :: w1, w2, w3, w4, w5, w1p, w2p, w3p, w4p, w5p
    double precision :: s6, s5, gam, del

    select case (StrLowCase(inte))

        case ('euler_explicite')

            s           = 1
            ordre       = 1
            infos       = 'explicit'

            A(1,1:s)    = 0d0
            b(1:s)      = (/ 1d0 /)
            c(1:s)      = (/ 0d0 /)

        case ('runge')

            s           = 2
            ordre       = 2
            infos       = 'explicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/2d0, 0d0 /)
            b(1:s)      = (/ 0d0, 1d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0 /)

        case ('heun')

            s           = 3
            ordre       = 3
            infos       = 'explicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/3d0, 0d0    , 0d0 /)
            A(3,1:s)    = (/ 0d0    , 2d0/3d0, 0d0 /)
            b(1:s)      = (/ 1d0/4d0, 0d0, 3d0/4d0 /)
            c(1:s)      = (/ 0d0, 1d0/3d0, 2d0/3d0 /)

        case ('rk4')

            s           = 4
            ordre       = 4
            infos       = 'explicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/2d0, 0d0       , 0d0, 0d0 /)
            A(3,1:s)    = (/ 0d0    , 1d0/2d0   , 0d0, 0d0 /)
            A(4,1:s)    = (/ 0d0    , 0d0       , 1d0, 0d0 /)

            b(1:s)      = (/ 1d0/6d0, 2d0/6d0, 2d0/6d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0, 1d0/2d0, 1d0 /)

        case ('rk5')
            !rk5a
            !page 271, Hairer I
            s           = 5
            ordre       = 5
            infos       = 'explicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/5d0    , 0d0       , 0d0       , 0d0, 0d0 /)
            A(3,1:s)    = (/ 0d0        , 2d0/5d0   , 0d0       , 0d0, 0d0 /)
            A(4,1:s)    = (/ 3d0/16d0   , 0d0       , 5d0/16d0  , 0d0, 0d0 /)
            A(5,1:s)    = (/ 1d0/4d0    , 0d0       , -5d0/4d0  , 2d0, 0d0 /)

            b(1:s)      = (/ 1d0/6d0, 0d0, 0d0, 2d0/3d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/5d0, 2d0/5d0, 1d0/2d0, 0d0 /)

        case ('euler_implicite','euler')

            s           = 1
            ordre       = 1
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0 /)
            b(1:s)      = (/ 1d0 /)
            c(1:s)      = (/ 1d0 /)

        case ('midpoint')

            s           = 1
            ordre       = 1
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/2d0 /)
            b(1:s)      = (/ 1d0 /)
            c(1:s)      = (/ 1d0/2d0 /)

        case ('gauss4')
            ! Description : Gauss (implicite, symetrique et symplectique ordre 4)
            ! Page 207, Hairer I

            s           = 2
            ordre       = 4
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/4d0,               1d0/4d0-sqrt(3d0)/6d0 /)
            A(2,1:s)    = (/ 1d0/4d0+sqrt(3d0)/6d0, 1d0/4d0  /)

            b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
            c(1:s)      = (/ 1d0/2d0-sqrt(3d0)/6d0, 1d0/2d0+sqrt(3d0)/6d0 /)

        case ('gauss6')
            ! Description : Gauss (implicite, symetrique et symplectique ordre 6)
            ! Page 209, Hairer I

            s           = 3
            ordre       = 6
            infos       = 'implicit'

            r536    = 5d0/36d0
            s15     = sqrt(15d0)
            s1524   = s15/24d0
            s1536   = s15/36d0
            s1530   = s15/30d0
            s1515   = s15/15d0

            A(1,1:s)    = (/ 5d0/36d0,          2d0/9d0-s1515,  5d0/36d0-s1530  /)
            A(2,1:s)    = (/ 5d0/36d0+s1524,    2d0/9d0,        5d0/36d0-s1524  /)
            A(3,1:s)    = (/ 5d0/36d0+s1530,    2d0/9d0+s1515,  5d0/36d0        /)

            b(1:s)      = (/ 5d0/18d0, 4d0/9d0, 5d0/18d0 /)
            c(1:s)      = (/ 1d0/2d0-s15/10d0, 1d0/2d0, 1d0/2d0+s15/10d0 /)

        case ('gauss8')
            ! Description : Gauss (implicite, symetrique et symplectique ordre 8)
            ! Page 209, Hairer I

            s           = 4
            ordre       = 8
            infos       = 'implicit'

            w1 = 1d0/8d0-sqrt(30d0)/144d0
            w2 = 1d0/2d0*sqrt((15d0+2d0*sqrt(30d0))/35d0)
            w3 = w2*(1d0/6d0+sqrt(30d0)/24d0)
            w4 = w2*(1d0/21d0+5d0*sqrt(30d0)/168d0)
            w5 = w2-2d0*w3

            w1p = 1d0/8d0+sqrt(30d0)/144d0
            w2p = 1d0/2d0*sqrt((15d0-2d0*sqrt(30d0))/35d0)
            w3p = w2p*(1d0/6d0-sqrt(30d0)/24d0)
            w4p = w2p*(1d0/21d0-5d0*sqrt(30d0)/168d0)
            w5p = w2p-2d0*w3p

            A(1,1:s)    = (/ w1,         w1p-w3+w4p,    w1p-w3-w4p,     w1-w5       /)
            A(2,1:s)    = (/ w1-w3p+w4,  w1p,           w1p-w5p,        w1-w3p-w4   /)
            A(3,1:s)    = (/ w1+w3p+w4,  w1p+w5p,       w1p,            w1+w3p-w4   /)
            A(4,1:s)    = (/ w1+w5,      w1p+w3+w4p,    w1p+w3-w4p,     w1          /)

            b(1:s)      = (/ 2d0*w1,        2d0*w1p,        2d0*w1p,        2d0*w1      /)
            c(1:s)      = (/ 1d0/2d0-w2,    1d0/2d0-w2p,    1d0/2d0+w2p,    1d0/2d0+w2  /)

        case ('radauia1')
            ! 
            ! Description : Radau IA (implicite ordre 1)
            ! Page 73, Hairer II

            s           = 1
            ordre       = 1
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0 /)
            b(1:s)      = (/ 1d0 /)
            c(1:s)      = (/ 0d0 /)

        case ('radauia3')
            ! Description : Radau IA (implicite ordre 3)
            ! Page 73, Hairer II

            s           = 2
            ordre       = 3
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/4d0, -1d0/4d0 /)
            A(2,1:s)    = (/ 1d0/4d0,  5d0/12d0/)
            b(1:s)      = (/ 1d0/4d0,  3d0/4d0 /)
            c(1:s)      = (/ 0d0,      2d0/3d0 /)

        case ('radauia5')
            ! Description : Radau IA (implicite ordre 5)
            ! Page 73, Hairer II

            s           = 3
            ordre       = 5
            infos       = 'implicit'

            s6 = sqrt(6d0)

            A(1,1:s)    = (/ 1d0/9d0, (-1d0-s6)/18d0,       (-1d0+s6)/18d0 /)
            A(2,1:s)    = (/ 1d0/9d0, (88d0+7d0*s6)/360d0,  (88d0-43d0*s6)/360d0 /)
            A(3,1:s)    = (/ 1d0/9d0, (88d0+43d0*s6)/360d0, (88d0-7d0*s6)/360d0 /)

            b(1:s)      = (/ 1d0/9d0, (16d0+s6)/36d0,   (16d0-s6)/36d0 /)
            c(1:s)      = (/ 0d0,     (6d0-s6)/10d0,    (6d0+s6)/10d0  /)

        case ('radauiia1')
            ! Description : Radau IIA (implicite ordre 1)
            ! Page 74, Hairer II

            s           = 1
            ordre       = 1
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0 /)
            b(1:s)      = (/ 1d0 /)
            c(1:s)      = (/ 1d0 /)

        case ('radauiia3')
            ! Description : Radau IIA (implicite ordre 3)
            ! Page 74, Hairer II

            s           = 2
            ordre       = 3
            infos       = 'implicit'

            A(1,1:s)    = (/ 5d0/12d0, -1d0/12d0 /)
            A(2,1:s)    = (/ 3d0/4d0,  1d0/4d0/)

            b(1:s)      = (/ 3d0/4d0,  1d0/4d0 /)
            c(1:s)      = (/ 1d0/3d0,  1d0     /)

        case ('radauiia5')
            ! Description : Radau IIA (implicite ordre 5)
            ! Page 215, Hairer I ou 74, Hairer II

            s           = 3
            ordre       = 5
            infos       = 'implicit'

            s6 = sqrt(6d0)

            A(1,1:s)    = (/ (88d0-7d0*s6)/360d0,       (296d0-169d0*s6)/1800d0,    (-2d0+3d0*s6)/225d0 /)
            A(2,1:s)    = (/ (296d0+169d0*s6)/1800d0,   (88d0+7d0*s6)/360d0,        (-2d0-3d0*s6)/225d0 /)
            A(3,1:s)    = (/ (16d0-s6)/36d0,            (16d0+s6)/36d0,             1d0/9d0 /)

            b(1:s)      = (/ (16d0-s6)/36d0,    (16d0+s6)/36d0,     1d0/9d0 /)
            c(1:s)      = (/ (4d0-s6)/10d0,     (4d0+s6)/10d0,      1d0     /)

        case ('radaus')
            ! Description : Radau IIA (implicite et symplectique mais non symetrique, ordre 5)
            ! Page 318, Hairer I

            s           = 3
            ordre       = 5
            infos       = 'implicit'

            s6 = sqrt(6d0)

            A(1,1:s)    = (/ (16d0-s6)/72d0,            (328d0-167d0*s6)/1800d0,    (-2d0+3d0*s6)/450d0 /)
            A(2,1:s)    = (/ (328d0+167d0*s6)/1800d0,   (16d0+s6)/72d0,             (-2d0-3d0*s6)/450d0 /)
            A(3,1:s)    = (/ (85d0-10d0*s6)/180d0,      (85d0+10d0*s6)/180d0,       1d0/18d0            /)

            b(1:s)      = (/ (16d0-s6)/36d0,    (16d0+s6)/36d0,     1d0/9d0 /)
            c(1:s)      = (/ (4d0-s6)/10d0,     (4d0+s6)/10d0,      1d0     /)

        case ('lobatto4')
            !Butcher's Lobatto formula (implicite ordre 4)
            !page 211 Hairer I

            s           = 3
            ordre       = 4
            infos       = 'implicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/4d0, 1d0/4d0, 0d0  /)
            A(3,1:s)    = (/ 0d0, 1d0, 0d0  /)

            b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

        case ('lobatto6')
            !Butcher's Lobatto formula (implicite ordre 6)
            !page 211 Hairer I

            s           = 4
            ordre       = 6
            infos       = 'implicit'

            s5 = sqrt(5d0)

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ (5d0+s5)/60d0, 1d0/6d0, (15d0-7d0*s5)/60d0, 0d0  /)
            A(3,1:s)    = (/ (5d0-s5)/60d0 ,(15d0+7d0*s5)/60d0, 1d0/6d0, 0d0  /)
            A(4,1:s)    = (/ 1d0/6d0, (5d0-s5)/12d0, (5d0+s5)/12d0, 0d0  /)

            b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
            c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

        case ('lobattoiiia2')
            !Lobatto IIIA (implicite ordre 2)
            !page 75 Hairer II
            !
            s           = 2
            ordre       = 2
            infos       = 'implicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 1d0/2d0, 1d0/2d0 /)

            b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
            c(1:s)      = (/ 0d0,     1d0     /)

        case ('lobattoiiia4')
            !Lobatto IIIA (implicite ordre 4)
            !page 75 Hairer II
            !
            s           = 3
            ordre       = 4
            infos       = 'implicit'

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ 5d0/24d0, 1d0/3d0, -1d0/24d0  /)
            A(3,1:s)    = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0  /)

            b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

        case ('lobattoiiia6')
            !Lobatto IIIA (implicite ordre 6)
            !page 75 Hairer II

            s           = 4
            ordre       = 6
            infos       = 'implicit'

            s5 = sqrt(5d0)

            A(1,1:s)    = 0d0
            A(2,1:s)    = (/ (11d0+s5)/120d0, (25d0-s5)/120d0, (25d0-13d0*s5)/120d0, (-1d0+s5)/120d0  /)
            A(3,1:s)    = (/ (11d0-s5)/120d0 ,(25d0+13d0*s5)/120d0, (25d0+s5)/120d0, (-1d0-s5)/120d0  /)
            A(4,1:s)    = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0  /)

            b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
            c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

        case ('lobattoiiib2')
            !Lobatto IIIB (implicite ordre 2)
            !page 76 Hairer II
            !
            s           = 2
            ordre       = 2
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/2d0, 0d0 /)
            A(2,1:s)    = (/ 1d0/2d0, 0d0 /)

            b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
            c(1:s)      = (/ 0d0,     1d0     /)

        case ('lobattoiiib4')
            !Lobatto IIIB (implicite ordre 4)
            !page 76 Hairer II
            !
            s           = 3
            ordre       = 4
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/6d0, -1d0/6d0, 0d0  /)
            A(2,1:s)    = (/ 1d0/6d0,  1d0/3d0, 0d0  /)
            A(3,1:s)    = (/ 1d0/6d0,  5d0/6d0, 0d0  /)

            b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

        case ('lobattoiiib6')
            !Lobatto IIIB (implicite ordre 6)
            !page 76 Hairer II

            s           = 4
            ordre       = 6
            infos       = 'implicit'

            s5 = sqrt(5d0)

            A(1,1:s)    = (/ 1d0/12d0, (-1d0-s5)/24d0,          (-1d0+s5)/24d0,         0d0  /)
            A(2,1:s)    = (/ 1d0/12d0, (25d0+s5)/120d0,         (25d0-13d0*s5)/120d0,   0d0  /)
            A(3,1:s)    = (/ 1d0/12d0, (25d0+13d0*s5)/120d0,    (25d0-s5)/120d0,        0d0  /)
            A(4,1:s)    = (/ 1d0/12d0, (11d0-s5)/24d0,          (11d0+s5)/24d0,         0d0  /)

            b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
            c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

        case ('lobattoiiic2')
            !Lobatto IIIC (implicite ordre 2)
            !page 76 Hairer II
            !
            s           = 2
            ordre       = 2
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/2d0, -1d0/2d0 /)
            A(2,1:s)    = (/ 1d0/2d0, 1d0/2d0 /)

            b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
            c(1:s)      = (/ 0d0,     1d0     /)

        case ('lobattoiiic4')
            !Lobatto IIIC (implicite ordre 4)
            !page 76 Hairer II
            !
            s           = 3
            ordre       = 4
            infos       = 'implicit'

            A(1,1:s)    = (/ 1d0/6d0, -1d0/3d0, 1d0/6d0  /)
            A(2,1:s)    = (/ 1d0/6d0,  5d0/12d0, -1d0/12d0  /)
            A(3,1:s)    = (/ 1d0/6d0,  2d0/3d0, 1d0/6d0  /)

            b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
            c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

        case ('lobattoiiic6')
            !Lobatto IIIC (implicite ordre 6)
            !page 76 Hairer II

            s           = 4
            ordre       = 6
            infos       = 'implicit'

            s5 = sqrt(5d0)

            A(1,1:s)    = (/ 1d0/12d0, -s5/12d0,            s5/12d0,            -1d0/12d0   /)
            A(2,1:s)    = (/ 1d0/12d0, 1d0/4d0,             (10d0-7d0*s5)/60d0, s5/60d0     /)
            A(3,1:s)    = (/ 1d0/12d0, (10d0+7d0*s5)/60d0,  1d0/4d0,            -s5/60d0    /)
            A(4,1:s)    = (/ 1d0/12d0, 5d0/12d0,            5d0/12d0,           1d0/12d0    /)

            b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
            c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

        case ('sdirk3')
            !SDIRK method (implicite ordre 3)
            !page 207 Hairer I
            !
            s           = 2
            ordre       = 3
            infos       = 'implicit'

            gam = (3d0+sqrt(3d0))/6d0

            A(1,1:s)    = (/ gam,           0d0 /)
            A(2,1:s)    = (/ 1d0-2d0*gam,   gam /)

            b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
            c(1:s)      = (/ gam, 1d0-gam /)

        case ('sdirk4l')
            !L-stable SDIRK method of order 4 (implicite ordre 4)
            !page 100 Hairer II
            !
            s           = 5
            ordre       = 4
            infos       = 'implicit'

            gam = 1d0/4d0

            A(1,1:s)    = (/ gam, 0d0, 0d0, 0d0, 0d0 /)
            A(2,1:s)    = (/ 1d0/2d0, gam, 0d0, 0d0, 0d0 /)
            A(3,1:s)    = (/ 17D0/50D0, -1D0/25D0, gam, 0D0, 0D0 /)
            A(4,1:s)    = (/ 371D0/1360D0, -137D0/2720D0, 15D0/544D0, gam, 0D0 /)
            A(5,1:s)    = (/ 25D0/24D0, -49D0/48D0, 125D0/16D0, -85D0/12D0, gam /)

            b(1:s)      = (/ 25D0/24D0, -49D0/48D0, 125D0/16D0, -85D0/12D0, gam /)
            c(1:s)      = (/ 1D0/4D0, 3D0/4D0, 11D0/20D0, 1D0/2D0, 1D0 /)

        case ('sdirk4a')
            !A-stable SDIRK method of order 4 (implicite ordre 4)
            !page 100 Hairer II
            !
            s           = 3
            ordre       = 4
            infos       = 'implicit'

            gam = cos(3.14159265359d0/18D0)/sqrt(3d0) + 1D0/2D0
            del = 1D0/(6D0*(2*gam-1D0)**2)

            A(1,1:s)    = (/ gam, 0D0, 0D0 /)
            A(2,1:s)    = (/ 1D0/2D0-gam, gam, 0D0 /)
            A(3,1:s)    = (/ 2D0*gam, 1D0-4D0*gam, gam /)

            b(1:s)      = (/ del, 1D0-2D0*gam, del /)
            c(1:s)      = (/ gam, 1D0/2d0, 1D0-gam /)

        case ('dirk5')
            !A-stable SDIRK method of order 5 (implicite ordre 5)
            !page 101 Hairer II
            !
            s           = 5
            ordre       = 5
            infos       = 'implicit'

            s6 = sqrt(6D0)
            gam = (6D0-s6)/10D0

            A(1,1:s)    = (/ gam, 0D0, 0D0, 0D0, 0D0 /)
            A(2,1:s)    = (/ (-6D0+5D0*s6)/14D0, gam, 0D0, 0D0, 0D0 /)
            A(3,1:s)    = (/ (888D0+607D0*s6)/2850D0, (126D0-161D0*s6)/1425D0, gam, 0D0, 0D0 /)
            A(4,1:s)    = (/ (3153D0-3082D0*s6)/14250D0, (3213D0+1148D0*s6)/28500D0, (-267D0+88D0*s6)/500D0, gam, 0D0 /)
            A(5,1:s)    = (/ (-32583D0+14638D0*s6)/71250D0, (-17199D0+364D0*s6)/142500D0, (1329D0-544D0*s6)/2500D0, &
                                (-96D0+131D0*s6)/625D0, gam /)

            b(1:s)      = (/ 0D0, 0D0, 1D0/9D0, (16D0-s6)/36D0, (16D0+s6)/36D0, 0D0 /)
            c(1:s)      = (/ (6D0-s6)/10D0, (6D0+9D0*s6)/35D0, 1D0, (4D0-s6)/10D0, (4D0+s6)/10D0 /)

        case default

            infos = 'error'

    end select

    select case (infos)

        case ('error')

            CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator' // ' ' // inte)

        case default

            call createButcherTable(BTable,A(1:s,1:s),b(1:s),c(1:s),s,ordre,infos)

    end select

end subroutine getButcherTab
