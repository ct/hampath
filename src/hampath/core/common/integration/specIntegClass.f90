!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module specIntegClass

    implicit none

!!     \param[in]       fun         RHS
!!     \param[in]       dfun        Jacobian of the RHS
!!     \param[in]       choixsolout id giving the method which has called dopri5int, see \ref globalVariables
!!     \param[in]       solout      auxiliary function called after accepted steps
!!     \param[in]       phidezV     non linear equations to solve (implicit scheme)
!!     \param[in]       phidphidezV redirect to phidez or dphidez
    type specInteg
        procedure(proc_fun),     pointer, nopass :: fun
        procedure(proc_dfun),    pointer, nopass :: dfun
        procedure(proc_phidez),  pointer, nopass :: phidez
        procedure(proc_pdpdez),  pointer, nopass :: pdpdez
        procedure(proc_solout),  pointer, nopass :: solout
        integer                                  :: choixsolout = -1
    end type specInteg

    abstract interface
    Subroutine proc_fun(neq,time,y,val,funpar,lfunpar)
        implicit none
        integer,           intent(in)                      :: neq
        integer,           intent(in)                      :: lfunpar
        double precision,  intent(in)                      :: time
        double precision,  intent(in),  dimension(lfunpar) :: funpar
        double precision,  intent(in),  dimension(neq)     :: y
        double precision,  intent(out), dimension(neq)     :: val
    end Subroutine proc_fun
    end interface

    abstract interface
    Subroutine proc_dfun(nz,t,z,val,paraux,lparaux)
        implicit none
        integer,            intent(in)                      :: nz
        integer,            intent(in)                      :: lparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(lparaux) :: paraux
        double precision,   intent(in),  dimension(nz)      :: z
        double precision,   intent(out), dimension(nz,nz)   :: val
    end Subroutine proc_dfun
    end interface

    abstract interface
    subroutine proc_phidez(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar
    end subroutine proc_phidez
    end interface

    abstract interface
    subroutine proc_pdpdez(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag
    end subroutine proc_pdpdez
    end interface

    abstract interface
    subroutine proc_solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn
        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface
    end subroutine proc_solout
    end interface

contains

end module specIntegClass
