!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module ModGestButcher

    implicit none

    !! -----------------------------------------------------------------------------
    !!
    !>     \ingroup commonLists
    !!     \brief Butcher Table: A, b, c, stages and infos=explicit or infos=implicit type.
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    type TButcherTable
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:,:), pointer :: A => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:),   pointer :: b => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:),   pointer :: c => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        integer                                   :: s = 0
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        integer                                   :: ordre = 0
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        character(8)                              :: infos
    end type TButcherTable


    contains

    !! -----------------------------------------------------------------------------
    !!
    !>     \ingroup commonLists
    !!     \brief Create new Butcher table.
    !!     \param[in]   A
    !!     \param[in]   b
    !!     \param[in]   c
    !!     \param[in]   s
    !!     \param[in]   infos = explicit or implicit
    !!
    !!     \param[out]  table the Butcher table
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    subroutine createButcherTable (table,A,b,c,s,ordre,infos)
        integer,                            intent(in)  :: s, ordre
        double precision, dimension(s,s),   intent(in)  :: A
        double precision, dimension(s),     intent(in)  :: b
        double precision, dimension(s),     intent(in)  :: c
        character(8),                       intent(in)  :: infos
        type(TButcherTable),                intent(out) :: table

        allocate(table%A(s,s),table%b(s),table%c(s))
        table%A = A
        table%b = b
        table%c = c

        table%s = s
        table%ordre = ordre
        table%infos = infos

    end subroutine createButcherTable

    !! -----------------------------------------------------------------------------
    !!
    !>     \ingroup commonLists
    !!     \brief Destroy the Butcher table.
    !!     \param[in,out]  table the table
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    subroutine destroyButcherTable (table)
        type(TButcherTable), intent(inout) :: table
        deallocate(table%A,table%b,table%c)
    end subroutine destroyButcherTable

    !! -----------------------------------------------------------------------------
    !!
    !>     \ingroup commonLists
    !!     \brief Get the number of stages of the Butcher table.
    !!     \param[in]   table the Butcher table
    !!
    !!     \param[out]  s
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    subroutine getNbStages (table,s)
        integer,                            intent(out) :: s
        type(TButcherTable),                intent(in)  :: table
        s = table%s
    end subroutine getNbStages

    !! -----------------------------------------------------------------------------
    !!    !>     \ingroup commonLists
    !!     \brief Get the matrix A.
    !!     \param[in]   table the Butcher table
    !!     \param[in]   s
    !!
    !!     \param[out]  A
    !!     \param[out]  b
    !!     \param[out]  c
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    subroutine getAll (table,A,b,c,s,ordre)
        integer,                            intent(in)  :: s
        integer,                            intent(out) :: ordre
        type(TButcherTable),                intent(in)  :: table
        double precision, dimension(s,s),   intent(out) :: A
        double precision, dimension(s),     intent(out) :: b
        double precision, dimension(s),     intent(out) :: c

        A = table%A
        b = table%b
        c = table%c
        ordre = table%ordre

    end subroutine getAll

    !! -----------------------------------------------------------------------------
    !!
    !>     \ingroup commonLists
    !!     \brief Get infos.
    !!     \param[in]   table the Butcher table
    !!
    !!     \param[out]  infos
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    subroutine getInfos (table,infos)
        type(TButcherTable),                intent(in)  :: table
        character(8),                       intent(out) :: infos
        infos = table%infos
    end subroutine getInfos


end module ModGestButcher
