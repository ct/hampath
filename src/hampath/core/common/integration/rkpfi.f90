!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module rkpfi
    implicit none

    !Module pour tout ce qui est spécifique aux intégrateurs à pas fixe implicite

    contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   non linear equations to solve during steps of implicit schemes
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!     \param[in]       fun         IVP rhs
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine phidez(nz, Zc, fvec, iflag, fpar, lfpar, fun)
        use utils
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        !local variables
        integer                         :: neq, s, lfunpar, icur, j, nfev
        double precision                :: ti, h
        double precision, allocatable   :: yi(:), A(:,:), c(:), funpar(:), K(:), Id(:,:), AI(:,:)

        !fpar = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        neq     = int(fpar(1))
        s       = int(fpar(2))
        lfunpar = int(fpar(3))
        icur    = 3
        allocate(yi(neq), funpar(lfunpar), A(s,s), c(s), K(neq*s), Id(neq,neq), AI(s*neq,s*neq))
        ti      = fpar(icur+1);                             icur = icur + 1
        yi      = fpar(icur+1:icur+neq);                    icur = icur + neq
        h       = fpar(icur+1);                             icur = icur + 1
        funpar  = fpar(icur+1:icur+lfunpar);                icur = icur + lfunpar
        A       = reshape(fpar(icur+1:icur+s*s),(/s,s/));   icur = icur + s*s
        c       = fpar(icur+1:icur+s);                      icur = icur + s
        nfev    = int(fpar(icur+1))

        do j=1,s
            call fun(neq, ti+c(j)*h, yi + Zc((j-1)*neq+1:(j-1)*neq+neq),    &
            K((j-1)*neq+1:(j-1)*neq+neq), funpar, lfunpar)
            nfev = nfev + 1
        end do
        !construction de la matrice identite
        Id = 0d0
        do j=1,neq
            Id(j,j) = 1d0
        end do
        call kronecker(s,s,A,neq,neq,Id,AI)

        ! fvec = Zc - h * kron(A,eye(neq)) * K(:);
        fvec = Zc
        call DGEMV('N',s*neq,s*neq,-h,AI,s*neq,K,1,1d0,fvec,1) ! fvec = fvec - h * AI * K
        fpar(icur+1) = dble(nfev)

        iflag = 0

        deallocate(yi, funpar, A, c, K, Id, AI)

    end subroutine phidez

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   Jacobian of phidez function which codes the non linear
!!                      equations solved during steps of implicit schemes.
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!     \param[in]       fun         IVP rhs
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine dphidez(nz, Zc, fjac, iflag, fpar, lfpar, fun, dfun)
        use utils
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz,nz)       :: fjac
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

        !local variables
        integer :: j
        double precision :: h !fvec1(nz), fvec2(nz), Zctemp(nz), fjacaux(nz,nz) ,eps

        !specific analytic
        integer                         :: neq, s, lfunpar, icur
        double precision                :: ti
        double precision, allocatable   :: yi(:), A(:,:), c(:), funpar(:), df(:,:), Id1(:,:), Id2(:,:), AI(:,:), dFdZ(:,:)

        iflag = 1

        !fpar = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        neq     = int(fpar(1))
        s       = int(fpar(2))
        lfunpar = int(fpar(3))
        icur    = 3
        allocate(yi(neq), funpar(lfunpar), A(s,s), c(s), df(neq,neq))
        allocate(Id1(neq,neq), Id2(neq*s,neq*s), AI(s*neq,s*neq), dFdZ(neq*s,neq*s))
        ti      = fpar(icur+1);                             icur = icur + 1
        yi      = fpar(icur+1:icur+neq);                    icur = icur + neq
        h       = fpar(icur+1);                             icur = icur + 1
        funpar  = fpar(icur+1:icur+lfunpar);                icur = icur + lfunpar
        A       = reshape(fpar(icur+1:icur+s*s),(/s,s/));   icur = icur + s*s
        c       = fpar(icur+1:icur+s);                      icur = icur + s

        dFdZ = 0d0
        do j=1,s
            call dfun(neq, ti+c(j)*h, yi + Zc((j-1)*neq+1:(j-1)*neq+neq), df, funpar, lfunpar)
            dFdZ((j-1)*neq+1:(j-1)*neq+neq,(j-1)*neq+1:(j-1)*neq+neq) = df
        end do
        Id1 = 0d0
        do j=1,neq
            Id1(j,j) = 1d0
        end do
        call kronecker(s,s,A,neq,neq,Id1,AI)
        Id2 = 0d0
        do j=1,neq*s
            Id2(j,j) = 1d0
        end do

        call DGEMM('N','N',neq*s,neq*s,neq*s,-h,AI,neq*s,dFdZ,neq*s,1d0,Id2,neq*s)
        fjac = Id2

        deallocate(yi, A, c, funpar, df, Id1, Id2, AI, dFdZ)

        !----------------------------------------------------------------!
        !Comparaison avec les différences finies
!                eps  = epsilon(1d0)
!                Zctemp = Zc
!                call phidez(nz, Zctemp, fvec2, iflag, fpar, lfpar, fun)
!                do j=1,nz
!                    h = dsqrt(eps*max(1.d-5,abs(Zc(j))))
!                    Zctemp(j) = Zc(j) + h
!                    call phidez(nz, Zctemp, fvec1, iflag, fpar, lfpar, fun)
!                    Zctemp(j) = Zc(j)
!                    fjacaux(:,j) = (fvec1 - fvec2)/h
!                end do
!                call printMatrice('diffFJAC',fjacAux-fjac,neq*s,neq*s)
        !----------------------------------------------------------------!

    end subroutine dphidez

!! -----------------------------------------------------------------------------
!!
!>     @ingroup commonIntegration
!!     @brief   Redirect to phidez or dphidez.
!!        @param[in]    nz       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    Zc       Unknowns
!!        @param[in]    lparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Function value
!!        @param[out]   fjac     Function jacobian
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    Subroutine phidphidez(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag,fun,dfun)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(inout), dimension(nz)        :: fvec
        double precision, intent(inout), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

        !Local variables

        ! iflag = 1 : function at x and return fvec. do not alter fjac.
        ! iflag = 2 : jacobian at x and return fjac. do not alter fvec.

        if (iflag == 1) then

            !Function evaluation
            call phidez(nz, Zc, fvec, iflag, fpar, lfpar, fun)

        elseif(iflag == 2) then

            !Jacobian evaluation
            call dphidez(nz, Zc, fjac, iflag, fpar, lfpar, fun, dfun)

        end if

    end subroutine phidphidez

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   interface for implicit integrator
!!     \param[in]       fun         RHS
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in,out]   tin         initial time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!     \param[in]       solout      auxiliary function called after accepted steps
!!     \param[in]       BTable      Butcher table
!!     \param[in]       phidezV     non linear equations to solve
!!     \param[in]       phidphidezV redirect to phidez or dphidez
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine integImp(fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,solout,BTable,phidezV,phidphidezV)
        use defs
        use utils
        use ModGestButcher
        use ModGestListe
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: neq
        double precision,   intent(inout)                        :: tin
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag,nfev
        type(TButcherTable),intent(in)                           :: BTable

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,val,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: val
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        interface
        subroutine phidezV(nz, Zc, fvec, iflag, fpar, lfpar)
            implicit none
            integer,            intent(in)                          :: nz, lfpar
            double precision,   intent(in),  dimension(nz)          :: Zc
            double precision,   intent(out), dimension(nz)          :: fvec
            integer,            intent(inout)                       :: iflag
            double precision,   intent(inout),  dimension(lfpar)    :: fpar
        end Subroutine phidezV
        end interface

        interface
        Subroutine phidphidezV(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
            implicit none
            integer,          intent(in)                        :: nz
            integer,          intent(in)                        :: lfpar
            integer,          intent(in)                        :: ldfjac
            integer,          intent(in)                        :: nfev
            double precision, intent(inout), dimension(lfpar)   :: fpar
            double precision, intent(in), dimension(nz)         :: Zc
            double precision, intent(out), dimension(nz)        :: fvec
            double precision, intent(out), dimension(ldfjac,nz) :: fjac
            integer,          intent(inout)                     :: iflag
        end subroutine phidphidezV
        end interface

        !Local variables
        integer                                         :: Niter, irtrn, i, j, nsteps, lipar, ldpar
        double precision                                :: ti, tip1, tim1, h, him1
        double precision, dimension(neq)                :: y, yim1
        type(TNOEUD), pointer                           :: noeud
        double precision, dimension(1)                  :: dpar
        integer,          dimension(1)                  :: ipar
        CHARACTER(len=120)                              :: LINE


        integer :: s, ordre, sbis, INFO
        double precision, dimension(:,:), allocatable   :: K, A, Adgesv, d
        double precision, dimension(:)  , allocatable   :: b, c, IPIV

        double precision, dimension(:)  , allocatable   :: tinInterp, toutInterp
        double precision, dimension(:,:), allocatable   :: yinInterp, youtInterp

        !specific implicit
        double precision    :: fpeps, normprog, dnrm2, eta, normDZcm1
        integer             :: fpitermax, nbiter, NiterS
        CHARACTER(len=32)   :: choixSolveur, choixInit
        double precision    :: eps

        double precision, dimension(:),   allocatable :: fpar, Zc, DZc, fdeZc
        double precision, dimension(:,:), allocatable :: dfdeZc
        integer :: lfpar, flagSolveur, icur, flagPdP

        !specific hybrj
        integer :: njac

        eps  = 1d0*epsilon(1d0)

        choixInit       = irkiG ! '2'
        choixSolveur    = irksG ! 'pfixe'

        fpitermax       = 7 !15

        lipar   = 1
        ldpar   = 1
        Niter   = 0
        irtrn   = 1
        flag    = 1
        ti      = tin
        tip1    = tin
        tim1    = tin
        y       = y0
        yim1    = y0
        NiterS  = 0

        !get Butcher Table
        call getNbStages(BTable,s)

        allocate(A(s,s), Adgesv(s,s), b(s), c(s), d(s,1), IPIV(neq*s), K(neq,s))

        allocate(tinInterp(max(4,s+1)), yinInterp(neq,max(4,s+1)), toutInterp(s), youtInterp(neq,s))

        lfpar = 1 + 1 + 1 + 1 + neq + 1 + lfunpar + s*s + s + 1
        allocate(fpar(lfpar), Zc(neq*s), DZc(neq*s), fdeZc(neq*s), dfdeZc(neq*s,neq*s))

        ! On récupère les infos du tableau de Butcher
        call getAll(BTable,A,b,c,s,ordre)

        ! On calcule d tel que A d = b
        Adgesv  = transpose(A)
        d(:,1)  = b
        call DGESV( s, 1, Adgesv, s, IPIV(1:s), d, s, INFO )

        !Appel a solout en t0 pour enregistrer la premiere valeur
        call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

        !Gestion des pas ou du premier pas
        !!! Indiquer que l'on ne tient pas compte des options : si display = 1
        if(displayG.eq.1 .and. isFirstDisplayG) then
            isFirstDisplayG = .false.
            WRITE (LINE,'(a)') 'Warning: fixed-step integrator with a given'  ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' grid of the form: [t0 t1 ... tf].';    call myprint(LINE,.true.)
            WRITE (LINE,'(a)') 'Non used options for this choice:' ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' MaxStepsOde, MaxStepSizeOde, TolOdeAbs, TolOdeRel.';   call myprint(LINE,.true.)
        end if
        nsteps  = LONGUEUR(grilleG) - 1
        call GET_NOEUD_SUIVANT(noeud,grilleG)
        ti      = GET_TIME(noeud)

        eta     = 1d0
        nfev    = 0

        ! Mise à jour du vecteur fpar
        !
        !fpar  = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        !lfpar = 1 + 1 + 1 + 1 + neq + 1 + lfunpar + s*s + s + 1
        !Zc(neq*s), fdeZc(neq*s), flagSolveur

        fpar(1)  = dble(neq)
        fpar(2)  = dble(s)
        fpar(3)  = dble(lfunpar)
        icur                                    = 3
        !fpar(icur+1)                            = ti;
        icur = icur + 1         ! Mis à jour dans les itérations
        !fpar(icur+1:icur+neq)                   = y ;
        icur = icur + neq       ! Mis à jour dans les itérations
        !fpar(icur+1)                            = h ;
        icur = icur + 1         ! Mis à jour dans les itérations
        fpar(icur+1:icur+lfunpar)               = funpar;
        icur = icur + lfunpar
        fpar(icur+1:icur+s*s)                   = reshape(A,(/s*s/));   icur = icur + s*s
        fpar(icur+1:icur+s)                     = c;                    icur = icur + s
        !fpar(icur+1)                            = dble(nfev)                                    ! Mis à jour dans les itérations

        !Integration numerique
        do i=1,nsteps

            !temps suivant
            call GET_NOEUD_SUIVANT(noeud)
            tip1    = GET_TIME(noeud)
            h       = tip1 - ti
            him1    = h
            fpeps   = 1d-1*(h**ordre)

            !Initialisation des Zi
            select case (choixInit)

                case ('0')

                    Zc = 0d0

                case ('1')

                    call fun(neq, ti, y, K(:,1), funpar, lfunpar); nfev = nfev + 1
                    do j=1,s
                        Zc((j-1)*neq+1:(j-1)*neq+neq) = c(j) * h * K(:,1);
                    end do

                case ('2')

                    if(i.eq.1) then

                        call fun(neq, ti, y, K(:,1), funpar, lfunpar); nfev = nfev + 1
                        do j=1,s
                            Zc((j-1)*neq+1:(j-1)*neq+neq) = c(j) * h * K(:,1);
                        end do

                    else

                        sbis            = 0
                        tinInterp(1)    = tim1
                        yinInterp(:,1)  = yim1
                        do j=1,s
                            if(abs(c(j)).gt.eps) then
                                sbis                = sbis + 1
                                tinInterp(sbis+1)   = tim1 + c(j) * him1
                                yinInterp(:,sbis+1) = yim1 + Zc((j-1)*neq+1:(j-1)*neq+neq)
                            end if
                            toutInterp(j) = ti + c(j) * h
                        end do
                        call interpolationLagrange(neq,sbis+1,tinInterp(1:sbis+1),yinInterp(1:neq,1:sbis+1),    &
                                                                    s,toutInterp(1:s),youtInterp(1:neq,1:s))
                        do j=1,s
                            Zc((j-1)*neq+1:(j-1)*neq+neq) = youtInterp(:,j) - y
                        end do
                    end if

                case default

                    !free memory
                    deallocate(A, b, c, K, tinInterp, yinInterp, toutInterp, youtInterp, fpar, Zc, &
                                DZc, fdeZc, dfdeZc)

                    CALL printandstop('  ||| ERROR: Implicit integrator -> Unknown Zi initialization choice ' // choixInit)

            end select


            ! Mise à jour du vecteur fpar

!            fpar(1)  = dble(neq)
!            fpar(2)  = dble(s)
!            fpar(3)  = dble(lfunpar)
            icur                                    = 3
            fpar(icur+1)                            = ti;                   icur = icur + 1         ! Mis à jour dans les itérations
            fpar(icur+1:icur+neq)                   = y ;                   icur = icur + neq       ! Mis à jour dans les itérations
            fpar(icur+1)                            = h ;                   icur = icur + 1         ! Mis à jour dans les itérations
            !fpar(icur+1:icur+lfunpar)               = funpar;
            icur = icur + lfunpar
            !fpar(icur+1:icur+s*s)                   = reshape(A,(/s*s/));
            icur = icur + s*s
            !fpar(icur+1:icur+s)                     = c;
            icur = icur + s
            fpar(icur+1)                            = dble(nfev)                                    ! Mis à jour dans les itérations

            ! Calcul des Ki
            select case (choixSolveur)

                case ('pfixe')

                    nbiter = 0
                    calculZi : DO

                        call phidezV(neq*s, Zc, fdeZc, flagSolveur, fpar, lfpar)
                        Zc       = Zc - fdeZc
                        normprog = dnrm2(neq*s, fdeZc, 1)/(dnrm2(neq*s, Zc, 1) + sqrt(eps))                           ! Delta Z = fdeZc
                        nbiter   = nbiter + 1

                    IF ((normprog.le.fpeps) .or. (nbiter.ge.fpitermax)) EXIT calculZi
                    END DO calculZi

                case ('hybrd')

                    call hybrdint(phidezV,neq*s,Zc,fpeps,fpitermax,nbiter,Zc,fdeZc,flagSolveur,fpar,lfpar)

                case ('hybrj')

                    call hybrjint(phidphidezV,neq*s,Zc,lfpar,fpar,fpeps,fpitermax,Zc,fdeZc,nbiter,njac,flagSolveur)

                case ('newton')

                    !On calcule la jacobienne de phi que l'on cherche à annuler
                    flagPdP = 2
                    call phidphidezV(neq*s,Zc,fpar,lfpar,nfev,fdeZc,dfdeZc,neq*s,flagPdP)

                    !On fait la facto LU qui est stockée dans dfdeZc
                    CALL DGETRF( neq*s, neq*s, dfdeZc, neq*s, IPIV, INFO )

                    nbiter = 0
                    calculZiNewton : DO

                        ! On résout le système : phi(Zc) + dphi(Zc) * DZc = 0
                        call phidezV(neq*s, Zc, fdeZc, INFO, fpar, lfpar)
                        DZc = -fdeZc
                        CALL DGETRS( 'No transpose', neq*s, 1, dfdeZc, neq*s, IPIV, DZc, neq*s, INFO )

                        Zc       = Zc + DZc
                        normprog = dnrm2(neq*s, DZc, 1)/(dnrm2(neq*s, Zc, 1) + sqrt(eps))                           ! Delta Z = fdeZc
                        nbiter   = nbiter + 1

                    IF ((normprog.le.fpeps) .or. (nbiter.ge.fpitermax)) EXIT calculZiNewton
                    END DO calculZiNewton


                case default

                    !free memory
                    deallocate(A, Adgesv, b, c, d, IPIV, K, tinInterp, yinInterp, toutInterp, &
                                youtInterp, fpar, Zc, DZc, fdeZc, dfdeZc)

                    CALL printandstop('  ||| ERROR: Implicit integrator -> Unknown Ki computation choice ' // choixSolveur)

            end select

            yim1    = y
            him1    = h
            tim1    = ti

            ! On met à jour y
            ! Attention : Zc est en colonne mais c'est bon car fortran stocke les matrices en colonnes
            call DGEMV('N',neq,s,1d0,Zc,neq,d(:,1),1,1d0,y,1) !y       = y + matmul(Z,d)

            ! On stocke la nouvelle valeur
            call solout(NiterS+1,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

            ti      = tip1
            NiterS  = NiterS + 1

            if(nbiter.gt.fpitermax) then
                flag = -8
            end if

            ! On doit mettre à jour car nfev est modifié si on choisit l'initialisation 1 ou 2
            nfev    = int(fpar(icur+1))

        end do !fin integration

        yf      = y

        !free memory
        deallocate(A, Adgesv, b, c, d, IPIV, K, tinInterp, yinInterp, toutInterp, youtInterp, fpar, &
                    Zc, DZc, fdeZc, dfdeZc)

    end subroutine integImp

end module rkpfi
