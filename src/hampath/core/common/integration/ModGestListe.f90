!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module ModGestListe

!! -----------------------------------------------------------------------------
!!
!!     Written on 2013
!!     by Olivier Cots    - Math. Institute, Bourgogne Univ
!!                        - ENSEEIHT-IRIT
!!                        - INRIA Sophia Antipolis
!!
!!
    implicit none

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief List type managed like FILO.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    type TLISTE
        !>  @ingroup commonLists
        !! \memberof modgestliste::tliste
        type(TNOEUD), pointer   :: racine => null()
        !>  @ingroup commonLists
        !! \memberof modgestliste::tliste
        integer                 :: nb_elt = 0
        !>  @ingroup commonLists
        !! \memberof modgestliste::tliste
        integer        :: parametre = -1
    end type TLISTE

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Node type.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    type TNOEUD
        !>  @ingroup commonLists
        !! \memberof modgestliste::tnoeud
        double precision :: time
        !>  @ingroup commonLists
        !! \memberof modgestliste::tnoeud
        double precision, dimension(:), pointer :: valeur => null()
        !>  @ingroup commonLists
        !! \memberof modgestliste::tnoeud
        type(TNOEUD)  , pointer :: suivant  => null()
    end type TNOEUD

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface for list structure creation.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface CREER_STRUCTURE
    module procedure CREER_LISTE, CREER_P_LISTE
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface for insertion in a list.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface INSERER
    module procedure INSERER_TETE
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface for extraction from a list.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface EXTRAIRE
    module procedure EXTRAIRE_TETE
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface for destruction of node and list.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface DETRUIRE
    module procedure DETRUIRE_LISTE, DETRUIRE_NOEUD
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface to get next node of a list.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface GET_NOEUD_SUIVANT
    module procedure GET_SUIVANT, GET_RACINE
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface to get time in list or node.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface GET_TIME
    module procedure GET_TIME_NOEUD, GET_TIME_LISTE
    end interface

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Interface to get value in top of the list.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface GET_VALEUR
    module procedure GET_VALEUR_UNIQUE_LISTE, GET_VALEUR_TOUTE_LISTE, GET_VALEUR_UNIQUE_NOEUD, GET_VALEUR_TOUTE_NOEUD
    end interface

    contains

    !!---------------
    !!--- GET_NOEUD_SUIVANT --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get following node.
    !!     \param[in,out] noeud the node
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine GET_SUIVANT(noeud)
        type(TNOEUD), pointer   :: noeud
        noeud    => noeud%suivant
    end subroutine GET_SUIVANT

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief get root node from list.
    !!     \param[in]     liste the list
    !!     \param[in,out] noeud the node
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine GET_RACINE(noeud,liste)
        type(TLISTE), intent(in) :: liste
        type(TNOEUD), pointer    :: noeud
        noeud    => liste%racine
    end subroutine GET_RACINE

    !!---------------
    !!--- GET_TIME --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get time from the input node.
    !!     \param[in] noeud the node
    !!     \retval RES the time
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    double precision function GET_TIME_NOEUD(noeud) result(RES)
        type(TNOEUD), pointer           :: noeud
        RES    = noeud%time
    end function GET_TIME_NOEUD

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get time from the root node of the input list.
    !!     \param[in] liste the list
    !!     \retval RES the time
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    double precision function GET_TIME_LISTE(liste) result(RES)
        type(TLISTE), intent(in)           :: liste
        RES = GET_TIME_NOEUD(liste%racine)
    end function GET_TIME_LISTE

    !!---------------
    !!--- GET_VALEUR --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get scalar value at index n from vector of values in the node of dimension dime.
    !!     \param[in]  noeud the node
    !!     \param[in]  n     the indx
    !!     \param[in]  dime  the dimension of the vector of values
    !!     \retval RES   the value
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    double precision function GET_VALEUR_UNIQUE_NOEUD(noeud,n,dime) result(RES)
        integer, intent(in)                 :: n, dime
        type(TNOEUD), pointer               :: noeud
        double precision, dimension(dime)   :: val
        val = noeud%valeur
        RES = val(n)
    end function GET_VALEUR_UNIQUE_NOEUD

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get vector value of dimension dime from noeud.
    !!     \param[in]  noeud the node
    !!     \param[in] dime  the dimension of the vector of values
    !!     \retval RES  the vector of values
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    function GET_VALEUR_TOUTE_NOEUD(noeud,dime) result(RES)
        integer, intent(in)                 :: dime
        double precision, dimension(dime)   :: RES
        type(TNOEUD), pointer               :: noeud
        RES    = noeud%valeur
    end function GET_VALEUR_TOUTE_NOEUD

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get scalar value at index n from vector of values in the root node of the list, of dimension dime.
    !!     \param[in]  liste the list
    !!     \param[in]  n     the indx
    !!     \param[in]  dime  the dimension of the vector of values
    !!     \retval RES   the value
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    double precision function GET_VALEUR_UNIQUE_LISTE(liste,n,dime) result(RES)
        integer, intent(in)                 :: n,dime
        type(TLISTE), intent(in)            :: liste
        RES    = GET_VALEUR_UNIQUE_NOEUD(liste%racine,n,dime)
    end function GET_VALEUR_UNIQUE_LISTE

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get vector value of dimension dime from the root noeud of the list.
    !!     \param[in]  liste the list
    !!     \param[in] dime  the dimension of the vector of values
    !!     \retval RES  the vector of values
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    function GET_VALEUR_TOUTE_LISTE(liste,dime) result(RES)
        integer, intent(in) :: dime
        double precision, dimension(dime) :: RES
        type(TLISTE), intent(in)          :: liste
        RES    = GET_VALEUR_TOUTE_NOEUD(liste%racine,dime)
    end function GET_VALEUR_TOUTE_LISTE

    !!---------------
    !!--- GET AUTRES --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get the dimension of the vector values in the list.
    !!     \param[in]  liste the list
    !!     \retval RES the dimension
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    integer function GET_VALEUR_DIM(liste) result(RES)
        type(TLISTE), intent(in)           :: liste
        RES    = size(liste%racine%valeur)
    end function GET_VALEUR_DIM

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Get the scalar value parameter of the list.
    !!     \param[in]  liste the list
    !!     \retval RES the scalar parameter
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    integer function GET_PARAM(liste) result(RES)
        type(TLISTE), intent(in)           :: liste
        RES    = liste%parametre
    end function GET_PARAM

    !!------------------------------------
    !!Longueur liste
    !>     \ingroup commonLists
    !!     \brief Get the number of elements in the list.
    !!     \param[in]  liste the list
    !!     \retval RES the number of elements
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    integer function LONGUEUR(liste) result(RES)
        type(TLISTE), intent(in) :: liste
        RES = liste%nb_elt
    end function LONGUEUR

    !!---------------
    !!--- CREATION --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Create new list with default value for the parameter.
    !!     \param[in,out]  liste the list
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine CREER_LISTE (liste)
        type(TLISTE), intent(inout) :: liste
        liste%parametre = 0
    end subroutine CREER_LISTE

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Create a new list with parameter param.
    !!     \param[in,out]  liste the list
    !!     \param[in] param the parameter value
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine CREER_P_LISTE (liste,param)
        type(TLISTE), intent(inout) :: liste
        integer, intent(in) :: param
        liste%parametre = param
    end subroutine CREER_P_LISTE

    !!---------------
    !!-- INSERSION --
    !!---------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Insert on top, the value and time in the list.
    !!     \param[in] time      the time
    !!     \param[in] n         the dimension of the vector value
    !!     \param[in] valeur    the value
    !!     \param[in,out] liste the list
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine INSERER_TETE (time, n, valeur, liste)
        integer, intent(in)                         :: n
        double precision, intent(in)                :: time
        double precision, dimension(n), intent(in)  :: valeur
        type(TLISTE), intent(inout)                 :: liste

        !local variables
        type(TNOEUD), pointer :: noeud

        !On cree le noeud
        allocate(noeud)
        allocate(noeud%valeur(n))
        noeud%time = time
        noeud%valeur = valeur

        !On fait pointer le noeud sur l'ancienne racinne
        noeud%suivant => liste%racine

        !On insere en racine
        liste%racine => noeud
        liste%nb_elt = LONGUEUR(liste) + 1

    end subroutine INSERER_TETE

    !!----------------
    !!-- EXTRACTION --
    !!----------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Extract from top, the value and time from the list.
    !!     \param[out] time      the time
    !!     \param[in]  n         the dimension of the vector value
    !!     \param[out] valeur    the value
    !!     \param[in,out] liste  the list
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine EXTRAIRE_TETE (time, n, valeur, liste)
        integer, intent(in)                          :: n
        double precision, intent(out)                :: time
        double precision, dimension(n), intent(out)  :: valeur
        type(TLISTE), intent(inout)                  :: liste

        !local variables
        type(TNOEUD), pointer :: noeud

        if (liste%nb_elt == 0) then
            print *, "MOD_GEST_LISTE: erreur pas d'elt dans la LISTE"

        else if (liste%nb_elt == 1) then
            ! extraction de la racine
            time    = liste%racine%time
            valeur  = liste%racine%valeur
            ! remise � 0 de la liste
            call DETRUIRE(liste)
        else
            noeud => liste%racine
            ! extraction de la racine
            time    = noeud%time
            valeur  = noeud%valeur
            liste%racine => noeud%suivant
            deallocate(noeud%valeur)
            deallocate(noeud)
            liste%nb_elt = liste%nb_elt - 1
        end if
    end subroutine EXTRAIRE_TETE

    !!-----------------
    !!-- DESTRUCTION --
    !!-----------------

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Destroy node and following nodes.
    !!     \param[in,out] noeud the node
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    RECURSIVE subroutine DETRUIRE_NOEUD (noeud)
        type(TNOEUD), pointer :: noeud
        if (associated(noeud)) then
            call DETRUIRE_NOEUD(noeud%suivant)
            deallocate(noeud%valeur)
            deallocate(noeud)
        end if
    end subroutine

    !!------------------------------------
    !>     \ingroup commonLists
    !!     \brief Destroy the list, ie all the node in the list and the list itself.
    !!     \param[in,out]  liste the list
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine DETRUIRE_LISTE (liste)
        type(TLISTE) :: liste
        call DETRUIRE_NOEUD(liste%racine)
        nullify(liste%racine)
        liste%nb_elt = 0
    end subroutine DETRUIRE_LISTE

end module ModGestListe
