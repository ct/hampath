!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module rkpfe
    implicit none

    !Module pour tout ce qui est spécifique aux intégrateurs à pas fixe implicite

    contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   interface for dopri5 integrator
!!     \param[in]       fun         RHS
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in,out]   tin         initial time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!     \param[in]       solout      auxiliary function called after accepted steps
!!     \param[in]       BTable      Butcher table
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine integExp(fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,solout,BTable)
        use defs
        use utils
        use ModGestButcher
        use ModGestListe
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: neq
        double precision,   intent(inout)                        :: tin
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag,nfev
        type(TButcherTable),intent(in)                           :: BTable

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
            Subroutine fun(neq,time,y,value,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: value
            end Subroutine fun
        end interface

        !Local variables
        integer                                         :: Niter, irtrn, i, j, nsteps, lipar, ldpar
        double precision                                :: ti, tip1, h
        double precision, dimension(neq)                :: y, yaux
        type(TNOEUD), pointer                           :: noeud
        double precision, dimension(1)                  :: dpar
        integer,          dimension(1)                  :: ipar
        CHARACTER(len=120)  :: LINE

        integer :: s, ordre
        double precision, dimension(:,:), allocatable   :: K, A
        double precision, dimension(:)  , allocatable   :: b, c

        lipar   = 1
        ldpar   = 1
        Niter   = 0
        irtrn   = 1
        flag    = 1
        ti      = tin
        tip1    = tin
        y       = y0

        !get Butcher Table
        call getNbStages(BTable,s)
        allocate(A(s,s), b(s), c(s), K(neq,s))
        call getAll(BTable,A,b,c,s,ordre)

        !Appel a solout en t0 pour enregistrer la premiere valeur
        call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

        !Gestion des pas ou du premier pas
        !!! Indiquer que l'on ne tient pas compte des options : si display = 1
        if(displayG.eq.1 .and. isFirstDisplayG) then
            isFirstDisplayG = .false.
            WRITE (LINE,'(a)') 'Warning: fixed-step integrator with a given'  ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' grid of the form: [t0 t1 ... tf].';    call myprint(LINE,.true.)
            WRITE (LINE,'(a)') 'Non used options for this choice:' ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' MaxStepsOde, MaxStepSizeOde, TolOdeAbs, TolOdeRel.';   call myprint(LINE,.true.)
        end if
        nsteps  = LONGUEUR(grilleG) - 1
        call GET_NOEUD_SUIVANT(noeud,grilleG)
        ti      = GET_TIME(noeud)

        nfev = nsteps*ordre

        !Integration numerique
        do i=1,nsteps

            !temps suivant
            call GET_NOEUD_SUIVANT(noeud)
            tip1    = GET_TIME(noeud)
            h       = tip1 - ti

            ! Calcul des Ki
            call fun(neq, ti, y, K(:,1), funpar, lfunpar)
            do j=2,s
                yaux = y
                call DGEMV('N',neq,j-1,h,K(:,1:j-1),neq,A(j,1:(j-1)),1,1d0,yaux,1)
                call fun(neq, ti+c(j)*h, yaux, K(:,j), funpar, lfunpar)
                !call fun(neq, ti+c(j)*h, y + h * matmul(K(:,1:j-1), transpose(A(j,1:(j-1)))), K(:,j), funpar, lfunpar)
            end do

            call DGEMV('N',neq,s,h,K,neq,b,1,1d0,y,1) !y       = y + h * matmul(K,b)
            !y       = y + h * matmul(K,b) !mise à jour de y

            call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

            Niter   = Niter + 1
            ti      = tip1

        end do

        yf      = y

        !free memory
        deallocate(A, b, c, K)

    end subroutine integExp

!
!s       = length(b)       ; % Nombre d'étages
!N       = length(tspan)   ; % Nombre de points dans la grille
!nx      = length(x0);
!xout    = zeros(nx,N);
!K       = zeros(nx,s); % Les Ki des étages du schéma
!
!xout(:,1) = x0; % Initialisation de la première valeur
!
!nfe       = (N-1)*s;
!
!%Boucle principale calculant à chaque itération : x_i+1 à partir de x_i
!for i=1:N-1
!
!    h = tspan(i+1)-tspan(i); % La longueur du pas courant
!
!    % Calcul des Ki
!    K(:,1) = feval(phi, tspan(i), xout(:,i), par);
!    for j=2:s
!        K(:,j) = feval(phi, tspan(i)+c(j)*h, xout(:,i) + h * K(:,1:j-1) * transpose( A(j,1:(j-1)) ), par);
!    end
!
!    xout(:,i+1) = xout(:,i) + h * K * b; %mise à jour de xout
!
!end;

end module rkpfe
