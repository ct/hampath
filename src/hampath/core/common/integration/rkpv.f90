!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module rkpv
    implicit none

    !Module pour les interfaces aux intégrateurs à pas variable

    contains

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   interface for dopri5 integrator
!!     \param[in]       fun         RHS
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!     \param[in]       choixsolout id giving the method which has called dopri5int, see \ref globalVariables
!!     \param[in]       solout      auxiliary function called after accepted steps
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine dopri5Int(fun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,choixsolout,solout)
        use defs
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: choixsolout
        integer,            intent(in)                           :: neq
        integer,            intent(in)                           :: ncpas
        double precision,   intent(inout)                        :: tin
        double precision,   intent(inout)                        :: tout
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag, nfev

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,value,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: value
        end Subroutine fun
        end interface

        !local variables
        integer                                     :: iout
        integer                                     :: itol
        integer                                     :: lw
        integer                                     :: liw
        integer                                     :: NRDENS
        double precision                            :: tinin
        double precision                            :: toutout
        integer,      dimension(:), allocatable     :: iwork
        double precision, dimension(:), allocatable :: work
        double precision, dimension(:), allocatable :: atolv
        double precision, dimension(:), allocatable :: rtolv

        NRDENS=0
        select case (choixsolout)
        case(idExpG,idExpdG,idHamG)
            NRDENS = neq
            iout = 2
        case(idSFunG,idSJacG)
            NRDENS = 0
            iout = 0
        end select
        lw = 8*neq + 5*NRDENS + 21
        liw = NRDENS + 21
        allocate(work(lw), iwork(liw))
        work(1:20) = 0d0
        !work(7)=1d-4
        iwork(1:20) = 0

!    IWORK(4)  TEST FOR STIFFNESS IS ACTIVATED AFTER STEP NUMBER
!              J*IWORK(4) (J INTEGER), PROVIDED IWORK(4).GT.0.
!              FOR NEGATIVE IWORK(4) THE STIFFNESS TEST IS
!              NEVER ACTIVATED; DEFAULT VALUE IS IWORK(4)=1000
        iwork(4) = 300

        iwork(3) = -1
        flag = 0
        iwork(5)=NRDENS
        !use vector tolerances to force step control to use only y
        allocate(atolv(neq),rtolv(neq))
        atolv = 0d0
        rtolv = 0d0
        itol = 2
        if (choixsolout.eq.idHamG) then
            atolv(1:ncpas) = atolcontG
            rtolv(1:ncpas) = rtolcontG
            iwork(1) = maxstepscontG
            if(hmaxcontG.gt.0d0)then
                work(6) = hmaxcontG
            end if
        else
            atolv(1:ncpas) = atolG
            rtolv(1:ncpas) = rtolG
            iwork(1) = maxstepsG
            if(hmaxG.gt.0d0)then
                work(6) = hmaxG
            end if
        end if

        yf      = y0
        tinin   = tin
        toutout = tout

        call dopri5(neq,fun,tinin,yf,toutout,rtolv,atolv,itol,      &
                    solout,iout,work,lw,     &
                    iwork,liw,funpar,lfunpar,flag)

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(17)

!   IWORK(17)  NFCN    NUMBER OF FUNCTION EVALUATIONS
!   IWORK(18)  NSTEP   NUMBER OF COMPUTED STEPS
!   IWORK(19)  NACCPT  NUMBER OF ACCEPTED STEPS
!   IWORK(20)  NREJCT  NUMBER OF REJECTED STEPS (DUE TO ERROR TEST),

        deallocate(work,iwork,atolv,rtolv)

    end subroutine dopri5Int

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   interface for dop853 integrator
!!     \param[in]       fun         RHS
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!     \param[in]       choixsolout id giving the method which has called dop853Int, see \ref globalVariables
!!     \param[in]       solout      auxiliary function called after accepted steps
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine dop853Int(fun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,choixsolout,solout)
        use defs
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: choixsolout
        integer,            intent(in)                           :: neq
        integer,            intent(in)                           :: ncpas
        double precision,   intent(inout)                        :: tin
        double precision,   intent(inout)                        :: tout
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag, nfev

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,value,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: value
        end Subroutine fun
        end interface

        !local variables
        integer                                     :: iout
        integer                                     :: itol
        integer                                     :: lw
        integer                                     :: liw
        integer                                     :: NRDENS
        double precision                            :: tinin
        double precision                            :: toutout
        integer,      dimension(:), allocatable     :: iwork
        double precision, dimension(:), allocatable :: work
        double precision, dimension(:), allocatable :: atolv
        double precision, dimension(:), allocatable :: rtolv

        NRDENS=0
        select case (choixsolout)
        case(idExpG,idExpdG,idHamG)
            NRDENS = neq
            iout = 2
        case(idSFunG,idSJacG)
            NRDENS = 0
            iout = 0
        end select
        lw = 11*neq + 8*NRDENS + 21
        liw = NRDENS + 21
        allocate(work(lw), iwork(liw))
        work(1:20) = 0d0
        !work(7)=1d-4
        iwork(1:20) = 0

!    IWORK(4)  TEST FOR STIFFNESS IS ACTIVATED AFTER STEP NUMBER
!              J*IWORK(4) (J INTEGER), PROVIDED IWORK(4).GT.0.
!              FOR NEGATIVE IWORK(4) THE STIFFNESS TEST IS
!              NEVER ACTIVATED; DEFAULT VALUE IS IWORK(4)=1000
        iwork(4) = 300

        iwork(3) = -1
        flag = 0
        iwork(5)=NRDENS
        !use vector tolerances to force step control to use only y
        allocate(atolv(neq),rtolv(neq))
        atolv = 0d0
        rtolv = 0d0
        itol = 2
        if (choixsolout.eq.idHamG) then
            atolv(1:ncpas) = atolcontG
            rtolv(1:ncpas) = rtolcontG
            iwork(1) = maxstepscontG
            if(hmaxcontG.gt.0d0)then
                work(6) = hmaxcontG
            end if
        else
            atolv(1:ncpas) = atolG
            rtolv(1:ncpas) = rtolG
            iwork(1) = maxstepsG
            if(hmaxG.gt.0d0)then
                work(6) = hmaxG
            end if
        end if

        yf      = y0
        tinin   = tin
        toutout = tout

        call dop853(neq,fun,tinin,yf,toutout,rtolv,atolv,itol,      &
                    solout,iout,work,lw,                            &
                    iwork,liw,funpar,lfunpar,flag)

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(17)

!   IWORK(17)  NFCN    NUMBER OF FUNCTION EVALUATIONS
!   IWORK(18)  NSTEP   NUMBER OF COMPUTED STEPS
!   IWORK(19)  NACCPT  NUMBER OF ACCEPTED STEPS
!   IWORK(20)  NREJCT  NUMBER OF REJECTED STEPS (DUE TO ERROR TEST),

        deallocate(work,iwork,atolv,rtolv)

    end subroutine dop853Int


    subroutine mas_radau_dummy()
    end subroutine mas_radau_dummy

!    subroutine mas_radau_dummy(N,AM,LMAS,RPAR,IPAR)
!        implicit none
!        integer                             :: N,LMAS,IPAR
!        real(kind=8), dimension(LMAS,N)     :: AM
!        real(kind=8), dimension(IPAR)       :: RPAR
!    end subroutine mas_radau_dummy

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   interface for radau integrator
!!     \param[in]       fun         RHS
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!     \param[in]       choixsolout id giving the method which has called radauInt, see \ref globalVariables
!!     \param[in]       solout      auxiliary function called after accepted steps
!!     \param[in]       NSmin       minimal number of stages
!!     \param[in]       NSmax       maximal number of stages
!!     \param[in]       NSdeb       initial number of stages
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014-2015
!!  \copyright LGPL
!!
   subroutine radauInt(fun,dfun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,choixsolout,solout,NSmin,NSmax,NSdeb)
        use defs
        implicit none
        integer,            intent(in)                           :: lfunpar,NSmin,NSmax,NSdeb
        integer,            intent(in)                           :: choixsolout
        integer,            intent(in)                           :: neq
        integer,            intent(in)                           :: ncpas
        double precision,   intent(inout)                        :: tin
        double precision,   intent(inout)                        :: tout
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag,nfev

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,value,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: value
        end Subroutine fun
        end interface

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

       !local variables
       integer                                 :: iout
       integer                                 :: itol
       integer                                 :: lw
       integer                                 :: liw
       !integer                                 :: NRDENS
       real(kind=8)                            :: tinin
       real(kind=8)                            :: toutout
       integer,      dimension(:), allocatable :: iwork
       real(kind=8), dimension(:), allocatable :: work
       real(kind=8), dimension(:), allocatable :: atolv
       real(kind=8), dimension(:), allocatable :: rtolv
        real(kind=8) :: H
        integer :: IJAC,MLJAC,MUJAC,IMAS,MLMAS,MUMAS,LJAC,LMAS,LE

        flag    = 0
        yf      = y0
        tinin   = tin
        toutout = tout
        H       = 0.D0

        select case (choixsolout)
        case(idExpG,idExpdG,idHamG)
            iout = 1
        case(idSFunG,idSJacG)
            iout = 0
        end select

        LJAC = neq
        LMAS = 0
        LE   = neq
        lw   = neq*(LJAC+LMAS+NSmax*LE+3*NSmax+3)+20
        liw  = (2+(NSmax-1)/2)*neq+20
        allocate(work(lw), iwork(liw))

        work(1:20)  = 0d0
        iwork(1:20) = 0
!       iwork(3) = -1 !Equivalent RADAU : nexiste pas ?! Pour rien afficher

        allocate(atolv(neq),rtolv(neq))
        !atolv = atolG
        !rtolv = rtolG
        atolv = 0d0
        rtolv = 0d0
        itol = 1

        if (choixsolout.eq.idHamG) then
            atolv(1:ncpas) = atolcontG
            rtolv(1:ncpas) = rtolcontG
            iwork(2) = maxstepscontG
            if(hmaxcontG.gt.0d0)then
                work(7) = hmaxcontG
            end if
        else
            atolv(1:ncpas) = atolG
            rtolv(1:ncpas) = rtolG
            iwork(2) = maxstepsG
            if(hmaxG.gt.0d0)then
                work(7) = hmaxG
            end if
        end if

        !We provide the jacobian
        IJAC  = 1
        MLJAC = neq
        MUJAC = 0

        IMAS  = 0
        MLMAS = neq
        MUMAS = 0

        iwork(11) = NSmin
        iwork(12) = NSmax
        iwork(13) = NSdeb

        if(choixsolout.eq.idHamG) then
            !On duplique à cause des problèmes liés aux variables globales
            !Il faut changer integhamG pour appeler la bonne fonction pour la sortie dense
            integhamG = trim(integhamG)//'Hampath'
            call RADAUHAMPATH(neq,fun,tinin,yf,toutout,         &
                        H,rtolv,atolv,itol,                     &
                        dfun,IJAC,MLJAC,MUJAC,       &
                        mas_radau_dummy,IMAS,MLMAS,MUMAS,       &
                        solout,iout,                            &
                        work,lw,iwork,liw,                      &
                        funpar,lfunpar,flag)
        else
            call RADAU(neq,fun,tinin,yf,toutout,         &
                        H,rtolv,atolv,itol,                     &
                        dfun,IJAC,MLJAC,MUJAC,       &
                        mas_radau_dummy,IMAS,MLMAS,MUMAS,       &
                        solout,iout,                            &
                        work,lw,iwork,liw,                      &
                        funpar,lfunpar,flag)
        end if

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(14)

      deallocate(work,iwork,atolv,rtolv)

   end subroutine radauInt

end module rkpv
