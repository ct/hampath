!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   Lagrange interpolation
!!      \param[in] neq
!!      \param[in] ntin
!!      \param[in] tin
!!      \param[in] yin
!!      \param[in] tout
!!      \param[out] tout
!!      \param[out] yout
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
subroutine interpolationLagrange(neq,ntin,tin,yin,ntout,tout,yout)
    implicit none
    integer, intent(in)             :: ntin, ntout, neq
    double precision, intent(in)    :: tin(ntin), yin(neq,ntin)
    double precision, intent(out)   :: tout(ntout), yout(neq,ntout)

    !local variables
    integer :: i, n, j, k
    double precision :: ti, tj, L(1,ntout)

    !n : degre du polynome
    n = ntin - 1

    yout = 0d0
    do i=1,n+1

        ti = tin(i)
        L  = 1d0
        do j=1,i-1
            tj      = tin(j)
            do k=1,ntout
                L(1,k)  = L(1,k) * (tout(k) - tj)/(ti - tj)
            end do
        end do
        do j=i+1,n+1
            tj = tin(j)
            do k=1,ntout
                L(1,k)  = L(1,k) * (tout(k) - tj)/(ti - tj)
            end do
        end do

        call DGEMM('N','N',neq,ntout,1,1d0,yin(:,i),neq,L,1,1d0,yout,neq)

    end do

end subroutine interpolationLagrange
