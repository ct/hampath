!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   performs the integration chosen
!!     \param[in]       spec        .
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in]       ninfos      dimension of infos
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)
    use defs
    use adds
    use ModGestListe
    use ModGestButcher
    use specIntegClass
    use rkpv
    use rkpfi
    use rkpfe
    implicit none
    type(specInteg),    intent(in)                      :: spec
    integer,            intent(in)                      :: lfunpar
    integer,            intent(in)                      :: neq
    integer,            intent(in)                      :: ncpas
    integer,            intent(in)                      :: ninfos
    double precision,   intent(inout)                   :: tin
    double precision,   intent(inout)                   :: tout
    double precision,   intent(in),  dimension(neq)     :: y0
    double precision,   intent(in),  dimension(lfunpar) :: funpar
    integer,            intent(out), dimension(ninfos)  :: infos
    double precision,   intent(out), dimension(neq)     :: yf

    !local variables
    character(32)       :: inte
    character(32)       :: method
    CHARACTER(len=120)  :: LINE
    character(8)        :: infosBT
    type(TButcherTable) :: BTable
    integer             :: flag, nfev, dummyInt(1), irtrn
    double precision    :: eps, dummyDbl(1)

    select case (spec%choixsolout)
        !SFUN
        case (idSFunG)
        inte    = integsfunG
        method  = 'SFUN'

        !SJAC
        case (idSJacG)
        inte    = integeqvarG
        method  = 'VAREQ'

        !EXPHVFUN
        case (idExpG)
        inte    = integexphvfunG
        method  = 'Exphvfun'

        !EXPDHVFUN
        case (idExpdG)
        inte    = integexpdhvfunG
        method  = 'Expdhvfun'

        !CONTINUATION
        case (idHamG)
        inte    = integhamG
        method  = 'Continuation'
    end select

    flag = 0
    nfev = 0

    eps  = 10d0*epsilon(1d0)
    if(abs(tout-tin).le.eps) then

        yf      = y0
        irtrn   = 1
        call spec%solout(1,tin,tout,yf,neq,dummyDbl,0,dummyInt,0,spec%fun,funpar,lfunpar,irtrn)

    else

        select case (inte)

            case ('dopri5')

                !!Dormand Prince 5th order (dopri5)
                call dopri5Int(spec%fun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,spec%choixsolout,spec%solout)

            case ('dop853')

                !!Dormand Prince 8th order (dop853)
                call dop853Int(spec%fun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,spec%choixsolout,spec%solout)

            case ('radau5')

                call radauInt(spec%fun,spec%dfun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,&
                        spec%choixsolout,spec%solout,3,3,3)

            case ('radau9')

                call radauInt(spec%fun,spec%dfun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,&
                    spec%choixsolout,spec%solout,5,5,5)

            case ('radau13')

                call radauInt(spec%fun,spec%dfun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,&
                    spec%choixsolout,spec%solout,7,7,7)

            case ('radau')

                call radauInt(spec%fun,spec%dfun,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag,nfev,&
                    spec%choixsolout,spec%solout,3,7,3)

            case default

                call getButcherTab(inte,BTable,infosBT)

                !On affiche un warning si la grille d'intégration est réduite qu'à [t0 tf]
                if(displayG.eq.1 .and. isFirstDisplayG .and. LONGUEUR(grilleG).eq.2) then
                    WRITE (LINE,'(a)') 'Warning: fixed-step integrator is used with only one step! tspan = [t0 tf]!';
                    call myprint(LINE,.true.)
                end if

                select case (infosBT)

                    case ('explicit')

                        call integExp(spec%fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,spec%solout,BTable)

                    case ('implicit')

                        call integImp(spec%fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,spec%solout,BTable,&
                            spec%phidez,spec%pdpdez)

                    case default

                        CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator ' // inte)

                end select

                call destroyButcherTable(BTable)

        end select

    end if

    infos(1) = flag
    if(ninfos.ge.2) then
        infos(2) = nfev
    end if
    !WRITE (LINE,'("nfev=      ",i0.1)') nfev; call myprint(LINE,.true.)

    if(displayG.eq.1) then

        if(flag.lt.0)then
            WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
            WRITE (LINE,'(a)') '  ||| WARNING: ';   call myprint(LINE,.false.)
            WRITE (LINE,'(a)') method;              call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' Integration (';    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') inte;                call myprint(LINE,.false.)
        end if

        select case (flag)
            case (-1)
                WRITE (LINE,'(a)') ')  ->  INPUT IS NOT CONSISTENT '
            case (-2)
                WRITE (LINE,'(a)') ')  ->  LARGER MAXSTEPS IS NEEDED '
            case (-3)
                WRITE (LINE,'(a)') ')  ->  STEP SIZE BECOMES TOO SMALL '
            case (-4)
                select case (inte)
                    case ('dopri5','dop853')
                        WRITE (LINE,'(a)') ')  ->  PROBLEM IS PROBABLY STIFF (INTERRUPTED) '
                    case ('radau5','radau9','radau13','radau')
                        WRITE (LINE,'(a)') ')  ->  MATRIX IS REPEATEDLY SINGULAR '
                    case default
                  end select
            case (-5)
                WRITE (LINE,'(a)') ')  ->  |S| BECOME TOO BIG '
            case (-6)
                WRITE (LINE,'(a)') ')  ->  HAMPATH STEP SIZE BECOMES TO SMALL '
            case (-7)
                WRITE (LINE,'(a)') ')  ->  HAMPATH STOPS AFTER A TURNING POINT '
            case (-8)
                WRITE (LINE,'(a)') ')  ->  MAXIMUM ITERATIONS REACHED TO SOLVE NLE OF IRK METHOD '
        end select

        if(flag.lt.-10)then
                WRITE (LINE,'(a)') ')  ->  HAMPATH STOPS BECAUSE OF USER IMPLEMENTATION OF MFUN! '
        end if

        if(flag.lt.0)then
            call myprint(LINE,.true.)
        end if

    end if

end Subroutine integration
