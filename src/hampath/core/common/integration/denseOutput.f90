!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!

!! -----------------------------------------------------------------------------
!!
!>  \ingroup commonIntegration
!!  \brief Interface for dense output function adapted to the integrator.
!!  \param[in] nameinte  integrator name
!!  \param[in] i         index
!!  \param[in] t         time at approximation
!!  \param[out] y        approximate value
!!  \param[in] told      last time
!!  \param[in] time      next time
!!  \param[in] hstep     steplength of current iteration: 0 .le. t-told .le. hstep
!!  \param[in] lipar     dimension of ipar
!!  \param[in] ipar      integer parameters
!!  \param[in] ldpar     dimension of dpar
!!  \param[in] dpar      double precision parameters
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine denseOutput(nameinte,i,t,y,told,time,hstep,lipar,ipar,ldpar,dpar)
    use utils
    implicit none
    character(32),      intent(in)                         :: nameinte
    integer,            intent(in)                         :: lipar,ldpar,i
    integer,            intent(in),    dimension(lipar)    :: ipar
    double precision,   intent(in)                         :: t,told,time,hstep
    double precision,   intent(out)                        :: y
    double precision,   intent(in),    dimension(ldpar)    :: dpar

    !local variables
    double precision :: CONTD5, CONTD8, CONTRA, CONTRAHAMPATH

    select case (nameinte)
        case ('dopri5')
            y = CONTD5(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
        !case ('radau5old')
        !    y = CONTR5(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
        case ('dop853')
            y = CONTD8(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
        case ('radau5','radau9','radau13','radau')
            y = CONTRA(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
        case ('radau5Hampath','radau9Hampath','radau13Hampath','radauHampath')
            y = CONTRAHAMPATH(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
        case default
            CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator...')
    end select

end subroutine denseOutput
