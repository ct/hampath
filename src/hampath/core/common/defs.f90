!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module defs
    use ModgestListe
        implicit none

!!******************************************************************************************!!
    !Initialization and solver choices for implicit runge kutta methods
    !On donne des valeurs par défaut pour ssolve, hampath, sfun et sjac
!>  \ingroup globalVariables
!!  \brief initialization choice for implicit runge kutta methods
!!  \date   2014
    character(32), save                                :: irkiG     = 'nodef'
!>  \ingroup globalVariables
!!  \brief solver choice for implicit runge kutta methods
!!  \date   2014
    character(32), save                                :: irksG     = 'nodef'

!!******************************************************************************************!!
    !Path of zeros during continuation : save or not
    !
!>  \ingroup globalVariables
!!  \brief File name to save homotopic path during execution
!!  \date   2009-2013
    character(32), save                                :: filenamepathg     = "nodef"
!>  \ingroup globalVariables
!!  \brief If 1, the path of zeros is saved in a file during continuation
!!  \date   2009-2013
    integer                                    ,  save :: doSavePathInFileg = -1


!!******************************************************************************************!!
    !Stop after a turning point or not during continuation
    !
!>  \ingroup globalVariables
!!  \brief If true, then hampath() stops after having encountered a turning point
!!  \date   2009-2013
    logical                                    ,  save :: stopAtTurningPtg  = .false.

!!******************************************************************************************!!
    !Some parameters for continuation process
    !
!>  \ingroup globalVariables
!!  \brief Maximum step size of homotopic parameter during homotopy
!!  \date   2017
    double precision                            , save :: msshpG            = -1d0
!>  \ingroup globalVariables
!!  \brief Maximum arclength for continuation
!!  \date   2015
    double precision                            , save :: msfG              = -1d0
!>  \ingroup globalVariables
!!  \brief Current tangent vector
!!  \date   2009-2013
    double precision, dimension(:), allocatable,  save :: tocg
!>  \ingroup globalVariables
!!  \brief Current dot product between two tagent vectors following
!!  \date   2009-2013
    double precision                           ,  save :: pdsg              = -1d0
!>  \ingroup globalVariables
!!  \brief Sign of the det for the right direction
!!  \date   2009-2013
    double precision                           ,  save :: sdetg             = -1d0
!>  \ingroup globalVariables
!!  \brief Sign of the det if bifurcation research
!!  \date   2009-2013
    double precision                           ,  save :: sdetbifg          = -1d0
!>  \ingroup globalVariables
!!  \brief Current value of the det of the augmented matrix
!!  \date   2009-2013
    double precision                           ,  save :: deterg            = -1d0
!>  \ingroup globalVariables
!!  \brief Previous tangent vector in hampath
!!  \date   2009-2013
    double precision, dimension(:), allocatable,  save :: tocoldg
!>  \ingroup globalVariables
!!  \brief Used by hampath to know if dopri5 is initializating
!!  \date   2009-2013
    integer                                    ,  save :: hinitg            = -1
!>  \ingroup globalVariables
!!  \brief Initial parameters: before homotopy
!!  \date   2009-2013
    double precision, dimension(:), allocatable,  save :: par0g
!>  \ingroup globalVariables
!!  \brief Final parameters wanted after homotopy
!!  \date   2009-2013
    double precision, dimension(:), allocatable,  save :: parfg
!>  \ingroup globalVariables
!!  \brief Dimension of the parameters
!!  \date   2009-2013
    integer                                    ,  save :: lparg             = -1
!>  \ingroup globalVariables
!!  \brief Dimension of homotopic parameters
!!  \date   2009-2013
    integer                                    ,  save :: nparhomg          = -1
!>  \ingroup globalVariables
!!  \brief Indices of homotopic parameters
!!  \date   2009-2013
    integer,          dimension(:), allocatable,  save :: iparhomg

    !      double precision                           ,  save :: t0g     = -1d0
    !      double precision                           ,  save :: tfg     = -1d0
    !      integer                                    ,  save :: ntfg    = -1
    !      integer                                    ,  save :: neg     = -1

!!******************************************************************************************!!
    !display parameters
    !
!>  \ingroup globalVariables
!!  \brief If equals n then print lambda at each mod(n) steps
!!  \date   2009-2013
    integer, save          :: dispiterg  = -1
!>  \ingroup globalVariables
!!  \brief If equals 1 then print results
!!  \date   2009-2013
    integer, save          :: displayg   = -1
    logical, save          :: isFirstDisplayG

!!******************************************************************************************!!
    !Id to classify from which method, the integrator is called
    !
!>  \ingroup globalVariables
!!  \brief Id for sfun()
!!  \date   2009-2013
    integer, parameter :: idSFung = 0
!>  \ingroup globalVariables
!!  \brief Id for sjac()
!!  \date   2009-2013
    integer, parameter :: idSJacg = 1
!>  \ingroup globalVariables
!!  \brief Id for exphvfun()
!!  \date   2009-2013
    integer, parameter :: idExpg  = 2
!>  \ingroup globalVariables
!!  \brief Id for expdhvfun()
!!  \date   2009-2013
    integer, parameter :: idExpdg = 3
!>  \ingroup globalVariables
!!  \brief Id for hampath()
!!  \date   2009-2013
    integer, parameter :: idHamg  = 4
!>  \ingroup globalVariables
!!  \brief Id for ssolve()
!!  \date   2015
    integer, parameter :: idSsog  = 5

!!******************************************************************************************!!
    !Integrator name for each method which use it
    !
!>  \ingroup globalVariables
!!  \brief Integrator choice for sfun()
!!  \date   2009-2013
    character(32), save    :: integsfung      = "nodef"
!>  \ingroup globalVariables
!!  \brief Integrator choice for sjac()
!!  \date   2009-2013
    character(32), save    :: integeqvarg     = "nodef"
!>  \ingroup globalVariables
!!  \brief Integrator choice for hampath()
!!  \date   2009-2013
    character(32), save    :: integhamg       = "nodef"
!>  \ingroup globalVariables
!!  \brief Integrator choice for exphvfun()
!!  \date   2009-2013
    character(32), save    :: integexphvfung  = "nodef"
!>  \ingroup globalVariables
!!  \brief Integrator choice for expdhvfun()
!!  \date   2009-2013
    character(32), save    :: integexpdhvfung = "nodef"

!!******************************************************************************************!!
    !derivative methods
    !
!>  \ingroup globalVariables
!!  \brief Derivative method choice
!!  \date   2009-2013
    character(32), save    :: derivativeg     = "nodef"

!!******************************************************************************************!!
    !tspan for exphvfun or expdhvfun
    !
!>  \ingroup globalVariables
!!  \brief The integration grid
!!  \date   2009-2013
    type(TLISTE), save     :: grilleg ! pb si on parallelise lors d'un tir multiple si integrateur a pas fixe il semblerait

!!******************************************************************************************!!
    !Parameters for Powell solver (hybrd/hybrj)
    !
!>  \ingroup globalVariables
!!  \brief maxfev for ssolve()
!!  \date   2009-2013
    integer, save           :: maxfevg = -1
!>  \ingroup globalVariables
!!  \brief xtol for ssolve()
!!  \date   2009-2013
    double precision, save  :: xtolg   = -1d0
!>  \ingroup globalVariables
!!  \brief Root solver method choice
!!  \date   2016
    character(32), save     :: rootmethodg = "nodef"

!!******************************************************************************************!!
    !parameters for integration
    !
!>  \ingroup globalVariables
!!  \brief maxsteps of ODE integrator
!!  \date   2009-2013
    integer,          save    :: maxstepsg = -1
!>  \ingroup globalVariables
!!  \brief Absolute tolerance of ODE integrator
!!  \date   2009-2013
    double precision, save    :: atolg     = -1d0
!>  \ingroup globalVariables
!!  \brief Relative tolerance of ODE integrator
!!  \date   2009-2013
    double precision, save    :: rtolg     = -1d0
!>  \ingroup globalVariables
!!  \brief Maximum step size
!!  \date   2009-2013
    double precision, save    :: hmaxg     = -1d0

!!******************************************************************************************!!
    !parameters for continuation integration
    !
!>  \ingroup globalVariables
!!  \brief maxsteps of ODE integrator for hampath() method
!!  \date   2009-2013
    integer,          save    :: maxstepscontg  = -1
!>  \ingroup globalVariables
!!  \brief Absolute tolerance of ODE integrator for hampath() method
!!  \date   2009-2013
    double precision, save    :: atolcontg      = -1d0
!>  \ingroup globalVariables
!!  \brief Relative tolerance of ODE integrator for hampath() method
!!  \date   2009-2013
    double precision, save    :: rtolcontg      = -1d0
!>  \ingroup globalVariables
!!  \brief sfun maximal norm for hampath()
!!  \date   2009-2013
    double precision, save    :: NormeSfunMaxg  = -1d0
!>  \ingroup globalVariables
!!  \brief Maximum step size for hampath()
!!  \date   2009-2013
    double precision, save    :: hmaxcontg      = -1d0
!>  \ingroup globalVariables
!!  \brief Maximum iteration during correction for hampath()
!!  \date   2016
    integer, save             :: maxitercorrg   = -1

!!******************************************************************************************!!
    !Parameters for dense output
    !
!>  \ingroup globalVariables
!!  \brief dense output tolerance to compute last homotopic parameter
!!  \date   2009-2013
    double precision, save    :: densetolg      = -1d0
!>  \ingroup globalVariables
!!  \brief relative tolerance used to check if the homotopic parameter still evolve
!!  \date   2009-2013
    double precision, save    :: densetolrelg   = -1d0

   end module defs
