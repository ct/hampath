!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  \ingroup expdhvfunPackage
!!  \brief   Computes the (right) chronological exponential of the
!!           variational system associated to hv.
!!
!!  \param[in]  n       state dimension
!!  \param[in]  k       number of jacobi fields
!!  \param[in]  z0      initial flow
!!  \param[in]  dz0     initial Jacobi fields
!!  \param[in]  npar    number of optional parameters
!!  \param[in]  par     optional parameters
!!  \param[in]  t0      initial time
!!  \param[in]  tf      final time
!!  \param[in]  ninfos  dimension of infos
!!  \param[in]  nbarc   number of arcs
!!  \param[in]  ti      ti = [t0 t1 .. t_nbarc-1 tf], where t0 is the initial time and tf the final time.
!!
!!  \param[out] infos   information on the integration process
!!                          infos(1) = flag : should be 1
!!                          infos(2) = nfev ; number of function evaluations
!!  \param[out] dimeT   Number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine expdhvfun(n,k,z0,dz0,npar,par,t0,tf,nbarc,ti,ninfos,infos,dimeT)
    use utils
    use defs
    use adds
    implicit none
    integer,            intent(in)                     :: n,nbarc,ninfos
    integer,            intent(in)                     :: k
    integer,            intent(in)                     :: npar
    double precision,   intent(in)                     :: t0
    double precision,   intent(in)                     :: tf
    double precision,   intent(in), dimension(nbarc+1) :: ti
    double precision,   intent(in), dimension(npar)    :: par
    double precision,   intent(in), dimension(2*n)     :: z0
    double precision,   intent(in), dimension(2*n,k)   :: dz0
    integer,            intent(out)                    :: infos(ninfos)
    integer,            intent(out)                    :: dimeT


    !local variables
    integer                                         :: neq, nparaux
    double precision                                :: tin
    double precision                                :: tfin
    double precision, dimension(npar+3)             :: paraux
    double precision, dimension(2*n*k,1)            :: aux
    double precision, dimension(2*n*(k+1),nbarc+1)  :: dzi
    double precision, dimension(2*n*(k+1))          :: dzin
    integer                                         :: arc,i,j,inc,infosaux(ninfos),deb,fin
    double precision, dimension(:), allocatable     :: tspan

    call getArea(t0,tf,nbarc,ti,deb,fin,inc)

    !expdhvfun can be called only on one arc because we do not deal with jump on the jacobi equations
    if(fin.ne.deb)then
        CALL printandstop('Error: you can call expdhvfun only along one arc.')
    end if

    allocate(tspan(inc*(fin-deb)+2))
    call getTspan(t0,tf,nbarc,ti,deb,fin,inc,tspan)

    neq = 2*n*k+2*n
    dzi  = 0d0
    dzin = 0d0
    !dz0 is converted into a vector and put at the first element of dzi
    !dzi will contain dz for each instant of tspan
    dzi(1:2*n,1) = z0
    aux = reshape(dz0,(/2*n*k,1/))
    dzi(2*n+1:neq,1) = aux(:,1)

    nparaux        = npar+3
    paraux(1:npar) = par
    paraux(npar+2) = dble(n)
    paraux(npar+3) = dble(k)

    infos(1) = 1 !flag
    j = 1
    infos(2) = 0
    !it is written like this but actually deb=fin, so there is only one step
    do i=deb,fin,inc
        arc = i
        paraux(npar+1) = dble(arc)
        !integration
        tin  = tspan(j)
        tfin = tspan(j+1)
        dzin = dzi(:,j)

        call odeexpdhv(neq,dzin,paraux,nparaux,2*n,tin,tfin,dzi(:,j+1),ninfos,infosaux)
        if(infosaux(1).ne.1)then !gestion du flag
            if(infos(1).eq.1)then
                infos(1) = infosaux(1) !if there is even a single integration which went wrong, it is indicated to flag
            end if
        end if
        infos(2) = infos(2) + infosaux(2)
        j = j + 1
    end do
    dimeT = LONGUEUR(etatsdhvG)

    deallocate(tspan)

end subroutine expdhvfun
