!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!!
!!     Subroutine getstatesdhv
!!
!>     @ingroup expdhvfunPackage
!!     @brief  Transforms the list etatsdhvG into an array for matlab dealing. Then remove the lists etatsdhvG and grilleG.
!!        @param[in]    n        State dimension
!!        @param[in]    k        Number of jacobi fields
!!        @param[in]    dimeT    Dimension of Ts (number of steps from expdhvfun)
!!
!!        @param[out]    Ts     Times of integration step from expdhvfun
!!        @param[out]    zs     Flow at each integration step
!!        @param[out]    dzs    Jacobi fields at each integration step
!!                  step from expdhvfun
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
Subroutine getstatesdhv(n,k,dimeT,Ts,zs,dzs)
    use ModGestListe
    use adds
    use defs
    implicit none
    integer,                                    intent(in)  :: n
    integer,                                    intent(in)  :: k
    integer,                                    intent(in)  :: dimeT
    double precision, dimension(dimeT),         intent(out) :: Ts
    double precision, dimension(2*n,dimeT),     intent(out) :: zs
    double precision, dimension(2*n,dimeT*k),   intent(out) :: dzs

    !local variables
    integer                                 :: i
    double precision,dimension(2*n)         :: z
    double precision,dimension(2*n,k)       :: dz
    double precision                        :: time
    double precision, dimension(2*n*(k+1))  :: val

    do i=1,dimeT
      call EXTRAIRE(time,2*n*(k+1),val,etatsdhvG)
      Ts(dimeT-i+1)                         = time
      z                                     = val(1:2*n)
      dz                                    = reshape(val(2*n+1:2*n*(k+1)),(/2*n,k/))
      dzs(:,k*(dimeT-i)+1:k*(dimeT-i+1))    = dz
      zs(:,dimeT-i+1)                       = z
    end do

    call DETRUIRE(etatsdhvG)
    call DETRUIRE(grilleG)

end subroutine getstatesdhv
