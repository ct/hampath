!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup ssolvePackage
!!     @brief   Solves systems of nonlinear equations of several variables.
!!        Uses hybrid Powell method (hybrj). fun(y)=0.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial guess: shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!
!!        @param[out]    ysol   Solution : y solution
!!        @param[out]    ssol   Solution : sfun(ysol)
!!        @param[out]    nfev   Number of sfun evaluation
!!        @param[out]    njev   Number of jacfun evaluation
!!        @param[out]    flag   solver output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine ssolve(ny,y0,npar,par,ysol,ssol,nfev,njev,flag)
    use defs
    use utils
    implicit none
    integer,            intent(in)                      :: ny
    double precision,   intent(in),  dimension(ny)      :: y0
    integer,            intent(in)                      :: npar
    double precision,   intent(in),  dimension(npar)    :: par
    double precision,   intent(out), dimension(ny)      :: ysol
    double precision,   intent(out), dimension(ny)      :: ssol
    integer,            intent(out)                     :: nfev
    integer,            intent(out)                     :: njev
    integer,            intent(out)                     :: flag

    external sfunjac

    !local variables
    integer                                 :: i
    CHARACTER(len=120)                      :: LINE

    !---------------------------------------!
    !--------------- DISPLAY ---------------!
    !---------------------------------------!
    if(displayG.eq.1) then
!            call myprint('---------------------',.false.)
!            call myprint('---------------------',.false.)
!            call myprint('---------------------',.false.)
!            call myprint('-------------',.true.)
        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '     Calls  |S(y)|                 |y|                    '
        call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
    end if
    !---------------------------------------!
    !---------------------------------------!

    select case (rootmethodg)
        case ('hybrj')

            !call the mere function
            call hybrjint(sfunjac,ny,y0,npar,par,xtolG,maxfevG,ysol,ssol,nfev,njev,flag)

        case default
            CALL printandstop('  ||| ERROR: Solving -> Unknown solver ' // rootmethodg)
    end select

    !---------------------------------------!
    !--------------- DISPLAY ---------------!
    !---------------------------------------!
    if(displayG.eq.1) then

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' Results of the shooting method: '; call myprint(LINE,.true.);

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' ysol  = ['
        call myprint(LINE,.false.)
        DO i = 1, ny
            if(i.eq.1)then
                WRITE (LINE,'(e23.15)') ysol(i)
                call myprint(LINE,.true.)
            elseif(i.eq.ny)then
                WRITE (LINE,'("          ",e23.15)') ysol(i)
                call myprint(LINE,.false.)
            else
                WRITE (LINE,'("          ",e23.15)') ysol(i)
                call myprint(LINE,.true.)
            end if
        END DO
        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' s     = ['
        call myprint(LINE,.false.)
        DO i = 1, ny
            if(i.eq.1)then
                WRITE (LINE,'(e23.15)') ssol(i)
                call myprint(LINE,.true.)
            elseif(i.eq.ny)then
                WRITE (LINE,'("          ",e23.15)') ssol(i)
                call myprint(LINE,.false.)
            else
                WRITE (LINE,'("          ",e23.15)') ssol(i)
                call myprint(LINE,.true.)
            end if
        END DO
        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(" nfev   = ",i0.1)') nfev;    call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(" njev   = ",i0.1)') njev;    call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(" flag   = ",i0.1)') flag;    call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)


        if(flag.eq.1)then
            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
            write(LINE,'(a)') 'Successfully completed : relative error between two consecutive'
            call myprint(LINE,.true.)
            write(LINE,'(a)') '                         iterates is at most xtol.'
        else
            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
            call myprint('  ||| WARNING: Solver (hybrj)  ->  ',.false.)
            select case (flag)
                case (0)
                    WRITE (LINE,'(a)') 'improper input parameters'
                case (1)
                case (2)
                    write(LINE,'(a)') 'number of calls to fcn'
                    call myprint(LINE,.true.)
                    write(LINE,'(a)') 'with iflag = 1 has reached maxfev'
                case (3)
                    write(LINE,'(a)') 'xtol is too small. no further'
                    call myprint(LINE,.true.)
                    write(LINE,'(a)') 'improvement in the approximate solution y is possible'
                case (4)
                    write(LINE,'(a)') 'iteration is not making good progress,'
                    call myprint(LINE,.true.)
                    write(LINE,'(a)') 'as measured by the improvement from the last five jacobian evaluations'
                case (5)
                    write(LINE,'(a)') 'iteration is not making good progress,'
                    call myprint(LINE,.true.)
                    write(LINE,'(a)') 'as measured by the improvement from the last ten iterations'
                case default

            end select
            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        end if
        call myprint(LINE,.true.)
    end if !display
    !---------------------------------------!
    !---------------------------------------!

end subroutine ssolve
