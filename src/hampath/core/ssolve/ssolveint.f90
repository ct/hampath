!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup ssolvePackage
!!     @brief  Interface of ssolve.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial guess: shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    ysol   Solution : y solution
!!        @param[out]    ssol   Solution : sfun(ysol)
!!        @param[out]    nfev   Number of sfun evaluation
!!        @param[out]    njev   Number of jacfun evaluation
!!        @param[out]    flag   solver output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine ssolveint(ny,y0,npar,par,ysol,ssol,nfev,njev,flag, &
                            ndw,niw,nsw,dw,iw,sw,lsw)
    use utils
    use defs
    use init
    implicit none
    integer,            intent(in)                      :: ny
    double precision,   intent(in),  dimension(ny)      :: y0
    integer,            intent(in)                      :: npar
    double precision,   intent(in),  dimension(npar)    :: par
    double precision,   intent(out), dimension(ny)      :: ysol
    double precision,   intent(out), dimension(ny)      :: ssol
    integer,            intent(out)                     :: nfev
    integer,            intent(out)                     :: njev
    integer,            intent(out)                     :: flag
    integer,            intent(in)                      :: ndw, niw, nsw
    double precision,   intent(in)                      :: dw(ndw)
    integer,            intent(in)                      :: iw(niw), lsw(nsw)
    character(32*nsw),  intent(in)                      :: sw

    !Initialize parameters
    call initOptions(ndw,niw,nsw,dw,iw,sw,lsw,idSsog)

    !call the mere function
    call ssolve(ny,y0,npar,par,ysol,ssol,nfev,njev,flag)

end subroutine ssolveint
