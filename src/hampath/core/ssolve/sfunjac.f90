!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup ssolvePackage
!!     @brief   Subroutine given to hybrj. Call sfun or sjac,
!!              depending on the value iflag.
!!        @param[in]    ny       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    y        tate and costate
!!        @param[in]    lparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Shooting function value
!!        @param[out]   fjac     Shooting function jacobian
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine sfunjac(ny,y,par,npar,nfev,fvec,fjac,ldfjac,iflag)
    use utils
    use defs
    implicit none
    integer,          intent(in)                            :: ny
    integer,          intent(in)                            :: npar
    integer,          intent(in)                            :: ldfjac
    integer,          intent(in)                            :: nfev
    double precision, intent(in), dimension(npar)           :: par
    double precision, intent(in), dimension(ny)             :: y
    double precision, intent(inout), dimension(ny)          :: fvec
    double precision, intent(inout), dimension(ldfjac,ny)   :: fjac
    integer,          intent(inout)                         :: iflag

    !Local variables
    double precision   :: DNRM2
    CHARACTER(len=120) :: LINE

    ! iflag = 1 : function at y and return fvec. do not alter fjac.
    ! iflag = 2 : jacobian at y and return fjac. do not alter fvec.

    if (iflag == 1) then

        !Shooting Function evaluation
        fvec = 0d0
        call sfun(ny,y,npar,par,fvec)

        if(displayG.eq.1) then
            WRITE (LINE,'(    i10, e23.15, e23.15)') nfev, DNRM2(ny, fvec, 1), DNRM2(ny, y(1:ny), 1)
            call myprint(LINE,.true.)
        end if

    elseif(iflag == 2) then

        !Jacobian evaluation
        fjac = 0d0
        call sjac(ny,y,npar,par,0,-1d0,fjac)

    end if

end subroutine sfunjac
