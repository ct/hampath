!!********************************************************************
!!
!!     Subroutine conditionner
!!
!>     @ingroup hampathPackage
!!     @brief   Conditionning
!!        @param[in]    A   matrix (m,n) to conditionate
!!        @param[in]    m
!!        @param[in]    n
!!
!!        @param[out]   A conditionate matrix
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright Eclipse Public License
    Subroutine conditionner(A,m,n)
        implicit none
        integer, intent(in)                                 :: m, n
        double precision, dimension(m,n), intent(inout)     :: A

        !Local variables
        double precision, dimension(m)      :: S !m<n
        double precision                    :: dummyD
        integer                             :: dummyI = 1, LWORK, INFO, i, j
        double precision, dimension(5*m)    :: WORK
        double precision, dimension(m,n)    :: Aaux

        LWORK = 5*m

        Aaux = A

        call DGESVD( 'N', 'N', m, n, Aaux, m, S, dummyD, dummyI, dummyD, dummyI, WORK, LWORK, INFO )

        !write(*,*) 'm = ', m
        !write(*,*) 'S = ', S
        do i=1,m
            do j=1,n
                A(i,j) = A(i,j)/S(i)
            enddo
        enddo

    end subroutine conditionner

!!******************************************************************************************!!
! Written on 2009-2013                                                                      !!
! by Oivier Cots - Math. Institute, Bourgogne Univ / ENSEEIHT-IRIT / INRIA Sophia-Antipolis !!
!!******************************************************************************************!!
