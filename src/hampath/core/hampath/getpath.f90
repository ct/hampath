!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief  Transforms the list pathG into arrays for matlab dealing.
!!
!!     We extract all information and then we remove the list pathG and
!!     deallocate the initial and final paramters par0G and parfG,
!!     calling closePar(). The parameters at each step are retrieved by
!!     the method getPar() and zi and ti with the method undoc().
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    npar      Number of parameters
!!        @param[in]    dimepath  Number of steps from hampath
!!
!!        @param[out]   parout  Parameters at each integration step from hampath
!!        @param[out]   yout    y at each integration step
!!        @param[out]   viout   Tangent vector at each integration step
!!        @param[out]   sout    Arc length at each integration step
!!        @param[out]   dets    Det(S'(c(s)),c'(s)) at each integration step
!!        @param[out]   normS   |S(c(s))| at each integration step
!!        @param[out]   ps      <c'(s_old)|c'(s_new)> at each integration step
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine getpath(ny,npar,dimepath,parout,yout,viout,sout,dets,normS,ps)
    use ModGestListe
    use adds
    use utils
    use init
    implicit none
    integer,                                     intent(in)  :: ny,npar
    integer,                                     intent(in)  :: dimepath
    double precision, dimension(npar,dimepath),  intent(out) :: parout
    double precision, dimension(ny,dimepath),    intent(out) :: yout
    double precision, dimension(ny+1,dimepath) , intent(out) :: viout
    double precision, dimension(dimepath),       intent(out) :: sout
    double precision, dimension(dimepath),       intent(out) :: dets
    double precision, dimension(dimepath),       intent(out) :: normS
    double precision, dimension(dimepath),       intent(out) :: ps

    !local variables
    integer                                     :: i
    integer                                     :: dime
    double precision                            :: time
    double precision, dimension(2*(ny+1)+3)     :: valpath
    double precision, dimension(ny+1)           :: c
    double precision, dimension(ny)             :: y
    double precision, dimension(npar)           :: par
    double precision                            :: lambda

    dime = 2*(ny+1)+3

    do i=1,dimepath

        call EXTRAIRE(time,dime,valpath,pathG)
        c = valpath(1:ny+1)
        call undoc(ny,y,lambda,c)
        call getPar(lambda,npar,par)

        parout(:,dimepath-i+1)  = par
        yout(:,dimepath-i+1)    = y
        viout(:,dimepath-i+1)   = valpath(ny+1+1:2*(ny+1))
        sout(dimepath-i+1)      = time
        dets(dimepath-i+1)      = valpath(dime-2)
        normS(dimepath-i+1)     = valpath(dime-1)
        ps(dimepath-i+1)        = valpath(dime-0)

    end do

    call DETRUIRE(pathG)
    call closePar()

end subroutine getpath
