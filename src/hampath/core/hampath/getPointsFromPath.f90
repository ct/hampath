!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   get the N last points from the path of zeros and split
!!              them into different arrays
!!     @param[in]    ny     : length of shooting variables
!!     @param[in]    npar   : length of paramaters given to hfun and sfun
!!     @param[in]    N      : number of last points from path wanted
!!     @param[out]   times  : the N last arc length from the path of zeros
!!     @param[out]   ys     : the N last shooting variables
!!     @param[out]   pars   : the N last parameters
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
Subroutine getPointsFromPath(ny,npar,N,times,ys,pars)
    use ModGestListe
    use adds
    implicit none
    integer, intent(in)             :: ny, npar, N
    double precision, intent(out)   :: times(N), ys(ny,N), pars(npar,N)

    !local variables
    type(TNOEUD), pointer   :: P
    integer                 :: i,j,dime
    double precision        :: lambda

    if(N.gt.0)then
        dime = GET_VALEUR_DIM(pathG)
        do j=1,N
            if(j.eq.1)then
                call GET_NOEUD_SUIVANT(P,pathG)
            else
                call GET_NOEUD_SUIVANT(P)
            endif
            times(j) = GET_TIME(P)
            do i=1,ny
                ys(i,j) = GET_VALEUR(P,i,dime)
            end do
            lambda = GET_VALEUR(P,ny+1,dime)
            call getPar(lambda,npar,pars(:,j))
        end do
    end if

End Subroutine getPointsFromPath
