!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief  Interface for the continuation method.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial solution, ie "sfun(y0,options,par0) = 0"
!!        @param[in]    nparspan  Number of optional parameters
!!        @param[in]    mparspan  Size of homotopic parameters grid
!!        @param[in]    parspan   Parameters grid
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    dimepath    Number of steps
!!        @param[out]    flag        Integration output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine hampathint(ny,y0,nparspan,mparspan,parspan,  &
                            dimepath,flag,                      &
                            ndw,niw,nsw,dw,iw,sw,lsw)
    use adds
    use init
    use utils
    use defs
    implicit none
    integer,          intent(in)                               :: ny,nparspan,mparspan
    double precision, intent(in), dimension(ny)                :: y0
    double precision, intent(in), dimension(nparspan,mparspan) :: parspan
    integer,          intent(out)                              :: dimepath,flag
    integer,          intent(in)                               :: ndw, niw, nsw
    double precision, intent(in)                               :: dw(ndw)
    integer,          intent(in)                               :: iw(niw), lsw(nsw)
    character(32*nsw),intent(in)                               :: sw

    !local variables
    integer :: sfreached

    if(mparspan.ne.2)then
        call printandstop('Error in hampath call : parspan must have two columns!')
    end if

    !Initialize parameters
    call initOptions(ndw,niw,nsw,dw,iw,sw,lsw,idHamg)
    call initListe(idHamg)
    call initPar(nparspan,parspan(:,1),parspan(:,2))

    !call the mere function
    call hampath(ny,y0,nparspan,sfreached,dimepath,flag)

end subroutine hampathint
