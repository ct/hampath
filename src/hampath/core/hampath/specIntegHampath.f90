!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module specIntegHampath

    use rkpfi
    implicit none

    procedure(), private, pointer :: jac_mod
    procedure(), private, pointer :: fun_mod

contains

    subroutine set_fun_jac(sfun, sjac)
        interface
            Subroutine sfun(ny,y,npar,par,s)
                integer, intent(in)                             :: ny
                integer, intent(in)                             :: npar
                double precision,  dimension(ny),   intent(in)  :: y
                double precision,  dimension(npar), intent(in)  :: par
                double precision,  dimension(ny),   intent(out) :: s
            end Subroutine sfun
            Subroutine sjac(ny,y,npar,par,nl,lambda,fjac)
                integer, intent(in)                                 :: ny, nl
                integer, intent(in)                                 :: npar
                double precision,  dimension(ny),   intent(in)      :: y
                double precision,  dimension(npar), intent(in)      :: par
                double precision, intent(in)                        :: lambda
                double precision, intent(out), dimension(ny,ny+nl)  :: fjac
            end Subroutine sjac
        end interface
        fun_mod => sfun
        jac_mod => sjac
    end subroutine set_fun_jac

    Subroutine tangentvector(n1,t,c,toc,paraux,lparaux)
        use defs
        use utils
        implicit none
        integer,      intent(in)                            :: n1
        integer,      intent(in)                            :: lparaux
        double precision, intent(in)                        :: t
        double precision, intent(in),  dimension(n1)        :: c
        double precision, intent(in),  dimension(lparaux)   :: paraux
        double precision, intent(out), dimension(n1)        :: toc

        !local variables
        integer                               :: INFO,ny,npar,i,NB
        double precision, dimension(n1-1)     :: y
        double precision, dimension(lparG)    :: par
        double precision                      :: lambda,sensLambda
        double precision                      :: detR,detQ,deter,pds,norms
        double precision                      :: DDOT
        double precision, dimension(n1-1,n1)  :: A
        double precision, dimension(n1,n1-1)  :: AT
        double precision, dimension(n1,1)     :: e
        double precision, dimension(n1-1)     :: TAU,WORK
        double precision, dimension(1*32)     :: WORK2
        double precision                      :: zero, eps

        NB = 32 ! Optimal block size for LWORK for DORMQR

        eps  = epsilon(1d0)
        zero = eps

        npar     = lparG
        ny       = n1-1

        if(nparhomG.eq.1)then
            senslambda = par0G(iparhomG(1)) - parfG(iparhomG(1))
        else
            senslambda = -1d0
        end if

        call undoc(ny,y,lambda,c)
        call getPar(lambda,npar,par) ! On recupere le vecteur par avec la bonne valeur du parametre homotopique

        !AT = the transpose of the jacobian of S(y,lambda)
        call jac_mod(ny,y,npar,par,1,lambda,A)

        !call conditionner(A,n1-1,n1)

        AT = transpose(A)
        toc = 0

        !computation of AT=QR
        call DGEQRF( n1, n1-1, AT, n1, TAU, WORK, n1-1, INFO )

        !computation of toc
        e = 0d0
        e(n1,1) = 1d0
        call DORMQR( 'L', 'N', n1, 1, n1-1, AT, n1, TAU, e, n1, WORK2, 1*NB, INFO )
        toc = e(:,1)

        !computation of det(Q) and det(R) where AT=QR
        detQ = 0d0
        detR = 1d0
        do i=1,n1-1
            if(TAU(i)>0d0) then
                detQ = detQ + 1
            end if
            detR = detR * AT(i,i)
        end do
        detQ = -1**detQ
        deter = detQ*detR

        !check if we scan the curve in the right direction
        if(abs(sdetG).le.zero) then
            pds = 0d0
            if(abs(sign(1d0,toc(n1))-sign(1d0,sensLambda)).le.zero) then
                !not the right direction
                toc    = -1d0*toc
                sdetG  = -1d0*sign(1d0,deter)
                deter  = -1d0*deter
            else
                sdetG  =      sign(1d0,deter)
            end if
        else
            if(abs(sdetG-sign(1d0,deter)).gt.zero) then
                toc   = -1d0*toc
                deter = -1d0*deter
            end if
            pds = DDOT(n1,toc,1,tocoldG,1)
            if(pds.lt.0d0) then
                pds       = -1d0*pds
                toc       = -1d0*toc
                deter     = -1d0*deter
            end if
        end if

        select case (integhamG)
            case ('dopri5','dop853')
                if(hinitG.ne.1) then !this is because of dopri which calls tangentVector twice for initialization
                    !save current global values
                    tocG    = toc
                    pdsG    = pds
                    deterG  = deter
                end if
                hinitG = hinitG + 1
            case ('radau5Hampath','radau9Hampath','radau13Hampath','radauHampath')
                !save current global values
                tocG    = toc
                pdsG    = pds
                deterG  = deter
            case default
                CALL printandstop('  ||| ERROR in tangentVector...')
        end select

    end subroutine tangentvector

!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   derivative of tangentVector
!!     @param[in]   neq     dimension of y
!!     @param[in]   t       time
!!     @param[in]   y expdhvfun variable put in an array
!!     @param[in]   lparaux Number of optional parameters
!!     @param[in]   paraux  Optional parameters
!!
!!     @param[out]  val    derivative of tangentVector
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
      Subroutine dtangentVector(neq,t,y,val,paraux,lparaux)
        use utils
        use derivatives
        implicit none
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: lparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(lparaux) :: paraux
        double precision,   intent(in),  dimension(neq)     :: y
        double precision,   intent(out), dimension(neq,neq) :: val

        !local variables
        integer :: j
        double precision :: eps, fvec1(neq), fvec2(neq), ytemp(neq), h

        eps  = epsilon(1d0)

        ytemp = y
        call tangentvector(neq,t,ytemp,fvec2,paraux,lparaux)

        do j=1,neq
            h = dsqrt(eps*max(1.d-5,abs(y(j))))
            ytemp(j) = y(j) + h
            call tangentvector(neq,t,ytemp,fvec1,paraux,lparaux)
            ytemp(j) = y(j)
            val(:,j) = (fvec1 - fvec2)/h
        end do

   end subroutine dtangentVector

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   non linear equations to solve during steps of implicit schemes
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine phidezCont(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

!        external tangentVector
        call phidez(nz, Zc, fvec, iflag, fpar, lfpar, tangentvector)

    end subroutine phidezCont

!! -----------------------------------------------------------------------------
!!
!>     \ingroup hampathPackage
!!     \brief   call phidphidez
!!        @param[in]    nz       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    Zc       Unknowns
!!        @param[in]    lparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Function value
!!        @param[out]   fjac     Function jacobian
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine pdpCont(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

!        external tangentVector
!        external dtangentVector

        call phidphidez(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag,tangentVector,dtangentVector)

    end subroutine pdpCont

!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   implementation of solout
!!     @param[in]   Niter       Number of iterations
!!     @param[in]   told        Previous time
!!     @param[in]   neq         Dimension of y
!!     @param[in]   dpar        Double precision parameters
!!     @param[in]   ldpar       Dimension of dpar
!!     @param[in]   ipar        Integer parameters
!!     @param[in]   lipar       Dimension of ipar
!!     @param[in]   fun         RHS
!!     @param[in]   lfunpar     Dimension of funpar
!!     @param[in,out]   time    Current time
!!     @param[in,out]   y       Current point, can be changed by dense output
!!     @param[in,out]   funpar  Parameters given to fun, ie tangentVector
!!     @param[in,out]   irtrn   Flag
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
     Subroutine soloutCont(Niter,told,time,rtime,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        use defs
        use adds
        use savePathToFile
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: rtime
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,rtime,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: rtime
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !local variables
    double precision                 :: hstep, senslambda, lambdaf, lambda1, lambda2, ecart
    double precision                 :: lambda, lambdaTest, lambda_initial, lambda_courant
    double precision                 :: time_initial, t, t1, t2, told_initial, time_courant
    double precision, dimension(neq) :: toc, r, rtime_initial
    integer                          :: i, dime, fini, ny, npar
    type(TNOEUD), pointer            :: P
    double precision                 :: DNRM2
    character(32)                    :: nameinte
    CHARACTER(len=120)               :: LINE
    double precision                 :: zero, eps, norms
    double precision, dimension(neq-1)  :: y, s
    double precision, dimension(lparG)  :: par
    logical :: isfinished, islfreached

    ! turning point : tp
    double precision                 :: lambda_tp, time_tp, N_tp, delta_time_tp

    eps      = epsilon(1d0)
    zero     = eps

    nameinte = integhamG
    hstep    = time - told

    npar     = lparG
    ny       = neq-1

    time_initial  = time
    time_courant  = time_initial
    rtime_initial = rtime

    told_initial  = told

    t1       = told
    t2       = time_initial

    call denseOutput(nameinte,neq,t1,lambda1,told_initial,time_initial,hstep,lipar,ipar,ldpar,dpar)
    ! lambda1 : lambda du pas precedent
    lambda_initial = rtime_initial(neq) ! lambda courant
    lambda_courant = lambda_initial

    t        = time
    r        = rtime

!---------------------------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------------------------
    if(Niter.gt.1) then
        if(nparhomG.eq.1)then
            senslambda  = sign(1d0,par0G(iparhomG(1))-parfG(iparhomG(1)))
            lambdaf     = parfG(iparhomG(1))
        else
            senslambda  = -1d0
            lambdaf     = 1d0
        end if

        ! detect turning point
        ! attention si on a un turning point mais que le lambda ne decroit pas (en le supposant croissant)
        ! alors on detectera le turning point qu'au pas suivant
        if ((sign(1d0,(lambda1-lambda_initial)*senslambda).lt.0d0)) then
            ! on detecte un turning point

            ! on teste si entre les deux pas du turning point, on a franchi lambdaf
            ! si oui alors on n'a pas vraiment de turning point car il arrive apres lambdaf
            isfinished      = .FALSE.
            islfreached     = .FALSE. ! true si lambdaf est atteint
            i               = 1
            N_tp            = 10d0
            delta_time_tp   = (time_initial - told_initial) / N_tp
            time_tp         = told_initial
            do while (.not.isfinished)

                time_tp = time_tp + delta_time_tp
                call denseOutput(nameinte,neq,time_tp,lambda_tp,told_initial,time_initial,hstep,lipar,ipar,ldpar,dpar)

                ! test si lambda_tp apres lambdaf
                if((lambdaf-lambda1)*(lambdaf-lambda_tp).LE.0) then
                    islfreached = .TRUE.
                    isfinished  = .TRUE.
                    !write(LINE,'(a)') 'Lambda = '
                    !call myprint(LINE,.false.)
                    !write(LINE,'(e19.11)') lambda_tp
                    !call myprint(LINE,.true.)
                else
                    i = i + 1
                    if(i.eq.nint(N_tp))then
                        isfinished  = .TRUE.
                    end if
                end if

            end do

            ! si on a un turning point et que l'on a pas franchi lambdaf et que l'on doit s'arreter alors on met a jour
            ! irtrn
            ! si on a un turning point et que l'on a franchi lambdaf alors on met a jour lambda_courant
            if((.not.islfreached).and.(stopAtTurningPtG))then
                irtrn = -7
            end if
            if(islfreached)then
                lambda_courant = lambda_tp
                time_courant   = time_tp
            end if

        end if
    end if

    ! ----------------------------------------------------------------
    ! On ajoute les points si maxStepSizeHomPar > 0
    ! et on teste si on atteint lambdaf
    !
    isfinished = .FALSE.

    do while (.not.isfinished)

        if(msshpG.gt.0d0)then
            if(senslambda.lt.0d0) then
                lambdaTest = min(lambda1-senslambda*msshpG, lambdaf)
            else
                lambdaTest = max(lambda1-senslambda*msshpG, lambdaf)
            end if
        else
            lambdaTest = lambdaf
        end if

        !test if lambdaTest is reached
        if(((lambdaTest-lambda1)*(lambdaTest-lambda_courant).LE.0) .and. (Niter.gt.1)) then !lambdaTest reached

            t2 = time_courant
            t  = t1 + (t2-t1)/2
            call denseOutput(nameinte,neq,t,lambda,told_initial,time_initial,hstep,lipar,ipar,ldpar,dpar)

            if(sensLambda<0) then
                ecart =  lambdaTest-lambda
            else
                ecart = -lambdaTest+lambda
            end if

            !find the arc length t where lambda(t) = lambdaTest
            do while ((ecart>densetolG) .or. (ecart<0d0))
                if((lambda-lambdaTest)*(lambda1-lambdaTest)<0) then
                    t2 = t
                else
                    t1 = t
                    lambda1 = lambda
                end if
                t = t1 + (t2-t1)/2
                call denseOutput(nameinte,neq,t,lambda,told_initial,time_initial,hstep,lipar,ipar,ldpar,dpar)
                if(sensLambda<0) then
                    ecart =  lambdaTest-lambda
                else
                    ecart = -lambdaTest+lambda
                end if
            end do !end do while

            !compute the value of the vector at lambda
            do i=1,neq
                call denseOutput(nameinte,i,t,r(i),told_initial,time_initial,hstep,lipar,ipar,ldpar,dpar)
            end do

            ! Corrector step
            call undoc(ny,y,lambda,r)           ! on laisse ensuite lambdaTest car si c'est lambdaf on veut pas lambda mais lambdaf
            lambda = lambdaTest
            call getPar(lambda,npar,par)    ! we perform a correction for lambda = lambdaTest
            call correctorStepLambdaFixed(npar, par, lambda, ny, y, s) ! nx = ny, x = y
            call doc(ny,y,lambda,r)

            !this call permits to get the current global values required
            call tangentvector(neq,t,r,toc,funpar,lfunpar)

            if(abs(lambdaTest-lambdaf).lt.zero)then
                time  = t
                rtime = r
                irtrn = -1
                isfinished = .TRUE.
            end if

            t1      = t
            lambda1 = lambdaTest

        else

            t = time_initial
            r = rtime_initial

            ! Corrector step
            if(Niter.eq.1)then
                call undoc(ny,y,lambda,r)
                call getPar(lambda,npar,par) ! we perform a correction for lambda = lambda0
                call correctorStepLambdaFixed(npar, par, lambda, ny, y, s) ! nx = ny, x = y
                call doc(ny,y,lambda,r)
            else
                call correctorStep(npar, ny, r, s) ! nx = ny, y = r
            end if

            !ici t n'a pas chang\'e et vaut time qui lui m\^eme n'a pas a chang\'e
            rtime = r ! on met a jour apres la correction
            isfinished = .TRUE.

        end if !end lambdaf reached

        tocoldG = tocG
        if(abs(sdetG-sign(1d0,deterG)).gt.zero) then
        !we change the current sign of the determinant if we found a bifurcation
            sdetG   = -1d0*sdetG
        end if

        !we add the information to the path of zeros
        norms = DNRM2(ny, s, 1)
        call addpath(t,neq,r,tocG,deterG,norms,pdsG)

        ! We get the length of a value in the path
        dime = GET_VALEUR_DIM(pathG)

        !we display information at each step
        if((displayG.EQ.1) .and. (dispiterG.gt.0) .and. ((mod(Niter,dispiterG).eq.0) .or. (Niter.eq.1))) then
            WRITE (LINE,'(e19.11)') r(neq)
            call myprint(LINE,.false.)
            WRITE (LINE,'(e16.8)') t
            call myprint(LINE,.false.)
            WRITE (LINE,'(e16.8)') deterG !GET_VALEUR(pathG,dime-2,dime)
            call myprint(LINE,.false.)
            WRITE (LINE,'(e16.8)') norms !GET_VALEUR(pathG,dime-1,dime)
            call myprint(LINE,.false.)
            WRITE (LINE,'(e16.8)') pdsG !GET_VALEUR(pathG,dime,dime)
            call myprint(LINE,.false.)
            WRITE (LINE,'(e19.11)') DNRM2(neq-1, r(1:neq-1), 1)
            if(.not.isfinished)then
                call myprint(LINE,.false.)
                WRITE (LINE,'(a)') ' Additional point!'
            end if
            call myprint(LINE,.true.)
        end if

        ! Save the current path into files
        if(doSavePathInFileG.eq.1)then
            call savePathUpdate(neq-1,lparG,t,dime,GET_VALEUR(pathG,dime))
        end if

        !We call the mfun subroutine wich can be modified by hampath user
        call mfun(Niter,ny,npar,irtrn)
        if(irtrn.lt.0)then
            time  = t
            rtime = r
            isfinished = .TRUE.
        end if

    end do

    ! ----------------------------------------------------------------

    !we check if the norm of the shooting function is too big
    if(GET_VALEUR(pathG,dime-1,dime) .gt. NormeSfunMaxG)then
        irtrn = -5
    end if

    !if lambdaf is not reached and we have made at least 20 steps
    !we check if we are progressing or not
    if(irtrn.ge.0 .and. Niter.ge.20) then
        fini = 0
        i = 0
        call GET_NOEUD_SUIVANT(P,pathG)
        lambda1 = GET_VALEUR(P,neq,dime)
        !hampath stops if the last 10 steps are too close two y  wrt lambda
        do while((i.LE.10).AND.(fini.EQ.0))
            call GET_NOEUD_SUIVANT(P)
            lambda2 = GET_VALEUR(P,neq,dime)

                ecart=abs((lambda2-lambda1))/(abs(lambda1)-sqrt(eps))
                if(ecart.gt.densetolrelG)then
                    fini = 1
                end if

                lambda1 = lambda2
                i = i + 1
            end do
            if(fini.eq.0)then
                irtrn = -6
            end if
        end if

  end subroutine soloutCont

!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   Corrector step along the path following: modified Newton
!!     @param[in]       npar    Size of vector of parameters par
!!     @param[in]       ny      Size of shooting unknown
!!     @param[in,out]   r       Current point : r = (y, lambda)
!!     @param[out]      s       Shooting value at final r : s = S(r)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    subroutine correctorStep(npar, ny, r, s)
        use defs
        use utils
        implicit none
        integer,            intent(in)      :: ny, npar
        double precision,   intent(inout)   :: r(ny+1)
        double precision,   intent(out)     :: s(ny)

        !local variables
        double precision                  :: dr(ny+1), lambda, y(ny), ds(ny,ny+1), par(npar)
        double precision                  :: dsT(ny+1,ny), erreur, eps, tol, dnrm2, RT(ny,ny)
        integer                           :: INFO, nbiter, maxiter, i, NB, k
        double precision, dimension(ny)   :: TAU, WORK
        double precision, dimension(1*32) :: WORK2
        CHARACTER(len=120)  :: LINE

        ! Maximum iteration
        maxiter = maxitercorrg

        NB = 32 ! Optimal block size for LWORK for DORMQR

        ! Get y, lambda and par
        call undoc(ny,y,lambda,r)
        call getPar(lambda,npar,par)

        ! Compute sfun = s
        call fun_mod(ny,y,npar,par,s)

        nbiter = - 1

        if(maxiter.gt.0) then

            ! Compute sjac = ds
            ! We keep the same value of sjac for every iteration, ie we do some modified Newton
            call jac_mod(ny,y,npar,par,1,lambda,ds) ! 1 means we want the derivative wrt lambda

            ! Factorize ds
            dsT = transpose(ds)
            call DGEQRF( ny+1, ny, dsT, ny+1, TAU, WORK, ny, INFO )

            ! Parameters
            eps     = epsilon(1d0)
            tol     = sqrt(max(atolg,rtolg))
            nbiter  = 0

            iterationNewtonCorrector : DO

                ! On résout le système RT z = b avec b = -s, mais on place directement z dans dr(1:ny)
                dr(1+ny) = 0d0
                dr(1:ny) = -s
                CALL DTRSM( 'Left', 'Upper', 'transpose', 'Non-Unit', ny, 1, &
                           1d0, dsT, ny+1, dr, ny+1 )

                ! On calcule dr = Q (z, 0) qui ici vaut dr = Q (dr(1:ny), 0) = Q dr
                call DORMQR( 'L', 'N', ny+1, 1, ny, dsT , ny+1, TAU, dr, ny+1, WORK2, 1*NB, INFO )

!                CALL DTRSM( 'Left', 'Upper', 'transpose', 'Non-Unit', ny, 1, &
!                           1d0, dsT(1:ny,1:ny), ny, reshape(dr(1:ny),(/ny,1/)), ny )
!
!                ! On calcule dr = Q (z, 0)
!                call DORMQR( 'L', 'N', ny+1, 1, ny, dsT , ny+1, TAU, reshape(dr,(/ny+1,1/)), ny+1, WORK2, 1*NB, INFO )
!

                r       = r + dr
                erreur  = dnrm2(ny+1, dr, 1)/(dnrm2(ny+1, r, 1) + sqrt(eps))
                nbiter  = nbiter + 1

                ! On calcule sfun et récupère y et par
                call undoc(ny,y,lambda,r); call getPar(lambda,npar,par); call fun_mod(ny,y,npar,par,s)

            IF ((erreur.le.tol) .or. (nbiter.ge.maxiter)) EXIT iterationNewtonCorrector
            END DO iterationNewtonCorrector

        end if

        if(nbiter.eq.maxiter) then
            WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
            WRITE (LINE,'(a)') '  ||| WARNING: ';   call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' maximum iterations during correction reached!'; call myprint(LINE,.true.)
        end if

    end subroutine correctorStep

!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   Corrector step along the path following: modified Newton
!!     @param[in]       npar    Size of vector of parameters par
!!     @param[in]       par     Vector of parameters
!!     @param[in]       lambda  Current homotopic parameter
!!     @param[in]       ny      Size of shooting unknown
!!     @param[in,out]   y       Current point
!!     @param[out]      s       Shooting value at final y : s = S(y,par)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    subroutine correctorStepLambdaFixed(npar, par, lambda, ny, y, s)
        use defs
        use utils
        implicit none
        integer,            intent(in)      :: ny, npar
        double precision,   intent(in)      :: lambda, par(npar)
        double precision,   intent(inout)   :: y(ny)
        double precision,   intent(out)     :: s(ny)

        !local variables
        double precision                  :: dy(ny), ds(ny,ny)
        double precision                  :: dsT(ny,ny), erreur, eps, tol, dnrm2, RT(ny,ny)
        integer                           :: INFO, nbiter, maxiter, i, NB, k
        double precision, dimension(ny)   :: TAU, WORK
        double precision, dimension(1*32) :: WORK2
        CHARACTER(len=120)  :: LINE

        ! Maximum iteration
        maxiter = maxitercorrg

        NB = 32 ! Optimal block size for LWORK for DORMQR

        ! Compute sfun = s
        call fun_mod(ny,y,npar,par,s)

        nbiter = -1

        if(maxiter.gt.0) then

            ! Compute sjac = ds
            ! We keep the same value of sjac for every iteration, ie we do some modified Newton
            call jac_mod(ny,y,npar,par,0,lambda,ds) ! 0 means we do not want the derivative wrt lambda

            ! Factorize ds
            dsT = transpose(ds)
            call DGEQRF( ny, ny, dsT, ny, TAU, WORK, ny, INFO )

            ! Parameters
            eps     = epsilon(1d0)
            tol     = sqrt(max(atolg,rtolg))
            nbiter  = 0

            iterationNewtonCorrector : DO

                ! On résout le système RT z = b avec b = -s, mais on place directement z dans dy
                dy = -s
                CALL DTRSM( 'Left', 'Upper', 'transpose', 'Non-Unit', ny, 1, &
                           1d0, dsT, ny, dy, ny )

                ! On calcule dy = Q z qui ici devient dy = Q dy
                call DORMQR( 'L', 'N', ny, 1, ny, dsT , ny, TAU, dy, ny, WORK2, 1*NB, INFO )!

                y       = y + dy
                erreur  = dnrm2(ny, dy, 1)/(dnrm2(ny, y, 1) + sqrt(eps))
                nbiter  = nbiter + 1

                ! On calcule sfun
                call fun_mod(ny,y,npar,par,s)

            IF ((erreur.le.tol) .or. (nbiter.ge.maxiter)) EXIT iterationNewtonCorrector
            END DO iterationNewtonCorrector

        end if

        if(nbiter.eq.maxiter) then
            WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
            WRITE (LINE,'(a)') '  ||| WARNING: ';   call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' maximum iterations during correction reached!'; call myprint(LINE,.true.)
        end if

    end subroutine correctorStepLambdaFixed

end module specIntegHampath
