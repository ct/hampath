!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hampathPackage
!!     @brief   Performs the homotopy from (z00,lambda0) to (zf,lambdaf),
!!        then find possible bifurcations. Returns the number of
!!        steps during continuation, the number of bifurcations and
!!        flags telling if all went good.
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial solution, ie "sfun(y0,options,par0) = 0"
!!        @param[in]    npar      Number of optional parameters
!!
!!        @param[out]   sfreached  Equals 1 if sfmax has been reached
!!        @param[out]   dimepath   Number of steps
!!        @param[out]   flag       Integration output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine hampath(ny,y0,npar,sfreached,dimepath,flag)
    use utils
    use defs
    use adds
    use savePathToFile
    implicit none
    integer,          intent(in)                    :: ny,npar
    double precision, intent(in), dimension(ny)     :: y0
    integer,          intent(out)                   :: sfreached,dimepath,flag

    external sfun
    external sjac

    !local variables
    double precision, dimension(ny+1)   :: c0,cf
    double precision                    :: lambda,lambdaf,s0,sf
    integer                             :: nparaux
    double precision, dimension(0)      :: paraux
    double precision, dimension(ny)     :: yf
    double precision, dimension(npar)   :: parf
    double precision, dimension(ny)     :: shootf

    !allocate needed memory for some global parameters
    allocate(tocoldG(ny+1))                     !previous tangent vector
    allocate(tocG(ny+1))                        !current tangent vector

    tocoldG = 0d0; sdetG   = 0d0 ; hinitG  = 0  !used in tangentvector and soloutCont

    s0      = 0d0                               !initial arc length
    sf      = msfG                              !final arc length equals to maximum arc length

    !initialization of first homotopic parameter
    if(nparhomG.eq.1)then
        lambda = par0G(iparhomG(1))        !here we have an homotopy on a scalar parameter
    else
        lambda  = 0d0                      !here we have f(lambda,par0,parf) with lambda = 0 to 1 and par is a vector
    end if

    !making the initial vector : c(0) = (y0,lambda) in the generic case
    call doc(ny,y0,lambda,c0)

    !init print
    call printinit

    !init print in file
    call savePathInit

    !initialization of the parameters for hampath
    nparaux   = 0

    call odecont(sfun,sjac,ny+1,c0,paraux,nparaux,ny+1,s0,sf,cf,flag)

    if (sf >= msfG) then
        sfreached = 1
        flag = 0
    end if

    dimepath = LONGUEUR(pathG)

    !final display
    call undoc(ny,yf,lambdaf,cf)
    call getPar(lambdaf,npar,parf)
    call sfun(ny,yf,npar,parf,shootf)
    call printfinal(ny,yf,npar,parf,sf,msfG,lambdaf,shootf,dimepath,flag)

    !deallocate memory created at the beginning
    deallocate(tocoldG)
    deallocate(tocG)

end subroutine hampath

!! -----------------------------------------------------------------------------
!!
!>  @ingroup hampathPackage
!!  @brief   Intial priniting for hampath() method.
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine printinit()
    use utils
    use defs
    implicit none

    CHARACTER(len=120) :: LINE

    if((dispiterG.gt.0).AND.((displayG.EQ.-1).OR.(displayG.EQ.1))) then
!            WRITE (LINE,'(a19, $)') '-----------------';    call myprint(LINE,.false.)
!            WRITE (LINE,'(a16, $)') '--------------';       call myprint(LINE,.false.)
!            WRITE (LINE,'(a16, $)') '--------------';       call myprint(LINE,.false.)
!            WRITE (LINE,'(a16, $)') '--------------';       call myprint(LINE,.false.)
!            WRITE (LINE,'(a16, $)') '--------------';       call myprint(LINE,.false.)
!            WRITE (LINE,'(a19, $)') '-----------------';    call myprint(LINE,.false.)
!            WRITE (LINE,'(a19, $)') '-----------------';    call myprint(LINE,.false.)
!            WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') '  Homotopic param.   Arclength s     det(s)';   call myprint(LINE,.false.)
        WRITE (LINE,'(a)') '          |S(y,lambda)|   Inner product   |y|                '
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
    end if

end subroutine printinit

!! -----------------------------------------------------------------------------
!!
!>  @ingroup hampathPackage
!!  @brief   Final printing for hampath() method.
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    yf        Shooting variable at final homotopic step
!!        @param[in]    npar       Number of optional parameters
!!        @param[out]   parf       Final optional parameters
!!        \param[in]    sf         Final arc length
!!        \param[in]    msf        Maximum arc length
!!        \param[in]    lambdaf    Final homotopic parameter
!!        \param[in]    shootf     Final sfun() value at final point of the path of zeros
!!        @param[in]    dimepath   Number of steps
!!        @param[in]    flag       Integration output (should be 1)
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
subroutine printfinal(ny,yf,npar,parf,sf,msf,lambdaf,shootf,dimepath,flag)
    use utils
    implicit none
    integer,          intent(in)                    :: ny,npar
    double precision, intent(in), dimension(ny)     :: yf
    double precision, intent(in), dimension(npar)   :: parf
    double precision, intent(in)                    :: sf,msf,lambdaf
    double precision, intent(in), dimension(ny)     :: shootf
    integer,          intent(in)                    :: dimepath,flag

    !local variables
    integer             :: i
    CHARACTER(len=120)  :: LINE
    double precision    :: DNRM2

    if((displayG.eq.1).OR.(displayG.EQ.-3)) then

        if (sf >= msf) then
            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
            WRITE (LINE,'(a)')  ' ||| WARNING: Continuation integration ->'
            call myprint(LINE,.false.)
            WRITE (LINE,'(a)')  ' PROBLEM NEEDS A LARGER SF '
            call myprint(LINE,.true.)
        end if

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' Results of the homotopy:'; call myprint(LINE,.true.)
        if(flag.eq.1) then
            WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)
            WRITE (LINE,'(a)') ' Homotopy successfully completed !'
            call myprint(LINE,.true.)
            WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)
        end if
!            WRITE (LINE,'(a)') ' Remark: during the homotopy, the column corresponding to the norm'
!            call myprint(LINE,.false.)
!            WRITE (LINE,'(a)') ' of the shooting function is an indication of the real value.'
!            call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') ' The real value is given by sfun which is called at the end of the '
!            call myprint(LINE,.false.)
!            WRITE (LINE,'(a)') ' homotopy and given below.'
!            call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)

        !!
        WRITE (LINE,'(" steps              = ",i0.1)')  dimepath;    call myprint(LINE,.true.)
        WRITE (LINE,'(" flag               = ",i0.1)')      flag;    call myprint(LINE,.true.)
        WRITE (LINE,'(" sf                 =",e23.15)')      sf;    call myprint(LINE,.true.)
        WRITE (LINE,'(" lambdaf            =",e23.15)') lambdaf;    call myprint(LINE,.true.)
        WRITE (LINE,'(" |S(y_final)|       =",e23.15)') DNRM2(ny, shootf, 1);    call myprint(LINE,.true.)
        !!

        !!WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' y_final = ['
        call myprint(LINE,.false.)
        DO i = 1, ny
            WRITE (LINE,'(e23.15)') yf(i)
            call myprint(LINE,.false.)
        END DO
        WRITE (LINE,'(a)') ']'''
        call myprint(LINE,.true.)

        WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)
        WRITE (LINE,'(a)') ' par_final = ['
        call myprint(LINE,.false.)
        DO i = 1, npar
            WRITE (LINE,'(e23.15)') parf(i)
            call myprint(LINE,.false.)
        END DO
        WRITE (LINE,'(a)') ']'''
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') ''; call myprint(LINE,.true.)

    end if

end subroutine printfinal
