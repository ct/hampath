!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hybrdPackage
!!     @brief  hybrd interface.
!!     @param[in] fcn   function for one wants to find a zero
!!    \param[in] n      dimension of x
!!    \param[in] x      initial guess
!!    \param[in] xtol   tolerance
!!    \param[in] maxfev maximum number of evaluations of fcn
!!    \param[in] nfev   number of evaluations of fcn
!!    \param[in] xsol   solution
!!    \param[in] fxsol  evaluation of fcn at the solution
!!    \param[in] flag   should be 1
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
Subroutine hybrdint(fcn,n,x,xtol,maxfev,nfev,xsol,fxsol,flag,fpar,lfpar)
    implicit none
    integer,            intent(in)                    :: n, lfpar
    double precision,   intent(in), dimension(n)      :: x
    double precision,   intent(in)                    :: xtol
    double precision,   intent(inout)                 :: fpar(lfpar)
    integer,            intent(in)                    :: maxfev
    double precision,   intent(out), dimension(n)     :: xsol
    double precision,   intent(out), dimension(n)     :: fxsol
    integer,            intent(out)                   :: flag,nfev

    interface
        Subroutine fcn(n,x,fvec,iflag,fpar,lfpar)
        implicit none
        integer,            intent(in)                      :: n, lfpar
        double precision,   intent(in),  dimension(n)       :: x
        double precision,   intent(out), dimension(n)       :: fvec
        integer,            intent(inout)                   :: iflag
        double precision,   intent(inout),  dimension(lfpar)   :: fpar
        end Subroutine fcn
    end interface

    !local declarations for HYBRD
    integer                                     :: ml,mu,mode,nprint,ldfjac,lr,iflag
    double precision                            :: epsfcn,factor
    double precision, dimension(n)              :: diag,qtf,wa1,wa2,wa3,wa4
    double precision, dimension(n,n)            :: fjac
    double precision, dimension((n*(n+1))/2)    :: r

    !HYBRJ solver
    ml      = n-1
    mu      = n-1
    epsfcn  = 0d0
    diag    = 1d0
    mode    = 1
    factor  = 100
    nprint  = -1
    iflag   = -1
    ldfjac  = n
    nfev    = 0
    lr      = (n*(n+1))/2
!        r       = 0d0
!        qtf     = 0d0
!        wa1     = 0d0
!        wa2     = 0d0
!        wa3     = 0d0
!        wa4     = 0d0
!
!        fxsol   = 0d0
    xsol    = x

    call hybrd(fcn,n,xsol,fxsol,xtol,maxfev,ml,mu,epsfcn,diag,    &
                    mode,factor,nprint,iflag,nfev,fjac,ldfjac,r,lr,       &
                    qtf,wa1,wa2,wa3,wa4,fpar,lfpar)

    flag    = iflag

end subroutine hybrdint
