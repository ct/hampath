!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup sfunPackage
!!     @brief   integration interface for sfun
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      flag        information on the integration process, should be 1
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
Subroutine odesfun(neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag)
    use defs
    use specIntegClass
    use specIntegSfun
    implicit none
    integer,            intent(in)                      :: lfunpar
    integer,            intent(in)                      :: neq
    integer,            intent(in)                      :: ncpas
    double precision,   intent(in),  dimension(neq)     :: y0
    double precision,   intent(in),  dimension(lfunpar) :: funpar
    double precision,   intent(inout)                   :: tin
    double precision,   intent(inout)                   :: tout
    double precision,   intent(out), dimension(neq)     :: yf
    integer,            intent(out)                     :: flag

    !local variable
    integer :: flagAux(1)
    type(specInteg) :: spec

    spec%fun           => sfunrhs
    spec%dfun          => dsfunrhs
    spec%phidez        => phidezSfun
    spec%pdpdez        => pdpSfun
    spec%solout        => soloutSfun
    spec%choixsolout   = idSFunG

    call integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,1,flagAux)
    flag = flagAux(1)

end subroutine odesfun
