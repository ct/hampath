!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_exphv4sfun
    use defs
    use utils
    use init
    use adds
    use ModGestListe
    implicit none

    interface exphv
        module procedure exphv_simple_final_step
        module procedure exphv_simple_final_step_no_iarc
        module procedure exphv_multiple_steps
        module procedure exphv_multiple_steps_no_iarc
    end interface

    interface exphv_ds
        !module procedure exphv_d_tspan
        !module procedure exphv_d_tspan_z0
        !module procedure exphv_d_tspan_par
        module procedure exphv_d_tspan_z0_par
        module procedure exphv_d_tspan_z0_par_no_iarc
        !module procedure exphv_d_z0
        !module procedure exphv_d_z0_par
        !module procedure exphv_d_par
    end interface

    interface expdhv
        module procedure expdhv_simple_final_step
    end interface

    interface expdhv_ds
        module procedure expdhv_d_all
    end interface

    contains

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    ! Cette routine est appelable depuis sfun
    ! On ne peut intégrer que sur un seul arc à la fois
    subroutine exphv_simple_final_step(tspan,n,z0,iarc,npar,par,zf)
        implicit none
        integer, intent(in)             :: iarc, n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(out)   :: zf(2*n)

        !local variables
        integer             :: nparaux, flag, nt, nbarc
        double precision    :: t0, tf, zin(2*n), ti(2)
        double precision    :: paraux(npar+1)

        nt      = size(tspan)
        t0      = tspan(1)
        tf      = tspan(nt)

        zin     = z0

        nparaux                 = npar+1
        paraux(1:npar)          = par
        paraux(npar+1)          = dble(iarc)

        !On ne peut intégrer dans sfun que sur un seul arc à la fois
        nbarc   = 1
        ti      = (/t0, tf/)
        call initTspanTi(nt,tspan,nbarc,ti)

        call odesfun(2*n,zin,paraux,nparaux,2*n,t0,tf,zf,flag)

        !On supprime la grille qui a été utile lors de l'intégration
        !pour les schémas à pas fixe
        call DETRUIRE(grilleG)

    end subroutine exphv_simple_final_step

    subroutine exphv_simple_final_step_no_iarc(tspan,n,z0,npar,par,zf)
        implicit none
        integer, intent(in)             :: n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(out)   :: zf(2*n)

        ! local variable
        integer :: iarc

        iarc = 1
        call exphv_simple_final_step(tspan,n,z0,iarc,npar,par,zf)

    end subroutine exphv_simple_final_step_no_iarc

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    SUBROUTINE exphv_d_tspan_z0_par(tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
        implicit none
        integer, intent(in)             :: iarc, n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(in)    :: tspand(:), z0d(2*n), pard(npar)
        double precision, intent(out)   :: zf(2*n), zfd(2*n)

        !local variables
        integer :: nt

        nt      = size(tspan)

        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)

    END SUBROUTINE exphv_d_tspan_z0_par

    SUBROUTINE exphv_d_tspan_z0_par_no_iarc(tspan,tspand,n,z0,z0d,npar,par,pard,zf,zfd)
        implicit none
        integer, intent(in)             :: n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(in)    :: tspand(:), z0d(2*n), pard(npar)
        double precision, intent(out)   :: zf(2*n), zfd(2*n)

        !local variables
        integer :: nt, iarc

        nt      = size(tspan)
        iarc    = 1

        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)

    END SUBROUTINE exphv_d_tspan_z0_par_no_iarc


!----------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------
! expdhv
!
    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    ! Cette routine est appelable depuis sfun
    ! On ne peut intégrer que sur un seul arc à la fois
    subroutine expdhv_simple_final_step(tspan,n,k,z0,dz0,iarc,npar,par,zf,dzf)
        implicit none
        integer, intent(in)             :: iarc, n, npar, k
        double precision, intent(in)    :: tspan(:), z0(2*n), dz0(2*n,k), par(npar)
        double precision, intent(out)   :: zf(2*n), dzf(2*n,k)

        !local variables
        integer             :: nparaux, flag, nt, nbarc, neq
        double precision    :: t0, tf, dzin(2*n*k+2*n), dzout(2*n*k+2*n), ti(2)
        double precision    :: paraux(npar+3)

        nt      = size(tspan)
        t0      = tspan(1)
        tf      = tspan(nt)

        neq             = 2*n*k+2*n
        dzin(1:2*n)     = z0
        dzin(2*n+1:neq) = reshape(dz0,(/2*n*k/))

        nparaux         = npar+3
        paraux(1:npar)  = par
        paraux(npar+1)  = dble(iarc)
        paraux(npar+2)  = dble(n)
        paraux(npar+3)  = dble(k)

        !On ne peut intégrer dans sfun que sur un seul arc à la fois
        nbarc   = 1
        ti      = (/t0, tf/)
        call initTspanTi(nt,tspan,nbarc,ti)

        !call odeexpdhv_simple(neq,dzin,paraux,nparaux,2*n,t0,tf,dzout,flag)
        call odeexpdhv_simple(neq,dzin,paraux,nparaux,neq,t0,tf,dzout,flag)

        zf  = dzout(1:2*n)
        dzf = reshape(dzout(2*n+1:neq),(/2*n,k/))

        !On supprime la grille qui a été utile lors de l'intégration
        !pour les schémas à pas fixe
        call DETRUIRE(grilleG)

    end subroutine expdhv_simple_final_step

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    Subroutine odeexpdhv_simple(neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,flag)
        use defs
        use specIntegClass
        use specIntegExpdhv
        use specIntegSfun
        implicit none
        integer,            intent(in)                      :: lfunpar
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: ncpas
        double precision,   intent(in),  dimension(neq)     :: y0
        double precision,   intent(in),  dimension(lfunpar) :: funpar
        double precision,   intent(inout)                   :: tin
        double precision,   intent(inout)                   :: tout
        double precision,   intent(out), dimension(neq)     :: yf
        integer,            intent(out)                     :: flag

        !local
        integer :: flagAux(1)
        type(specInteg) :: spec

        spec%fun           => expdhvrhs
        spec%dfun          => dexpdhvrhs
        spec%phidez        => phidezExpdhv
        spec%pdpdez        => pdpExpdhv
        spec%solout        => soloutSfun
        spec%choixsolout   = idSFunG

        call integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,1,flagAux)
        flag = flagAux(1)

    end subroutine odeexpdhv_simple

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    subroutine expdhv_d_all(tspan,tspand,n,k,z0,z0d,dz0,dz0d,iarc,npar,par,pard,zf,zfd,dzf,dzfd)
        implicit none
        integer, intent(in)             :: iarc, n, npar, k
        double precision, intent(in)    :: tspan(:), z0(2*n), dz0(2*n,k), par(npar)
        double precision, intent(in)    :: tspand(:), z0d(2*n), dz0d(2*n,k), pard(npar)
        double precision, intent(out)   :: zf(2*n), dzf(2*n,k)
        double precision, intent(out)   :: zfd(2*n), dzfd(2*n,k)

        !local variables
        integer             :: nt

        nt      = size(tspan)

        call dexpdhv(nt,tspan,tspand,n,k,z0,z0d,dz0,dz0d,iarc,npar,par,pard,zf,zfd,dzf,dzfd)

    end subroutine expdhv_d_all

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    subroutine expdhv_d_all_finite(tspan,tspand,n,k,z0,z0d,dz0,dz0d,iarc,npar,par,pard,zf,zfd,dzf,dzfd)
        implicit none
        integer, intent(in)             :: iarc, n, npar, k
        double precision, intent(in)    :: tspan(:), z0(2*n), dz0(2*n,k), par(npar)
        double precision, intent(in)    :: tspand(:), z0d(2*n), dz0d(2*n,k), pard(npar)
        double precision, intent(out)   :: zf(2*n), dzf(2*n,k)
        double precision, intent(out)   :: zfd(2*n), dzfd(2*n,k)

        !local variables
        integer             :: nt, i, j
        double precision    :: zero, eps, h, DNRM2, dzf2(2*n,k), zf2(2*n), somme

        nt   = size(tspan)

!        call exphv_d_tspan_z0_par(tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)

        ! Diff finies
        eps  = epsilon(1d0)
        zero = eps

        somme = 0d0
        do i=1,nt
            if(abs(tspand(i)).gt.zero)then
                somme = somme + tspan(i)**2
            end if
        end do
        do i=1,2*n
            if(abs(z0d(i)).gt.zero)then
                somme = somme + z0(i)**2
            end if
        end do
        do i=1,npar
            if(abs(pard(i)).gt.zero)then
                somme = somme + par(i)**2
            end if
        end do
        do i=1,2*n
            do j=1,k
                if(abs(dz0d(i,j)).gt.zero)then
                    somme = somme + dz0(i,j)**2
                end if
            end do
        end do
        h    = dsqrt(eps*max(1.d-5,dsqrt(somme)))

        call expdhv_simple_final_step(tspan+h*tspand,n,k,z0+h*z0d,dz0+h*dz0d,iarc,npar,par+h*pard,zf2,dzf2)
        call expdhv_simple_final_step(tspan,n,k,z0,dz0,iarc,npar,par,zf,dzf)

        dzfd = (dzf2 - dzf) / h
        zfd  = (zf2  - zf)  / h

    end subroutine expdhv_d_all_finite

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    subroutine det(n, A, deter)
        implicit none
        integer, intent(in)             :: n
        double precision, intent(in)    :: A(n,n)
        double precision, intent(out)   :: deter

        !Local variables
        integer :: IPIV(n), INFO, i, j
        double precision :: M(n,n)

        M = A
        call dgetrf(n,n,M,n,IPIV,INFO)

        deter = 1d0
        do i=1,n
            deter = deter * M(i,i)
        end do

        do i=1,n
            if(IPIV(i).ne.i)then
                deter = -deter
            end if
        end do

    end subroutine det

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    subroutine det_ds(n, A, Ad, deter, deterd)
        implicit none
        integer, intent(in)             :: n
        double precision, intent(in)    :: A(n,n), Ad(n,n)
        double precision, intent(out)   :: deter, deterd

        !local variables
        integer             :: i
        double precision    :: AH(n,n), detAH, deterdBis, DNRM2, eps, zero

        eps  = epsilon(1d0)
        zero = eps

        call det(n, A, deter)

        AH      = A
        deterd  = 0d0
        do i=1,n
            if(DNRM2(n,Ad(:,i),1).gt.zero)then
                AH(:,i) = Ad(:,i)
                call det(n,AH,detAH)
                deterd  = deterd + detAH
                AH(:,i) = A(:,i)
            end if
        end do

    end subroutine det_ds

!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    !Diff finies
!    subroutine det_ds_diff(n, A, Ad, deter, deterd)
!        implicit none
!        integer, intent(in)             :: n
!        double precision, intent(in)    :: A(n,n), Ad(n,n)
!        double precision, intent(out)   :: deter, deterd
!
!        !local variables
!        integer             :: i, j
!        double precision    :: zero, eps, h, normeAd, deter2
!
!        ! Diff finies
!        eps  = epsilon(1d0)
!        zero = eps
!
!        normeAd = 0d0
!        do i = 1,n
!            do j=1,n
!                normeAd = normeAd + A(i,j)**2
!            end do
!        end do
!        normeAd = dsqrt(normeAd)
!
!        h    = dsqrt(eps*max(1.d-5, normeAd))
!
!        call det(n, A,          deter)
!        call det(n, A + h * Ad, deter2)
!
!        deterd = (deter2 - deter) / h
!
!    end subroutine det_ds_diff

!----------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    !----------------------------------------------------------------------------
    ! Cette routine n'est appelable que dans mfun.f90
    ! Il n'y a pas de calculs de dérivées pour tous les z
    subroutine exphv_multiple_steps(tspan,n,z0,iarc,npar,par,z)
        implicit none
        integer, intent(in)             :: iarc, n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(out)   :: z(:,:)

        !local variables
        integer             :: nt, nbarc, ninfos, dimeT, i
        double precision    :: t0, tf, ti(iarc+1), infos(2)
        double precision,dimension(:), allocatable   :: Ts
        double precision,dimension(:,:), allocatable :: Ys

        CHARACTER(len=120)  :: LINE

        nt      = size(tspan)

!        WRITE (LINE,'(a)') 'nt=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') dble(nt);  call myprint(LINE,.true.)

!        WRITE (LINE,'(a)') 'iarc=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') dble(iarc);  call myprint(LINE,.true.)


        t0      = tspan(1)
        tf      = tspan(nt)

!        WRITE (LINE,'(a)') 't0=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') t0;  call myprint(LINE,.true.)

!        WRITE (LINE,'(a)') 'tf=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') tf;  call myprint(LINE,.true.)

        if(nt.ne.size(z,2))then
            call printandstop('Error in exphv call (mfun.f90): check z dimension!');
        end if
        if(2*n.ne.size(z,1))then
            call printandstop('Error in exphv call (mfun.f90): check z dimension!');
        end if

        !Pour les schémas à pas fixe, il faut créer la grille d'intégration
        !Il faut faire attention car il y a beaucoup de variables globales
        !Et la création de la grilleG pourrait entrer en conflit s'il y a
        !des appels emboités avec exphvfun ou expdhvfun. Ici no soucis.
        ninfos  = 2
        nbarc   = iarc
        do i=1,iarc
            ti(i) = t0 - dble(iarc-i)*1d0
        end do
        ti(iarc+1) = tf

!        WRITE (LINE,'(a)') 'ti(1)=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') ti(1);  call myprint(LINE,.true.)

!        WRITE (LINE,'(a)') 'ti(2)=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') ti(2);  call myprint(LINE,.true.)

!        WRITE (LINE,'(a)') 'ti(3)=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') ti(3);  call myprint(LINE,.true.)

!        WRITE (LINE,'(a)') 'tf=';    call myprint(LINE,.false.)
!        WRITE (LINE,'(e19.11)') tf;  call myprint(LINE,.true.)


        call initListe(idExpg) !Crée etatsG et grilleG
        call initTspanTi(nt,tspan,nbarc,ti)
        call exphvfun(n,z0,npar,par,t0,tf,nbarc,ti,ninfos,infos,dimeT)

        !On récupère les points
        allocate(Ts(dimeT),Ys(2*n,dimeT))
        call getstates(n,dimeT,Ts,Ys)

        z(:,1)  = Ys(:,1)
        z(:,nt) = Ys(:,dimeT)
        if(nt.gt.2)then
            z(:,2:nt-1) = Ys(:,2:dimeT-1) ! dimeT=nt
        end if

        deallocate(Ts,Ys)

    end subroutine exphv_multiple_steps

    subroutine exphv_multiple_steps_no_iarc(tspan,n,z0,npar,par,z)
        implicit none
        integer, intent(in)             :: n, npar
        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
        double precision, intent(out)   :: z(:,:)

        ! local variable
        integer :: iarc

        iarc = 1
        call exphv_multiple_steps(tspan,n,z0,iarc,npar,par,z)

    end subroutine exphv_multiple_steps_no_iarc

!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_tspan(tspan,tspand,n,z0,iarc,npar,par,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: tspand(:)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: z0d(2*n), pard(npar)
!
!        z0d     = 0d0
!        pard    = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_tspan
!
!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_tspan_z0(tspan,tspand,n,z0,z0d,iarc,npar,par,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: tspand(:), z0d(2*n)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: pard(npar)
!
!        pard    = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_tspan_z0
!
!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_tspan_par(tspan,tspand,n,z0,iarc,npar,par,pard,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: tspand(:), pard(npar)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: z0d(2*n)
!
!        z0d     = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_tspan_par
!
!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_z0(tspan,n,z0,z0d,iarc,npar,par,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: z0d(2*n)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: tspand(size(tspan)), pard(npar)
!
!        tspand  = 0d0
!        pard    = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_z0
!
!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_z0_par(tspan,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: z0d(2*n), pard(npar)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: tspand(size(tspan))
!
!        tspand  = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_z0_par

!    !----------------------------------------------------------------------------
!    !----------------------------------------------------------------------------
!    SUBROUTINE exphv_d_par(tspan,n,z0,iarc,npar,par,pard,zf,zfd)
!        implicit none
!        integer, intent(in)             :: iarc, n, npar
!        double precision, intent(in)    :: tspan(:), z0(2*n), par(npar)
!        double precision, intent(in)    :: pard(npar)
!        double precision, intent(out)   :: zf(2*n), zfd(2*n)
!
!        !local variables
!        integer :: nt
!        double precision :: tspand(size(tspan)), z0d(2*n)
!
!        tspand  = 0d0
!        z0d     = 0d0
!
!        nt      = size(tspan)
!
!        call dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
!
!    END SUBROUTINE exphv_d_par

!    interface exphv_d_d
!        module procedure exphv_d_tspan
!        module procedure exphv_d_tspan_z0
!        module procedure exphv_d_tspan_par
!        module procedure exphv_d_tspan_z0_par
!        module procedure exphv_d_z0
!        module procedure exphv_d_z0_par
!        module procedure exphv_d_par
!    end interface

end module mod_exphv4sfun

