!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module specIntegSfun

    use specIntegExphv
    implicit none

contains

!! -----------------------------------------------------------------------------
!!
!>     @ingroup sfunPackage
!!     @brief   RHS of exphvfun and sfun.
!!       @param[in]    t       time
!!       @param[in]    nz      State and costate dimension
!!       @param[in]    z       State and adjoint state at t
!!       @param[in]    lparaux    Number of optional parameters
!!       @param[in]    paraux    Optional parameters
!!
!!       @param[out]   hv   The RHS
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    Subroutine sfunrhs(nz,t,z,hv,paraux,lparaux)
        use utils
        implicit none
        integer,      intent(in)                            :: nz
        integer,      intent(in)                            :: lparaux
        double precision, intent(in)                        :: t
        double precision, intent(in),  dimension(nz)        :: z
        double precision, intent(in),  dimension(lparaux)   :: paraux
        double precision, intent(out), dimension(nz)        :: hv

        call exphvrhs(nz,t,z,hv,paraux,lparaux)

    end subroutine sfunrhs

!! -----------------------------------------------------------------------------
!!
!>     @ingroup sfunPackage
!!     @brief   call to dexphvrhs
!!     @param[in]   nz     dimension of z
!!     @param[in]   t       time
!!     @param[in]   z       exphvfun variable
!!     @param[in]   lparaux Number of optional parameters
!!     @param[in]   paraux  Optional parameters
!!
!!     @param[out]  val    derivative of exphvfun RHS
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
      Subroutine dsfunrhs(nz,t,z,val,paraux,lparaux)
        use utils
        use derivatives
        implicit none
        integer,            intent(in)                      :: nz
        integer,            intent(in)                      :: lparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(lparaux) :: paraux
        double precision,   intent(in),  dimension(nz)      :: z
        double precision,   intent(out), dimension(nz,nz)   :: val

        call dexphvrhs(nz,t,z,val,paraux,lparaux)

   end subroutine dsfunrhs

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   call to phidezExphv
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!
!!  \author Olivier Cots
!!  \date   2016
!!  \copyright LGPL
!!
    subroutine phidezSfun(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

        call phidezExphv(nz, Zc, fvec, iflag, fpar, lfpar)

    end subroutine phidezSfun

!! -----------------------------------------------------------------------------
!!
!>     \ingroup exphvfunPackage
!!     \brief   call to pdpExphv
!!        @param[in]    nz       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    Zc       Unknowns
!!        @param[in]    lparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Function value
!!        @param[out]   fjac     Function jacobian
!!
!!  \author Olivier Cots
!!  \date   2016
!!  \copyright LGPL
!!
    subroutine pdpSfun(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

        call pdpExphv(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)

    end subroutine pdpSfun

!! -----------------------------------------------------------------------------
!!!>     @ingroup sfunPackage
!!     @brief   implementation of solout
!!     @param[in]   Niter       Number of iterations
!!     @param[in]   told        Previous time
!!     @param[in]   neq         Dimension of y
!!     @param[in]   dpar        Double precision parameters
!!     @param[in]   ldpar       Dimension of dpar
!!     @param[in]   ipar        Integer parameters
!!     @param[in]   lipar       Dimension of ipar
!!     @param[in]   fun         RHS
!!     @param[in]   lfunpar     Dimension of funpar
!!     @param[in,out]   time    Current time
!!     @param[in,out]   y       Current point, can be changed by dense output
!!     @param[in,out]   funpar  Parameters given to fun
!!     @param[in,out]   irtrn   Flag
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
    Subroutine soloutSfun(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !dummy subroutine
    end Subroutine soloutSfun

end module specIntegSfun
