!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  @ingroup hvfunPackage
!!  @brief   Defines the Hamiltonian vector field associated to H.
!!  \param[in] t      Time
!!  \param[in] n      State dimension
!!  \param[in] z      State and adjoint state at t
!!  \param[in] iarc   Index of the current arc
!!  \param[in] npar   Number of optional parameters
!!  \param[in] par    Optional parameters
!!
!!  \param[out] hv    Hamiltonian vector field associated to H
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine hvfun(t,n,z,iarc,npar,par,hv)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t
    double precision, dimension(2*n),  intent(in)   :: z
    double precision, dimension(npar), intent(in)   :: par
    double precision, dimension(2*n),  intent(out)  :: hv

    !Local variables
    double precision                                :: h, td, zd(2*n), pard(npar)
    integer                                         :: i

    ! hv = (dh/dp, -dh/dx)
    zd      = 0d0
    pard    = 0d0
    td      = 0d0
    DO i = 1, n
        zd(i+n) = 1.0D0
        call HFUN_DH(t, td, n, z, zd, iarc, npar, par, pard, H, hv(i))      !dh/dp
        zd(i+n) = 0.0D0

        zd(i  ) =-1.0D0
        call HFUN_DH(t, td, n, z, zd, iarc, npar, par, pard, H, hv(i+n))    !-dh/dx
        zd(i  ) = 0.0D0
    END DO

end subroutine hvfun
