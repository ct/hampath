!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  \ingroup hvfunPackage
!!  \brief  Interface to the function hvfun, which gives the vector field associated to H.
!!
!!  \param[in]    t        time
!!  \param[in]    n        State dimension
!!  \param[in]    z        State and adjoint state at t
!!  \param[in]    nbarc    Number of arcs
!!  \param[in]    ti       ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!  \param[in]    npar     Number of optional parameters
!!  \param[in]    par      Optional parameters
!!
!!  \param[out]   hv       Hamiltonian vector fields associated to H at each time t : hvs(:,i) = hv(t(i),z(:,i),par)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine hvfunint(t,n,z,nbarc,ti,npar,par,nt,hv)
    use utils
    implicit none
    integer,      intent(in)                             :: n, nbarc, nt, npar
    double precision, intent(in),  dimension(nt)         :: t
    double precision, intent(in),  dimension(nbarc+1)    :: ti
    double precision, intent(in),  dimension(2*n,nt)     :: z
    double precision, intent(in),  dimension(npar)       :: par
    double precision, intent(out), dimension(2*n,nt)     :: hv

    !local variables
    integer :: i, iarc

    !For each time t(i), get the index of the arc associated
    !and compute the value of the hamiltonian vector field
    do i=1,nt
        call getArc(t(i),nbarc,ti,iarc)                     !get the arc index
        call hvfun(t(i),n,z(:,i),iarc,npar,par,hv(:,i))    !get hvfun value
    end do

end subroutine hvfunint
