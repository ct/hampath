!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup dexpdhvPackage
!!     @brief   Derivative of the linearized of the exponential mapping w.r.t to
!!              initial time, final time, initial conditions and
!!              parameters.
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2017
!!  \copyright LGPL
!!
subroutine dexpdhv(nt,tspan,tspand,n,k,z0,z0d,dz0,dz0d,iarc,npar,par,pard,zf,zfd,dzf,dzfd)
    use defs
    use utils
    use init
    implicit none
    integer, intent(in)             :: iarc, n, npar, k, nt
    double precision, intent(in)    :: tspan(nt), z0(2*n), dz0(2*n,k), par(npar)
    double precision, intent(in)    :: tspand(nt), z0d(2*n), dz0d(2*n,k), pard(npar)
    double precision, intent(out)   :: zf(2*n), dzf(2*n,k)
    double precision, intent(out)   :: zfd(2*n), dzfd(2*n,k)

    !local variable
    double precision    :: hv0(2*n), hvf(2*n), dhv0(2*n), dhvf(2*n), Yinit(4*n*(1+k)), Yf(4*n*(1+k))
    double precision    :: t0, tf, ti(2), tdzero
    double precision    :: paraux(2*npar+3), pardzero(npar)
    integer             :: nparaux, flag, cpas, neq, nbarc, icur, i
    double precision    :: zero, eps

    eps  = epsilon(1d0)
    zero = eps

    tdzero   = 0d0
    pardzero = 0d0

    !On teste si il y dans tspand(i), i \in [2,nt-1], pour nt≥3,
    !une valeur différente de 0. Si c'est le cas, et si tspand(1) = tspand(nt) = 0
    !alors on indique une erreur.
    if(nt.ge.3)then
        if((sum(abs(tspand(2:nt-1))).gt.zero).and.(abs(tspand(1)).lt.zero).and.(abs(tspand(nt)).lt.zero))then
            call printandstop('Error in expdhv call from sfun.f90: ' //  &
                                'tspan(2:nt-1) can not contain '     //  &
                                'variables of the shooting function if tspan(1) and tspan(end) are fixed parameters!')
        end if
    end if

    !On crée la grille pour les éventuelles intégrations
    !avec un schéma à pas fixe
    t0      = tspan(1)
    tf      = tspan(nt)
    nbarc   = 1
    ti      = (/t0, tf/)
    call initTspanTi(nt,tspan,nbarc,ti)

    zf      = 0d0
    zfd     = 0d0
    dzf     = 0d0
    dzfd    = 0d0
    Yf      = 0d0
    neq     = 4*n*(1+k)

    nparaux = 2*npar+3
    paraux(1:npar) = par
    paraux(npar+1) = dble(iarc)
    paraux(npar+2) = dble(2*n)
    paraux(npar+3) = dble(k)
    paraux(npar+3+1:npar+3+npar) = pard

    !paraux = [par iarc nz k pard]

    ! z(t,t0,z0,    lambda)
    !dz(t,t0,z0,dz0,lambda)
    !

    !---------------------------------------------------------------------------------------------------------
    !On commence avec les dérivées par rapport au temps initial, à la condition initiale et aux paramètres

    !Initial condition
    icur = 1
    Yinit(icur:icur+2*n-1)      = z0;                               icur = icur + 2*n;
    Yinit(icur:icur+2*n*k-1)    = reshape(dz0,(/2*n*k/));           icur = icur + 2*n*k;
    call hvfun(t0,n,z0,iarc,npar,par,hv0)
    Yinit(icur:icur+2*n-1)      = -hv0 * tspand(1) + z0d;           icur = icur + 2*n;
    do i=1,k
        call dhvfun(t0,tdzero,n,z0,dz0(:,i),iarc,npar,par,pardzero,hv0,dhv0)
        Yinit(icur:icur+2*n-1)  = -dhv0 * tspand(1) + dz0d(:,i);    icur = icur + 2*n;
    end do

    !integrate variational equations
    cpas = 2*n*(k+1) ! seulement sur z, delta z
    call odedexpdhv(neq,Yinit,paraux,nparaux,cpas,t0,tf,Yf,flag)

    icur    = 1
    zf      = Yf(icur:icur+2*n-1);                          icur = icur + 2*n;
    dzf     = reshape(Yf(icur:icur+2*n*k-1),(/2*n,k/));     icur = icur + 2*n*k;
    zfd     = Yf(icur:icur+2*n-1);                          icur = icur + 2*n;
    dzfd    = reshape(Yf(icur:icur+2*n*k-1),(/2*n,k/));     icur = icur + 2*n*k;

    !La dérivée par rapport au temps final
    if(abs(tspand(nt)).gt.zero)then

        call hvfun(tf,n,zf,iarc,npar,par,hvf)
        zfd = zfd + hvf * tspand(nt)

        do i=1,k
            call dhvfun(tf,tdzero,n,zf,dzf(:,i),iarc,npar,par,pardzero,hvf,dhvf)
            dzfd(:,i)  = dzfd(:,i) + dhvf * tspand(nt);
        end do

    end if

    !On supprime la grille qui a été utile lors des intégrations
    !pour les schémas à pas fixe
    call DETRUIRE(grilleG)

end subroutine dexpdhv
