!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  @ingroup d2hvfunPackage
!!  @brief   Defines the derivative of the Hamiltonian vector field associated to H.
!!  \param[in] t      Time
!!  \param[in] n      State dimension
!!  \param[in] z      State and adjoint state at t
!!  \param[in] iarc   Index of the current arc
!!  \param[in] npar   Number of optional parameters
!!  \param[in] par    Optional parameters
!!
!!  \param[out] hv    Hamiltonian vector field associated to H
!!  \param[out] hvd   derivative of hv  : dhv(t,z,lambda).(dt0,dz0,dpar0)
!!  \param[out] hvdd  derivative of hvd : d2hv(t,z,lambda).((dt0,dz0,dpar0),(dt1,dz1,dpar1))
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
Subroutine d2hvfun(t, td1, td0, n, z, zd1, zd0, iarc, npar, par, &
                    pard1, pard0, hv, hvd, hvdd)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t, td0, td1
    double precision, dimension(2*n),  intent(in)   :: z, zd0, zd1
    double precision, dimension(npar), intent(in)   :: par, pard0, pard1
    double precision, dimension(2*n),  intent(out)  :: hv, hvd, hvdd

! A cause de l'option -nooption activity de tapenade, hvfun_dh est derivee \r a toutes les variables
! HVFUN_DH_DH(t, td1, td0, td0d, n, z, zd1, zd0, zd0d, iarc, npar, par, pard1, pard0, pard0d, hv, hvd0, hvd, hvdd)

!!  \param[out] hvd0   derivative of hv  : dhv(t,z,lambda).(dt1,dz1,dpar1)
!!  \param[out] hvdd  derivative of hvd : d2hv(t,z,lambda).((dt0,dz0,dpar0),(dt1,dz1,dpar1)) + dhv(t,z,lambda).zd0d

    double precision :: td0d, zd0d(2*n), pard0d(npar), hvd0(2*n)
    td0d = 0d0
    zd0d = 0d0
    pard0d = 0d0

    call HVFUN_DH_DH(t, td1, td0, td0d, n, z, zd1, zd0, zd0d, iarc, npar, par, &
        pard1, pard0, pard0d, hv, hvd0, hvd, hvdd)
    !call HVFUN_DH_DH(t, td1, td0, n, z, zd1, zd0, iarc, npar, par, pard1, pard0, hv, hvd, hvdd)

end subroutine d2hvfun

!! -----------------------------------------------------------------------------
!!
!>  @ingroup d2hvfunPackage
!!  @brief   Defines the derivative of the Hamiltonian vector field associated to H.
!!  \param[in] t      Time
!!  \param[in] n      State dimension
!!  \param[in] z      State and adjoint state at t
!!  \param[in] iarc   Index of the current arc
!!  \param[in] npar   Number of optional parameters
!!  \param[in] par    Optional parameters
!!
!!  \param[out] hv    Hamiltonian vector field associated to H
!!  \param[out] hvd0  derivative of hv  : dhv(t,z,lambda).(dt1,dz1,dpar1)
!!  \param[out] hvd   derivative of hv  : dhv(t,z,lambda).(dt0,dz0,dpar0)
!!  \param[out] hvdd  derivative of hvd : d2hv(t,z,lambda).((dt0,dz0,dpar0),(dt1,dz1,dpar1)) + dhv(t,z,lambda).zd0d
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2017
!!  \copyright LGPL
!!
Subroutine d2hvall(t, td1, td0, td0d, n, z, zd1, zd0, zd0d, iarc, npar, par, &
        pard1, pard0, pard0d, hv, hvd0, hvd, hvdd)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t, td0, td1, td0d
    double precision, dimension(2*n),  intent(in)   :: z, zd0, zd1, zd0d
    double precision, dimension(npar), intent(in)   :: par, pard0, pard1, pard0d
    double precision, dimension(2*n),  intent(out)  :: hv, hvd, hvdd, hvd0

! A cause de l'option -nooption activity de tapenade, hvfun_dh est derivee \r a toutes les variables
! HVFUN_DH_DH(t, td1, td0, td0d, n, z, zd1, zd0, zd0d, iarc, npar, par, pard1, pard0, pard0d, hv, hvd0, hvd, hvdd)

    call HVFUN_DH_DH(t, td1, td0, td0d, n, z, zd1, zd0, zd0d, iarc, npar, par, &
        pard1, pard0, pard0d, hv, hvd0, hvd, hvdd)
    !call HVFUN_DH_DH(t, td1, td0, n, z, zd1, zd0, iarc, npar, par, pard1, pard0, hv, hvd, hvdd)

end subroutine d2hvall

