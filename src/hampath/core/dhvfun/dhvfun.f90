!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  @ingroup dhvfunPackage
!!  @brief   Defines the derivative of the Hamiltonian vector field associated to H.
!!  \param[in] t      Time
!!  \param[in] n      State dimension
!!  \param[in] z      State and adjoint state at t
!!  \param[in] iarc   Index of the current arc
!!  \param[in] npar   Number of optional parameters
!!  \param[in] par    Optional parameters
!!
!!  \param[out] hv    Hamiltonian vector field associated to H
!!  \param[out] hvd   derivative of hv : dhv(t,z,lambda).(dt,dz,dl)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine dhvfun(t,td,n,z,zd,iarc,npar,par,pard,hv,hvd)
    implicit none
    integer, intent(in)                             :: n,npar,iarc
    double precision, intent(in)                    :: t, td
    double precision, dimension(2*n),  intent(in)   :: z, zd
    double precision, dimension(npar), intent(in)   :: par, pard
    double precision, dimension(2*n),  intent(out)  :: hv, hvd

    call HVFUN_DH(t, td, n, z, zd, iarc, npar, par, pard, hv, hvd)

end subroutine dhvfun
