!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief   integration interface for exphvfun
!!     \param[in]       neq         dimension of the state
!!     \param[in]       y0          initial point
!!     \param[in]       funpar      parameters given to fun during integration
!!     \param[in]       lfunpar     dimension of funpar
!!     \param[in]       ncpas       number of components of y for which we make a step control
!!     \param[in]       ninfos      dimension of infos
!!     \param[in,out]   tin         initial time
!!     \param[in,out]   tout        final time
!!     \param[out]      yf          final point
!!     \param[out]      infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!
!!  \author Olivier Cots
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine odeexphv(neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)
    use defs
    use specIntegClass
    use specIntegExphv
    implicit none
    integer,            intent(in)                      :: ninfos, lfunpar
    integer,            intent(in)                      :: neq
    integer,            intent(in)                      :: ncpas
    double precision,   intent(in),  dimension(neq)     :: y0
    double precision,   intent(in),  dimension(lfunpar) :: funpar
    double precision,   intent(inout)                   :: tin
    double precision,   intent(inout)                   :: tout
    double precision,   intent(out), dimension(neq)     :: yf
    integer,            intent(out)                     :: infos(ninfos)

    !local
    type(specInteg) :: spec

    spec%fun           => exphvrhs
    spec%dfun          => dexphvrhs
    spec%phidez        => phidezExphv
    spec%pdpdez        => pdpExphv
    spec%solout        => soloutExphv
    spec%choixsolout   = idExpG

    call integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)

end subroutine odeexphv
