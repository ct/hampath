!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module specIntegExphv

    use rkpfi
    implicit none

contains

!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief   RHS of exphvfun and sfun.
!!       @param[in]    t        time
!!       @param[in]    nz       State and costate dimension
!!       @param[in]    z        State and adjoint state at t
!!       @param[in]    nparaux  Number of optional parameters
!!       @param[in]    paraux   Optional parameters
!!
!!       @param[out]   hv   The RHS
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
    Subroutine exphvrhs(nz,t,z,hv,paraux,nparaux)
        use utils
        implicit none
        integer,      intent(in)                          :: nz
        integer,      intent(in)                          :: nparaux
        double precision, intent(in)                      :: t
        double precision, intent(in),  dimension(nz)      :: z
        double precision, intent(in),  dimension(nparaux) :: paraux
        double precision, intent(out), dimension(nz)      :: hv

        !local variables
        integer             :: npar, iarc
        double precision    :: par(nparaux-1)

        npar    = nparaux - 1
        iarc    = nint(paraux(npar+1))
        par     = paraux(1:npar)

        call hvfun(t,nz/2,z,iarc,npar,par,hv)

    end subroutine exphvrhs

!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief   derivative of RHS of exphvfun
!!     @param[in]   nz      dimension of z
!!     @param[in]   t       time
!!     @param[in]   z       exphvfun variable
!!     @param[in]   nhv     = nz
!!     @param[in]   nparaux number of optional parameters
!!     @param[in]   paraux  optional parameters
!!
!!     @param[out]  dhvdz   derivative of exphvfun RHS
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014-2016
!!  \copyright LGPL
!!
      Subroutine dexphvrhs(nz,t,z,dhvdz,paraux,nparaux)
        use utils
        use derivatives
        implicit none
        integer,            intent(in)                      :: nz, nparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(nparaux) :: paraux
        double precision,   intent(in),  dimension(nz)      :: z
        double precision,   intent(out), dimension(nz,nz)   :: dhvdz

        !local variables
        integer             :: npar, iarc, i, n
        double precision    :: par(nparaux-1), pard(nparaux-1), zd(nz), td, hv(nz)

        npar    = nparaux - 1
        iarc    = nint(paraux(npar+1))
        par     = paraux(1:npar)
        n       = nz/2
        td      = 0d0
        pard    = 0d0
        zd      = 0d0
        ! dhv/dz . zd
        do i=1,nz
            zd(i)   = 1d0
            call dhvfun(t,td,n,z,zd,iarc,npar,par,pard,hv,dhvdz(:,i))
            zd(i)   = 0d0
        end do

   end subroutine dexphvrhs

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   non linear equations to solve during steps of implicit schemes
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine phidezExphv(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

!        external exphvrhs

        call phidez(nz, Zc, fvec, iflag, fpar, lfpar, exphvrhs)

    end subroutine phidezExphv

!! -----------------------------------------------------------------------------
!!
!>     \ingroup exphvfunPackage
!!     \brief   call phidphidez (hybrj)
!!        @param[in]    nz       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    Zc       Unknowns
!!        @param[in]    lparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Function value
!!        @param[out]   fjac     Function jacobian
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine pdpExphv(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

!        external exphvrhs
!        external dexphvrhs

        call phidphidez(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag,exphvrhs,dexphvrhs)

    end subroutine pdpExphv

!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief   implementation of solout
!!     @param[in]   Niter       Number of iterations
!!     @param[in]   told        Previous time
!!     @param[in]   neq         Dimension of y
!!     @param[in]   dpar        Double precision parameters
!!     @param[in]   ldpar       Dimension of dpar
!!     @param[in]   ipar        Integer parameters
!!     @param[in]   lipar       Dimension of ipar
!!     @param[in]   fun         RHS
!!     @param[in]   lfunpar     Dimension of funpar
!!     @param[in,out]   time    Current time
!!     @param[in,out]   y       Current point, can be changed by dense output
!!     @param[in,out]   funpar  Parameters given to fun
!!     @param[in,out]   irtrn   Flag
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    Subroutine soloutExphv(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        use defs
        use adds
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !local variables
        character(32) :: nameinte

        nameinte    = integexphvfunG

        call addstatesgeneral(addstateshv,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)

  end subroutine soloutExphv

end module specIntegExphv
