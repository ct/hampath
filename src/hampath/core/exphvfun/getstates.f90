!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief  Transforms the list etatsG into an array and remove the list, for matlab dealing.
!!        @param[in]    n         State dimension
!!        @param[in]    dimeT     Dimension of Ts (number of steps from exphvfun)
!!
!!        @param[out]    Ts       Times of integration step from exphvfun
!!        @param[out]    Ys       Flow at each integration step from exphvfun
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
Subroutine getstates(n,dimeT,Ts,Ys)
    use ModGestListe
    use adds
    use defs
    implicit none
    integer,                                intent(in)  :: n
    integer,                                intent(in)  :: dimeT
    double precision,dimension(dimeT),     intent(out)  :: Ts
    double precision,dimension(2*n,dimeT), intent(out)  :: Ys

    !local variables
    integer                             :: i
    double precision                    :: time
    double precision, dimension(2*n)    :: val

    do i=1,dimeT
      call EXTRAIRE(time,2*n,val,etatsG)
      Ts(dimeT-i+1) = time
      Ys(:,dimeT-i+1) = val
    end do

    call DETRUIRE(etatsG)
    call DETRUIRE(grilleG)

end subroutine getstates
