!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup exphvfunPackage
!!     @brief  Interface of exphvfun.
!!
!!        @param[in]    n         State dimension
!!        @param[in]    z0        initial flow
!!        @param[in]    lpar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        @param[in]    nt        Dimension of tspan
!!        @param[in]    tspan     Grid time : [t0 t1 ... tf]
!!        @param[in]    nbarc     Number of arcs
!!        @param[in]    ti        Contains t0, tf and the intermediate times if any. 
!!                                ti = [t0 t1 .. t_{nbarc-1} tf]
!!        \param[in]    ninfos      dimension of infos
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]   infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!        @param[out]   dimeT    Number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine exphvfunint(n,z0,lpar,par,nt,tspan,nbarc,ti,ninfos,infos,dimeT,ndw,niw,nsw,dw,iw,sw,lsw)
    use init
    use adds
    implicit none
    integer,            intent(in)                     :: n,nbarc,ninfos
    integer,            intent(in)                     :: lpar
    integer,            intent(in)                     :: nt
    double precision,   intent(in), dimension(nt)      :: tspan
    double precision,   intent(in), dimension(nbarc+1) :: ti
    double precision,   intent(in), dimension(2*n)     :: z0
    double precision,   intent(in), dimension(lpar)    :: par
    integer,            intent(out)                    :: infos(ninfos)
    integer,            intent(out)                    :: dimeT
    integer,          intent(in)                       :: ndw, niw, nsw
    double precision, intent(in)                       :: dw(ndw)
    integer,          intent(in)                       :: iw(niw), lsw(nsw)
    character(32*nsw),intent(in)                       :: sw

    !Initialize parameters
    call initOptions(ndw,niw,nsw,dw,iw,sw,lsw,idExpg)
    call initListe(idExpg)
    call initTspanTi(nt,tspan,nbarc,ti)

    call exphvfun(n,z0,lpar,par,tspan(1),tspan(nt),nbarc,ti,ninfos,infos,dimeT)

end subroutine exphvfunint
