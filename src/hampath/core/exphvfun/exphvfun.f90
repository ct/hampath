!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  \ingroup exphvfunPackage
!!  \brief   Computes the (right) chronological exponential of the
!!              Hamiltonian vector field hv defined by h.
!!
!!  \param[in]  n       state dimension
!!  \param[in]  z0      initial flow
!!  \param[in]  npar    number of optional parameters
!!  \param[in]  par     optional parameters
!!  \param[in]  t0      initial time
!!  \param[in]  tf      final time
!!  \param[in]  nbarc   number of arcs
!!  \param[in]  ti      ti = [t0 t1 .. t_nbarc-1 tf], where t0 is the initial time and tf the final time.
!!  \param[in]  ninfos  dimension of infos
!!
!!  \param[out] infos   information on the integration process
!!                          infos(1) = flag : should be 1
!!                          infos(2) = nfev ; number of function evaluations
!!  \param[out] dimeT   number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine exphvfun(n,z0,npar,par,t0,tf,nbarc,ti,ninfos,infos,dimeT)
    use utils
    use defs
    use adds
    implicit none
    integer,            intent(in)                      :: n,nbarc,ninfos
    integer,            intent(in)                      :: npar
    double precision,   intent(in)                      :: t0
    double precision,   intent(in)                      :: tf
    double precision,   intent(in), dimension(nbarc+1)  :: ti
    double precision,   intent(in), dimension(2*n)      :: z0
    double precision,   intent(in), dimension(npar)     :: par
    integer,            intent(out)                     :: infos(ninfos)
    integer,            intent(out)                     :: dimeT

    !local variables
    double precision                            :: tin
    double precision                            :: tfin
    double precision, dimension(2*n)            :: zin
    double precision, dimension(npar+1)         :: paraux
    double precision, dimension(:), allocatable :: tspan
    integer                                     :: arc,i,j,infosaux(ninfos),deb,fin,inc,nparaux
    double precision, dimension(2*n,nbarc+1)    :: zi

    !get first and last arc
    call getArea(t0,tf,nbarc,ti,deb,fin,inc)

    allocate(tspan(inc*(fin-deb)+2))
    !tspan contains all instants in ti between t0 and tf
    call getTspan(t0,tf,nbarc,ti,deb,fin,inc,tspan)

    zin = z0

    nparaux         = npar+1
    zi(:,1)         = zin
    paraux(1:npar)  = par
    infos(1)        = 1 !flag
    j = 1
    infos(2) = 0
    do i=deb,fin,inc
        arc = i
        paraux(npar+1) = dble(arc) !we indicate in which arc we are
        !integration
        tin  = tspan(j)
        tfin = tspan(j+1)
        zin  = zi(:,j)

        call odeexphv(2*n,zin,paraux,nparaux,2*n,tin,tfin,zi(:,j+1),ninfos,infosaux)
        if(infosaux(1).ne.1)then !gestion du flag
            if(infos(1).eq.1)then
                infos(1) = infosaux(1) !if there is even a single integration which went wrong, it is indicated to flag
            end if
        end if
        infos(2) = infos(2) + infosaux(2)
        j = j + 1
    end do
    dimeT = LONGUEUR(etatsG)

    deallocate(tspan)

end subroutine exphvfun
