!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief   Derivative of the exponential mapping w.r.t to
!!              initial time, final time, initial conditions or
!!              paramters.
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015
!!  \copyright LGPL
!!
subroutine dexphv(nt,tspan,tspand,n,z0,z0d,iarc,npar,par,pard,zf,zfd)
    use defs
    use utils
    use init
    implicit none
    integer, intent(in)             :: iarc, n, npar, nt
    double precision, intent(in)    :: tspan(nt),  z0(2*n),  par(npar)
    double precision, intent(in)    :: tspand(nt), z0d(2*n), pard(npar)
    double precision, intent(out)   :: zf(2*n), zfd(2*n)

    !local variable
    double precision    :: hv0(2*n), hvf(2*n), Yinit(4*n), Yf(4*n)
    double precision    :: t0, tf, ti(2)
    double precision    :: paraux(2*npar+2)
    integer             :: nparaux, flag, cpas, neq, iszfcomputed, nbarc
    double precision    :: zero, eps

    eps  = epsilon(1d0)
    zero = eps

    !On teste si il y dans tspand(i), i \in [2,nt-1], pour nt≥3,
    !une valeur différente de 0. Si c'est le cas, et si tspand(1) = tspand(nt) = 0
    !alors on indique une erreur.
    if(nt.ge.3)then
        if((sum(abs(tspand(2:nt-1))).gt.zero).and.(abs(tspand(1)).lt.zero).and.(abs(tspand(nt)).lt.zero))then
            call printandstop('Error in exphv call from sfun.f90: ' //  &
                                'tspan(2:nt-1) can not contain '     //  &
                                'variables of the shooting function if tspan(1) and tspan(end) are fixed parameters!')
        end if
    end if

    !On crée la grille pour les éventuelles intégrations
    !avec un schéma à pas fixe
    t0      = tspan(1)
    tf      = tspan(nt)
    nbarc   = 1
    ti      = (/t0, tf/)
    call initTspanTi(nt,tspan,nbarc,ti)

    zfd     = 0d0
    Yf      = 0d0
    neq     = 4*n
    iszfcomputed = 0

    nparaux = 2*npar+2
    paraux(1:npar) = par
    paraux(npar+1) = dble(iarc)
    paraux(npar+2) = dble(2*n)
    paraux(npar+2+1:npar+2+npar) = pard

    !paraux = [par iarc nz pard]

    !z(t,t0,z0,lambda)
    !dz(0) = z0         for z(t)
    !dz(0) = z0d        for dz/dz0
    !dz(0) = -Hv(z0)    for dz/dt0
    !dz(0) = 0          for dz/dlambda
    !hvfun(tf) for dz/dt


    !On commence avec les dérivées par rapport au temps initial, à la condition initiale et aux paramètres
    if((abs(tspand(1)).gt.zero).or.(sum(abs(z0d)).gt.zero).or.(sum(abs(pard)).gt.zero))then

        !Initial condition
        Yinit(1:2*n) = z0

        call hvfun(t0,n,z0,iarc,npar,par,hv0)
        Yinit(2*n+1:neq) = -hv0 * tspand(1) + z0d ! dérivée par rapport au temps initial, resp. condition initiale

        !integrate variational equations
        cpas = 2*n
        call odesjac(neq,Yinit,paraux,nparaux,cpas,t0,tf,Yf,flag)

        zf  = Yf(1:2*n)
        zfd = Yf(2*n+1:neq) ! dz/dt0 * delta t_0 + dz/dz0 * delta z_0 + dz/dpar * delta par

        iszfcomputed = 1

    end if

    ! On doit nécessairement calculer zf
    if(iszfcomputed.eq.0)then

        nparaux        = npar+1
        paraux(1:npar) = par
        paraux(npar+1) = dble(iarc)

        call  odesfun(2*n,z0,paraux(1:nparaux),nparaux,2*n,t0,tf,zf,flag)

    end if

    !La dérivée par rapport au temps final
    if(abs(tspand(nt)).gt.zero)then

        call hvfun(tf,n,zf,iarc,npar,par,hvf)
        zfd = zfd + hvf * tspand(nt)

    end if

    !On supprime la grille qui a été utile lors des intégrations
    !pour les schémas à pas fixe
    call DETRUIRE(grilleG)

end subroutine dexphv
