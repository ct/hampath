!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief  Interface of sjac
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y         Shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        @param[in]    nipar     Number of index parameters
!!        @param[in]    ipar      Index of parameters for which the jacobian is computed
!!        @param[in]    nl        nl is 1 if we derivate the homotopic function and 0 if it is the shooting function.
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    fjac     Value of the shooting function
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine sjacint(ny,y,npar,par,nipar,ipar,nl,fjac,ndw,niw,nsw,dw,iw,sw,lsw)
    use utils
    use init
    implicit none
    integer,            intent(in)                          :: ny, nl
    double precision,   intent(in),  dimension(ny)          :: y
    integer,            intent(in)                          :: npar
    double precision,   intent(in),  dimension(npar)        :: par
    integer,            intent(in)                          :: nipar
    double precision,   intent(in),  dimension(nipar)       :: ipar
    double precision,   intent(out), dimension(ny,ny+nl)    :: fjac
    integer,            intent(in)                          :: ndw, niw, nsw
    double precision,   intent(in)                          :: dw(ndw)
    integer,            intent(in)                          :: iw(niw), lsw(nsw)
    character(32*nsw),  intent(in)                          :: sw

    !local variables
    double precision, dimension(npar)                       :: paraux
    integer, dimension(nipar)                               :: iparint
    integer :: i

    !Initialize parameters
    call initOptions(ndw,niw,nsw,dw,iw,sw,lsw,idSJacg)

    !call the mere function with initialization
    if(nl.eq.1)then !we derivate wrt to parameter and y
        do i=1,nipar
            iparint(i) = int(ipar(i))
        end do
        paraux = par
        paraux(iparint) = paraux(iparint) + 1d0 !this permit to tell hampath from which parameters we derivate

        call initpar(npar,par,paraux)           !we initialize par0G, parfG which permits hampath to be aware
                                                !from which parameters index it has to derivate

        if(nipar.eq.1)then
            !if there is just a scalar parameter, we give it to sjac
            call sjac(ny,y,npar,par,1,par(iparint(1)),fjac)
        else
            !if there is a vector of parameters, then we derivate at lambda = 0, ie at par0G
            call sjac(ny,y,npar,par,1,0d0,fjac)
        endif
        call closePar() !we free the memory allocated
    else !we derivate only wrt to y
        call sjac(ny,y,npar,par,0,0d0,fjac) !no additional parameters
    endif

end subroutine sjacint
