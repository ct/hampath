!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module specIntegSjac

    use rkpfi
    implicit none

contains

!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief   RHS of variational equations
!!     @param[in]   neq     State dimension
!!     @param[in]   t        time
!!     @param[in]   y        variable put in an array
!!     @param[in]   nparaux     Number of optional parameters
!!     @param[in]   paraux     Optional parameters
!!
!!     @param[out]  val    RHS of variational equations
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
        Subroutine dexpzirhs(neq,t,y,val,paraux,nparaux)
            use defs
            use utils
            use derivatives
            implicit none
            integer,          intent(in)                        :: neq
            integer,          intent(in)                        :: nparaux
            double precision, intent(in),  dimension(nparaux)   :: paraux
            double precision, intent(in)                        :: t
            double precision, intent(in),  dimension(neq)       :: y
            double precision, intent(out), dimension(neq)       :: val

            !local variables
            integer                                         :: nz, npar, iarc, n
            double precision                                :: td
            double precision, dimension(:), allocatable     :: z, zd, hv, hvd, par, pard, hv1, hv2
            double precision                                :: zero, eps, h, DNRM2

            !ICI on ne considère que neq = 4*n = 2*nz, ie il n'y a qu'un champ de Jacobi

            !paraux = [par iarc nz pard]
            npar    = (nparaux - 2)/2
            iarc    = nint(paraux(npar+1))
            nz      = nint(paraux(npar+2))
            n       = nz/2

            allocate(z(nz),zd(nz),hv(nz),hv1(nz),hv2(nz),hvd(nz),par(npar),pard(npar))

            par     = paraux(1:npar)
            pard    = paraux(npar+2+1:npar+2+npar)
            td      = 0d0
            z       = y(1:nz)
            zd      = y(nz+1:nz+nz)

            !On veut hv et dhv/dz . dz + dhv/dl . dl
            select case (derivativeG)
                case ('eqvar')

                    call dhvfun(t, td, n, z, zd, iarc, npar, par, pard, hv, hvd)

                    val(1:nz)       = hv
                    val(nz+1:nz+nz) = hvd

                case ('ind')

                    eps  = epsilon(1d0)
                    zero = eps
                    if(sum(abs(pard)).gt.zero)then
                        h   = dsqrt(eps*max(1.d-5,DNRM2(nz+npar, (/z, par/), 1)))
                    else
                        h   = dsqrt(eps*max(1.d-5,DNRM2(nz, z, 1))) !On ne tient pas compte de par pour la normalisation
                    endif

                    call hvfun(t,n,z     ,iarc,npar,par       ,hv1)
                    call hvfun(t,n,z+h*zd,iarc,npar,par+h*pard,hv2)

                    val(1:nz)       = hv1
                    val(nz+1:nz+nz) = (hv2-hv1)/h

            end select

            deallocate(z,zd,hv,hv1,hv2,hvd,par,pard)

        end subroutine dexpzirhs

!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief   derivative of RHS of dexpzi
!!     @param[in]   neq     dimension of y
!!     @param[in]   t       time
!!     @param[in]   y       dexpzirhs variable put in an array
!!     @param[in]   nparaux Number of optional parameters
!!     @param[in]   paraux  Optional parameters
!!
!!     @param[out]  dphi    derivative of dexpzi RHS
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2014
!!  \copyright LGPL
!!
      Subroutine ddexpzirhs(neq,t,y,dphi,paraux,nparaux)
        use utils
        use derivatives
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: nparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(nparaux) :: paraux
        double precision,   intent(in),  dimension(neq)     :: y
        double precision,   intent(out), dimension(neq,neq) :: dphi

        !local variables
        integer             :: j, n, iarc, npar, nz
        double precision    :: eps, fvec1(neq), fvec2(neq), ytemp(neq), h, val(neq,neq)
        double precision    :: td
        double precision, allocatable :: z(:), zd1(:), zd2(:), hv(:), hvd(:), dhvdz(:,:), d2hvdz(:,:), par(:), pard1(:), pard2(:)

        select case (derivativeG)
            case ('eqvar')

                !paraux = [par iarc nz pard]
                npar    = (nparaux - 2)/2
                iarc    = nint(paraux(npar+1))
                nz      = nint(paraux(npar+2))
                n       = nz/2

                allocate(z(nz),zd1(nz),zd2(nz),hv(nz),hvd(nz),dhvdz(nz,nz),d2hvdz(nz,nz),par(npar),pard1(npar),pard2(npar))

                !neq = 2*n * (1+1)
                td    = 0d0
                z     = y(1:nz)
                par   = paraux(1:npar)
                pard1 = paraux(npar+2+1:npar+2+npar)
                pard2 = 0d0

                zd1   = y(nz+1:nz+nz)
                zd2   = 0d0
                do j=1,nz
                    zd2(j) = 1d0
                    call dhvfun(t, td, n, z, zd2, iarc, npar, par, pard2, hv, dhvdz(:,j))
                    call d2hvfun(t, td, td, n, z, zd2, zd1, iarc, npar, par, pard2, pard1, hv, hvd, d2hvdz(:,j))
                    zd2(j) = 0d0
                end do

                dphi = 0d0
                dphi(1:nz,1:nz) = dhvdz
                dphi(nz+1:nz+nz,1:nz) = d2hvdz
                dphi(nz+1:nz+nz,nz+1:nz+nz) = dhvdz

                deallocate(z,zd1,zd2,par,pard1,pard2,hv,hvd,dhvdz,d2hvdz)

!                !-----------------------------------------------------!
!                !Comparaison
!                eps  = epsilon(1d0)
!
!                ytemp = y
!                call dexpzirhs(neq,t,ytemp,fvec2,paraux,nparaux)
!
!                do j=1,neq
!                    h = dsqrt(eps*max(1.d-5,abs(y(j))))
!                    ytemp(j)  = y(j) + h
!                    call dexpzirhs(neq,t,ytemp,fvec1,paraux,nparaux)
!                    ytemp(j)  = y(j)
!                    val(:,j) = (fvec1 - fvec2)/h
!                end do
!
!                call printMatrice('Diff Eqvar Ind',dphi-val,neq,neq)
!                !-----------------------------------------------------!

            case ('ind')

                eps  = epsilon(1d0)

                ytemp = y
                call dexpzirhs(neq,t,ytemp,fvec2,paraux,nparaux)

                do j=1,neq
                    h = dsqrt(eps*max(1.d-5,abs(y(j))))
                    ytemp(j)  = y(j) + h
                    call dexpzirhs(neq,t,ytemp,fvec1,paraux,nparaux)
                    ytemp(j)  = y(j)
                    dphi(:,j) = (fvec1 - fvec2)/h
                end do

            case default

                CALL printandstop('  ||| ERROR: Derivative choice unvalid: please choose ind or eqvar')

        end select


   end subroutine ddexpzirhs

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonIntegration
!!     \brief   non linear equations to solve during steps of implicit schemes
!!     \param[in]       nz          dimension of Zc
!!     \param[in]       Zc          unknown
!!     \param[out]      fvec        value of the function at Zc
!!     \param[in,out]   iflag       parameter of hybrd
!!     \param[in,out]   fpar        parameters
!!     \param[in]       lfpar       dimension of fpar
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine phidezdexpzi(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

!        external dexpzirhs

        call phidez(nz, Zc, fvec, iflag, fpar, lfpar, dexpzirhs)

    end subroutine phidezdexpzi

!! -----------------------------------------------------------------------------
!!
!>     \ingroup sjacPackage
!!     \brief   call phidphidez
!!        @param[in]    nz       State and costate dimension
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    Zc       Unknowns
!!        @param[in]    nparaux  Number of optional parameters
!!        @param[in]    paraux   Optional parameters
!!        @param[in]    nfev     Number of calls to fvec
!!        @param[in]    ldfjac   Leading dimension of fjac
!!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
!!
!!        @param[out]   fvec     Function value
!!        @param[out]   fjac     Function jacobian
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
    subroutine pdpdexpzi(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        integer,          intent(in)                        :: nfev
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

!        external dexpzirhs
!        external ddexpzirhs

        call phidphidez(nz,Zc,fpar,lfpar,nfev,fvec,fjac,ldfjac,iflag,dexpzirhs,ddexpzirhs)

    end subroutine pdpdexpzi

!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief   implementation of solout
!!     @param[in]   Niter       Number of iterations
!!     @param[in]   told        Previous time
!!     @param[in]   neq         Dimension of y
!!     @param[in]   dpar        Double precision parameters
!!     @param[in]   ldpar       Dimension of dpar
!!     @param[in]   ipar        Integer parameters
!!     @param[in]   lipar       Dimension of ipar
!!     @param[in]   fun         RHS
!!     @param[in]   lfunpar     Dimension of funpar
!!     @param[in,out]   time    Current time
!!     @param[in,out]   y       Current point, can be changed by dense output
!!     @param[in,out]   funpar  Parameters given to fun
!!     @param[in,out]   irtrn   Flag
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
    Subroutine soloutSjac(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !dummy subroutine
    end Subroutine soloutSjac

end module specIntegSjac
