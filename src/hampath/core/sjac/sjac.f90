!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup sjacPackage
!!     @brief   Computes the Jacobian of the shooting function.
!!      @param[in] ny       Shooting variable dimension
!!      @param[in] y        Shooting variable
!!      @param[in] npar     Number of optional parameters
!!      @param[in] par      Optional parameters
!!      @param[in] nl       if nl=1 then fjac contains the derivatives wrt lambda
!!      @param[in] lambda   Homotopic parameter lambda
!!
!!      @param[out]   fjac   Jacobian of the shooting function
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine sjac(ny,y,npar,par,nl,lambda,fjac)
    use defs
    use utils
    use derivatives
    implicit none
    integer, intent(in)                                 :: ny, nl
    integer, intent(in)                                 :: npar
    double precision,  dimension(ny),   intent(in)      :: y
    double precision,  dimension(npar), intent(in)      :: par
    double precision, intent(in)                        :: lambda
    double precision, intent(out), dimension(ny,ny+nl)  :: fjac

    !local variables
    integer                                             :: i
    double precision                                    :: eps, h
    double precision, dimension(ny)                     :: ytemp, yd
    double precision, dimension(npar)                   :: ptemp, pard
    double precision, dimension(ny)                     :: fvec1, fvec2, s

    select case (derivativeG)
        case ('finite') !finite differences

            eps  = max(atolg,rtolg)

            call sfun(ny,y,npar,par,fvec2)

            ytemp=y
            ptemp=par

            !dS/dy
            do i=1,ny
                h = dsqrt(eps*max(1.d-5,abs(y(i))))
                ytemp(i) = y(i) + h
                call sfun(ny,ytemp,npar,par,fvec1)
                ytemp(i) = y(i)
                fjac(:,i) = (fvec1 - fvec2)/h
            end do

            !dS/dlambda
            if(nl.eq.1)then
                h = dsqrt(eps*max(1.d-5,abs(lambda)))
                call getPar(lambda+h,npar,ptemp)
                call sfun(ny,y,npar,ptemp,fvec1)
                ptemp = par
                fjac(:,ny+1) = (fvec1 - fvec2)/h
            end if

        case ('eqvar','ind') !variationnal equations or IND

            !dS/dy
            pard   = 0d0
            yd     = 0d0
            do i=1,ny
                yd(i) = 1d0
                call SFUN_DS(ny, y, yd, npar, par, pard, s, fjac(:,i))
                yd(i) = 0d0
                !write(*,*) "i = ", i
                !call printVector('fjac(:,i)',fjac(:,i),ny)
            end do


            !dS/dlambda
            if(nl.eq.1)then
                yd     = 0d0
                call dpardl(lambda,npar,pard)
                call SFUN_DS(ny, y, yd, npar, par, pard, s, fjac(:,ny+1))
            end if


        case default

            CALL printandstop('  ||| ERROR: Derivative choice unknown ...')

    end select

end subroutine sjac
