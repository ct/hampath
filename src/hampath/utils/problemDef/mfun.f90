!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  \ingroup problemDefinition
!!  \brief Monitoring during continuation. This function is called
!!          at each accepted homotopy step.
!!
!!  \param[in]       Niter    Number of iterations
!!  \param[in]       ny       Shooting variable dimension
!!  \param[in]       npar     Number of optional parameters
!!  \param[in,out]   flag    Flag : homotopy stop when flag < 0
!!                            Values from -1 to -10 are already assigned
!!                            Please choose values less than -11
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2015-2016
!!  \copyright LGPL
!!
Subroutine mfun(Niter,ny,npar,flag)
    use mod_exphv4sfun
    implicit none
    integer, intent(in)     :: Niter, ny, npar
    integer, intent(inout)  :: flag

    !! ------------------------------------------------------------------------------
    !!
    !!  The user can call getPointsFromPath and exphv subroutines inside mfun
    !!
    !! ------------------------------------------------------------------------------
    !!
    !! getPointsFromPath: get the N last points from the path of zeros.
    !!
    !!      syntax: call getPointsFromPath(ny,npar,N,arclengths,ys,pars)
    !!
    !!       integer         , intent(in)    :: ny
    !!       integer         , intent(in)    :: npar
    !!       integer         , intent(in)    :: N
    !!       double precision, intent(out)   :: arclengths(N)
    !!       double precision, intent(out)   :: ys(ny,N)
    !!       double precision, intent(out)   :: pars(npar,N)
    !!
    !! ------------------------------------------------------------------------------
    !!
    !!  exphv: computes the chronological exponential of the
    !!          Hamiltonian vector field hv defined by h.
    !!
    !!      syntax: call exphv(tspan,n,z0,iarc,npar,par,z)
    !!
    !!       integer         , intent(in)    :: n
    !!       integer         , intent(in)    :: iarc
    !!       integer         , intent(in)    :: npar
    !!       double precision, intent(in)    :: tspan(:) = [t0, t1, ... tf]
    !!       double precision, intent(in)    :: z0(2*n)
    !!       double precision, intent(in)    :: par(npar)
    !!
    !!  Either
    !!
    !!       double precision, intent(out)   :: z(2*n) = z(tf)
    !!
    !!  Or
    !!
    !!       double precision, intent(out)   :: z(2*n,nt) = [z(t0), z(t1), ... z(tf)]
    !!
    !!  Remark: in each case, the integration is made on only one single arc.
    !!
    !! ------------------------------------------------------------------------------

    !local variables
    flag = flag

End Subroutine mfun
