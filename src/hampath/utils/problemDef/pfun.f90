!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>  \ingroup problemDefinition
!!  \brief Parameters wrt to lambda, with lambda from 0 to 1.
!!  \details
!!   The purpose of pfun is to give an homotopic relation between par0 and parf.
!!   By default, it is coded an affine homotopy.
!!  \param[in] lpar     Number of optional parameters
!!  \param[in] lambda   Current homotopic parameter
!!  \param[in] par0     Optional intital parameters
!!  \param[in] parf     Optional final parameters
!!  \param[out] par     Optional parameters at lambda
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2016
!!  \copyright LGPL
!!
Subroutine pfun(lambda,lpar,par0,parf,par)
    implicit none
    integer,                           intent(in)  :: lpar
    double precision,                  intent(in)  :: lambda
    double precision, dimension(lpar), intent(in)  :: par0,parf
    double precision, dimension(lpar), intent(out) :: par

    par = (1d0-lambda)*par0 + lambda*parf

end subroutine pfun
