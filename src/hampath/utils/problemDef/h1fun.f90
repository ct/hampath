!! -----------------------------------------------------------------------------
!!
!! Copyright 2020, Olivier Cots.
!!
Subroutine h1fun(t,n,x,p,npar,par,h1)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: h1

    h1 = 0d0

end subroutine h1fun
