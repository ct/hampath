!! -----------------------------------------------------------------------------
!!
!! Copyright 2020, Olivier Cots.
!!
Subroutine h0fun(t,n,x,p,npar,par,h0)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: h0

    h0 = 0d0

end subroutine h0fun
