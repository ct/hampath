"""

    Description

        This module contains all the different exceptions for hampath code

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

class HampathError(Exception):
    """
        This exception is the generic class
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class ArgumentTypeError(HampathError):
    """
        This exception may be raised when one argument of a function has a wrong type
    """

class ArgumentDimensionError(HampathError):
    """
        This exception may be raised when one array argument of a function has not consistent dimensions with respect to others parameters
    """

class InputArgumentError(HampathError):
    """
        This exception may be raised when the number of input arguments is wrong
    """

class OptionNameError(HampathError):
    """
        This exception may be raised when one tries to get an option which does not exist
    """

