"""

    Description

        This module contains the hfun function which is linked to the fortran interface hfunint

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .build.hfunpy import *

def hfun(t,z,*optional):
    """
    -------------------------------------------------------------------------------------------

        hfun (needs hfun.f90)

        Description

            Computes the Hamiltonian.

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            h = hfun(t, z,     par) : single arc
            h = hfun(t, z, ti, par) : multiple arcs

        Inputs

            t    -  real vector, t[i-1]     = i-th time
            z    -  real matrix, z[:,i-1]   = i-th state and costate
            ti   -  real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            par  -  real vector, parameters given to hfun

        Outputs

            h    -  real vector, Hamiltonian at time(s) t

    -------------------------------------------------------------------------------------------
    """

    #Variable t must be a vector or a float
    #We initialize t0 and tf to build ti = [t0 tf] if needed
    msgErreur = 'Variable t must be either a one dimensional numpy array or a float!';
    if(isinstance(t,np.ndarray)):
        #Variable t must be a vector
        if(t.ndim==1):
            lvalue  = t.size
            if(lvalue<1): raise ArgumentDimensionError(msgErreur)
            t0      = t[0]
            tf      = t[lvalue-1]
        elif(t.ndim==2):
            (lig,col) = t.shape
            if(lig==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[0,lvalue-1]
            elif(col==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[lvalue-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(t,float)):
        lvalue  = 1
        t0      = t
        tf      = t
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z must be a vector or a matrix, with an odd number of rows and the same number of columns than lvalue
    msgErreur = 'Variable z must be either a one or two dimensional numpy array!';
    if(isinstance(z,np.ndarray)):
        #Test if z is a vector or a matrix
        if(z.ndim==1):
            nz  = z.size
            nt  = 1
        elif(z.ndim==2):
            (nz,nt) = z.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if nz is even : ie z = (x, p)
        if(nz%2==0 and nz!=0):
            n       = nz/2
        else :
            raise ArgumentDimensionError('Variable z must have an even number of rows!')
        #Test if t and z have consitent dimensions
        if(nt!=lvalue):
            raise ArgumentDimensionError('Variable z must have the same number of columns than the size of t!')
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==1):
        par     = optional[0]
        ti      = np.array([t0,tf])
    elif(lop==2):
        par     = optional[1]
        ti      = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(hfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    h       = hfunpy(t,z,ti,par,n,nbarc,lpar,lvalue)

    return np.copy(h)
