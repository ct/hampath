"""

    Description

        This module contains the sfun function which is linked to the fortran interface sfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.sfunpy import *

def sfun(y,options,par):
    """
    -------------------------------------------------------------------------------------------

        sfun (needs sfun.f90)

        Description

            Computes the shooting function

        Options used

            FreeFinalTime, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            s = sfun(y,options,par)

        Inputs

            y       - real vector, shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun

        Outputs

            s       - real vector, shooting value

    -------------------------------------------------------------------------------------------
    """

    #Variable y must be a vector
    msgErreur = 'Variable y must be a one dimensional numpy array!';
    if(isinstance(y,np.ndarray)):
        #Variable y must be a vector
        if(y.ndim==1):
            ny  = y.size
        elif(y.ndim==2):
            (lig,col) = y.shape
            if(lig==1 or col==1):
                ny  = y.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    s = mod_sfunpy.sfunpy(y,par,dw,iw,swint,lsw,ny,npar,ndw,niw,nsw,nswint)

    return np.copy(s)
