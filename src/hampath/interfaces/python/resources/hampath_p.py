"""

    Description

        This module contains the ssolve function which is linked to the fortran interface ssolvepy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.hampathpy import *

def hampath(parspan,y0,options):
    """
    -------------------------------------------------------------------------------------------

        hampath (needs sfun.f90)

        Description

            Interface of the homotopic method to solve a family of optimal control problems

        Options used

            Derivative, DispIter, Display, DoSavePath, FreeFinalTime, MaxIterCorrection,
            MaxSf, MaxSfunNorm, MaxStepsOde, MaxStepsOdeHam, MaxStepSizeOde, MaxStepSizeOdeHam,
            ODE, ODEHam, StopAtTurningPoint, TolOdeAbs, TolOdeRel, TolOdeHamAbs, TolOdeHamRel,
            TolDenseHamEnd, TolDenseHamX

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [parout,yout,sout,viout,dets,normS,ps,flag] = hampath(parspan,y0,options)

        Inputs

            parspan - real matrix, parspan = [par0, parf]. parspan[:,i] is a parameter vector
                        given to hfun and efun. The homotopy is from par0 to parf and of
                        the form : (1-lambda)*par0 + lambda*parf, unless a fortran file with
                        the subroutine parfun(lambda,lpar,par0,parf,parout), with lambda
                        from 0 to 1, is provided.
            y0      - initial solution, ie "sfun(y0,options,par0) = 0"
            options - HampathOptions

        Outputs

            parout  - real matrix, parameters at each integration step.
            yout    - real matrix, solutions along the paths, ie
                    "sfun(yout[:,k],options,parout[:,k]) = 0"
            sout    - real vector, arc length at each integration step
            viout   - real matrix, viout[:,k] = dot_c[:,k] where c = (y, lambda) and
                    dot is the derivative w.r.t. arc length
            dets    - real vector, det(h'(c(s)),c'(s)) at each integration step, where
                    h is the homotpic function
            normS   - real vector, norm of the shooting function at each integration step
            ps      - real vector, <c'(s_old),c'(s)> at each integ. step
            flag    - integer, flag should be 1 (ODE integrator output)
                        if flag==0  then Sfmax is too small
                        if flag==-1 then input is not consistent
                        if flag==-2 then larger MaxSteps is needed
                        if flag==-3 then step size became too small
                        if flag==-4 then problem is problably stiff
                        if flag==-5 then |S| is greater than NormeSfunMax
                        if flag==-6 then the homotopic parameter do not
                                evolve relatively wrt TolDenseHamX
                        if flag==-7 then a turning point occurs

    -------------------------------------------------------------------------------------------
    """
    #Variable parspan must be a matrix with 2 columns
    msgErreur = 'Variable parspan must be a two dimensional numpy array with 2 columns!';
    if(isinstance(parspan,np.ndarray)):
        if(parspan.ndim==2):
            (lig,col) = parspan.shape
            if(col==2):
                nparspan = lig
                mparspan = 2
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable y0 must be a vector
    msgErreur = 'Variable y0 must be a one dimensional numpy array!';
    if(isinstance(y0,np.ndarray)):
        #Variable y0 must be a vector
        if(y0.ndim==1):
            ny  = y0.size
        elif(y0.ndim==2):
            (lig,col) = y0.shape
            if(lig==1 or col==1):
                ny  = y0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y0,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    [dimepath,flag] = mod_hampathpy.hampathpy(y0,parspan,dw,iw,swint,lsw,ny,nparspan,mparspan,ndw,niw,nsw,nswint)

    parout  = mod_hampathpy.parout
    yout    = mod_hampathpy.yout
    sout    = mod_hampathpy.sout
    viout   = mod_hampathpy.viout
    dets    = mod_hampathpy.dets
    normS   = mod_hampathpy.norms
    ps      = mod_hampathpy.ps

    return [np.copy(parout),np.copy(yout),np.copy(sout),np.copy(viout),np.copy(dets),np.copy(normS),np.copy(ps),flag]
