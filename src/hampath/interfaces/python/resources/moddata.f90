!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod
    integer i
    integer :: x(4)
    real, dimension(2,3) :: a
    contains
    subroutine foo
        integer k
        print*, "i=",i
        print*, "x=[",x,"]"
        print*, "a=["
        print*, "[",a(1,1),",",a(1,2),",",a(1,3),"]"
        print*, "[",a(2,1),",",a(2,2),",",a(2,3),"]"
        print*, "]"
        print*, "Setting a(1,2)=a(1,2)+3"
        a(1,2) = a(1,2)+3
    end subroutine foo
end module mod
