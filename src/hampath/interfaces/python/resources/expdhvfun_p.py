"""

    Description

        This module contains the expdhvfun function which is linked to the fortran interface expdhvfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.expdhvfunpy import *

def expdhvfun(tspan,z0,dz0,*optional):
    """
    -------------------------------------------------------------------------------------------

        expdhvfun (needs hfun.f90)

        Description

            Computes the chronological exponential of the variational system associated to hv.

        Options used

            Derivative, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [tout,z,dz,flag] = expdhvfun(tspan, z0, dz0,     options, par) : single arc
            [tout,z,dz,flag] = expdhvfun(tspan, z0, dz0, ti, options, par) : multiple arcs

        Inputs

            tspan   - real vector, tspan = [tspan1 tspan2 ... tspanf] must be sorted and
                        included in ti, if any.
            z0      - real vector, initial flow
            dz0     - real matrix, initial Jacobi fields
            ti      - real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            options - HampathOptions
            par     - real vector, parameters given to hfun

        Outputs

            tout    - real vector, time at each integration step
            z       - real matrix, z[:,i] : flow at tout[i]
            dz      - real matrix, Jacobi fields at time tout
            flag    - integer, flag should be 1 (ODE integrator output)

    -------------------------------------------------------------------------------------------
    """

    #Variable tspan must be a vector of length >= 2
    msgErreur = 'Variable tspan must be a one dimensional numpy array of length >=2!';
    if(isinstance(tspan,np.ndarray)):
        #Variable tspan must be a vector
        if(tspan.ndim==1):
            nt  = tspan.size
            if(nt<2): raise ArgumentDimensionError(msgErreur)
            t0  = tspan[0]
            tf  = tspan[nt-1]
        elif(tspan.ndim==2):
            (lig,col) = tspan.shape
            if(lig==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[0,nt-1]
            elif(col==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[nt-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z0 must be a vector of even length
    msgErreur = 'Variable z0 must be a one dimensional numpy array of even length!';
    if(isinstance(z0,np.ndarray)):
        #Variable z0 must be a vector
        if(z0.ndim==1):
            nz  = z0.size
        elif(z0.ndim==2):
            (lig,col) = z0.shape
            if(lig==1 or col==1):
                nz  = z0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
        if(nz%2==0 and nz!=0):
            n = nz/2
        else:
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable dz0 must be a vector or a matrix, with the same number of rows than z and with k x lvalue columns, with k to determine
    msgErreur = 'Variable dz0 must be either a one or two dimensional numpy array!';
    if(isinstance(dz0,np.ndarray)):
        #Test if dz0 is a vector or a matrix
        if(dz0.ndim==1):
            ndz     = dz0.size
            ntdz    = 1
        elif(dz0.ndim==2):
            (ndz,ntdz) = dz0.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if ndz = nz
        if(nz!=ndz):
            raise ArgumentDimensionError('Variable dz0 must have the same number of rows than z!')
        k   = ntdz
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==2):
        ti      = np.array([t0,tf])
        options = optional[0]
        par     = optional[1]
    elif(lop==3):
        ti      = optional[0]
        options = optional[1]
        par     = optional[2]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(exphvfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswInt  = len(sw)

    swInt   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    ninfos  = 2

    [infos, dimet] = mod_expdhvfunpy.expdhvfunpy(z0,dz0,par,tspan,ti,ninfos,dw,iw,swInt,lsw,n,k,lpar,nt,nbarc,ndw,niw,nsw,nswInt)

    tout    = mod_expdhvfunpy.tout
    z       = mod_expdhvfunpy.exphv
    dz      = mod_expdhvfunpy.dexphv
    flag    = infos[0]
    nfev    = infos[1]

    return [np.copy(tout), np.copy(z), np.copy(dz), flag]
