"""

    Description

        This module contains the class to handle the hampath options

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#from .hampathError import *

class HampathOptions:
    """

        HampathOptions -- options for hampath code.

        Examples of constructor usage

            options = HampathOptions()
            options = HampathOptions(opt1=val1, opt2=val2, ...)
            options = HampathOptions({'opt1' : val1, 'opt2' : val2, ...})

            examples:

                options = HampathOptions(Display='off', MaxSf=10)
                options = HampathOptions({'Display' : 'off', 'MaxSf' : 1000})

        Update values

            options.update(Display='on', MaxSf=1000)

        Get value

            disp = options.get('Display')

        Options names and default values are:

            Derivative       - Derivation method.           [{'eqvar'},'finite','ind']
                                'finite' corresponds to finite differences.
                                'eqvar'  corresponds to variational equations.
                                'ind'    corresponds to internal numerical derivative.
                                Used by expdhvfun (only ind and eqvar),
                                hampath, sjac, ssolve.

            DispIter         - Display each mod(iter,DispIter)==0            [     1 ]
                                Used by hampath.

            Display          - Display results                       [ 'off' | {'on'}]
                                Used by hampath, ssolve.

            DoSavePath       - Save path in file "SavedPath(n).py" (python interface)
                                or "SavedPath(n).m" (other interface) during homotopy
                                                                    [ {'off'} | 'on']
                                Used by hampath.

            MaxFEval         - Maximum number of calls to the function       [   2000]
                                Used by ssolve.

            MaxIterCorrection- Maximum number of iterations during correction [     7]
                                Used by hampath.

            MaxSf            - Maximum final arc length for hampath          [   1e5 ]
                                Used by hampath.

            MaxSfunNorm      - Maximal norm of the shooting function during homotopy.
                                Used by hampath                              [   1e-1]

            MaxStepsOde      - Maximum number of integration steps           [   1e5 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            MaxStepsOdeHam   - Maximum number of homotopy steps              [   1e5 ]
                                Used by hampath.

            MaxStepSizeOde   - Maximum step size                             [     0 ]
                                If equals 0 then the integrator 'ODE'
                                has no constraint on the maximal step size.
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            MaxStepSizeOdeHam- Maximum step size during homotopy             [     0 ]
                                If equals 0 then the integrator 'ODEHam'
                                has no constraint on the maximal step size.
                                Used by hampath.

            MaxStepSizeHomPar- Maximum step size of homotopic parameter      [     0 ]
                                during homotopy.
                                If 0 then no constraint on the maximal step size.
                                Used by hampath.

            ODE              - Integrator name.

                                - No step size control:

                                    - Explicit: 'rk4',
                                                'rk5'
                                    - Implicit: 'gauss4',
                                                'gauss6',
                                                'gauss8',
                                                'radaus' (symplectic radau of order 5)

                                - With step size control:

                                    - Explicit: {'dopri5'} (default),
                                                'dop853'

                                    - Implicit: 'radau5',
                                                'radau9',
                                                'radau13'
                                                'radau' (adaptative order 5, 9 and 13)

                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            ODEHam           - Integrator name for homotopy.

                                - With step size control:

                                    - Explicit: {'dopri5'} (default),
                                                'dop853'

                                    - Implicit: 'radau5',
                                                'radau9',
                                                'radau13'
                                                'radau' (adaptative order 5, 9 and 13)

                                Used by hampath.

            SolverMethod     - Type of solver                              [{'hybrj'}]
                                It is possible to give a solver type from python scipy
                                package: try
                                >> from scipy import optimize
                                >> print(optimize.show_options(solver='root'))
                                Used by ssolve.

            ScipyRootOptions - A dictionary of solver options for python scipy solver.
                                E.g. xtol or maxiter, see show_options() for details.
                                Used by ssolve

            StopAtTurningPoint - Stop or not after turning point             [{0} | 1]
                                Used by hampath.

            TolOdeAbs        - Absolute error tolerance of the integrator    [ 1e-10 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            TolOdeRel        - Relative error tolerance of the integrator    [  1e-8 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            TolOdeHamAbs     - Absolute error tolerance for hampath          [ 1e-10 ]
                                Used by hampath.

            TolOdeHamRel     - Relative error tolerance for hampath          [  1e-8 ]
                                Used by hampath.

            TolDenseHamEnd   - Absolute Dense output tolerance               [  1e-8 ]
                                Absolute tolerance to detect if the final homotopic
                                parameter has been reached.
                                Used by hampath.

            TolDenseHamX     - Relative Dense output tolerance               [  1e-8 ]
                                The homotopy stops when the homotopic parameter do not
                                evolve relatively, iteration per iteration, during ten 
                                following steps.
                                Used by hampath.

            TolX             - Relative tolerance between iterates (ssolve)  [  1e-8 ]
                                Used by ssolve.

    """

    #On donne les valeurs par defaut de options
    options = dict( Derivative          = 'eqvar',
                    DispIter            = 1,
                    Display             = 'on',
                    DoSavePath          = 'off',
                    MaxSf               = 1e5,
                    MaxSfunNorm         = 1e-1,
                    MaxStepsOde         = 1e5,
                    MaxStepsOdeHam      = 1e5,
                    MaxStepSizeOde      = 0.0,
                    MaxStepSizeOdeHam   = 0.0,
                    ODE                 = 'dopri5',
                    ODEHam              = 'dopri5',
                    StopAtTurningPoint  = 0,
                    TolOdeAbs           = 1e-10,
                    TolOdeRel           = 1e-8,
                    TolOdeHamAbs        = 1e-10,
                    TolOdeHamRel        = 1e-8,
                    TolDenseHamEnd      = 1e-8,
                    TolDenseHamX        = 1e-8,
                    TolX                = 1e-8,
                    IrkInit             = '2',
                    IrkSolver           = 'newton',
                    MaxFEval            = 2000,
                    SolverMethod        = 'hybrj',
                    MaxIterCorrection   = 7,
                    MaxStepSizeHomPar   = 0.0,
                    ScipyRootOptions    = dict())

    #On peut initialiser les options de 4 manieres :
    # soit avec dictionnaire
    # soit avec une liste de dictionnaire
    # soit avec une liste de param=value
    # soit avec une liste de dictionnaire et de param=value
    #
    # Exemple
    #
    # def f(*args, **kwargs):
    #   print 'args: ', args, ' kwargs: ', kwargs
    #
    # >>> f('a')
    #   args:  ('a',)  kwargs:  {}
    # >>> f(ar='a')
    #   args:  ()  kwargs:  {'ar': 'a'}
    # >>> f(1,2,param=3)
    #   args:  (1, 2)  kwargs:  {'param': 3}
    #
    def __init__(self, *args, **kwargs):
        """

            Doc de __init__

        """
        #args   -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        self.update(*args, **kwargs)

    def update(self, *args, **kwargs):
        """

            Doc de update

        """
        for dico in args:
            if(isinstance(dico,dict)):
                self.__private_update(dico)
            else:
                raise ArgumentTypeError('HampathOptions requires arguments of type dict or a list of param=value, see help(HampathOptions)!')

        self.__private_update(kwargs)

    def __private_update(self, dictionary):
        """

            Doc de __private_update

        """
        for key, val in dictionary.items():
            if key in self.options:
                if(isinstance(self.options[key],val.__class__)):
                    self.options[key] = val
                else:
                    raise ArgumentTypeError('Hampath option ' + key + ' must be of type: ' + str(self.options[key].__class__) \
                            + ' instead of type: ' + str(val.__class__) + '!')
            else:
                raise OptionNameError(key + ' is not a valid Hampath option, see help(HampathOptions)!\n')

    def get(self, key):
        """

            Doc de get

        """
        try:
            value = self.options[key]
        except KeyError as err:
            raise OptionNameError(key + ' is not a Hampath option, see help(HampathOptions)!\n')

        return value

    def __str__(self):
        """

            Doc de __str__

        """
        keys    = sorted(self.options.keys())
        string  = self.__class__.__name__ + ':\n\n'
        for key in keys:
            string = string + "\t" + key.ljust(20) + ' = ' + str(self.options[key]) + '\n'

        return string

    def getAllOptions(self):
        """

            Doc de getAllOptions

        """
        #Cette methode permet de regrouper les options a donner en parametre aux fonctions fortran faisant l interface

        dw  = [ self.options['MaxSf']               ,
                self.options['MaxSfunNorm']         ,
                self.options['MaxStepSizeOde']      ,
                self.options['MaxStepSizeOdeHam']   ,
                self.options['MaxStepSizeHomPar']   ,
                self.options['TolOdeAbs']           ,
                self.options['TolOdeRel']           ,
                self.options['TolOdeHamAbs']        ,
                self.options['TolOdeHamRel']        ,
                self.options['TolDenseHamEnd']      ,
                self.options['TolDenseHamX']        ,
                self.options['TolX']                ]

        iw  = [ self.options['DispIter']            ,
                self.options['MaxFEval']            ,
                self.options['MaxIterCorrection']   ,
                self.options['MaxStepsOde']         ,
                self.options['MaxStepsOdeHam']      ,
                self.options['StopAtTurningPoint']  ]

        lsw = [ len(self.options['Derivative'])  ,
                len(self.options['Display'])     ,
                len(self.options['DoSavePath'])  ,
                len(self.options['IrkInit'])     ,
                len(self.options['IrkSolver'])   ,
                len(self.options['ODE'])         ,
                len(self.options['ODEHam'])      ,
                len(self.options['SolverMethod'])]

        sw  =   self.options['Derivative']  + \
                self.options['Display']     + \
                self.options['DoSavePath']  + \
                self.options['IrkInit']     + \
                self.options['IrkSolver']   + \
                self.options['ODE']         + \
                self.options['ODEHam']      + \
                self.options['SolverMethod']

        return [dw, iw, lsw, sw]

