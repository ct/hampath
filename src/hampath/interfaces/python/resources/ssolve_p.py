"""

    Description

        This module contains the ssolve function which is linked to the fortran interface ssolvepy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from scipy import optimize
#from .hampathError import *
#from .hampathOptions import *
#from .build.ssolvepy import *
#from .sfun_p import *
#from .sjac_p import *

def ssolve(y0,options,par):
    """
    -------------------------------------------------------------------------------------------

        ssolve (needs sfun.f90)

        Description

            Interface of non linear solvers to solve the optimal
            control problem described by the Fortran subroutines, hfun.f90 and sfun.f90

        Options used

            Derivative, Display, FreeFinalTime, MaxFEval, MaxStepsOde, MaxStepSizeOde, ODE,
            SolverMethod, TolOdeAbs, TolOdeRel, TolX

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [ysol,ssol,nfev,njev,flag] = ssolve(y0,options,par)

        Inputs

            y0      - real vector, intial guess for shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun

        Outputs

            ysol    - real vector, shooting variable solution
            ssol    - real vector, value of sfun at ysol
            nfev    - integer, number of evaluations of sfun
            njev    - integer, number of evaluations of sjac
            flag    - integer, solver output (should be 1)

    -------------------------------------------------------------------------------------------
    """

    #Variable y0 must be a vector
    msgErreur = 'Variable y0 must be a one dimensional numpy array!';
    if(isinstance(y0,np.ndarray)):
        #Variable y0 must be a vector
        if(y0.ndim==1):
            ny  = y0.size
        elif(y0.ndim==2):
            (lig,col) = y0.shape
            if(lig==1 or col==1):
                ny  = y0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y0,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    solverMethod = options.get('SolverMethod')

    if(solverMethod=='hybrj'):
        [ysol,ssol,nfev,njev,flag] = mod_ssolvepy.ssolvepy(y0,par,dw,iw,swint,lsw,ny,npar,ndw,niw,nsw,nswint)
    else:
        if(solverMethod=='hybr' or solverMethod=='lm'):
            sol  = optimize.root(sfun, y0, args=(options,par), jac=sjac, \
                method=solverMethod, callback=None, options=options.get('ScipyRootOptions'))
        else:
            sol  = optimize.root(sfun, y0, args=(options,par), jac=None, \
                method=solverMethod, callback=callbackssolve, options=options.get('ScipyRootOptions'))
        ysol = sol.y
        flag = sol.status
        ssol = sol.fun
        try:
            nfev = sol.nfev
        except AttributeError as err: nfev = -1
        try:
            njev = sol.njev
        except AttributeError as err: njev = -1
        print('\n Results of the shooting method:\n')
        print(' ysol  = ', ysol, '\n');
        print(' s     = ', ssol, '\n');
        print(' nfev  = ', nfev, '\n');
        print(' njev  = ', njev, '\n');
        print(' flag  = ', flag, '\n');
        print(sol.message)

    return [np.copy(ysol),np.copy(ssol),nfev,njev,flag]

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(counter=0)
def callbackssolve(y,f):
    if(callbackssolve.counter==0):
        print('\n      Calls  |S(y)|                 |y|\n ')
    print('{0:10}'.format(callbackssolve.counter) + '{0:23.15e}'.format(np.linalg.norm(f)) + '{0:23.15e}'.format(np.linalg.norm(y)))
    callbackssolve.counter += 1
