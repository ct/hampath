"""

    Description

        This module contains the sfun function which is linked to the fortran interface sfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.sjacpy import *

def sjac(y,options,par,*optional):
    """
    -------------------------------------------------------------------------------------------

        sjac (needs sfun.f90)

        Description

            Computes the Jacobian of the shooting-homotopic function

        Options used

            Derivative, FreeFinalTime, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs,
            TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            j = sjac(y,options,par)
            j = sjac(y,options,par,ipar)

        Inputs

            y       - real vector, shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun
            ipar    - integer vector, index of parameters for which the derivative is computed

        Outputs

            j       - real matrix, jacobian of the shooting/homotopic function

    -------------------------------------------------------------------------------------------
    """

    #Variable y must be a vector
    msgErreur = 'Variable y must be a one dimensional numpy array!';
    if(isinstance(y,np.ndarray)):
        #Variable y must be a vector
        if(y.ndim==1):
            ny  = y.size
        elif(y.ndim==2):
            (lig,col) = y.shape
            if(lig==1 or col==1):
                ny  = y.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at most one parameter
    lop = len(optional)
    if(lop==0):
        ipar  = np.array([])
    elif(lop==1):
        ipar  = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(sjac)!')

    # Test sur ipar qui doit etre un vecteur d entiers ou un entier
    # Les indices doivent etre compris entre 1 et npar
    msgErreur = 'Variable ipar must be either a one dimensional numpy array of int or an int!';
    if(isinstance(ipar,np.ndarray)):
        #Variable ipar must be a vector
        if(ipar.ndim==1):
            nipar  = ipar.size
        elif(ipar.ndim==2):
            (lig,col) = ipar.shape
            if(lig==1 or col==1):
                nipar = ipar.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(ipar,int)):
        nipar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    if(nipar==0):
        nl = 0
    else:
        nl = 1
        minipar = np.min(ipar)
        maxipar = np.max(ipar)
        msgErreur = 'ipar must contain values between 1 and length(par)'
        if(minipar<1 or maxipar>npar):
            raise ArgumentDimensionError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    fjac = mod_sjacpy.sjacpy(y,par,ipar,nl,dw,iw,swint,lsw,ny,npar,nipar,ndw,niw,nsw,nswint)

    return np.copy(fjac)
