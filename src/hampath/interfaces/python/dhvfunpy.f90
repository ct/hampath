!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup dhvfunPackage
!!     @brief  Interface specific for python of dhvfunint.
!!
!!        @param[in]    t        time
!!        @param[in]    n        State dimension
!!        @param[in]    k        Number of jacobi fields
!!        @param[in]    z        State and adjoint state at t
!!        @param[in]    dz       Jacobi fields at t
!!        @param[in]    nbarc    Number of arcs
!!        @param[in]    ti       ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!        @param[in]    lpar     Number of optional parameters
!!        @param[in]    par      Optional parameters
!!        @param[in]    nt       Dimension of t and number of columns of z and value
!!
!!        @param[out]    hv      The second member of the BVP
!!        @param[out]   dhv      The second member of the Variationnal equations
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
Subroutine dhvfunpy(t,n,k,z,dz,nbarc,ti,lpar,par,nt,hv,dhv)
    implicit none
    integer,      intent(in)                             :: n,nbarc
    integer,      intent(in)                             :: lpar
    integer,      intent(in)                             :: nt
    integer,      intent(in)                             :: k
    double precision, intent(in),  dimension(nt)         :: t
    double precision, intent(in),  dimension(nbarc+1)    :: ti
    double precision, intent(in),  dimension(2*n,nt)     :: z
    double precision, intent(in),  dimension(2*n,nt*k)   :: dz
    double precision, intent(in),  dimension(lpar)       :: par
    double precision, intent(out), dimension(2*n,nt)     :: hv
    double precision, intent(out), dimension(2*n,nt*k)   :: dhv

    call dhvfunint(t,n,k,z,dz,nbarc,ti,lpar,par,nt,hv,dhv)

end subroutine dhvfunpy
