!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_hampathpy

    implicit none

    double precision, allocatable, dimension(:)     :: sout, dets, normS, ps
    double precision, allocatable, dimension(:,:)   :: parout, yout, viout

    contains

!!********************************************************************
!!
!!     Subroutine hampathpy
!!
!>     @ingroup hampathPackage
!!     @brief  Interface specific for python of hampathint.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial solution, ie "sfun(y0,options,par0) = 0"
!!        @param[in]    nparspan  Number of optional parameters
!!        @param[in]    mparspan  Size of homotopic parameters grid
!!        @param[in]    parspan   Parameters grid
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     Integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    dimepath    Number of steps
!!        @param[out]    flag        Integration output (should be 1)!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    Subroutine hampathpy(ny,y0,nparspan,mparspan,parspan,          &
                                dimepath,flag,                      &
                                ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        integer,          intent(in)                               :: ny,nparspan,mparspan
        double precision, intent(in), dimension(ny)                :: y0
        double precision, intent(in), dimension(nparspan,mparspan) :: parspan
        integer,          intent(out)                              :: dimepath,flag
        integer,          intent(in)                               :: ndw, niw, nsw, nswInt
        double precision, intent(in)                               :: dw(ndw)
        integer,          intent(in)                               :: iw(niw), lsw(nsw)
        integer,          intent(in)                               :: swInt(nswInt)

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        call hampathint(ny,y0,nparspan,mparspan,parspan,        &
                                dimepath,flag,                  &
                                ndw,niw,nsw,dw,iw,sw,lsw)

        if(allocated(parout))then
            deallocate(parout)
        end if

        if(allocated(yout))then
            deallocate(yout)
        end if

        if(allocated(viout))then
            deallocate(viout)
        end if

        if(allocated(sout))then
            deallocate(sout)
        end if

        if(allocated(normS))then
            deallocate(normS)
        end if

        if(allocated(dets))then
            deallocate(dets)
        end if

        if(allocated(ps))then
            deallocate(ps)
        end if

        allocate(parout(nparspan,dimepath),yout(ny,dimepath),viout(ny+1,dimepath))
        allocate(sout(dimepath),dets(dimepath),normS(dimepath),ps(dimepath))

        call getpath(ny,nparspan,dimepath,parout,yout,viout,sout,dets,normS,ps)

   end subroutine hampathpy


end module mod_hampathpy
