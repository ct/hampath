!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_ssolvepy

    implicit none

    contains

!!********************************************************************
!!
!!     Subroutine ssolvepy
!!
!>     @ingroup ssolvePackage
!!     @brief  Interface specific for python of ssolveint.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y0        Initial guess: shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     Integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    ysol   Solution : y solution
!!        @param[out]    ssol   Solution : sfun(ysol)
!!        @param[out]    nfev   Number of sfun evaluation
!!        @param[out]    njev   Number of jacfun evaluation
!!        @param[out]    flag   solver output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    Subroutine ssolvepy(ny,y0,npar,par,ysol,ssol,nfev,njev,flag, &
                                ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        integer,            intent(in)                      :: ny
        double precision,   intent(in),  dimension(ny)      :: y0
        integer,            intent(in)                      :: npar
        double precision,   intent(in),  dimension(npar)    :: par
        double precision,   intent(out), dimension(ny)      :: ysol
        double precision,   intent(out), dimension(ny)      :: ssol
        integer,            intent(out)                     :: nfev
        integer,            intent(out)                     :: njev
        integer,            intent(out)                     :: flag
        integer,            intent(in)                      :: ndw, niw, nsw, nswInt
        double precision,   intent(in)                      :: dw(ndw)
        integer,            intent(in)                      :: iw(niw), lsw(nsw)
        integer,            intent(in)                      :: swInt(nswInt)

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        call ssolveint(ny,y0,npar,par,ysol,ssol,nfev,njev,flag,ndw,niw,nsw,dw,iw,sw,lsw)

   end subroutine ssolvepy

end module mod_ssolvepy
