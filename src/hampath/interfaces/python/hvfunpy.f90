!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup hvfunPackage
!!     @brief  Interface specific for python of hvfunint.
!!
!!        @param[in]    t        time
!!        @param[in]    n        State dimension
!!        @param[in]    z        State and adjoint state at t
!!        @param[in]    nbarc    Number of arcs
!!        @param[in]    ti       ti = [t0 t1 .. t_{nbarc-1} tf], where t0 is the initial time and tf the final time.
!!        @param[in]    lpar     Number of optional parameters
!!        @param[in]    par      Optional parameters
!!        @param[in]    lvalue   Dimension of t and number of columns of z and value
!!
!!        @param[out]    value   The second member of the BVP
!!
!!  \author Olivier Cots
!!  \date   2016
!!  \copyright LGPL
!!
Subroutine hvfunpy(t,n,z,nbarc,ti,lpar,par,lvalue,value)
    implicit none
    integer,      intent(in)                         :: n,nbarc
    integer,      intent(in)                         :: lpar
    integer,      intent(in)                         :: lvalue
    double precision, intent(in),  dimension(lvalue)     :: t
    double precision, intent(in),  dimension(nbarc+1)    :: ti
    double precision, intent(in),  dimension(2*n,lvalue) :: z
    double precision, intent(in),  dimension(lpar)       :: par
    double precision, intent(out), dimension(2*n,lvalue) :: value

    call hvfunint(t,n,z,nbarc,ti,lpar,par,lvalue,value)

end subroutine hvfunpy
