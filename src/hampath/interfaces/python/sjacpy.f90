!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_sjacpy

    implicit none

    contains

!!********************************************************************
!!
!!     Subroutine sjacpy
!!
!>     @ingroup sjacPackage
!!     @brief  Interface specific for python of sjacint.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    y         Shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        @param[in]    nipar     Number of index parameters
!!        @param[in]    ipar      Index of parameters for which the jacobian is computed
!!        @param[in]    nl        nl is 1 if we derivate the homotopic function and 0 if it is the shooting function.
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     Integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    fjac     Value of the shooting function
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    Subroutine sjacpy(ny,y,npar,par,nipar,ipar,nl,fjac,ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        integer,            intent(in)                          :: ny, nl
        double precision,   intent(in),  dimension(ny)          :: y
        integer,            intent(in)                          :: npar
        double precision,   intent(in),  dimension(npar)        :: par
        integer,            intent(in)                          :: nipar
        double precision,   intent(in),  dimension(nipar)       :: ipar
        double precision,   intent(out), dimension(ny,ny+nl)    :: fjac
        integer,            intent(in)                          :: ndw, niw, nsw, nswInt
        double precision,   intent(in)                          :: dw(ndw)
        integer,            intent(in)                          :: iw(niw), lsw(nsw)
        integer,            intent(in)                          :: swInt(nswInt)

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        call sjacint(ny,y,npar,par,nipar,ipar,nl,fjac,ndw,niw,nsw,dw,iw,sw,lsw)

   end subroutine sjacpy

end module mod_sjacpy
