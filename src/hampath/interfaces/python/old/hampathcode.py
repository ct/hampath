
"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

from __future__ import absolute_import
import numpy as np
#from .hampathError import *
#from .hampathOptions import *
from .dhvfunpy import *
from .expdhvfunpy import *
from .exphvfunpy import *
from .hampathpy import *
from .hfunpy import *
from .hvfunpy import *
from .sfunpy import *
from .sjacpy import *
from .ssolvepy import *
from scipy import optimize
#from .sfun_p import *
#from .sjac_p import *

"""

    Description

        This module contains the dhvfun function which is linked to the fortran interface dhvfunint

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .build.dhvfunpy import *

def dhvfun(t,z,dz,*optional):
    """
    -------------------------------------------------------------------------------------------

        dhvfun (needs hfun.f90)

        Description

            Computes the second member of the variational system associated to H.

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [hv, dhv] = dhvfun(t, z, dz,     par) : single arc
            [hv, dhv] = dhvfun(t, z, dz, ti, par) : multiple arcs

        Inputs

            t    -  real vector, t[i-1]             = i-th time
            z    -  real matrix, z[:,i-1]           = i-th state and costate
            dz   -  real matrix, z[:,(i-1)*k:i*k-1] = the k Jacobi fields at time t[i-1]
            ti   -  real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            par  -  real vector, parameters given to hfun

        Outputs

            hv   -  real matrix, Hamiltonian vector field at time(s) t
            dhv  -  real matrix, Linearized of the Hamiltonian vector field at time(s) t

    -------------------------------------------------------------------------------------------
    """

    #Variable t must be a vector or a float
    #We initialize t0 and tf to build ti = [t0 tf] if needed
    msgErreur = 'Variable t must be either a one dimensional numpy array or a float!';
    if(isinstance(t,np.ndarray)):
        #Variable t must be a vector
        if(t.ndim==1):
            lvalue  = t.size
            if(lvalue<1): raise ArgumentDimensionError(msgErreur)
            t0      = t[0]
            tf      = t[lvalue-1]
        elif(t.ndim==2):
            (lig,col) = t.shape
            if(lig==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[0,lvalue-1]
            elif(col==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[lvalue-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(t,float)):
        lvalue  = 1
        t0      = t
        tf      = t
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z must be a vector or a matrix, with an odd number of rows and the same number of columns than lvalue
    msgErreur = 'Variable z must be either a one or two dimensional numpy array!';
    if(isinstance(z,np.ndarray)):
        #Test if z is a vector or a matrix
        if(z.ndim==1):
            nz  = z.size
            nt  = 1
        elif(z.ndim==2):
            (nz,nt) = z.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if nz is even : ie z = (x, p)
        if(nz%2==0 and nz!=0):
            n       = nz/2
        else :
            raise ArgumentDimensionError('Variable z must have an even number of rows!')
        #Test if t and z have consitent dimensions
        if(nt!=lvalue):
            raise ArgumentDimensionError('Variable z must have the same number of columns than the size of t!')
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable dz must be a vector or a matrix, with the same number of rows than z and with k x lvalue columns, with k to determine
    msgErreur = 'Variable dz must be either a one or two dimensional numpy array!';
    if(isinstance(dz,np.ndarray)):
        #Test if dz is a vector or a matrix
        if(dz.ndim==1):
            ndz     = dz.size
            ntdz    = 1
        elif(dz.ndim==2):
            (ndz,ntdz) = dz.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if ndz = nz
        if(nz!=ndz):
            raise ArgumentDimensionError('Variable dz must have the same number of rows than z!')
        #Test if t and dz have consistent dimensions
        k   = ntdz/lvalue
        if(ntdz%lvalue!=0 or k<1):
            raise ArgumentDimensionError('Variable dz must have k x nt columns, with k >= 1 the number of Jacobi fields and nt the length of t!')
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==1):
        par     = optional[0]
        ti      = np.array([t0,tf])
    elif(lop==2):
        par     = optional[1]
        ti      = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(hvfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [hv, dhv]       = dhvfunpy(t,k,z,dz,ti,par,n,nbarc,lpar,lvalue)

    return [np.copy(hv), np.copy(dhv)]

"""

    Description

        This module contains the exphvfun function which is linked to the fortran interface exphvfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.exphvfunpy import *

def exphvfun(tspan,z0,*optional):
    """
    -------------------------------------------------------------------------------------------

        exphvfun (needs hfun.f90)

        Description

            Computes the chronological exponential of the Hamiltonian vector field hv
            defined by h.

        Options used

            MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [tout,z,flag] = exphvfun(tspan, z0,     options, par) : single arc
            [tout,z,flag] = exphvfun(tspan, z0, ti, options, par) : multiple arcs

        Inputs

            tspan   - real vector, tspan = [tspan1 tspan2 ... tspanf] must be sorted and
                        included in ti, if any.
            z0      - real vector, initial flow
            ti      - real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            options - HampathOptions
            par     - real vector, parameters given to hfun

        Outputs

            tout    - real vector, time at each integration step
            z       - real matrix, z[:,i] : flow at tout[i]
            flag    - integer, flag should be 1 (ODE integrator output)

    -------------------------------------------------------------------------------------------
    """

    #Variable tspan must be a vector of length >= 2
    msgErreur = 'Variable tspan must be a one dimensional numpy array of length >=2!';
    if(isinstance(tspan,np.ndarray)):
        #Variable tspan must be a vector
        if(tspan.ndim==1):
            nt  = tspan.size
            if(nt<2): raise ArgumentDimensionError(msgErreur)
            t0  = tspan[0]
            tf  = tspan[nt-1]
        elif(tspan.ndim==2):
            (lig,col) = tspan.shape
            if(lig==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[0,nt-1]
            elif(col==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[nt-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z0 must be a vector of even length
    msgErreur = 'Variable z0 must be a one dimensional numpy array of even length!';
    if(isinstance(z0,np.ndarray)):
        #Variable z0 must be a vector
        if(z0.ndim==1):
            nz  = z0.size
        elif(z0.ndim==2):
            (lig,col) = z0.shape
            if(lig==1 or col==1):
                nz  = z0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
        if(nz%2==0 and nz!=0):
            n = nz/2
        else:
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==2):
        ti      = np.array([t0,tf])
        options = optional[0]
        par     = optional[1]
    elif(lop==3):
        ti      = optional[0]
        options = optional[1]
        par     = optional[2]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(exphvfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswInt  = len(sw)

    swInt   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    ninfos  = 2

    [infos, dimet] = mod_exphvfunpy.exphvfunpy(z0,par,tspan,ti,ninfos,dw,iw,swInt,lsw,n,lpar,nt,nbarc,ndw,niw,nsw,nswInt)

    tout    = mod_exphvfunpy.tout
    z       = mod_exphvfunpy.exphv
    flag    = infos[0]
    nfev    = infos[1]

    return [np.copy(tout), np.copy(z), flag]
"""

    Description

        This module contains the expdhvfun function which is linked to the fortran interface expdhvfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.expdhvfunpy import *

def expdhvfun(tspan,z0,dz0,*optional):
    """
    -------------------------------------------------------------------------------------------

        expdhvfun (needs hfun.f90)

        Description

            Computes the chronological exponential of the variational system associated to hv.

        Options used

            Derivative, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [tout,z,dz,flag] = expdhvfun(tspan, z0, dz0,     options, par) : single arc
            [tout,z,dz,flag] = expdhvfun(tspan, z0, dz0, ti, options, par) : multiple arcs

        Inputs

            tspan   - real vector, tspan = [tspan1 tspan2 ... tspanf] must be sorted and
                        included in ti, if any.
            z0      - real vector, initial flow
            dz0     - real matrix, initial Jacobi fields
            ti      - real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            options - HampathOptions
            par     - real vector, parameters given to hfun

        Outputs

            tout    - real vector, time at each integration step
            z       - real matrix, z[:,i] : flow at tout[i]
            dz      - real matrix, Jacobi fields at time tout
            flag    - integer, flag should be 1 (ODE integrator output)

    -------------------------------------------------------------------------------------------
    """

    #Variable tspan must be a vector of length >= 2
    msgErreur = 'Variable tspan must be a one dimensional numpy array of length >=2!';
    if(isinstance(tspan,np.ndarray)):
        #Variable tspan must be a vector
        if(tspan.ndim==1):
            nt  = tspan.size
            if(nt<2): raise ArgumentDimensionError(msgErreur)
            t0  = tspan[0]
            tf  = tspan[nt-1]
        elif(tspan.ndim==2):
            (lig,col) = tspan.shape
            if(lig==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[0,nt-1]
            elif(col==1):
                nt  = tspan.size
                if(nt<2): raise ArgumentDimensionError(msgErreur)
                t0  = tspan[0,0]
                tf  = tspan[nt-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z0 must be a vector of even length
    msgErreur = 'Variable z0 must be a one dimensional numpy array of even length!';
    if(isinstance(z0,np.ndarray)):
        #Variable z0 must be a vector
        if(z0.ndim==1):
            nz  = z0.size
        elif(z0.ndim==2):
            (lig,col) = z0.shape
            if(lig==1 or col==1):
                nz  = z0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
        if(nz%2==0 and nz!=0):
            n = nz/2
        else:
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable dz0 must be a vector or a matrix, with the same number of rows than z and with k x lvalue columns, with k to determine
    msgErreur = 'Variable dz0 must be either a one or two dimensional numpy array!';
    if(isinstance(dz0,np.ndarray)):
        #Test if dz0 is a vector or a matrix
        if(dz0.ndim==1):
            ndz     = dz0.size
            ntdz    = 1
        elif(dz0.ndim==2):
            (ndz,ntdz) = dz0.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if ndz = nz
        if(nz!=ndz):
            raise ArgumentDimensionError('Variable dz0 must have the same number of rows than z!')
        k   = ntdz
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==2):
        ti      = np.array([t0,tf])
        options = optional[0]
        par     = optional[1]
    elif(lop==3):
        ti      = optional[0]
        options = optional[1]
        par     = optional[2]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(exphvfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswInt  = len(sw)

    swInt   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    ninfos  = 2

    [infos, dimet] = mod_expdhvfunpy.expdhvfunpy(z0,dz0,par,tspan,ti,ninfos,dw,iw,swInt,lsw,n,k,lpar,nt,nbarc,ndw,niw,nsw,nswInt)

    tout    = mod_expdhvfunpy.tout
    z       = mod_expdhvfunpy.exphv
    dz      = mod_expdhvfunpy.dexphv
    flag    = infos[0]
    nfev    = infos[1]

    return [np.copy(tout), np.copy(z), np.copy(dz), flag]
"""

    Description

        This module contains the ssolve function which is linked to the fortran interface ssolvepy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.hampathpy import *

def hampath(parspan,y0,options):
    """
    -------------------------------------------------------------------------------------------

        hampath (needs sfun.f90)

        Description

            Interface of the homotopic method to solve a family of optimal control problems

        Options used

            Derivative, DispIter, Display, DoSavePath, FreeFinalTime, MaxIterCorrection,
            MaxSf, MaxSfunNorm, MaxStepsOde, MaxStepsOdeHam, MaxStepSizeOde, MaxStepSizeOdeHam,
            ODE, ODEHam, StopAtTurningPoint, TolOdeAbs, TolOdeRel, TolOdeHamAbs, TolOdeHamRel,
            TolDenseHamEnd, TolDenseHamX

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [parout,yout,sout,viout,dets,normS,ps,flag] = hampath(parspan,y0,options)

        Inputs

            parspan - real matrix, parspan = [par0, parf]. parspan[:,i] is a parameter vector
                        given to hfun and efun. The homotopy is from par0 to parf and of
                        the form : (1-lambda)*par0 + lambda*parf, unless a fortran file with
                        the subroutine parfun(lambda,lpar,par0,parf,parout), with lambda
                        from 0 to 1, is provided.
            y0      - initial solution, ie "sfun(y0,options,par0) = 0"
            options - HampathOptions

        Outputs

            parout  - real matrix, parameters at each integration step.
            yout    - real matrix, solutions along the paths, ie
                    "sfun(yout[:,k],options,parout[:,k]) = 0"
            sout    - real vector, arc length at each integration step
            viout   - real matrix, viout[:,k] = dot_c[:,k] where c = (y, lambda) and
                    dot is the derivative w.r.t. arc length
            dets    - real vector, det(h'(c(s)),c'(s)) at each integration step, where
                    h is the homotpic function
            normS   - real vector, norm of the shooting function at each integration step
            ps      - real vector, <c'(s_old),c'(s)> at each integ. step
            flag    - integer, flag should be 1 (ODE integrator output)
                        if flag==0  then Sfmax is too small
                        if flag==-1 then input is not consistent
                        if flag==-2 then larger MaxSteps is needed
                        if flag==-3 then step size became too small
                        if flag==-4 then problem is problably stiff
                        if flag==-5 then |S| is greater than NormeSfunMax
                        if flag==-6 then the homotopic parameter do not
                                evolve relatively wrt TolDenseHamX
                        if flag==-7 then a turning point occurs

    -------------------------------------------------------------------------------------------
    """
    #Variable parspan must be a matrix with 2 columns
    msgErreur = 'Variable parspan must be a two dimensional numpy array with 2 columns!';
    if(isinstance(parspan,np.ndarray)):
        if(parspan.ndim==2):
            (lig,col) = parspan.shape
            if(col==2):
                nparspan = lig
                mparspan = 2
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable y0 must be a vector
    msgErreur = 'Variable y0 must be a one dimensional numpy array!';
    if(isinstance(y0,np.ndarray)):
        #Variable y0 must be a vector
        if(y0.ndim==1):
            ny  = y0.size
        elif(y0.ndim==2):
            (lig,col) = y0.shape
            if(lig==1 or col==1):
                ny  = y0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y0,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    [dimepath,flag] = mod_hampathpy.hampathpy(y0,parspan,dw,iw,swint,lsw,ny,nparspan,mparspan,ndw,niw,nsw,nswint)

    parout  = mod_hampathpy.parout
    yout    = mod_hampathpy.yout
    sout    = mod_hampathpy.sout
    viout   = mod_hampathpy.viout
    dets    = mod_hampathpy.dets
    normS   = mod_hampathpy.norms
    ps      = mod_hampathpy.ps

    return [np.copy(parout),np.copy(yout),np.copy(sout),np.copy(viout),np.copy(dets),np.copy(normS),np.copy(ps),flag]
"""

    Description

        This module contains all the different exceptions for hampath code

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

class HampathError(Exception):
    """
        This exception is the generic class
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class ArgumentTypeError(HampathError):
    """
        This exception may be raised when one argument of a function has a wrong type
    """

class ArgumentDimensionError(HampathError):
    """
        This exception may be raised when one array argument of a function has not consistent dimensions with respect to others parameters
    """

class InputArgumentError(HampathError):
    """
        This exception may be raised when the number of input arguments is wrong
    """

class OptionNameError(HampathError):
    """
        This exception may be raised when one tries to get an option which does not exist
    """

"""

    Description

        This module contains the class to handle the hampath options

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#from .hampathError import *

class HampathOptions:
    """

        HampathOptions -- options for hampath code.

        Examples of constructor usage

            options = HampathOptions()
            options = HampathOptions(opt1=val1, opt2=val2, ...)
            options = HampathOptions({'opt1' : val1, 'opt2' : val2, ...})

            examples:

                options = HampathOptions(Display='off', MaxSf=10)
                options = HampathOptions({'Display' : 'off', 'MaxSf' : 10})

        Update values

            options.update(Display='on', MaxSf=1000)

        Get value

            disp = options.get('Display')

        Options names and default values are:

            Derivative       - Derivation method.           [{'eqvar'},'finite','ind']
                                'finite' corresponds to finite differences.
                                'eqvar'  corresponds to variational equations.
                                'ind'    corresponds to internal numerical derivative.
                                Used by expdhvfun (only ind and eqvar),
                                hampath, sjac, ssolve.

            DispIter         - Display each mod(iter,DispIter)==0            [     1 ]
                                Used by hampath.

            Display          - Display results                       [ 'off' | {'on'}]
                                Used by hampath, ssolve.

            DoSavePath       - Save path in file "SavedPath(n).m" during homotopy
                                                                    [ {'off'} | 'on']
                                Used by hampath.

            MaxFEval         - Maximum number of calls to the function       [   2000]
                                Used by ssolve.

            MaxIterCorrection- Maximum number of iterations during correction [     7]
                                Used by hampath.

            MaxSf            - Maximum final arc length for hampath          [   1e5 ]
                                Used by hampath.

            MaxSfunNorm      - Maximal norm of the shooting function during homotopy.
                                Used by hampath                              [   1e-1]

            MaxStepsOde      - Maximum number of integration steps           [   1e5 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            MaxStepsOdeHam   - Maximum number of homotopy steps              [   1e5 ]
                                Used by hampath.

            MaxStepSizeOde   - Maximum step size                             [     0 ]
                                If equals 0 then the integrator 'ODE'
                                has no constraint on the maximal step size.
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            MaxStepSizeOdeHam- Maximum step size during homotopy             [     0 ]
                                If equals 0 then the integrator 'ODEHam'
                                has no constraint on the maximal step size.
                                Used by hampath.

            ODE              - Integrator name.

                                - No step size control:

                                    - Explicit: 'rk4',
                                                'rk5'
                                    - Implicit: 'gauss4',
                                                'gauss6',
                                                'gauss8',
                                                'radaus' (symplectic radau of order 5)

                                - With step size control:

                                    - Explicit: {'dopri5'} (default),
                                                'dop853'

                                    - Implicit: 'radau5',
                                                'radau9',
                                                'radau13'
                                                'radau' (adaptative order 5, 9 and 13)

                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            ODEHam           - Integrator name for homotopy.   [{'dopri5'}, 'dop853' ]
                                Used by hampath.

            SolverMethod     - Type of solver                              [{'hybrj'}]
                                It is possible to give a solver type from python scipy
                                package: try
                                >> from scipy import optimize
                                >> print(optimize.show_options(solver='root'))
                                Used by ssolve.

            ScipyRootOptions - A dictionary of solver options for python scipy solver.
                                E.g. xtol or maxiter, see show_options() for details.
                                Used by ssolve

            StopAtTurningPoint - Stop or not after turning point             [{0} | 1]
                                Used by hampath.

            TolOdeAbs        - Absolute error tolerance of the integrator    [ 1e-14 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            TolOdeRel        - Relative error tolerance of the integrator    [  1e-8 ]
                                Used by exp[d]hvfun, hampath, sfun, sjac, ssolve.

            TolOdeHamAbs     - Absolute error tolerance for hampath          [ 1e-14 ]
                                Used by hampath.

            TolOdeHamRel     - Relative error tolerance for hampath          [  1e-8 ]
                                Used by hampath.

            TolDenseHamEnd   - Absolute Dense output tolerance               [  1e-8 ]
                                Absolute tolerance to detect if the final homotopic
                                parameter has been reached.
                                Used by hampath.

            TolDenseHamX     - Relative Dense output tolerance               [  1e-8 ]
                                The homotopy stops when the homotopic parameter do not
                                evolve relatively, iteration per iteration, during ten 
                                following steps.
                                Used by hampath.

            TolX             - Relative tolerance between iterates (ssolve)  [  1e-8 ]
                                Used by ssolve.

    """

    #On donne les valeurs par defaut de options
    options = dict( Derivative          = 'eqvar',
                    DispIter            = 1,
                    Display             = 'on',
                    DoSavePath          = 'off',
                    MaxSf               = 1e5,
                    MaxSfunNorm         = 1e-1,
                    MaxStepsOde         = 1e5,
                    MaxStepsOdeHam      = 1e5,
                    MaxStepSizeOde      = 0.0,
                    MaxStepSizeOdeHam   = 0.0,
                    ODE                 = 'dopri5',
                    ODEHam              = 'dopri5',
                    StopAtTurningPoint  = 0,
                    TolOdeAbs           = 1e-14,
                    TolOdeRel           = 1e-8,
                    TolOdeHamAbs        = 1e-14,
                    TolOdeHamRel        = 1e-8,
                    TolDenseHamEnd      = 1e-8,
                    TolDenseHamX        = 1e-8,
                    TolX                = 1e-8,
                    IrkInit             = '2',
                    IrkSolver           = 'newton',
                    MaxFEval            = 2000,
                    SolverMethod        = 'hybrj',
                    MaxIterCorrection   = 7,
                    ScipyRootOptions    = dict())

    #On peut initialiser les options de 4 manieres :
    # soit avec dictionnaire
    # soit avec une liste de dictionnaire
    # soit avec une liste de param=value
    # soit avec une liste de dictionnaire et de param=value
    #
    # Exemple
    #
    # def f(*args, **kwargs):
    #   print 'args: ', args, ' kwargs: ', kwargs
    #
    # >>> f('a')
    #   args:  ('a',)  kwargs:  {}
    # >>> f(ar='a')
    #   args:  ()  kwargs:  {'ar': 'a'}
    # >>> f(1,2,param=3)
    #   args:  (1, 2)  kwargs:  {'param': 3}
    #
    def __init__(self, *args, **kwargs):
        """

            Doc de __init__

        """
        #args   -- tuple of anonymous arguments
        #kwargs -- dictionary of named arguments
        self.update(*args, **kwargs)

    def update(self, *args, **kwargs):
        """

            Doc de update

        """
        for dico in args:
            if(isinstance(dico,dict)):
                self.__private_update(dico)
            else:
                raise ArgumentTypeError('HampathOptions requires arguments of type dict or a list of param=value, see help(HampathOptions)!')

        self.__private_update(kwargs)

    def __private_update(self, dictionary):
        """

            Doc de __private_update

        """
        for key, val in dictionary.items():
            if key in self.options:
                if(isinstance(self.options[key],val.__class__)):
                    self.options[key] = val
                else:
                    raise ArgumentTypeError('Hampath option ' + key + ' must be of type: ' + str(self.options[key].__class__) \
                            + ' instead of type: ' + str(val.__class__) + '!')
            else:
                raise OptionNameError(key + ' is not a valid Hampath option, see help(HampathOptions)!\n')

    def get(self, key):
        """

            Doc de get

        """
        try:
            value = self.options[key]
        except KeyError as err:
            raise OptionNameError(key + ' is not a Hampath option, see help(HampathOptions)!\n')

        return value

    def __str__(self):
        """

            Doc de __str__

        """
        keys    = sorted(self.options.keys())
        string  = self.__class__.__name__ + ':\n\n'
        for key in keys:
            string = string + "\t" + key.ljust(20) + ' = ' + str(self.options[key]) + '\n'

        return string

    def getAllOptions(self):
        """

            Doc de getAllOptions

        """
        #Cette methode permet de regrouper les options a donner en parametre aux fonctions fortran faisant l interface

        dw  = [ self.options['MaxSf']               ,
                self.options['MaxSfunNorm']         ,
                self.options['MaxStepSizeOde']      ,
                self.options['MaxStepSizeOdeHam']   ,
                self.options['TolOdeAbs']           ,
                self.options['TolOdeRel']           ,
                self.options['TolOdeHamAbs']        ,
                self.options['TolOdeHamRel']        ,
                self.options['TolDenseHamEnd']      ,
                self.options['TolDenseHamX']        ,
                self.options['TolX']                ]

        iw  = [ self.options['DispIter']            ,
                self.options['MaxFEval']            ,
                self.options['MaxIterCorrection']   ,
                self.options['MaxStepsOde']         ,
                self.options['MaxStepsOdeHam']      ,
                self.options['StopAtTurningPoint']  ]

        lsw = [ len(self.options['Derivative'])  ,
                len(self.options['Display'])     ,
                len(self.options['DoSavePath'])  ,
                len(self.options['IrkInit'])     ,
                len(self.options['IrkSolver'])   ,
                len(self.options['ODE'])         ,
                len(self.options['ODEHam'])      ,
                len(self.options['SolverMethod'])]

        sw  =   self.options['Derivative']  + \
                self.options['Display']     + \
                self.options['DoSavePath']  + \
                self.options['IrkInit']     + \
                self.options['IrkSolver']   + \
                self.options['ODE']         + \
                self.options['ODEHam']      + \
                self.options['SolverMethod']

        return [dw, iw, lsw, sw]

"""

    Description

        This module contains the hfun function which is linked to the fortran interface hfunint

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .build.hfunpy import *

def hfun(t,z,*optional):
    """
    -------------------------------------------------------------------------------------------

        hfun (needs hfun.f90)

        Description

            Computes the Hamiltonian.

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            h = hfun(t, z,     par) : single arc
            h = hfun(t, z, ti, par) : multiple arcs

        Inputs

            t    -  real vector, t[i-1]     = i-th time
            z    -  real matrix, z[:,i-1]   = i-th state and costate
            ti   -  real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            par  -  real vector, parameters given to hfun

        Outputs

            h    -  real vector, Hamiltonian at time(s) t

    -------------------------------------------------------------------------------------------
    """

    #Variable t must be a vector or a float
    #We initialize t0 and tf to build ti = [t0 tf] if needed
    msgErreur = 'Variable t must be either a one dimensional numpy array or a float!';
    if(isinstance(t,np.ndarray)):
        #Variable t must be a vector
        if(t.ndim==1):
            lvalue  = t.size
            if(lvalue<1): raise ArgumentDimensionError(msgErreur)
            t0      = t[0]
            tf      = t[lvalue-1]
        elif(t.ndim==2):
            (lig,col) = t.shape
            if(lig==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[0,lvalue-1]
            elif(col==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[lvalue-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(t,float)):
        lvalue  = 1
        t0      = t
        tf      = t
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z must be a vector or a matrix, with an odd number of rows and the same number of columns than lvalue
    msgErreur = 'Variable z must be either a one or two dimensional numpy array!';
    if(isinstance(z,np.ndarray)):
        #Test if z is a vector or a matrix
        if(z.ndim==1):
            nz  = z.size
            nt  = 1
        elif(z.ndim==2):
            (nz,nt) = z.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if nz is even : ie z = (x, p)
        if(nz%2==0 and nz!=0):
            n       = nz/2
        else :
            raise ArgumentDimensionError('Variable z must have an even number of rows!')
        #Test if t and z have consitent dimensions
        if(nt!=lvalue):
            raise ArgumentDimensionError('Variable z must have the same number of columns than the size of t!')
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==1):
        par     = optional[0]
        ti      = np.array([t0,tf])
    elif(lop==2):
        par     = optional[1]
        ti      = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(hfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    h       = hfunpy(t,z,ti,par,n,nbarc,lpar,lvalue)

    return np.copy(h)
"""

    Description

        This module contains the hvfun function which is linked to the fortran interface hvfunint

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .build.hvfunpy import *

def hvfun(t,z,*optional):
    """
    -------------------------------------------------------------------------------------------

        hvfun (needs hfun.f90)

        Description

            Computes the Hamiltonian vector field associated to H.

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            hv = hvfun(t, z,     par) : single arc
            hv = hvfun(t, z, ti, par) : multiple arcs

        Inputs

            t    -  real vector, t[i-1]     = i-th time
            z    -  real matrix, z[:,i-1]   = i-th state and costate
            ti   -  real vector (in multiple shooting case), ti = [t0 t1 ... t_nbarc-1 tf]
            par  -  real vector, parameters given to hfun

        Outputs

            hv   -  real matrix, Hamiltonian vector field at time(s) t

    -------------------------------------------------------------------------------------------
    """

    #Variable t must be a vector or a float
    #We initialize t0 and tf to build ti = [t0 tf] if needed
    msgErreur = 'Variable t must be either a one dimensional numpy array or a float!';
    if(isinstance(t,np.ndarray)):
        #Variable t must be a vector
        if(t.ndim==1):
            lvalue  = t.size
            if(lvalue<1): raise ArgumentDimensionError(msgErreur)
            t0      = t[0]
            tf      = t[lvalue-1]
        elif(t.ndim==2):
            (lig,col) = t.shape
            if(lig==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[0,lvalue-1]
            elif(col==1):
                lvalue  = t.size
                if(lvalue<1): raise ArgumentDimensionError(msgErreur)
                t0      = t[0,0]
                tf      = t[lvalue-1,0]
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(t,float)):
        lvalue  = 1
        t0      = t
        tf      = t
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable z must be a vector or a matrix, with an odd number of rows and the same number of columns than lvalue
    msgErreur = 'Variable z must be either a one or two dimensional numpy array!';
    if(isinstance(z,np.ndarray)):
        #Test if z is a vector or a matrix
        if(z.ndim==1):
            nz  = z.size
            nt  = 1
        elif(z.ndim==2):
            (nz,nt) = z.shape
        else :
            raise ArgumentDimensionError(msgErreur)
        #Test if nz is even : ie z = (x, p)
        if(nz%2==0 and nz!=0):
            n       = nz/2
        else :
            raise ArgumentDimensionError('Variable z must have an even number of rows!')
        #Test if t and z have consitent dimensions
        if(nt!=lvalue):
            raise ArgumentDimensionError('Variable z must have the same number of columns than the size of t!')
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at least one parameter
    lop = len(optional)
    if(lop==1):
        par     = optional[0]
        ti      = np.array([t0,tf])
    elif(lop==2):
        par     = optional[1]
        ti      = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(hvfun)!')

    #Variable ti must be a vector of size >= 2
    msgErreur = 'Variable ti must be a one dimensional numpy array of length >= 2!';
    if(isinstance(ti,np.ndarray)):
        #Variable ti must be a vector
        if(ti.ndim==1):
            nbarc   = ti.size - 1
        elif(ti.ndim==2):
            (lig,col) = ti.shape
            if(lig==1 or col==1):
                nbarc   = ti.size - 1
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            lpar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                lpar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        lpar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    hv       = hvfunpy(t,z,ti,par,n,nbarc,lpar,lvalue)

    return np.copy(hv)
"""

    Description

        This module contains the sfun function which is linked to the fortran interface sfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.sfunpy import *

def sfun(y,options,par):
    """
    -------------------------------------------------------------------------------------------

        sfun (needs sfun.f90)

        Description

            Computes the shooting function

        Options used

            FreeFinalTime, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            s = sfun(y,options,par)

        Inputs

            y       - real vector, shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun

        Outputs

            s       - real vector, shooting value

    -------------------------------------------------------------------------------------------
    """

    #Variable y must be a vector
    msgErreur = 'Variable y must be a one dimensional numpy array!';
    if(isinstance(y,np.ndarray)):
        #Variable y must be a vector
        if(y.ndim==1):
            ny  = y.size
        elif(y.ndim==2):
            (lig,col) = y.shape
            if(lig==1 or col==1):
                ny  = y.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    s = mod_sfunpy.sfunpy(y,par,dw,iw,swint,lsw,ny,npar,ndw,niw,nsw,nswint)

    return np.copy(s)
"""

    Description

        This module contains the sfun function which is linked to the fortran interface sfunpy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from .hampathError import *
#from .hampathOptions import *
#from .build.sjacpy import *

def sjac(y,options,par,*optional):
    """
    -------------------------------------------------------------------------------------------

        sjac (needs sfun.f90)

        Description

            Computes the Jacobian of the shooting-homotopic function

        Options used

            Derivative, FreeFinalTime, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs,
            TolOdeRel

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            j = sjac(y,options,par)
            j = sjac(y,options,par,ipar)

        Inputs

            y       - real vector, shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun
            ipar    - integer vector, index of parameters for which the derivative is computed

        Outputs

            j       - real matrix, jacobian of the shooting/homotopic function

    -------------------------------------------------------------------------------------------
    """

    #Variable y must be a vector
    msgErreur = 'Variable y must be a one dimensional numpy array!';
    if(isinstance(y,np.ndarray)):
        #Variable y must be a vector
        if(y.ndim==1):
            ny  = y.size
        elif(y.ndim==2):
            (lig,col) = y.shape
            if(lig==1 or col==1):
                ny  = y.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #optional must have at most one parameter
    lop = len(optional)
    if(lop==0):
        ipar  = np.array([])
    elif(lop==1):
        ipar  = optional[0]
    else :
        raise InputArgumentError('Invalid number of arguments: see help(sjac)!')

    # Test sur ipar qui doit etre un vecteur d entiers ou un entier
    # Les indices doivent etre compris entre 1 et npar
    msgErreur = 'Variable ipar must be either a one dimensional numpy array of int or an int!';
    if(isinstance(ipar,np.ndarray)):
        #Variable ipar must be a vector
        if(ipar.ndim==1):
            nipar  = ipar.size
        elif(ipar.ndim==2):
            (lig,col) = ipar.shape
            if(lig==1 or col==1):
                nipar = ipar.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(ipar,int)):
        nipar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    if(nipar==0):
        nl = 0
    else:
        nl = 1
        minipar = np.min(ipar)
        maxipar = np.max(ipar)
        msgErreur = 'ipar must contain values between 1 and length(par)'
        if(minipar<1 or maxipar>npar):
            raise ArgumentDimensionError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    fjac = mod_sjacpy.sjacpy(y,par,ipar,nl,dw,iw,swint,lsw,ny,npar,nipar,ndw,niw,nsw,nswint)

    return np.copy(fjac)
"""

    Description

        This module contains the ssolve function which is linked to the fortran interface ssolvepy

    Author: Olivier Cots (INP-ENSEEIHT & IRIT)

    Date: 01/2016

"""

"""
-------------------------------------------------------------------------------------------

 Copyright 2016, Olivier Cots.

 This file is part of HamPath.

 HamPath is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HamPath is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with HamPath. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------------------
"""

#from __future__ import absolute_import
#import numpy as np
#from scipy import optimize
#from .hampathError import *
#from .hampathOptions import *
#from .build.ssolvepy import *
#from .sfun_p import *
#from .sjac_p import *

def ssolve(y0,options,par):
    """
    -------------------------------------------------------------------------------------------

        ssolve (needs sfun.f90)

        Description

            Interface of non linear solvers to solve the optimal
            control problem described by the Fortran subroutines, hfun.f90 and sfun.f90

        Options used

            Derivative, Display, FreeFinalTime, MaxFEval, MaxStepsOde, MaxStepSizeOde, ODE,
            SolverMethod, TolOdeAbs, TolOdeRel, TolX

    -------------------------------------------------------------------------------------------

        Python Usage: use numpy.array to define vectors and matrices

            [ysol,ssol,nfev,njev,flag] = ssolve(y0,options,par)

        Inputs

            y0      - real vector, intial guess for shooting variable
            options - HampathOptions
            par     - real vector, parameters given to hfun and sfun

        Outputs

            ysol    - real vector, shooting variable solution
            ssol    - real vector, value of sfun at ysol
            nfev    - integer, number of evaluations of sfun
            njev    - integer, number of evaluations of sjac
            flag    - integer, solver output (should be 1)

    -------------------------------------------------------------------------------------------
    """

    #Variable y0 must be a vector
    msgErreur = 'Variable y0 must be a one dimensional numpy array!';
    if(isinstance(y0,np.ndarray)):
        #Variable y0 must be a vector
        if(y0.ndim==1):
            ny  = y0.size
        elif(y0.ndim==2):
            (lig,col) = y0.shape
            if(lig==1 or col==1):
                ny  = y0.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(y0,float)):
        ny = 1
    else :
        raise ArgumentTypeError(msgErreur)

    #Variable options must be a HampathOptions object
    msgErreur = 'Variable options must be a HampathOptions object!';
    if(not isinstance(options,HampathOptions)):
        raise ArgumentTypeError(msgErreur)

    #Variable par must be a vector or a float
    msgErreur = 'Variable par must be either a one dimensional numpy array or a float (or int)!';
    if(isinstance(par,np.ndarray)):
        #Variable par must be a vector
        if(par.ndim==1):
            npar  = par.size
        elif(par.ndim==2):
            (lig,col) = par.shape
            if(lig==1 or col==1):
                npar = par.size
            else :
                raise ArgumentDimensionError(msgErreur)
        else :
            raise ArgumentDimensionError(msgErreur)
    elif(isinstance(par,float) or isinstance(par,int)):
        npar  = 1
    else :
        raise ArgumentTypeError(msgErreur)

    [dw, iw, lsw, sw] = options.getAllOptions()

    ndw     = len(dw)
    niw     = len(iw)
    nsw     = len(lsw)
    nswint  = len(sw)

    swint   = [ord(c) for c in sw] #Code ascii de la chaine de caractere

    solverMethod = options.get('SolverMethod')

    if(solverMethod=='hybrj'):
        [ysol,ssol,nfev,njev,flag] = mod_ssolvepy.ssolvepy(y0,par,dw,iw,swint,lsw,ny,npar,ndw,niw,nsw,nswint)
    else:
        if(solverMethod=='hybr' or solverMethod=='lm'):
            sol  = optimize.root(sfun, y0, args=(options,par), jac=sjac, \
                method=solverMethod, callback=None, options=options.get('ScipyRootOptions'))
        else:
            sol  = optimize.root(sfun, y0, args=(options,par), jac=None, \
                method=solverMethod, callback=callbackssolve, options=options.get('ScipyRootOptions'))
        ysol = sol.y
        flag = sol.status
        ssol = sol.fun
        try:
            nfev = sol.nfev
        except AttributeError as err: nfev = -1
        try:
            njev = sol.njev
        except AttributeError as err: njev = -1
        print('\n Results of the shooting method:\n')
        print(' ysol  = ', ysol, '\n');
        print(' s     = ', ssol, '\n');
        print(' nfev  = ', nfev, '\n');
        print(' njev  = ', njev, '\n');
        print(' flag  = ', flag, '\n');
        print(sol.message)

    return [np.copy(ysol),np.copy(ssol),nfev,njev,flag]

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(counter=0)
def callbackssolve(y,f):
    if(callbackssolve.counter==0):
        print('\n      Calls  |S(y)|                 |y|\n ')
    print('{0:10}'.format(callbackssolve.counter) + '{0:23.15e}'.format(np.linalg.norm(f)) + '{0:23.15e}'.format(np.linalg.norm(y)))
    callbackssolve.counter += 1
