## -----------------------------------------------------------------------------
##
## Copyright 2016, Olivier Cots.
##
## This file is part of HamPath.
##
## HamPath is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## HamPath is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with HamPath. If not, see <http://www.gnu.org/licenses/>.
##
## -----------------------------------------------------------------------------
##

rep=resources

filedest=hampathcode.py

if [ -f ${filedest} ]; then
    rm ${filedest}
fi

cat ${rep}/import.py             > ${filedest}
cat ${rep}/dhvfun_p.py          >> ${filedest}
cat ${rep}/exphvfun_p.py        >> ${filedest}
cat ${rep}/expdhvfun_p.py       >> ${filedest}
cat ${rep}/hampath_p.py         >> ${filedest}
cat ${rep}/hampathError.py      >> ${filedest}
cat ${rep}/hampathOptions.py    >> ${filedest}
cat ${rep}/hfun_p.py            >> ${filedest}
cat ${rep}/hvfun_p.py           >> ${filedest}
cat ${rep}/sfun_p.py            >> ${filedest}
cat ${rep}/sjac_p.py            >> ${filedest}
cat ${rep}/ssolve_p.py          >> ${filedest}
