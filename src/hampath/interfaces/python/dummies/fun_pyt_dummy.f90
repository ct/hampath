!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
Subroutine MYFUNCTIONpy(t,n,z,nbarc,ti,lpar,par,lvalue,value)
    implicit none
    integer, intent(in) :: n,nbarc
    integer, intent(in) :: lpar
    integer, intent(in) :: lvalue
    double precision, intent(in), dimension(lvalue) :: t
    double precision, intent(in), dimension(nbarc+1) :: ti
    double precision, intent(in), dimension(2*n,lvalue) :: z
    double precision, intent(in), dimension(lpar) :: par
    double precision, intent(out), dimension(MYDIMENSION,lvalue) :: value

    call MYFUNCTIONint(t,n,z,nbarc,ti,lpar,par,lvalue,value)

end subroutine MYFUNCTIONpy
