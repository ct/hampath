!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_expdhvfunpy

    implicit none

    double precision, allocatable, dimension(:)     :: tout
    double precision, allocatable, dimension(:,:)   :: exphv, dexphv

    contains

!!********************************************************************
!!
!!     Subroutine expdhvfunpy
!!
!>     @ingroup expdhvfunPackage
!!     @brief  Interface of expdhvfun.
!!
!!        @param[in]    n           State dimension
!!        @param[in]    k           Number of jacobi fields
!!        @param[in]    z0          initial flow
!!        @param[in]    dz0         initial Jacobi fields
!!        @param[in]    lpar        Number of optional parameters
!!        @param[in]    par         Optional parameters
!!        @param[in]    nt          Dimension of tspan
!!        @param[in]    tspan       Grid time : [t0 t1 ... tf]
!!        @param[in]    nbarc       Number of arcs
!!        @param[in]    ti          Contains t0, tf and the intermediate times if any.
!!                                  ti = [t0 t1 .. t_{nbarc-1} tf]
!!        \param[in]    ninfos      dimension of infos
!!        \param[in]    ndw         Size of dw
!!        \param[in]    niw         Size of iw
!!        \param[in]    nsw         Size of lsw
!!        \param[in]    nswInt      Size of swInt
!!        \param[in]    iw          Integer hampath code options
!!        \param[in]    dw          Double precision hampath code options
!!        \param[in]    swInt       Integer hampath code options
!!        \param[in]    lsw         Length of each string option
!!
!!        \param[out]   infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!        @param[out]    dimeT      Number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2016
!!  \copyright LGPL
!!
    Subroutine expdhvfunpy(n,k,z0,dz0,lpar,par,nt,tspan,nbarc,ti,ninfos,infos,dimeT,   &
                            ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        integer,            intent(in)                     :: n,nbarc,ninfos
        integer,            intent(in)                     :: k, lpar
        integer,            intent(in)                     :: nt
        double precision,   intent(in), dimension(nt)      :: tspan
        double precision,   intent(in), dimension(nbarc+1) :: ti
        double precision,   intent(in), dimension(2*n)     :: z0
        double precision, intent(in), dimension(2*n,k)   :: dz0
        double precision,   intent(in), dimension(lpar)    :: par
        integer,            intent(out)                    :: infos(ninfos)
        integer,            intent(out)                    :: dimeT
        integer,          intent(in)                       :: ndw, niw, nsw, nswInt
        double precision, intent(in)                       :: dw(ndw)
        integer,          intent(in)                       :: iw(niw), lsw(nsw), swInt(nswInt)

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        call expdhvfunint(n,k,z0,dz0,lpar,par,nt,tspan,nbarc,ti,ninfos,infos,dimeT,ndw,niw,nsw,dw,iw,sw,lsw)

        if(allocated(tout))then
            deallocate(tout)
        end if

        if(allocated(exphv))then
            deallocate(exphv)
        end if

        if(allocated(dexphv))then
            deallocate(dexphv)
        end if

        allocate(tout(dimeT), exphv(2*n,dimeT), dexphv(2*n,k*dimeT))

        call getstatesdhv(n,k,dimeT,tout,exphv,dexphv)

    end subroutine expdhvfunpy

end module mod_expdhvfunpy
