C -----------------------------------------------------------------------------
C
C Copyright 2016, Olivier Cots, Jean-Baptiste Caillau.
C
C This file is part of HamPath.
C
C HamPath is free software: you can redistribute it and/or modify
C it under the terms of the GNU Lesser General Public License as published by
C the Free Software Foundation, either version 3 of the License, or
C (at your option) any later version.
C
C HamPath is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU Lesser General Public License for more details.
C
C You should have received a copy of the GNU Lesser General Public License
C along with HamPath. If not, see <http://www.gnu.org/licenses/>.
C
C -----------------------------------------------------------------------------
C
      SUBROUTINE HYBRDFUN(N, X, Y, IFLAG, fpar, lfpar)
      IMPLICIT NONE
C     Written on Thu 12 Jun 2008 14:57:08 CEST
C     by Jean-Baptiste Caillau - Math. Institute, Bourgogne Univ.
C
C     Function y = nlefun(x, p1, ...). HYBRD function argument.
C     Matlab gateway.

C     .. Global ..
#include "fintrf.h"
      INTEGER          NVINMAX
      PARAMETER       (NVINMAX = 32)
      INTEGER          NVIN
      MWPOINTER        MXVIN(NVINMAX)
      INTEGER          NFEV2
      CHARACTER*32     NLEFUN

      COMMON           /CNVIN/NVIN
      COMMON           /CMXVIN/MXVIN
      COMMON           /CNFEV2/NFEV2
      COMMON           /CNLEFUN/NLEFUN

C     .. Arguments ..
C     N                (input) INTEGER
C                      Dimension
C     X                (input) DOUBLE PRECISION array, dimension N
C                      Argument
C     Y                (output) DOUBLE PRECISION array, dimension N
C                      Result
C     IFLAG            (input) INTEGER
C                      Solver flag
      INTEGER          N, lfpar
      INTEGER          IFLAG
      DOUBLE PRECISION X(N), fpar(lfpar)
      DOUBLE PRECISION Y(N)
      CHARACTER*120    LINE

C     .. Local declarations ..
      INTEGER          NLHS0
      PARAMETER       (NLHS0 = 1)
      INTEGER          NRHS0
      PARAMETER       (NRHS0 = 1)
      INTEGER          NRHS
      INTEGER          NLHS
      MWPOINTER        PLHS(NLHS0)
      MWPOINTER        PRHS(NRHS0 + NVINMAX)
      MWPOINTER        PX
      MWPOINTER        PY
      INTEGER          RET
      INTEGER          MXGETM
      INTEGER          MXGETN
      MWPOINTER        MXGETPR
      MWPOINTER        MXCREATEDOUBLEMATRIX
      INTEGER          MEXCALLMATLAB
      INTEGER          I
      DOUBLE PRECISION DNRM2

C     .. First executable statement ..
      NRHS = NRHS0 + NVIN
      PRHS(1) = MXCREATEDOUBLEMATRIX(N, 1, 0)
      PX = MXGETPR(PRHS(1))
      CALL MXCOPYREAL8TOPTR(X, PX, N)

      DO I = 1, NVIN
        PRHS(NRHS0 + I) = MXVIN(I)
      END DO

      NLHS = NLHS0

      RET = MEXCALLMATLAB(NLHS, PLHS, NRHS, PRHS, NLEFUN)
      IF (RET.NE.0) THEN
        CALL printandstop('Problem calling nlefun.')
      END IF
      NFEV2 = NFEV2 + 1

      IF (MXGETM(PLHS(1)) * MXGETN(PLHS(1)).NE.N) THEN
        CALL printandstop('nlefun result has wrong dimension.')
      ENDIF
      PY = MXGETPR(PLHS(1))
      CALL MXCOPYPTRTOREAL8(PY, Y, N)

      IF(fpar(1).eq.1d0)THEN
        WRITE(LINE, '(    i6, $)') NFEV2
        call myprint(LINE,.false.)
        WRITE(LINE, '(e23.15, $)') DNRM2(N, Y, 1)
        call myprint(LINE,.false.)
        WRITE(LINE, '(e23.15, $)') DNRM2(N, X, 1)
        call myprint(LINE,.true.)
C      WRITE(LINE, '(a)') ''
C      call myprint(LINE,.true.)
      ENDIF

      RETURN
      END

