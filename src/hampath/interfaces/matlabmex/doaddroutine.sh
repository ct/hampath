## -----------------------------------------------------------------------------
##
## Copyright 2016, Olivier Cots.
##
## This file is part of HamPath.
##
## HamPath is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## HamPath is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with HamPath. If not, see <http://www.gnu.org/licenses/>.
##
## -----------------------------------------------------------------------------
##

#Executer depuis le repertoire de travail, ie le tmp

hfunfile=$2
nomfun=$1
hampathHome=$3
hampathInstallType=$4
output=$5

var=`cat ${hfunfile}      | sed "s/[ ][ ]*$//" | tr '[A-Z]' '[a-z]' | grep "subroutine [ ]*"${nomfun}"[ ]*(" | sed "s/.*,\([a-zA-Z0-9]*\)).*/\1/" | sed "s/ //g"`
lignedeb=`cat ${hfunfile} | sed "s/[ ][ ]*$//" | tr '[A-Z]' '[a-z]' | grep -n "subroutine [ ]*"${nomfun}"[ ]*(" | sed "s/\([0-9]*\):.*/\1/"`
lignefin=`cat ${hfunfile} | sed "s/[ ][ ]*$//" | tr '[A-Z]' '[a-z]' | grep -n "end [ ]*subroutine[ ]*"${nomfun}"$" | sed "s/\([0-9]*\):.*/\1/"`
declavar=`cat ${hfunfile} | sed "s/[ ][ ]*$//" | tr '[A-Z]' '[a-z]' | sed ${lignefin}',$d' | sed '1,'${lignedeb}'d' | grep ".*::.*[^0-9a-zA-Z]"${var}"[^0-9a-zA-Z].*"`
case ${declavar} in
    '') 
        declavar=`cat ${hfunfile} | sed "s/[ ][ ]*$//" | tr '[A-Z]' '[a-z]' | sed ${lignefin}',$d' | sed '1,'${lignedeb}'d' | grep ".*::.*[^0-9a-zA-Z]"${var}"$"` ;;
esac
type=`echo ${declavar} | grep -i "integer" | sed "s/ //g"`

case ${type} in
    '') 
         typefini=false ;;
    *) 
         typefini=true
         type=integer ;;
esac

if [ ${typefini} = "false" ]; then
    type=`echo ${declavar} | grep -i "real" | sed "s/ //g"`
    case ${type} in
        '') 
             typefini=false ;;
        *) 
             typefini=true
             type=real ;;
    esac
fi

if [ ${typefini} = "false" ]; then
    type=`echo ${declavar} | grep -i "double [ ]*precision" | sed "s/ //g"`
    case ${type} in
        '') 
             typefini=false ;;
        *) 
             typefini=true
             type=real ;;
    esac
fi

dimension=`echo ${declavar} | grep -i "dimension" | sed "s/ //g"`
case ${dimension} in
    '') 
         dimension=1 ;;
    *) 
         dimension=`echo ${declavar} | sed "s/.*dimension[ ]*(\([ ,0-9mnnparMNNPAR\-\+\*\/]*\)).*/\1/" | sed "s/ //g"` ;;
esac

testDimension=`echo ${dimension} | grep -i "," | sed "s/ //g"`
case ${testDimension} in
    '') 
         ;;
     *) echo "The dimension ("${dimension}") is not valid! Only scalar or vector are authorized!"
       echo "No interface created for "${nomfun}"!"
       echo ""
       exit ;;
esac

#echo "hfunfile="$hfunfile
#echo "fun="$nomfun
#
#echo "type="$type
#echo "dimension="${dimension}
#echo "testDimension="${testDimension}
#echo "var="$var
#
#echo "declavar="$declavar
#echo "lignedeb="$lignedeb
#echo "lignefin="$lignefin

case ${type} in
    'integer') 
        echo "The output variable "${var}" must not be an integer."
        echo "No interface created for "${nomfun}"!"
        echo ""
        ;;
    'real')

         libdirdummies=${hampathHome}/src/interfaces/matlabmex/dummies

         cat ${libdirdummies}/makefile_dummy        | sed "s/MYFUNCTION/"${nomfun}"/g"                                        > makefile${nomfun}
         cat ${libdirdummies}/fun_matlab_dummy.m    | sed "s/MYFUNCTION/"${nomfun}"/g"                                        > ${nomfun}".m"
         cat ${libdirdummies}/fun_mex_dummy.F       | sed "s/MYFUNCTION/"${nomfun}"/g" | sed "s/MYDIMENSION/"${dimension}"/g"   > ${nomfun}"_m.F"
         cat ${libdirdummies}/fun_int_dummy.f90     | sed "s/MYFUNCTION/"${nomfun}"/g" | sed "s/MYDIMENSION/"${dimension}"/g"   > ${nomfun}"int.f90"

        if make -f ./makefile${nomfun} --silent hampathHome=${hampathHome} hampathInstallType=${hampathInstallType} mex
        then
           echo " Mex-file created for "${nomfun}"(t,n,z,iarc,npar,par,"${var}") where "${var}" is a "${type}" of dimension "${dimension}"."
           echo "       matlab file, "${nomfun}".m, created and is similar to hfun.m."
           echo ""
        fi
        mv ${nomfun}.m ${output}
        ;;
esac



