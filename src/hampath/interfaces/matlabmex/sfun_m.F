C -----------------------------------------------------------------------------
C
C Copyright 2016, Olivier Cots.
C
C This file is part of HamPath.
C
C HamPath is free software: you can redistribute it and/or modify
C it under the terms of the GNU Lesser General Public License as published by
C the Free Software Foundation, either version 3 of the License, or
C (at your option) any later version.
C
C HamPath is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU Lesser General Public License for more details.
C
C You should have received a copy of the GNU Lesser General Public License
C along with HamPath. If not, see <http://www.gnu.org/licenses/>.
C
C -----------------------------------------------------------------------------
C
#include "fintrf.h"
C======================================================================
#if 0
C
C     sfun_m.F
C     .F file needs to be preprocessed to generate .for equivalent
C
#endif
C       Written on 2009-2015
C       by Oivier Cots - Math. Inst., Bourgogne Univ. / ENSEEIHT-IRIT
C       updated at INRIA Sophia Antipolis
C
C       [s, flag] = sfun_m(y,par,options.TolOdeAbs,options.TolOdeRel, ...
C                           options.ODE,options.MaxStepsOde,          ...
C                           options.MaxStepSizeOde,options.TolScaling);
C
C       .. Global Arguments ..
C
C       NLHS            (input) INTEGER
C                       Number of outputs
C       PLHS            (input) MWPOINTER array
C                       Outputs
C       NRHS            (input) INTEGER
C                       Number of inputs
C       PRHS            (input) MWPOINTER array
C                       Inputs
C                       Inputs
C
C       .. Input Arguments ..
C
C       y               : double precision vector
C       par             : double precision vector
C       dw
C       iw
C       sw
C       lsw
C
C       .. Output Arguments
C
C       s           : double precision vector
C
C======================================================================
C   Gateway routine
      subroutine mexFunction(nlhs, plhs, nrhs, prhs)

C   Declaration
      implicit none

C   mexFunction arguments
      mwPointer plhs(*), prhs(*)
      integer nlhs, nrhs

C   Arguments for checking
      integer nlhs0, nrhs0
      parameter(nlhs0 = 1)
      parameter(nrhs0 = 6)

C   Function declarations
      mwSize mxGetM, mxGetN
      mwPointer mxCreateDoubleMatrix, mxGetPr
      integer*4 mxIsNumeric, mxIsChar
      integer*4 cpxf

C   Pointers to input/output arguments
      mwPointer yPr,parPr,sPr
      mwPointer dwPr,iwPr,swPr,lswPr

C   Real, char or integer associated to pointer argument
      integer icur

C   Array information
      mwSize ny,npar,nvec
      mwSize un,ndw,niw,nsw

C-----------------------------------------------------------------------
C       Check for proper number of arguments.
      if(nrhs .ne. nrhs0) then
         call printandstop ('Wrong number of inputs.')
      elseif(nlhs .ne. nlhs0) then
         call printandstop ('Wrong number of outputs.')
      endif

C-----------------------------------------------------------------------

      un = 1
      icur = 1

C       .. Get input : y
C       Vector. isNumeric.
      nvec = min(mxGetN(prhs(icur)),mxGetM(prhs(icur)))
      if(nvec .ne. 1) then
          call printandstop ('y must be a vector.')
      end if
      if(mxIsNumeric(prhs(icur)) .eq. 0) then
         call printandstop ('y must be a numeric array.')
      endif
      ny  = max(mxGetN(prhs(icur)),mxGetM(prhs(icur)))
      yPr = mxGetPr(prhs(icur))

      icur = icur + 1
C       .. Get input : par
C       Vector. isNumeric.
      nvec = min(mxGetN(prhs(icur)),mxGetM(prhs(icur)))
      if((nvec .ne. 1) .and. (nvec .ne. 0)) then
          call printandstop ('par must be a vector.')
      end if
      if(mxIsNumeric(prhs(icur)) .eq. 0) then
         call printandstop ('par must be a numeric vector.')
      endif
      npar  = max(mxGetN(prhs(icur)),mxGetM(prhs(icur)))
      parPr = mxGetPr(prhs(icur))

      icur = icur + 1
C       .. Get input: dwork
C     isNumeric.
      if(mxGetM(prhs(icur)) .ne. 1) then
          call printandstop ('dwork must have one row.')
      end if
      if(mxIsNumeric(prhs(icur)) .eq. 0) then
         call printandstop ('dwork must be a numeric array.')
      endif
      ndw  = mxGetN(prhs(icur))
      dwPr = mxGetPr(prhs(icur))

      icur = icur + 1
C       .. Get input: iwork
C     isNumeric.
      if(mxGetM(prhs(icur)) .ne. 1) then
          call printandstop ('iwork must have one row.')
      end if
      if(mxIsNumeric(prhs(icur)) .eq. 0) then
         call printandstop ('iwork must be a numeric array.')
      endif
      niw = mxGetN(prhs(icur))
      iwPr = mxGetPr(prhs(icur))

      icur = icur + 1
C       .. Get input : swork
C     isChar.
      if(mxIsChar(prhs(icur)) .ne. 1) then
         call printandstop ('swork must be a string.')
      endif
      if(mxGetM(prhs(icur)) .ne . 1) then
         call printandstop('swork must be a row vector.')
      endif
      swPr = prhs(icur)

      icur = icur + 1
C       .. Get input: lswork
C     isNumeric.
      if(mxGetM(prhs(icur)) .ne. 1) then
          call printandstop ('lswork must have one row.')
      end if
      if(mxIsNumeric(prhs(icur)) .eq. 0) then
         call printandstop ('lswork must be a numeric array.')
      endif
      nsw = mxGetN(prhs(icur))
      lswPr = mxGetPr(prhs(icur))

C-----------------------------------------------------------------------

C    .. Create outputs ..

      cpxf = 0

      plhs(1) = mxCreateDoubleMatrix(ny, un, cpxf)
      sPr     = mxGetPr(plhs(1))

C     .. Computation ..
      call sfunmexint(ny,yPr,npar,parPr,sPr,
     +                  ndw,niw,nsw,dwPr,iwPr,swPr,lswPr)

      return
      end

C-----------------------------------------------------------------------
C     Computational routine
      subroutine sfunmexint(ny,yPr,npar,parPr,sPr,
     +                  ndw,niw,nsw,dwPr,iwPr,swPr,lswPr)

      implicit none

      mwSize    ny,npar,ndw,niw,nsw
      mwPointer yPr,parPr,sPr,dwPr,iwPr,swPr,lswPr

      !Local variables
      double precision y(ny),par(npar)
      double precision s(ny)

      !Local variables for options
      integer*4             mxGetString, statu
      mwSize                maxBuf, mxGetM, mxGetN
      double precision      dw(ndw), iwD(niw), lswD(nsw)
      integer               iw(niw), lsw(nsw)
      character*(32*nsw)    sw
      maxBuf = 32*nsw

      !Copy to double precision arrays
      call mxCopyPtrToReal8(yPr,y,ny)
      call mxCopyPtrToReal8(parPr,par,npar)

      call mxCopyPtrToReal8(dwPr ,dw  ,ndw)
      call mxCopyPtrToReal8(iwPr ,iwD ,niw)
      call mxCopyPtrToReal8(lswPr,lswD,nsw)

      !Copy integer arrays
      iw    = nint(iwD)
      lsw   = nint(lswD)

      !Get strings options
      if(mxGetM(swPr)*mxGetN(swPr) .gt. maxBuf) then
         call printandstop ('swork max length sticked out.')
      endif
      statu = mxGetString(swPr,sw,maxbuf)
      if(statu .ne. 0) then
         call printandstop('swork error reading.')
      endif

      call sfunint(ny,y,npar,par,s,ndw,niw,nsw,dw,iw,sw,lsw)

      call mxCopyReal8ToPtr(s,sPr,ny)

      return
      end

