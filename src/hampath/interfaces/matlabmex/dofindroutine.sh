## -----------------------------------------------------------------------------
##
## Copyright 2016, Olivier Cots.
##
## This file is part of HamPath.
##
## HamPath is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## HamPath is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with HamPath. If not, see <http://www.gnu.org/licenses/>.
##
## -----------------------------------------------------------------------------
##

#Executer depuis le repertoire de travail, ie le tmp

testafun=$1
output=$2
hampathHome=$3
hampathInstallType=$4

if [ $testafun = "true" ]; then
    afunfile=afun.f90
    funs=`cat $afunfile | sed "/^!/d" | tr '[A-Z]' '[a-z]' | grep "[ ]*end [ ]*subroutine" | awk '{print $3}'`

    case $funs in
        '') ;;
        *)  echo " Only extra subroutines in afun.f90 with the same input(in) variables as hfun and only one output"
            echo " (a scalar or a vector) will have the possibility to be called as matlab functions."
            echo ""
            ;;
    esac

    for fun in $funs
    do
    funclean=`echo $fun | sed "s/ //g" | sed "s/\(.*\)!.*/\1/"`
    doit=`cat $afunfile | tr '[A-Z]' '[a-z]' | grep "subroutine [ ]*"$funclean"[ ]*(t,n,z,iarc,npar,par,[a-zA-Z0-9]*)"`
    case $doit in
        '') echo "Nothing done for "${funclean}"." ;;
        *)  sh doaddroutine.sh ${funclean} ${afunfile} ${hampathHome} ${hampathInstallType} ${output};;
    esac
    done
fi

