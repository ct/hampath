!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
program mod_redirect
    implicit none

    !arguments names
    character(len=200) :: nomfilein, nomfileout
    character(len=32)  :: nomfun, nomfunint, nominterface
    logical            :: file_exists

    integer :: unitfile, iost

    call getarg(1,nomfilein)
    call getarg(2,nomfileout)
    call getarg(3,nomfun)
    call getarg(4,nominterface)
    call getarg(5,nomfunint)

    inquire(file=trim(nomfilein), exist=file_exists)
    if(file_exists) then
        unitfile = 10
        open(unit=unitfile,file=trim(nomfilein),action="read",iostat=iost)

        if(iost.eq.0)then
            select case (trim(nomfun))

                case('hfun')
                    call      hfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('hvfun')
                    call     hvfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('dhvfun')
                    call    dhvfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('exphvfun')
                    call  exphvfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('expdhvfun')
                    call expdhvfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('sfun')
                    call      sfunRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('sjac')
                    call      sjacRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('ssolve')
                    call    ssolveRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
                case('hampath')
                    call   hampathRoutine(unitfile,trim(nomfileout),trim(nominterface),trim(nomfunint))
!ADDHERE
                case default
                    write(*,*) "Error: no function ", trim(nomfun), "!"

            end select

            close(unit=unitfile)
        else
            write(*,*) "Error during ", trim(nomfun), "call!" 
        end if

    else
        write(*,*) "Error: no file ", trim(nomfilein), "!"
    end if

end program mod_redirect

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  hfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_hamfun::hamfun routine to execute main process        </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine hfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_hamfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, nbarc, lpar, i, unitfileout, iost
    double precision, dimension(:)  , allocatable :: t, par, h, ti
    double precision, dimension(:,:), allocatable :: z

    CHARACTER(len=120)  :: LINE
!    CHARACTER(len = 8)  :: frmt ! format
!    CHARACTER(len = 8)  :: DateINFO ! ccyymmdd
!    CHARACTER(len = 10) :: TimeINFO ! hhmmss.sss

    !Read datas from file nomfilein
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(t(nt), z(2*n,nt), h(nt), par(lpar), ti(nbarc+1))
    read(unitfile,*) t
    do i=1,2*n
        read(unitfile,*) z(i,:)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call hamfun(t,z,ti,par,h)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!!
                WRITE (LINE,*) 'function h = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
!                CALL DATE_AND_TIME(DateINFO, TimeINFO) !The current time, in the form hhmmss.sss, where hh is the hour, 
!                                                    !mm minutes, and ss.sss seconds and milliseconds. 
!                WRITE (LINE,*) '%Created by Hampath on ', DateINFO(1:4),'/',DateINFO(5:6),'/',DateINFO(7:8), &
!                                                ' at ',TimeINFO(1:2),'h ',TimeINFO(3:4),'min ',TimeINFO(5:6),'s.'
!                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeRowVectorDbleMatlab(h,nt,'h',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during hfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(t,z,h,par,ti)

end subroutine hfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  hvfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_hvfun::hvfun routine to execute main process          </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine hvfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_hvfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, nbarc, lpar, i, unitfileout, iost
    double precision, dimension(:)  , allocatable :: t, par, ti
    double precision, dimension(:,:), allocatable :: z, hv

    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(t(nt), z(2*n,nt), hv(2*n,nt), par(lpar), ti(nbarc+1))
    read(unitfile,*) t
    do i=1,2*n
        read(unitfile,*) z(i,:)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call hvfun(t,z,ti,par,hv)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then
        select case (nominterface)

            case('matlab')

                WRITE (LINE,*) 'function hv = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeMatrixDbleMatlab(hv,2*n,nt,'hv',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during hvfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(t,z,hv,par,ti)

end subroutine hvfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  dhvfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_dhvfun::dhvfun routine to execute main process        </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
subroutine dhvfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_dhvfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, kdz, nbarc, lpar, i, unitfileout, iost
    double precision, dimension(:)  , allocatable :: t, par, ti
    double precision, dimension(:,:), allocatable :: z, dz, hv, dhv

    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) kdz
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(t(nt), z(2*n,nt), dz(2*n,nt*kdz), hv(2*n,nt), dhv(2*n,nt*kdz), par(lpar), ti(nbarc+1))
    read(unitfile,*) t
    do i=1,2*n
        read(unitfile,*) z(i,:)
    end do
    do i=1,2*n
        read(unitfile,*) dz(i,:)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call dhvfun(t,z,dz,ti,par,hv,dhv)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then
        select case (nominterface)

            case('matlab')

                WRITE (LINE,*) 'function [hv,dhv] = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeMatrixDbleMatlab(hv,2*n,nt,'hv',unitfileout)
                call writeMatrixDbleMatlab(dhv,2*n,nt*kdz,'dhv',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during dhvfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(t,z,dz,hv,dhv,par,ti)

end subroutine dhvfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  Retrieve options from an input text file.
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile            </li>
!!      <li> Set options calling mod_options::setOption     </li>
!!      </ul>
!!
!!  \param[in]  unitfile     unit number corresponding to the file containing input datas
!!  \param[out] options      structure containing all options written in the input file
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
subroutine getOptionsFromFile(unitfile,options)
    use mod_options
    implicit none
    integer,        intent(in)  :: unitfile
    type(TOptions), intent(out) :: options

    !local variables
    character(32)       :: Derivative
    integer             :: Dispiter
    character(32)       :: Display
    character(32)       :: DoSavePath
    character(32)       :: IrkInit
    character(32)       :: IrkSolver
    integer             :: MaxFEval
    integer             :: MaxIterCorrection
    double precision    :: MaxSf
    double precision    :: MaxSfunNorm
    integer             :: MaxStepsOde
    integer             :: MaxStepsOdeHam
    double precision    :: MaxStepSizeOde
    double precision    :: MaxStepSizeOdeHam
    double precision    :: MaxStepSizeHomPar
    character(32)       :: ODE
    character(32)       :: ODEHam
    character(32)       :: SolverMethod
    integer             :: StopAtTurningPoint
    double precision    :: TolOdeAbs
    double precision    :: TolOdeRel
    double precision    :: TolOdeHamAbs
    double precision    :: TolOdeHamRel
    double precision    :: TolDenseHamEnd
    double precision    :: TolDenseHamX
    double precision    :: TolX

    read(unitfile,*)     Derivative
    read(unitfile,*)     Dispiter
    read(unitfile,*)     Display
    read(unitfile,*)     DoSavePath
    read(unitfile,*)     IrkInit
    read(unitfile,*)     IrkSolver
    read(unitfile,*)     MaxFEval
    read(unitfile,*)     MaxIterCorrection
    read(unitfile,*)     MaxSf
    read(unitfile,*)     MaxSfunNorm
    read(unitfile,*)     MaxStepsOde
    read(unitfile,*)     MaxStepsOdeHam
    read(unitfile,*)     MaxStepSizeOde
    read(unitfile,*)     MaxStepSizeOdeHam
    read(unitfile,*)     MaxStepSizeHomPar
    read(unitfile,*)     ODE
    read(unitfile,*)     ODEHam
    read(unitfile,*)     SolverMethod
    read(unitfile,*)     StopAtTurningPoint
    read(unitfile,*)     TolOdeAbs
    read(unitfile,*)     TolOdeRel
    read(unitfile,*)     TolOdeHamAbs
    read(unitfile,*)     TolOdeHamRel
    read(unitfile,*)     TolDenseHamEnd
    read(unitfile,*)     TolDenseHamX
    read(unitfile,*)     TolX

    call setOption(options,'Derivative'         ,Derivative  )
    call setOption(options,'Display'            ,Display  )
    call setOption(options,'DoSavePath'         ,DoSavePath  )
    call setOption(options,'IrkInit'            ,IrkInit  )
    call setOption(options,'IrkSolver'          ,IrkSolver  )
    call setOption(options,'ODE'                ,ODE  )
    call setOption(options,'ODEHam'             ,ODEHam  )
    call setOption(options,'SolverMethod'       ,SolverMethod  )

    call setOption(options,'DispIter'           ,Dispiter  )
    call setOption(options,'MaxFEval'           ,MaxFEval  )
    call setOption(options,'MaxIterCorrection'  ,MaxIterCorrection  )
    call setOption(options,'MaxStepsOde'        ,MaxStepsOde  )
    call setOption(options,'MaxStepsOdeHam'     ,MaxStepsOdeHam  )
    call setOption(options,'StopAtTurningPoint' ,StopAtTurningPoint  )

    call setOption(options,'MaxSf'              ,MaxSf  )
    call setOption(options,'MaxSfunNorm'        ,MaxSfunNorm  )
    call setOption(options,'MaxStepSizeOde'     ,MaxStepSizeOde  )
    call setOption(options,'MaxStepSizeOdeHam'  ,MaxStepSizeOdeHam  )
    call setOption(options,'MaxStepSizeHomPar'  ,MaxStepSizeHomPar  )
    call setOption(options,'TolOdeAbs'          ,TolOdeAbs  )
    call setOption(options,'TolOdeRel'          ,TolOdeRel  )
    call setOption(options,'TolOdeHamAbs'       ,TolOdeHamAbs  )
    call setOption(options,'TolOdeHamRel'       ,TolOdeHamRel  )
    call setOption(options,'TolDenseHamEnd'     ,TolDenseHamEnd  )
    call setOption(options,'TolDenseHamX'       ,TolDenseHamX  )
    call setOption(options,'TolX'               ,TolX  )

end subroutine getOptionsFromFile

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  exphvfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_exphvfun::exphvfun routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine exphvfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_exphvfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, nbarc, lpar, i, unitfileout, iost, flag, nfev
    double precision, dimension(:)  , allocatable :: tspan, par, z0, ti, tout
    double precision, dimension(:,:), allocatable :: exphv
    type(TOptions)                                :: options


    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(tspan(nt), z0(2*n), par(lpar), ti(nbarc+1))
    read(unitfile,*) tspan
    do i=1,2*n
        read(unitfile,*) z0(i)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call exphvfun(tspan, z0, ti, options, par, tout, exphv, flag, nfev)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!
                WRITE (LINE,*) 'function [tout,exphv,flag,nfev] = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeRowVectorDbleMatlab(tout,size(tout),'tout',unitfileout)
                call writeMatrixDbleMatlab(exphv,size(exphv,1),size(exphv,2),'exphv',unitfileout)
                call writeIntegerMatlab(flag,'flag',unitfileout)
                call writeIntegerMatlab(nfev,'nfev',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during exphvfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(tspan,z0,par,ti,tout,exphv)

end subroutine exphvfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  expdhvfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_expdhvfun::expdhvfun routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
    subroutine expdhvfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_expdhvfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, nbarc, lpar, i, unitfileout, iost, flag, kdz, nfev
    double precision, dimension(:)  , allocatable :: tspan, par, z0, ti, tout
    double precision, dimension(:,:), allocatable :: z, dz, dz0
    type(TOptions)                                :: options


    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) kdz
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(tspan(nt), z0(2*n), dz0(2*n,kdz), par(lpar), ti(nbarc+1))
    read(unitfile,*) tspan
    do i=1,2*n
        read(unitfile,*) z0(i)
    end do
    do i=1,2*n
        read(unitfile,*) dz0(i,:)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

!    write(*,*) 'tspan   =', tspan
!    write(*,*) 'z0      =', z0
!    write(*,*) 'dz0     =', dz0
!    write(*,*) 'ti      =', ti
!    write(*,*) 'par     =', par
    !Call main routine
    call expdhvfun(tspan, z0, dz0, ti, options, par, tout, z, dz, flag, nfev)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!
                WRITE (LINE,*) 'function [tout,z,dz,flag,nfev] = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeRowVectorDbleMatlab(tout,size(tout),'tout',unitfileout)
                call writeMatrixDbleMatlab(z,size(z,1),size(z,2),'z',unitfileout)
                call writeMatrixDbleMatlab(dz,size(dz,1),size(dz,2),'dz',unitfileout)
                call writeIntegerMatlab(flag,'flag',unitfileout)
                call writeIntegerMatlab(nfev,'nfev',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during expdhvfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(tspan,z0,par,ti,tout,z,dz)

end subroutine expdhvfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  sfun interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_sfun::sfun routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
    subroutine sfunRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_options
    use mod_sfun
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: ny, npar, i, unitfileout, iost
    double precision, dimension(:)  , allocatable :: par, y, s
    type(TOptions)                                :: options
    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) ny
    read(unitfile,*) npar
    allocate(y(ny), par(npar)) !y, par sont ecrit en colonne dans le fichier
    do i=1,ny
        read(unitfile,*) y(i)
    end do
    do i=1,npar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    allocate(s(ny))
    call sfun(y, options, par, s)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!
                WRITE (LINE,*) 'function s = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeColVectorDbleMatlab(s,ny,'s',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'


            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during sfun call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(y,par,s)

end subroutine sfunRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  sjac interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_sjac::sjac routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
subroutine sjacRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_options
    use mod_sjac
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: ny, npar, nipar, i, unitfileout, iost, nl
    integer,          dimension(:)  , allocatable :: ipar
    double precision, dimension(:)  , allocatable :: par, y
    double precision, dimension(:,:), allocatable :: fjac
    type(TOptions)                                :: options
    CHARACTER(len=120)  :: LINE

!Description of input file
!options
!ny
!npar
!nipar
!ipar(1,nipar)
!y(1,ny)
!par(1,npar)
    !Read datas from file nomfilein
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) ny
    read(unitfile,*) npar
    read(unitfile,*) nipar
    allocate(y(ny), par(npar), ipar(nipar)) !y, par et ipar sont ecrit en colonne dans le fichier
    do i=1,nipar
        read(unitfile,*) ipar(i)
    end do
    do i=1,ny
        read(unitfile,*) y(i)
    end do
    do i=1,npar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    if(nipar.gt.0)then
        nl = 1
    else
        nl = 0
    end if
    allocate(fjac(ny,ny+nl))
    call sjac(y, options, par, ipar, fjac)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!
                WRITE (LINE,*) 'function j = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeMatrixDbleMatlab(fjac,ny,ny+nl,'j',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during sjac call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(y,par,ipar,fjac)

end subroutine sjacRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  ssolve interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_ssolve::ssolve routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
subroutine ssolveRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_options
    use mod_ssolve
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: ny, npar, i, unitfileout, iost, flag, nfev, njev
    double precision, dimension(:)  , allocatable :: par, y0, ysol, ssol
    type(TOptions)                                :: options
    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
!options
!ny
!npar
!y0(ny,1)
!par(npar,1)
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) ny
    read(unitfile,*) npar
    allocate(y0(ny), par(npar), ysol(ny), ssol(ny))
    do i=1,ny
        read(unitfile,*) y0(i)
    end do
    do i=1,npar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call ssolve(y0, options, par, ysol, ssol, nfev, njev, flag)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then
        select case (nominterface)

            case('matlab')

                WRITE (LINE,*) 'function [ ysol, ssol, nfev, njev, flag] = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeColVectorDbleMatlab(ysol,ny,'ysol',unitfileout)
                call writeColVectorDbleMatlab(ssol,ny,'ssol',unitfileout)
                call writeIntegerMatlab(nfev,'nfev',unitfileout)
                call writeIntegerMatlab(njev,'njev',unitfileout)
                call writeIntegerMatlab(flag,'flag',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during ssolve call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(y0,par,ssol,ysol)

end subroutine ssolveRoutine

!! -----------------------------------------------------------------------------
!>  \ingroup modulesInterfaces
!!  \brief  hampath interface between fortran and other language (Matlab, Octave...).
!!  \details
!!
!!      Detailed description:
!!
!!      <ul>
!!      <li> Get input datas: read from unitfile and allocate memory        </li>
!!      <li> Call mod_hampath::hampath routine to execute main process    </li>
!!      <li> Write output datas in nomfileout and deallocate memory         </li>
!!      </ul>
!!
!!  \param[in] unitfile     unit number corresponding to the file containing input datas
!!  \param[in] nomfileout   file name for output datas
!!  \param[in] nominterface platform name: Matlab...
!!  \param[in] nomfunint    function name called at the end by the interface to get output datas
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
subroutine hampathRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_options
    use mod_hampath
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                        :: ny, npar, i, unitfileout, iost, flag
    double precision, dimension(:)  , allocatable  :: y0
    double precision, dimension(:,:), allocatable  :: parspan

    double precision,  dimension(:,:), allocatable :: parout
    double precision,  dimension(:,:), allocatable :: yout
    double precision,  dimension(:),   allocatable :: sout
    double precision,  dimension(:,:), allocatable :: viout
    double precision,  dimension(:),   allocatable :: dets
    double precision,  dimension(:),   allocatable :: normS
    double precision,  dimension(:),   allocatable :: ps
    type(TOptions)                                 :: options
    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    call getOptionsFromFile(unitfile,options)!Get options
    read(unitfile,*) ny
    read(unitfile,*) npar
    allocate(y0(ny), parspan(npar,2))
    do i=1,ny
        read(unitfile,*) y0(i)
    end do
    do i=1,npar
        read(unitfile,*) parspan(i,:)
    end do

    !Call main routine
    call hampath(parspan,y0,options,parout,yout,sout,viout,dets,normS,ps,flag)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!
                WRITE (LINE,*) 'function [ parout, yout, sout, viout, dets, normS, ps, flag ] = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeMatrixDbleMatlab(parout,size(parout,1),size(parout,2),'parout',unitfileout)
                call writeMatrixDbleMatlab(yout,size(yout,1),size(yout,2),'yout',unitfileout)
                call writeRowVectorDbleMatlab(sout,size(sout),'sout',unitfileout)
                call writeMatrixDbleMatlab(viout,size(viout,1),size(viout,2),'viout',unitfileout)
                call writeRowVectorDbleMatlab(dets,size(dets),'dets',unitfileout)
                call writeRowVectorDbleMatlab(normS,size(normS),'normS',unitfileout)
                call writeRowVectorDbleMatlab(ps,size(ps),'ps',unitfileout)
                call writeIntegerMatlab(flag,'flag',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during hampath call!" 
    end if

    !Deallocate all allocatable variables
    deallocate(y0,parspan,parout,yout,sout,viout,dets,normS,ps)

end subroutine hampathRoutine
