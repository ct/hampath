!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!% dhvfun -- Vector field associated to H (needs hfun.f90)
!%
!% Usage
!%  [hv, dhv] = dhvfun(t, z, dz,     par) : single arc
!%  [hv, dhv] = dhvfun(t, z, dz, ti, par) : multiple arcs
!%
!% Inputs
!%  t    -  real row vector, t(i) = i-th time
!%  z    -  real matrix, z(:,i) = i-th state and costate
!%  dz   -  real matrix, dz(:,(i-1)*k+1:i*k) = the k i-th Jacobi fields
!%  ti   -  real row vector, in multiple shooting case 
!%          ti = [t0 t1 ... t_nbarc-1 tf]
!%  par  -  real row vector, parameters given to hfun
!%          par=[] if no parameters
!%
!% Outputs
!%  hv   -  real matrix, Hamiltonian vector field associated to H at time t
!%  dhv  -  real matrix, Linearized of the Hamiltonian vector field at time t
!%
!% Description
!%  Computes the second member of the variational system associated to H.
!%
module mod_dhvfun
    implicit none
    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   dhvfun interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    interface dhvfun
        module procedure dhvfunSimple, dhvfunSimpleSS, dhvfunSimpleSM
        module procedure dhvfunMultiple, dhvfunMultipleSS, dhvfunMultipleSM
    end interface
    contains

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the simple shooting case with a scalar time
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunSimpleSS(t,z,dz,par,hv,dhv)
        implicit none
        double precision,                   intent(in)   :: t
        double precision,  dimension(:),    intent(in)   :: z
        double precision,  dimension(:),    intent(in)   :: dz
        double precision,  dimension(:),    intent(in)   :: par
        double precision,  dimension(:),    intent(out)  :: hv
        double precision,  dimension(:),    intent(out)  :: dhv

        !local variable
        integer          :: n,nbarc,lpar,nt,k
        double precision :: ti(2)
        double precision :: tint(1), zint(size(z),1), dzint(size(z),1)
        double precision :: hvint(size(z),1), dhvint(size(z),1)

        nt      = 1
        n       = size(z)/2
        k       = 1
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = t
        ti(2)   = t+1d0

        tint(1)     = t
        zint(:,1)   = z
        dzint(:,1)  = dz

        call dhvfunint(tint,n,k,zint,dz,nbarc,ti,lpar,par,nt,hvint,dhvint)

        hv  =  hvint(:,1)
        dhv = dhvint(:,1)

    end subroutine dhvfunSimpleSS

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the simple shooting case with a scalar time
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunSimpleSM(t,z,dz,par,hv,dhv)
        implicit none
        double precision,                   intent(in)   :: t
        double precision,  dimension(:),    intent(in)   :: z
        double precision,  dimension(:,:),  intent(in)   :: dz
        double precision,  dimension(:),    intent(in)   :: par
        double precision,  dimension(:),    intent(out)  :: hv
        double precision,  dimension(:,:),  intent(out)  :: dhv

        !local variable
        integer          :: n,nbarc,lpar,nt,k
        double precision :: ti(2)
        double precision :: tint(1), zint(size(z),1)
        double precision :: hvint(size(z),1)

        nt      = 1
        n       = size(z)/2
        k       = size(dz,2)/nt
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = t
        ti(2)   = t+1d0

        tint(1)     = t
        zint(:,1)   = z

        call dhvfunint(tint,n,k,zint,dz,nbarc,ti,lpar,par,nt,hvint,dhv)

        hv = hvint(:,1)

    end subroutine dhvfunSimpleSM

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the simple shooting case with a vector of times
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunSimple(t,z,dz,par,hv,dhv)
        implicit none
        double precision,  dimension(:),   intent(in)   :: t
        double precision,  dimension(:,:), intent(in)   :: z
        double precision,  dimension(:,:), intent(in)   :: dz
        double precision,  dimension(:),   intent(in)   :: par
        double precision,  dimension(:,:), intent(out)  :: hv
        double precision,  dimension(:,:), intent(out)  :: dhv

        !local variable
        integer          :: n,nbarc,lpar,nt,k
        double precision :: ti(2)

        nt      = size(t)
        n       = size(z,1)/2
        k       = size(dz,2)/nt
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = t(1)
        ti(2)   = t(nt)

        call dhvfunint(t,n,k,z,dz,nbarc,ti,lpar,par,nt,hv,dhv)

    end subroutine dhvfunSimple

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the multiple shooting case with a scalar time
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf]
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunMultipleSS(t,z,dz,ti,par,hv,dhv)
        implicit none
        double precision,                   intent(in)  :: t
        double precision,  dimension(:),    intent(in)  :: z
        double precision,  dimension(:),    intent(in)  :: dz
        double precision,  dimension(:),    intent(in)  :: ti
        double precision,  dimension(:),    intent(in)  :: par
        double precision,  dimension(:),    intent(out) :: hv
        double precision,  dimension(:),    intent(out) :: dhv

        !local variable
        integer          :: n,nbarc,lpar,nt,k
        double precision :: tint(1), zint(size(z),1), dzint(size(z),1)
        double precision :: hvint(size(z),1), dhvint(size(z),1)

        nt      = 1
        n       = size(z)/2
        k       = 1
        lpar    = size(par)
        nbarc   = size(ti)-1

        tint(1)     = t
        zint(:,1)   = z
        dzint(:,1)  = dz

        call dhvfunint(tint,n,k,zint,dzint,nbarc,ti,lpar,par,nt,hvint,dhvint)

        hv  =  hvint(:,1)
        dhv = dhvint(:,1)

    end subroutine dhvfunMultipleSS

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the multiple shooting case with a scalar time
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf]
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunMultipleSM(t,z,dz,ti,par,hv,dhv)
        implicit none
        double precision,                   intent(in)   :: t
        double precision,  dimension(:),    intent(in)   :: z
        double precision,  dimension(:,:),  intent(in)   :: dz
        double precision,  dimension(:),    intent(in)   :: ti
        double precision,  dimension(:),    intent(in)   :: par
        double precision,  dimension(:),    intent(out)  :: hv
        double precision,  dimension(:,:),  intent(out)  :: dhv

        !local variable
        integer          :: n,nbarc,lpar,nt,k
        double precision :: tint(1), zint(size(z),1)
        double precision :: hvint(size(z),1)

        nt      = 1
        n       = size(z)/2
        k       = size(dz,2)/nt
        lpar    = size(par)
        nbarc   = size(ti)-1

        tint(1)     = t
        zint(:,1)   = z

        call dhvfunint(tint,n,k,zint,dz,nbarc,ti,lpar,par,nt,hvint,dhv)

        hv = hvint(:,1)

    end subroutine dhvfunMultipleSM

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to dhvfunint() in the multiple shooting case with a vector of times
    !!  \details This subroutine is called through mod_dhvfun::dhvfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  dz      Jacobi fields
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf]
    !!  \param[in]  par     parameters
    !!  \param[out] hv      Hamiltonian vector field at time t
    !!  \param[out] dhv     Linearized Hamiltonian vector field at time t
    !!
    !!  \author Olivier Cots
    !!  \date   2014
    !!  \copyright LGPL
    !!
    Subroutine dhvfunMultiple(t,z,dz,ti,par,hv,dhv)
        implicit none
        double precision,  dimension(:),   intent(in)   :: t
        double precision,  dimension(:,:), intent(in)   :: z
        double precision,  dimension(:,:), intent(in)   :: dz
        double precision,  dimension(:),   intent(in)   :: ti
        double precision,  dimension(:),   intent(in)   :: par
        double precision,  dimension(:,:), intent(out)  :: hv
        double precision,  dimension(:,:), intent(out)  :: dhv

        !local variable
        integer :: n,nbarc,lpar,nt,k

        nt      = size(t)
        n       = size(z,1)/2
        k       = size(dz,2)/nt
        lpar    = size(par)
        nbarc   = size(ti)-1

        call dhvfunint(t,n,k,z,dz,nbarc,ti,lpar,par,nt,hv,dhv)

    end subroutine dhvfunMultiple

end module mod_dhvfun
