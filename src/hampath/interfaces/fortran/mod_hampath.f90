!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_hampath
    use utils
    use mod_options
    implicit none

    contains
    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to hampathint() in the simple shooting case
    !!  \details This subroutine is called through mod_hampath::hampath interface.
    !!
    !!  \param[in]  parspan parspan = [par0, parf]. parspan[:,i] is a parameter vector
    !!                      given to hfun and efun. The homotopy is from par0 to parf and of
    !!                      the form : (1-lambda)*par0 + lambda*parf, unless a fortran file with
    !!                      the subroutine parfun(lambda,lpar,par0,parf,parout), with lambda
    !!                      from 0 to 1, is provided.
    !!  \param[in]  y0      initial solution, ie "sfun(y0,options,par0) = 0"
    !!  \param[in]  options hampath options
    !!  \param[out] parout  parameters at each integration step
    !!  \param[out] yout    solutions along the paths, ie "sfun(yout(:,k),options,parout(:,k) = 0"
    !!  \param[out] sout    arc length at each integration step
    !!  \param[out] viout   viout[:,k] = dot_c[:,k] where c = (y, lambda) and
    !!                      dot is the derivative w.r.t. arc length
    !!  \param[out] dets    det(h'(c(s)),c'(s)) at each integration step, where
    !!                      h is the homotopic function
    !!  \param[out] normS   Norm of the shooting function at each integration step
    !!  \param[out] ps      <c'(s_old),c'(s)> at each integreation step
    !!  \param[out] flag    flag should be 1 (ODE integrator output) <br>
    !!                      if flag==0  then Sfmax is too small <br>
    !!                      if flag==-1 then input is not consistent <br>
    !!                      if flag==-2 then larger MaxSteps is needed <br>
    !!                      if flag==-3 then step size became too small <br>
    !!                      if flag==-4 then problem is problably stiff <br>
    !!                      if flag==-5 then |S| is greater than NormeSfunMax <br>
    !!                      if flag==-6 then the homotopic parameter do not
    !!                          evolve relatively wrt TolDenseHamX <br>
    !!                      if flag==-7 then a turning point occurs
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine hampath(parspan,y0,options,parout,yout,sout,viout,dets,normS,ps,flag)
        implicit none
        double precision,  dimension(:,:),              intent(in)  :: parspan
        double precision,  dimension(:),                intent(in)  :: y0
        type(TOptions),                                 intent(in)  :: options
        double precision,  dimension(:,:), allocatable, intent(out) :: parout
        double precision,  dimension(:,:), allocatable, intent(out) :: yout
        double precision,  dimension(:),   allocatable, intent(out) :: sout
        double precision,  dimension(:,:), allocatable, intent(out) :: viout
        double precision,  dimension(:),   allocatable, intent(out) :: dets
        double precision,  dimension(:),   allocatable, intent(out) :: normS
        double precision,  dimension(:),   allocatable, intent(out) :: ps
        integer,                                        intent(out) :: flag

        !local variable
        integer                                         :: ny,nparspan,mparspan,dimepath
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable   :: sw

        ny       = size(y0)
        nparspan = size(parspan,1)
        mparspan = size(parspan,2)

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call hampathint(ny,y0,nparspan,mparspan,parspan,     &
                        dimepath,flag,                       &
                        ndw,niw,nsw,dw,iw,sw,lsw)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

        allocate(parout(nparspan,dimepath))
        allocate(yout(ny,dimepath))
        allocate(sout(dimepath))
        allocate(viout(ny+1,dimepath))
        allocate(dets(dimepath))
        allocate(normS(dimepath))
        allocate(ps(dimepath))

        call getpath(ny,nparspan,dimepath,parout,yout,viout,sout,dets,normS,ps)

    end subroutine hampath

end module mod_hampath
