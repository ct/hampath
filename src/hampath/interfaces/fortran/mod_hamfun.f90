!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_hamfun
    implicit none
    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   hamfun interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    interface hamfun
        module procedure hamfunSimple, hamfunSimpleSingle
        module procedure hamfunMultiple, hamfunMultipleSingle
    end interface
    contains

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to hfunint() in the simple shooting case with a scalar time
    !!  \details This subroutine is called through mod_hamfun::hamfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  par     parameters
    !!  \param[out] h       Hamiltonian
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    Subroutine hamfunSimpleSingle(t,z,par,h)
        implicit none
        double precision,                   intent(in)   :: t
        double precision,  dimension(:),    intent(in)   :: z
        double precision,  dimension(:),    intent(in)   :: par
        double precision,                   intent(out)  :: h

        !local variable
        integer          :: n,nbarc,lpar,lvalue
        double precision :: ti(2)
        double precision :: tint(1), zint(size(z),1), hint(1)

        lvalue  = 1
        n       = size(z)/2
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = t
        ti(2)   = t+1d0

        tint(1)     = t
        zint(:,1)   = z

        call hfunint(tint,n,zint,nbarc,ti,lpar,par,lvalue,hint)

        h = hint(1)

    end subroutine hamfunSimpleSingle

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to hfunint() in the simple shooting case with a vector of times
    !!  \details This subroutine is called through mod_hamfun::hamfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  par     parameters
    !!  \param[out] h       Hamiltonian
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    Subroutine hamfunSimple(t,z,par,h)
        implicit none
        double precision,  dimension(:),   intent(in)   :: t
        double precision,  dimension(:,:), intent(in)   :: z
        double precision,  dimension(:),   intent(in)   :: par
        double precision,  dimension(:),   intent(out)  :: h

        !local variable
        integer          :: n,nbarc,lpar,lvalue
        double precision :: ti(2)

        lvalue  = size(t)
        n       = size(z,1)/2
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = t(1)
        ti(2)   = t(lvalue)

        call hfunint(t,n,z,nbarc,ti,lpar,par,lvalue,h)

    end subroutine hamfunSimple

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to hfunint() in the multiple shooting case with a scalar time
    !!  \details This subroutine is called through mod_hamfun::hamfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf]
    !!  \param[in]  par     parameters
    !!  \param[out] h       Hamiltonian
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    Subroutine hamfunMultipleSingle(t,z,ti,par,h)
        implicit none
        double precision,                   intent(in)  :: t
        double precision,  dimension(:),    intent(in)  :: z
        double precision,  dimension(:),    intent(in)  :: ti
        double precision,  dimension(:),    intent(in)  :: par
        double precision,                   intent(out) :: h

        !local variable
        integer          :: n,nbarc,lpar,lvalue
        double precision :: tint(1), zint(size(z),1), hint(1)

        lvalue  = 1
        n       = size(z)/2
        lpar    = size(par)
        nbarc   = size(ti)-1

        tint(1)     = t
        zint(:,1)   = z

        call hfunint(tint,n,zint,nbarc,ti,lpar,par,lvalue,hint)

        h = hint(1)

    end subroutine hamfunMultipleSingle

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to hfunint() in the multiple shooting case with a vector of times
    !!  \details This subroutine is called through mod_hamfun::hamfun interface.
    !!
    !!  \param[in]  t       time
    !!  \param[in]  z       state-costate
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf]
    !!  \param[in]  par     parameters
    !!  \param[out] h       Hamiltonian
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    Subroutine hamfunMultiple(t,z,ti,par,h)
        implicit none
        double precision, intent(in),  dimension(:)     :: t
        double precision, intent(in),  dimension(:,:)   :: z
        double precision, intent(in),  dimension(:)     :: ti
        double precision, intent(in),  dimension(:)     :: par
        double precision, intent(out), dimension(:)     :: h

        !local variable
        integer          :: n,nbarc,lpar,lvalue

        lvalue  = size(t)
        n       = size(z,1)/2
        lpar    = size(par)
        nbarc   = size(ti)-1

        call hfunint(t,n,z,nbarc,ti,lpar,par,lvalue,h)

    end subroutine hamfunMultiple

end module mod_hamfun
