!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_ssolve
    use utils
    use mod_options
    implicit none

    contains

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to ssolveint() in the simple shooting case
    !!  \details This subroutine is called through mod_ssolve::ssolve interface.
    !!
    !!  \param[in]  y0      initial guess for shooting variable
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[out] ysol    shooting variable solution
    !!  \param[out] ssol    value of sfun at ysol
    !!  \param[out] nfev    number of evaluations of sfun
    !!  \param[out] njev    number of evaluations of sjac
    !!  \param[out] flag    solver output: should be 1
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine ssolve(y0, options, par, ysol, ssol, nfev, njev, flag)
        implicit none
        double precision,  dimension(:), intent(in)  :: y0
        type(TOptions),                  intent(in)  :: options
        double precision,  dimension(:), intent(in)  :: par
        double precision,  dimension(:), intent(out) :: ssol, ysol
        integer,                         intent(out) :: flag, nfev, njev

        !local variable
        integer                                         :: ny,npar
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable   :: sw

        ny       = size(y0)
        npar     = size(par)

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call ssolveint(ny,y0,npar,par,ysol,ssol,nfev,njev,flag, &
                        ndw,niw,nsw,dw,iw,sw,lsw)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

    end subroutine ssolve

end module mod_ssolve
