!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!% hybrd -- Hybrid Powell method.
!%
!%  Usage
!%    [ x, y, iflag, nfev ] = hybrd(nlefun, x0, options, p1, ...)
!%
!%  Inputs
!%    nlefun  string, function y = nlefun(x, p1, ...)
!%    x0      real vector, initial guess
!%    options struct vector, options
!%    p1      any, optional argument passed to nlefun
!%    ...
!%
!%  Outputs
!%    x       real vector, zero
!%    y       real vector, value of nlefun at x
!%    iflag   integer, hybrd solver output (should be 1)
!%    nfev    number of evaluations of nlefun
!%
!%  Description
!%    Matlab interface of Fortran hybrd. Function nlefun must return
!%    a column vector.
!%
!%  See also
!%    hybrdset, hybrdget
!%
module mod_hybrd
    implicit none
    contains

    !>  @ingroup modulesInterfaces
    !!  \brief   Link to hybrdint()
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    !!  \param[in]  fcn     function for which one wants to find a zero
    !!  \param[in]  n       dimension of the vector x
    !!  \param[in]  x       initial unknown
    !!  \param[in]  xtol    tolerance for hybrd subroutine
    !!  \param[in]  maxfev  number of maximum evaluations of fcn
    !!  \param[out] nfev    number of evaluations of fcn 
    !!  \param[out] xsol    solution of fcn(x) = 0 
    !!  \param[out] fxsol   fxsol = fcn(xsol)
    !!  \param[out] flag    should be 1
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    Subroutine hybrd(fcn,n,x,xtol,maxfev,nfev,xsol,fxsol,flag)
        implicit none
        integer,            intent(in)                    :: n
        double precision,   intent(in), dimension(n)      :: x
        double precision,   intent(in)                    :: xtol
        integer,            intent(in)                    :: maxfev
        double precision,   intent(out), dimension(n)     :: xsol
        double precision,   intent(out), dimension(n)     :: fxsol
        integer,            intent(out)                   :: flag,nfev

        interface
            Subroutine fcn(n,x,fvec,iflag)
            implicit none
            integer,            intent(in)                        :: n
            double precision,   intent(in),  dimension(n)         :: x
            double precision,   intent(out), dimension(n)         :: fvec
            integer,            intent(inout)                     :: iflag
            end Subroutine fcn
        end interface

        call hybrdint(fcn,n,x,xtol,maxfev,nfev,xsol,fxsol,flag)

    end subroutine hybrd

end module mod_hybrd
