!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!% Options for hampath packages.
module mod_options
    !use utils
    implicit none

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   Options structure.
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    type TOptions
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: Derivative           = 'eqvar'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: Dispiter             = 1
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: Display              = 'on'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: DoSavePath           = 'off'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: MaxSf                = 1d5
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: MaxSfunNorm          = 1d-1
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: MaxStepsOde          = 100000
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: MaxStepsOdeHam       = 100000
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: MaxStepSizeOde       = 0d0
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: MaxStepSizeOdeHam    = 0d0
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: MaxStepSizeHomPar    = 0d0
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: ODE                  = 'dopri5'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: ODEHam               = 'dopri5'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: StopAtTurningPoint   = 0
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolOdeAbs            = 1d-10
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolOdeRel            = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolOdeHamAbs         = 1d-10
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolOdeHamRel         = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolDenseHamEnd       = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolDenseHamX         = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        double precision    :: TolX                 = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: IrkInit              = '2'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: IrkSolver            = 'newton'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: SolverMethod         = 'hybrj'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: MaxFEval             = 2000
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: MaxIterCorrection    = 7
    end type TOptions

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   getOption interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface getOption
        module procedure getInt, getReal, getChar
    end interface

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   setOption interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface setOption
        module procedure setInt, setReal, setChar
    end interface

    contains


    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Transform options into dw, iw, sw and lsw.
    !!      @param[in]  options     stucture containing all options
    !!        \param[out]    ndw       Size of dw
    !!        \param[out]    niw       Size of iw
    !!        \param[out]    nsw       Size of lsw
    !!        \param[out]    iw        Integer hampath code options
    !!        \param[out]    dw        Double precision hampath code options
    !!        \param[out]    sw        String hampath code options
    !!        \param[out]    lsw       Length of each string option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2015
    !!  \copyright LGPL
    !!
    subroutine optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)
        type(TOptions), intent(in)          :: options
        integer,                                        intent(out) :: ndw, niw, nsw
        double precision,   dimension(:), allocatable,  intent(out) :: dw
        integer,            dimension(:), allocatable,  intent(out) :: iw, lsw
        character(len=:),                 allocatable,  intent(out) :: sw

        !local variables
        integer         :: i, j
        character(32)   :: optValue

        ndw = 12
        niw = 6
        nsw = 8

        allocate(dw(ndw),iw(niw),lsw(nsw))
        allocate(character(len=32*nsw) :: sw)

        i = 1
        call getOption(options,'MaxSf'               ,dw(i)); i = i + 1;
        call getOption(options,'MaxSfunNorm'         ,dw(i)); i = i + 1;
        call getOption(options,'MaxStepSizeOde'      ,dw(i)); i = i + 1;
        call getOption(options,'MaxStepSizeOdeHam'   ,dw(i)); i = i + 1;
        call getOption(options,'MaxStepSizeHomPar'   ,dw(i)); i = i + 1;
        call getOption(options,'TolOdeAbs'           ,dw(i)); i = i + 1;
        call getOption(options,'TolOdeRel'           ,dw(i)); i = i + 1;
        call getOption(options,'TolOdeHamAbs'        ,dw(i)); i = i + 1;
        call getOption(options,'TolOdeHamRel'        ,dw(i)); i = i + 1;
        call getOption(options,'TolDenseHamEnd'      ,dw(i)); i = i + 1;
        call getOption(options,'TolDenseHamX'        ,dw(i)); i = i + 1;
        call getOption(options,'TolX'                ,dw(i)); i = i + 1;

        i = 1
        call getOption(options,'DispIter'            ,iw(i)); i = i + 1;
        call getOption(options,'MaxFEval'            ,iw(i)); i = i + 1;
        call getOption(options,'MaxIterCorrection'   ,iw(i)); i = i + 1;
        call getOption(options,'MaxStepsOde'         ,iw(i)); i = i + 1;
        call getOption(options,'MaxStepsOdeHam'      ,iw(i)); i = i + 1;
        call getOption(options,'StopAtTurningPoint'  ,iw(i)); i = i + 1;

        i = 1
        j = 1
!        sw      = ''
        call getOption(options,'Derivative',optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'Display'   ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'DoSavePath',optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'IrkInit'   ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'IrkSolver' ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'ODE'       ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'ODEHam'    ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'SolverMethod',optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;

    end subroutine optionsToArrays


    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Print options.
    !!  @param[in] options options to be printed
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    subroutine printOptions(options)
        type(TOptions), intent(in) :: options

        !local variables
        CHARACTER(len=120)                  :: LINE

        WRITE (LINE,'(a)')  'Options :'; call myprint(LINE,.true.);

        WRITE (LINE,'("  Derivative         = ",     a)')  options%Derivative;          call myprint(LINE,.true.)
        WRITE (LINE,'("  DispIter           = ",  i0.1)')  options%Dispiter;            call myprint(LINE,.true.)
        WRITE (LINE,'("  Display            = ",     a)')  options%Display;             call myprint(LINE,.true.)
        WRITE (LINE,'("  DoSavePath         = ",     a)')  options%DoSavePath;          call myprint(LINE,.true.)
        WRITE (LINE,'("  IrkInit            = ",     a)')  options%IrkInit;             call myprint(LINE,.true.)
        WRITE (LINE,'("  IrkSolver          = ",     a)')  options%IrkSolver;           call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxFEval           = ",  i0.1)')  options%MaxFEval;            call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxIterCorrection  = ",  i0.1)')  options%MaxIterCorrection;   call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxSf              = ",e23.15)')  options%MaxSf;               call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxSfunNorm        = ",e23.15)')  options%MaxSfunNorm;         call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepsOde        = ",  i0.1)')  options%MaxStepsOde;         call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepsOdeHam     = ",  i0.1)')  options%MaxStepsOdeHam;      call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepSizeOde     = ",e23.15)')  options%MaxStepSizeOde;      call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepSizeOdeHam  = ",e23.15)')  options%MaxStepSizeOdeHam;   call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepSizeHomPar  = ",e23.15)')  options%MaxStepSizeHomPar;   call myprint(LINE,.true.)
        WRITE (LINE,'("  ODE                = ",     a)')  options%ODE;                 call myprint(LINE,.true.)
        WRITE (LINE,'("  ODEHam             = ",     a)')  options%ODEHam;              call myprint(LINE,.true.)
        WRITE (LINE,'("  SolverMethod       = ",     a)')  options%SolverMethod;        call myprint(LINE,.true.)
        WRITE (LINE,'("  StopAtTurningPoint = ",  i0.1)')  options%StopAtTurningPoint;  call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeAbs          = ",e23.15)')  options%TolOdeAbs;           call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeRel          = ",e23.15)')  options%TolOdeRel;           call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeHamAbs       = ",e23.15)')  options%TolOdeHamAbs;        call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeHamRel       = ",e23.15)')  options%TolOdeHamRel;        call myprint(LINE,.true.)
        WRITE (LINE,'("  TolDenseHamEnd     = ",e23.15)')  options%TolDenseHamEnd;      call myprint(LINE,.true.)
        WRITE (LINE,'("  TolDenseHamX       = ",e23.15)')  options%TolDenseHamX;        call myprint(LINE,.true.)
        WRITE (LINE,'("  TolX               = ",e23.15)')  options%TolX;                call myprint(LINE,.true.)
        !!!
        WRITE (LINE,'(a)')  ''; call myprint(LINE,.true.);

    end subroutine printOptions

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get integer option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine getInt(options,optName,optValue)
        type(TOptions), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        integer,        intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('DispIter')
                optValue = options%Dispiter
            case ('MaxFEval')
                optValue = options%MaxFEval
            case ('MaxIterCorrection')
                optValue = options%MaxIterCorrection
            case ('MaxStepsOde')
                optValue = options%MaxStepsOde
            case ('MaxStepsOdeHam')
                optValue = options%MaxStepsOdeHam
            case ('StopAtTurningPoint')
                optValue = options%StopAtTurningPoint
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getInt

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get real option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine getReal(options,optName,optValue)
        type(TOptions),     intent(in)  :: options
        character(LEN=*),     intent(in)  :: optName
        double precision,   intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxSf')
                optValue = options%MaxSf
            case ('MaxSfunNorm')
                optValue = options%MaxSfunNorm
            case ('MaxStepSizeOde')
                optValue = options%MaxStepSizeOde
            case ('MaxStepSizeOdeHam')
                optValue = options%MaxStepSizeOdeHam
            case ('MaxStepSizeHomPar')
                optValue = options%MaxStepSizeHomPar
            case ('TolOdeAbs')
                optValue = options%TolOdeAbs
            case ('TolOdeRel')
                optValue = options%TolOdeRel
            case ('TolOdeHamAbs')
                optValue = options%TolOdeHamAbs
            case ('TolOdeHamRel')
                optValue = options%TolOdeHamRel
            case ('TolDenseHamEnd')
                optValue = options%TolDenseHamEnd
            case ('TolDenseHamX')
                optValue = options%TolDenseHamX
            case ('TolX')
                optValue = options%TolX
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getReal

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get string option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    subroutine getChar(options,optName,optValue)
        type(TOptions), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        character(LEN=*)  , intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Derivative')
                optValue = options%Derivative
            case ('Display')
                optValue = options%Display
            case ('DoSavePath')
                optValue = options%DoSavePath
            case ('IrkInit')
                optValue = options%IrkInit
            case ('IrkSolver')
                optValue = options%IrkSolver
            case ('ODE')
                optValue = options%ODE
            case ('ODEHam')
                optValue = options%ODEHam
            case ('SolverMethod')
                optValue = options%SolverMethod
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getChar

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set integer option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine setInt(options,optName,optValue)
        type(TOptions),   intent(inout)   :: options
        character(LEN=*), intent(in)      :: optName
        integer,          intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('DispIter')
                  options%Dispiter              = optValue
            case ('MaxFEval')
                  options%MaxFEval              = optValue
            case ('MaxIterCorrection')
                  options%MaxIterCorrection     = optValue
            case ('MaxStepsOde')
                  options%MaxStepsOde           = optValue
            case ('MaxStepsOdeHam')
                  options%MaxStepsOdeHam        = optValue
            case ('StopAtTurningPoint')
                  options%StopAtTurningPoint    = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setInt

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set real option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine setReal(options,optName,optValue)
        type(TOptions),     intent(inout)   :: options
        character(LEN=*),   intent(in)      :: optName
        double precision,   intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxSf')
                  options%MaxSf             = optValue
            case ('MaxSfunNorm')
                  options%MaxSfunNorm       = optValue
            case ('MaxStepSizeOde')
                  options%MaxStepSizeOde    = optValue
            case ('MaxStepSizeOdeHam')
                  options%MaxStepSizeOdeHam = optValue
            case ('MaxStepSizeHomPar')
                  options%MaxStepSizeHomPar = optValue
            case ('TolOdeAbs')
                  options%TolOdeAbs         = optValue
            case ('TolOdeRel')
                  options%TolOdeRel         = optValue
            case ('TolOdeHamAbs')
                  options%TolOdeHamAbs      = optValue
            case ('TolOdeHamRel')
                  options%TolOdeHamRel      = optValue
            case ('TolDenseHamEnd')
                  options%TolDenseHamEnd    = optValue
            case ('TolDenseHamX')
                  options%TolDenseHamX      = optValue
            case ('TolX')
                  options%TolX              = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setReal

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set string option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    subroutine setChar(options,optName,optValue)
        type(TOptions), intent(inout)   :: options
        character(LEN=*)  , intent(in)      :: optName
        character(LEN=*)  , intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Derivative')
                  options%Derivative    = trim(optValue)
            case ('Display')
                  options%Display       = trim(optValue)
            case ('DoSavePath')
                  options%DoSavePath    = trim(optValue)
            case ('IrkInit')
                options%IrkInit         = trim(optValue)
            case ('IrkSolver')
                options%IrkSolver       = trim(optValue)
            case ('ODE')
                  options%ODE           = trim(optValue)
            case ('ODEHam')
                  options%ODEHam        = trim(optValue)
            case ('SolverMethod')
                  options%SolverMethod  = trim(optValue)
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setChar

end module mod_options
