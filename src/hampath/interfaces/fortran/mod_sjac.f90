!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_sjac
    use utils
    use mod_options
    implicit none
    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   sjac interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    interface sjac
        module procedure sjacy, sjacyipar
    end interface
    contains

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to sjacint() with no indexpar
    !!  \details This subroutine is called through mod_sjac::sjac interface.
    !!
    !!  \param[in]  y       shooting variable
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[out] fjac    Jacobian of the shooting/homotopic function
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine sjacy(y, options, par, fjac)
        implicit none
        double precision,  dimension(:),   intent(in)  :: y
        type(TOptions),                    intent(in)  :: options
        double precision,  dimension(:),   intent(in)  :: par
        double precision,  dimension(:,:), intent(out) :: fjac

        !local variable
        integer                                         :: ny,npar,nipar,nl
        double precision                                :: ipardble(1)
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable   :: sw

        ny          = size(y)
        npar        = size(par)
        nipar       = 0
        ipardble    = 0d0
        nl          = 0

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call sjacint(ny,y,npar,par,nipar,ipardble,nl,fjac,ndw,niw,nsw,dw,iw,sw,lsw)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

    end subroutine sjacy

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to sjacint() with no indexpar
    !!  \details This subroutine is called through mod_sjac::sjac interface.
    !!
    !!  \param[in]  y       shooting variable
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[in]  ipar    index of parameters for which the jacobian is computed
    !!  \param[out] fjac    Jacobian of the shooting/homotopic function
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine sjacyipar(y, options, par, ipar, fjac)
        implicit none
        double precision,  dimension(:),   intent(in)  :: y
        type(TOptions),                    intent(in)  :: options
        double precision,  dimension(:),   intent(in)  :: par
        integer,           dimension(:),   intent(in)  :: ipar
        double precision,  dimension(:,:), intent(out) :: fjac

        !local variable
        integer                                         :: ny,npar,nipar,nl,i
        double precision                                :: ipardble(size(ipar))
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable   :: sw

        ny          = size(y)
        npar        = size(par)
        nipar       = size(ipar)
        if(nipar.eq.0)then
            nl    = 0
        else
            nl    = 1
        end if
        do i=1,nipar
                ipardble(i) = dble(ipar(i))
        end do

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call sjacint(ny,y,npar,par,nipar,ipardble,nl,fjac,ndw,niw,nsw,dw,iw,sw,lsw)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

    end subroutine sjacyipar

end module mod_sjac
