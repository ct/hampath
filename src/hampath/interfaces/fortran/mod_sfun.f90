!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_sfun
    use utils
    use mod_options
    implicit none

    contains

    !! sfunint(ny,y,npar,par,s,at,rt,inte,ms,mss,tsc)

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to sfunint() in the simple shooting case
    !!  \details This subroutine is called through mod_sfun::sfun interface.
    !!
    !!  \param[in]  y       shooting variable
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[out] s       shooting value
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2014-2016
    !!  \copyright LGPL
    !!
    Subroutine sfun(y, options, par, s)
        implicit none
        double precision,  dimension(:), intent(in)  :: y
        type(TOptions),                  intent(in)  :: options
        double precision,  dimension(:), intent(in)  :: par
        double precision,  dimension(:), intent(out) :: s

        !local variable
        integer                                         :: ny,npar
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable   :: sw

        ny      = size(y)
        npar    = size(par)

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call sfunint(ny,y,npar,par,s,ndw,niw,nsw,dw,iw,sw,lsw)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

    end subroutine sfun

end module mod_sfun
