!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module mod_exphvfun
    use mod_options
    implicit none
    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   exphvfun interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    interface exphvfun
        module procedure exphvfunSimple
        module procedure exphvfunMultiple
    end interface
    contains

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to exphvfunint() in the simple shooting case
    !!  \details This subroutine is called through mod_exphvfun::exphvfun interface.
    !!
    !!  \param[in]  tspan   tspan = [tspan1 tspan2 ... tspanf] must be sorted
    !!  \param[in]  z0      initial flow
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[out] tout    time at each integration step
    !!  \param[out] z       flow at time tout
    !!  \param[out] flag    flag should be 1 (ODE integrator output)
    !!  \param[out] nfev    number of function evaluations
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine exphvfunSimple(tspan, z0, options, par, tout, z, flag, nfev)
        implicit none
        double precision,  dimension(:),        intent(in)  :: tspan
        double precision,  dimension(:),        intent(in)  :: z0
        type(TOptions),                         intent(in)  :: options
        double precision,  dimension(:),        intent(in)  :: par
        double precision,  dimension(:)  , allocatable,     intent(out) :: tout
        double precision,  dimension(:,:), allocatable,     intent(out) :: z
        integer,                                            intent(out) :: flag, nfev

        !local variable
        integer                                         :: n,nbarc,lpar,nt,dimeT,infos(2)
        double precision                                :: ti(2)
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable :: sw

        nt      = size(tspan)
        n       = size(z0,1)/2
        lpar    = size(par)
        nbarc   = 1
        ti(1)   = tspan(1)
        ti(2)   = tspan(nt)

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call exphvfunint(n,z0,lpar,par,nt,tspan,nbarc,ti,2,infos,dimeT,ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

        allocate(tout(dimeT),z(2*n,dimeT))

        call getstates(n,dimeT,tout,z)

    end subroutine exphvfunSimple

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  \ingroup modulesInterfaces
    !!  \brief   Link to exphvfunint() in the multiple shooting case
    !!  \details This subroutine is called through mod_exphvfun::exphvfun interface.
    !!
    !!  \param[in]  tspan   tspan = [tspan1 tspan2 ... tspanf] must be sorted
    !!  \param[in]  z0      initial flow
    !!  \param[in]  ti      ti = [t0 t1 ... t_nbarc-1 tf] must be increasing
    !!  \param[in]  options hampath options
    !!  \param[in]  par     parameters given to hfun
    !!  \param[out] tout    time at each integration step
    !!  \param[out] z       flow at time tout
    !!  \param[out] flag    flag should be 1 (ODE integrator output)
    !!  \param[out] nfev    number of function evaluations
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    Subroutine exphvfunMultiple(tspan, z0, ti, options, par, tout, z, flag, nfev)
        implicit none
        double precision,  dimension(:),        intent(in)  :: tspan
        double precision,  dimension(:),        intent(in)  :: z0
        double precision,  dimension(:),        intent(in)  :: ti
        type(TOptions),                         intent(in)  :: options
        double precision,  dimension(:),        intent(in)  :: par
        double precision,  dimension(:)  , allocatable,     intent(out) :: tout
        double precision,  dimension(:,:), allocatable,     intent(out) :: z
        integer,                                            intent(out) :: flag, nfev

        !local variable
        integer                                         :: n,nbarc,lpar,nt,dimeT,infos(2)
        integer                                         :: ndw, niw, nsw
        double precision,   dimension(:), allocatable   :: dw
        integer,            dimension(:), allocatable   :: iw, lsw
        character(len=:),                 allocatable :: sw

        nt      = size(tspan)
        n       = size(z0,1)/2
        lpar    = size(par)
        nbarc   = size(ti)-1

        call optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)

        call exphvfunint(n,z0,lpar,par,nt,tspan,nbarc,ti,2,infos,dimeT,ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        !On élimine les vecteurs utiles pour les options
        deallocate(dw, iw, sw, lsw)

        allocate(tout(dimeT),z(2*n,dimeT))

        call getstates(n,dimeT,tout,z)

    end subroutine exphvfunMultiple

end module mod_exphvfun
