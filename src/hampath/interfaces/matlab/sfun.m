function s  = sfun(y,options,par)
%-------------------------------------------------------------------------------------------
%
%    sfun (needs sfun.f90)
%
%    Description
%
%        Computes the shooting function
%
%    Options used
%
%        FreeFinalTime, MaxStepsOde, MaxStepSizeOde, ODE, TolOdeAbs, TolOdeRel
%
%-------------------------------------------------------------------------------------------
%
%    Matlab / Octave Usage
%
%        s = sfun(y,options,par)
%
%    Inputs
%
%        y       - real vector, shooting variable
%        options - struct, hampathset options
%        par     - real vector, par in hfun and sfun
%
%    Outputs
%
%        s       - real vector, shooting value
%
%-------------------------------------------------------------------------------------------
if(nargin~=3 || nargout>1)
    error('wrong syntax: try help');
end

nominterface='matlab';
nomfun='sfun';

[status,nomfilein ] = system(['mktemp ' nomfun 'InXXXXXXXX']); n=length(nomfilein); nomfilein = nomfilein(1:n-1);
s = strcat('mv', [' ' nomfilein], [' ' nomfilein], '.txt'); [~,~ ] = system(s); nomfilein = strcat([nomfilein], '.txt');

[status,nomfileout] = system(['mktemp ' nomfun 'OuXXXXXXXX']);  n=length(nomfileout); nomfileout = nomfileout(1:n-1);
nomfunint = nomfileout;
s = strcat('mv', [' ' nomfileout], [' ' nomfileout], '.m'); [~,~] = system(s); nomfileout = strcat([nomfileout], '.m');


%!Description of input file
%!options
%!ny
%!npar
%!y(ny,1)
%!par(npar,1)

%Create file
[m,n]   = size(y)  ;  if(m~=1 & n~=1) error('Variable y must be a vector!')  ;  end; ny   = max(m,n);
[m,n]   = size(par);  if(m>1 & n>1) error('Variable par must be a vector!');  end; npar = max(m,n);
y       = y(:);    %On ecrit en colonne
par     = par(:);
fid     = fopen(nomfilein,'w');
fprintf(fid,hampathget(options));
fprintf(fid,'%i\n%i\n',ny,npar);
fclose(fid);
save(nomfilein,'-ascii','-double','-append','y','par');

%Call main routine
p = mfilename('fullpath'); [pathstr,name,ext] = fileparts(p);
gfortranMatlab(1); flag = system([pathstr '/mainProg ' nomfilein ' ' nomfileout ' ' nomfun ' ' nominterface ' ' nomfunint]); gfortranMatlab(2);

%Get results from file out
try
    s = eval(nomfunint);
catch err
    if(exist(nomfilein,'file'))  flagAux = system(['rm ' nomfilein]);  end;
    if(exist(nomfileout,'file')) flagAux = system(['rm ' nomfileout]); end;
    error('Problem during execution!');
end;

flagAux = system(['rm ' nomfilein]);
flagAux = system(['rm ' nomfileout]);

return;
%% -----------------------------------------------------------------------------
%%
%% Copyright 2016, Olivier Cots.
%%
%% This file is part of HamPath.
%%
%% HamPath is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Lesser General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% HamPath is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Lesser General Public License for more details.
%%
%% You should have received a copy of the GNU Lesser General Public License
%% along with HamPath. If not, see <http://www.gnu.org/licenses/>.
%%
%% -----------------------------------------------------------------------------
%%
