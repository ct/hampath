function [ ysol, ssol, nfev, njev, flag] = ssolve(y0,options,par)
%-------------------------------------------------------------------------------------------
%
%    ssolve (needs sfun.f90)
%
%    Description
%
%        Interface of the Fortran non linear solver (hybrj) to solve the optimal
%        control problem described by the Fortran subroutines.
%
%    Options used
%        Derivative, Display, FreeFinalTime, MaxFEval, MaxStepsOde, MaxStepSizeOde, ODE,
%        SolverMethod, TolOdeAbs, TolOdeRel, TolX
%
%-------------------------------------------------------------------------------------------
%
%    Matlab / Octave Usage
%
%        [ysol,ssol,nfev,njev,flag] = ssolve(y0,options,par)
%
%    Inputs
%
%        y0      - real vector, intial guess for shooting variable
%        options - struct vector, hampathset options
%        par     - real vector, par in hfun and sfun, par=[] if no parameters
%
%    Outputs
%
%        ysol    - real vector, shooting variable solution
%        ssol    - real vector, value of sfun at ysol
%        nfev    - integer, number of evaluations of sfun
%        njev    - integer, number of evaluations of sjac
%        flag    - integer, solver output (should be 1)
%
%-------------------------------------------------------------------------------------------
if(nargin~=3 || nargout~=5)
    error('wrong syntax: try help');
end

nominterface='matlab';
nomfun='ssolve';

[status,nomfilein ] = system(['mktemp ' nomfun 'InXXXXXXXX']); n=length(nomfilein); nomfilein = nomfilein(1:n-1);
s = strcat('mv', [' ' nomfilein], [' ' nomfilein], '.txt'); [~,~ ] = system(s); nomfilein = strcat([nomfilein], '.txt');

[status,nomfileout] = system(['mktemp ' nomfun 'OuXXXXXXXX']);  n=length(nomfileout); nomfileout = nomfileout(1:n-1);
nomfunint = nomfileout;
s = strcat('mv', [' ' nomfileout], [' ' nomfileout], '.m'); [~,~] = system(s); nomfileout = strcat([nomfileout], '.m');

%!Description of input file
%!options
%!ny
%!npar
%!y0(ny,1)
%!par(npar,1)

%Create input file
[m,n]   = size(y0) ;  if(m~=1 & n~=1) error('Variable y0 must be a vector!')  ;  end; ny   = max(m,n);
[m,n]   = size(par);  if(m>1 & n>1) error('Variable par must be a vector!');  end; npar = max(m,n);
y0      = y0(:);    %On ecrit en colonne
par     = par(:);

fid = fopen(nomfilein,'w');
fprintf(fid,hampathget(options));
fprintf(fid,'%i\n%i\n',ny,npar);
fclose(fid);
save(nomfilein,'-ascii','-double','-append','y0','par');

%Call main routine
p = mfilename('fullpath'); [pathstr,name,ext] = fileparts(p);
gfortranMatlab(1); flag = system([pathstr '/mainProg ' nomfilein ' ' nomfileout ' ' nomfun ' ' nominterface ' ' nomfunint]); gfortranMatlab(2);

%Get results from file out
try
    [ ysol, ssol, nfev, njev, flag] = eval(nomfunint);
catch err
    if(exist(nomfilein,'file'))  flagAux = system(['rm ' nomfilein]);  end;
    if(exist(nomfileout,'file')) flagAux = system(['rm ' nomfileout]); end;
    error('Problem during execution!');
end;

flagAux = system(['rm ' nomfilein]);
flagAux = system(['rm ' nomfileout]);

return;
%% -----------------------------------------------------------------------------
%%
%% Copyright 2016, Olivier Cots.
%%
%% This file is part of HamPath.
%%
%% HamPath is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Lesser General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% HamPath is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Lesser General Public License for more details.
%%
%% You should have received a copy of the GNU Lesser General Public License
%% along with HamPath. If not, see <http://www.gnu.org/licenses/>.
%%
%% -----------------------------------------------------------------------------
%%
