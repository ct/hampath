!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
subroutine dummyRoutine(unitfile,nomfileout,nominterface,nomfunint)
    use mod_options
    use utils
    implicit none
    character(len=*), intent(in) :: nomfileout,nominterface,nomfunint
    integer         , intent(in) :: unitfile

    !local variables
    integer                                       :: nt, n, nbarc, lpar, i, unitfileout, iost
    double precision, dimension(:)  , allocatable :: t, par, ti
    double precision, dimension(:,:), allocatable :: z, resdummy

    CHARACTER(len=120)  :: LINE

    !Read datas from file nomfilein
    read(unitfile,*) nt
    read(unitfile,*) n
    read(unitfile,*) nbarc
    read(unitfile,*) lpar
    allocate(t(nt), z(2*n,nt), resdummy(dimRes,nt), par(lpar), ti(nbarc+1))
    read(unitfile,*) t
    do i=1,2*n
        read(unitfile,*) z(i,:)
    end do
    read(unitfile,*) ti
    do i=1,lpar
        read(unitfile,*) par(i)
    end do

    !Call main routine
    call dummyInt(t,n,z,nbarc,ti,lpar,par,nt,resdummy)

    !Write results in output file
    unitfileout = unitfile + 1
    open(unit=unitfileout,file=nomfileout,action="write",status="replace",iostat=iost)
    if(iost.eq.0)then

        select case (nominterface)

            case('matlab')

                !!!
                WRITE (LINE,*) 'function resdummy = ', nomfunint,'()'
                WRITE (unit=unitfileout,fmt='(a)') LINE
                call writeMatrixDbleMatlab(resdummy,dimRes,nt,'resdummy',unitfileout)
                WRITE (unit=unitfileout,fmt='(a)') 'return;'

            case default
                write(*,*) "Error: no interface called ", nominterface, "!"

        end select

        close(unit=unitfileout)
    else
        write(*,*) "Error during dummy call!"
    end if

    !Deallocate all allocatable variables
    deallocate(t,z,resdummy,par,ti)

end subroutine dummyRoutine

Subroutine dummyInt(t,n,z,nbarc,ti,lpar,par,lvalue,value)
    use utils
    implicit none
    integer,      intent(in)                             :: n,nbarc
    integer,      intent(in)                             :: lpar
    integer,      intent(in)                             :: lvalue
    double precision, intent(in),  dimension(lvalue)     :: t
    double precision, intent(in),  dimension(nbarc+1)    :: ti
    double precision, intent(in),  dimension(2*n,lvalue) :: z
    double precision, intent(in),  dimension(lpar)       :: par
    double precision, intent(out), dimension(dimRes,lvalue)     :: value

    !local variable
    integer :: i,arc

    !For each time t(i), get the index of the arc associated
    !and compute the value of the hamiltonian
    do i=1,lvalue
        call getArc(t(i),nbarc,ti,arc) !get the arc index
        call dummy(t(i),n,z(:,i),arc,lpar,par,value(:,i)) !get hfun value
    end do

end subroutine dummyInt
