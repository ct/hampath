function [hv,dhv] = dhvfun(arg1,arg2,arg3,arg4,arg5)
%-------------------------------------------------------------------------------------------
%
%    dhvfun (needs hfun.f90)
%
%    Description
%
%        Computes the second member of the variational system associated to H.
%
%-------------------------------------------------------------------------------------------
%
%    Matlab / Octave Usage
%
%        [hv, dhv] = dhvfun(t, z, dz,     par) : single arc
%        [hv, dhv] = dhvfun(t, z, dz, ti, par) : multiple arcs
%
%    Inputs
%
%        t    -  real row vector, t(i) = i-th time
%        z    -  real matrix, z(:,i) = i-th state and costate
%        dz   -  real matrix, dz(:,(i-1)*k+1:i*k) = the k i-th Jacobi fields
%        ti   -  real row vector, in multiple shooting case, ti = [t0 t1 ... t_nbarc-1 tf]
%        par  -  real vector, parameters given to hfun par=[] if no parameters
%
%    Outputs
%
%        hv   -  real matrix, hamiltonian vector field at time(s) t
%        dhv  -  real matrix, Linearized of the Hamiltonian vector field at time(s) t
%
%-------------------------------------------------------------------------------------------
nrhs0min = 4;
if(nargin<nrhs0min || nargin>nrhs0min+1 || nargout>2)
    error('wrong syntax: try help');
end

t=arg1;
z=arg2;
dz=arg3;
if(nargin==nrhs0min)
    ti =[t(1) t(end)];
    par=arg4;
elseif(nargin==nrhs0min+1)
    ti=arg4;
    par=arg5;
end

nominterface='matlab';
nomfun='dhvfun';

[status,nomfilein ] = system(['mktemp ' nomfun 'InXXXXXXXX']); n=length(nomfilein); nomfilein = nomfilein(1:n-1);
s = strcat('mv', [' ' nomfilein], [' ' nomfilein], '.txt'); [~,~ ] = system(s); nomfilein = strcat([nomfilein], '.txt');

[status,nomfileout] = system(['mktemp ' nomfun 'OuXXXXXXXX']);  n=length(nomfileout); nomfileout = nomfileout(1:n-1);
nomfunint = nomfileout;
s = strcat('mv', [' ' nomfileout], [' ' nomfileout], '.m'); [~,~] = system(s); nomfileout = strcat([nomfileout], '.m');

%!Description of input file
%!nt
%!n
%!kdz
%!nbarc
%!lpar
%!t(1,nt)
%!z(2*n,nt)
%!dz(2*n,nt*kdz)
%!ti(1,nbarc+1)
%!par(lpar,1)

%Create file in
[m ,nt ] = size(t);     if(m>1)             error('Variable t must be a row vector or scalar!');                end;
[nn,ntz] = size(z);     if(nt~=ntz)         error('Variables t and z must have the same number of columns!');   end;
                        if(mod(nn,2)~=0)    error('Variable z must have an even number of rows!');              end; n = nn/2;
[m,nti]  = size(ti);    if(m>1)             error('Variable ti must be a row vector!');                         end; nbarc = nti - 1;
[lpar,m] = size(par);   if(m>1)             error('Variable par must be a column vector!');                     end;
kdz = size(dz,2)/nt;

fid = fopen(nomfilein,'w');
fprintf(fid,'%i\n%i\n%i\n%i\n%i\n',nt,n,kdz,nbarc,lpar);
fclose(fid);
save(nomfilein,'-ascii','-double','-append','t','z','dz','ti','par');

%Call main routine
p = mfilename('fullpath'); [pathstr,name,ext] = fileparts(p);
gfortranMatlab(1); flag = system([pathstr '/mainProg ' nomfilein ' ' nomfileout ' ' nomfun ' ' nominterface ' ' nomfunint]); gfortranMatlab(2);

%Get results from file out
try
    [hv,dhv] = eval(nomfunint);
catch err
    if(exist(nomfilein,'file'))  flagAux = system(['rm ' nomfilein]);  end;
    if(exist(nomfileout,'file')) flagAux = system(['rm ' nomfileout]); end;
    error('Problem during execution!');
end;

flagAux = system(['rm ' nomfilein]);
flagAux = system(['rm ' nomfileout]);

return;
%% -----------------------------------------------------------------------------
%%
%% Copyright 2016, Olivier Cots.
%%
%% This file is part of HamPath.
%%
%% HamPath is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Lesser General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% HamPath is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Lesser General Public License for more details.
%%
%% You should have received a copy of the GNU Lesser General Public License
%% along with HamPath. If not, see <http://www.gnu.org/licenses/>.
%%
%% -----------------------------------------------------------------------------
%%
