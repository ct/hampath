# Welcome to hampath code

You need first [conda](https://docs.conda.io>) to install `hampath`.
You can install `conda` as part of a [Miniforge](https://github.com/conda-forge/miniforge>) installer.

```bash
conda install -c control-toolbox -c conda-forge hampath
```

Please take a look at the [user guide](user_guide.pdf) for details about `hampath`.

Some examples are given in the directory [examples](examples). They can be run [online](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fhampath-examples-env/master).
